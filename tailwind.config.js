module.exports = {
  mode: 'jit',
  content: ['./src/**/*.{html,ts}', './faircamper-lib/projects/fc-lib/src/**/*.{html,ts}'],
  darkMode: 'class',
    theme: {
    screens: {
      sm: '375px',
      md: '768px',
      lg: '1200px',
      },
      colors: {
        transparent: 'transparent',
        current: 'currentColor',
        'white': '#ffffff',
        'black': '#000000',
        'gray-100': '#F4F4F4',
        'gray-200': '#E0E0E0',
        'gray-400': '#999999',
        'gray-500': '#666666',
        'gray-700': '#535353',
        'gray-800': '#333333',
        'gray-900': '#1C1C1C',
        'orange-100': '#F9DABB',
        'orange-500': '#FF7F00',
        'blue-50': '#edf2f9',
        'blue-200': '#668DCD',
        'blue-300': '#5C83C3',
        'blue-400': '#4985D4',
        'blue-500': '#2C72C7',
        'blue-600': '#0546AD',
        'blue-900': '#002A57',
        'green-400': '#36CA54',
        'green-500': '#27b807',
        'error': '#FF3C0B',
        // custom by me
        'orange-light': '#ffb07b',
      },
    fontFamily: {
      sans: ['Fira Sans','Graphik', 'sans-serif'],
      serif: ['Merriweather', 'serif'],
    },
      extend: {
      maxWidth: {
        'container': '1300px',
      },
      transitionProperty: {
        'height': 'height',
        'margin': 'margin',
      },
      gridTemplateColumns: {
        '12': 'repeat(12, minmax(0, 1fr))',
        '13': 'repeat(13, minmax(0, 1fr))',
        '14': 'repeat(14, minmax(0, 1fr))',
        '15': 'repeat(15, minmax(0, 1fr))',
        '16': 'repeat(16, minmax(0, 1fr))',
        '17': 'repeat(17, minmax(0, 1fr))',
        '18': 'repeat(18, minmax(0, 1fr))',
        '19': 'repeat(19, minmax(0, 1fr))',
        '20': 'repeat(20, minmax(0, 1fr))',
        '21': 'repeat(21, minmax(0, 1fr))',
        '22': 'repeat(22, minmax(0, 1fr))',
        '23': 'repeat(23, minmax(0, 1fr))',
        '24': 'repeat(24, minmax(0, 1fr))',
        '25': 'repeat(25, minmax(0, 1fr))',
        '26': 'repeat(26, minmax(0, 1fr))',
        '27': 'repeat(27, minmax(0, 1fr))',
        '28': 'repeat(28, minmax(0, 1fr))',
        '29': 'repeat(29, minmax(0, 1fr))',
        '30': 'repeat(30, minmax(0, 1fr))',
        '31': 'repeat(31, minmax(0, 1fr))',
      },
      fontSize: {
        '2xs': '0.75rem'
      },
      height: theme => ({
          "screen/2": "50vh",
          "screen/3": "calc(100vh / 3)",
          "screen/4": "calc(100vh / 4)",
          "screen/5": "calc(100vh / 5)",
          "144": "36rem",
          "200": "50rem"
        }),
    }
  },
  variants: {
    extend: {
      backgroundColor: ['even'],
    },
  },
  plugins: [require('@tailwindcss/forms'),require('@tailwindcss/typography')],
}
