import { Injectable, Inject, Injector, LOCALE_ID } from '@angular/core';

import { OAuthService, OAuthStorage } from 'angular-oauth2-oidc';

import { EnvironmentToken, IEnvironment } from '@wobi/core';

import { SSOEventService } from '../services/event.service';

@Injectable({
    providedIn: 'root'
})
export class SSOInitializerService {

    constructor(
        @Inject(EnvironmentToken) private env: IEnvironment,
        @Inject(LOCALE_ID) private locale: string,
        private oauthSvc: OAuthService,
        private oauthStore: OAuthStorage,
        private evSvc: SSOEventService,       
    ) { 
        env.oidc.redirectUri = env.oidc.redirectUri?.replace('$1', locale)
        this.oauthSvc.configure(env.oidc); 
    } 

    public bootstrap(): Promise<void> {
        return new Promise( (resolve) => {
            this.oauthSvc
            .loadDiscoveryDocumentAndTryLogin()
            .then(
                () => this.resolveLogin(resolve)
            )
            .catch(
                (reason: any) => resolve()
            )
        }); 
    }


    private resolveLogin(cb: (value?: void | PromiseLike<void>) => void) {
        const rToken = this.oauthStore.getItem('refresh_token');
        if (!this.oauthSvc.hasValidAccessToken() && rToken) {
            this.oauthSvc
            .refreshToken()
            .then( () => cb() )
            .catch(
                (reason: any) => this.resolveLoginFailure(reason, cb)
            )
        } else if (cb) {
            cb();
        }
    }


    private resolveLoginFailure(reason: any, cb: (value?: void | PromiseLike<void>) => void) {
        this.oauthStore.removeItem('refresh_token');
        if (reason.error && reason.error.error) {
            const e = reason.error.error;
            switch(e) {
                case 'invalid_grant': {
                    console.warn('invalid_grant')
                    return
                }
            }
        } 
        console.error(reason);
        cb();
    }

}


export function SSOInitializerTrigger(injector: Injector): Function {
    const env = <IEnvironment>injector.get(EnvironmentToken);
    const locale = <string>injector.get(LOCALE_ID);
    const oauthSvc = injector.get(OAuthService);
    const oauthStore = injector.get(OAuthStorage);
    const evSvc = injector.get(SSOEventService);
    const ssoInitializer = new SSOInitializerService(
        env, 
        locale,
        oauthSvc, 
        oauthStore, 
        evSvc
    );

    return function fire() {
        return ssoInitializer.bootstrap();
    };
}