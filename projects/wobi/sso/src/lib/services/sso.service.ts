import { Injectable } from '@angular/core';
import { OAuthService, OAuthStorage } from 'angular-oauth2-oidc';

import { SSOStateService } from './state.service';

@Injectable({
    providedIn: 'root'
})
export class SSOService {

    private _lastAuthenticated: boolean;
    private _loggingOut: boolean;

    constructor(
        protected oauthSvc: OAuthService,
        protected oauthStore: OAuthStorage,
        protected stateSvc: SSOStateService,
    ) { 

        this._lastAuthenticated = false;
        this._loggingOut = false;

        this.stateSvc.authenticated$.subscribe( (authenticated:boolean) => {

            // handle silent refresh
            if (!authenticated) {
                this.oauthSvc.stopAutomaticRefresh();
            } 
            if (
                authenticated && 
                !this._loggingOut &&
                (authenticated !== this._lastAuthenticated)
            ) {
                this.oauthSvc.setupAutomaticSilentRefresh()
            }

            // track helper state
            this._lastAuthenticated = authenticated;
        });


        this.stateSvc.getLife$.subscribe( ([lifetime, remaining]) => {
            if ( (remaining < -1) && (lifetime > 0)) {
                this.logout();
            }
        });
    }


    login(targetUrl: string = ''): void {
        this._login(targetUrl);
    }

    loginSocial(providerId: string, targetUrl: string): void {
        this._login(
            targetUrl,
            {
                kc_idp_hint: providerId,
            }
        );
    }

    logout(): void { 
        this._loggingOut = true;
        this.oauthSvc.logOut(); 
    }

    logoutNoRedirect(): void { 
        this._loggingOut = true;
        this.oauthSvc.logOut(true); 
    }

    private _login(targetUrl: string = '', params = {}): void {
        targetUrl = targetUrl || window.location.pathname + window.location.search;    
        this.oauthSvc.initCodeFlow(targetUrl, params);
    }

}


