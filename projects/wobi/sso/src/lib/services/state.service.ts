import { Injectable } from '@angular/core';

import { Store } from '@ngrx/store';
import { Observable, combineLatest, timer, BehaviorSubject, of } from 'rxjs';
import { map, skipWhile, tap } from 'rxjs/operators';

import { UserInfo, OAuthStorage } from 'angular-oauth2-oidc';

import { IAccessToken, ISSOClaims } from '@wobi/sso-state';
import { ISSOState } from '@wobi/sso-state';

import { 
    ProcessDocumentAction,
    ProcessDocumentErrorAction,
    ProcessDocumentLoadErrorAction,
    ProcessRedirectAction,
} from '@wobi/sso-state';

import {
    DataIdTokenAction,
    DataAccessTokenAction,
} from '@wobi/sso-state';

import { 
    DataInfoAction,
    DataClaimsAction,
    DataScopesAction,
    DataResetAction
} from '@wobi/sso-state';

import { 
    selectAuthenticated, 
    selectUserInfo, 
    selectScopes, 
    selectClaims, 
    selectAccessToken,
    selectAccessTokenJson,
    selectIdTokenJson,
    selectIdToken,
    selectLifetime,
    selectRedirect,
    selectIsDocumentLoaded,
    selectHasDocumentError,
    selectHasDocumentLoadError,
} from '@wobi/sso-state';

import { ClaimsResetAction } from '@wobi/sso-state';



@Injectable({
    providedIn: 'root'
})
export class SSOStateService {

    private _prevAccess: string;
    private _prevId: string;
    private _scopes: Array<string>;
    private _claims: ISSOClaims;
    private _timer$: Observable<number>;

    private _discoveryDocumentLoaded$: Observable<boolean>;
    private _discoveryDocumentError$: Observable<boolean>;
    private _discoveryDocumentLoadError$: Observable<boolean>;
    private _authenticatedSubject: BehaviorSubject<boolean>;
    private _authenticated$: Observable<boolean>;
    private _prevAuthenticated: boolean;
    private _prevDiscoveryDocumentLoaded: boolean;

    private _idToken$: Observable<string>;   
    private _idTokenJson$: Observable<any>;   
    
    private _accessToken$: Observable<string>;
    private _accessTokenJson$: Observable<IAccessToken>;
       
    private _redirect$: Observable<string>;

    private _info$: Observable<UserInfo>;
    private _scopes$: Observable<Array<string>>;
    private _claims$: Observable<ISSOClaims>;
    private _lifetime$: Observable<number>;  
    private _lifetimeLeft$: Observable<number>;

    constructor(
        private store: Store<ISSOState>,
        private oauthStore: OAuthStorage,
    ) {
        this._prevAccess = "";
        this._prevId = "";
        this._prevAuthenticated = false;
        this._prevDiscoveryDocumentLoaded = false;
        this._scopes = [];
        this._claims = <ISSOClaims>{};

        this._timer$ = timer(0, 1000);
        
        this._discoveryDocumentLoaded$ = this.store.select(selectIsDocumentLoaded);
        this._discoveryDocumentError$ = this.store.select(selectHasDocumentError);
        this._discoveryDocumentLoadError$ = this.store.select(selectHasDocumentLoadError);
        
        this._idToken$ = this.store.select(selectIdToken);
        this._idTokenJson$ = this.store.select(selectIdTokenJson);

        this._accessToken$ = this.store.select(selectAccessToken);
        this._accessTokenJson$ = this.store.select(selectAccessTokenJson);

        this._redirect$ = this.store.select(selectRedirect);
        this._info$ = this.store.select(selectUserInfo);
        this._scopes$ = this.store.select(selectScopes);
        this._claims$ = this.store.select(selectClaims);
        this._lifetime$ = this.store.select(selectLifetime);


        this._authenticatedSubject = new BehaviorSubject<boolean>(this._prevAuthenticated);
        this._authenticated$ = this._authenticatedSubject.asObservable();

        this.scopes$.subscribe( (scopes: Array<string>) => {
            this._scopes = scopes || [];
        });

        this.claims$.subscribe( (claims: ISSOClaims) => {
            this._claims = claims;
        })

        // patch authenticated from store into relay obbservable
        this.store.select(selectAuthenticated)
            .pipe(
                skipWhile(
                    (authenticated: boolean) => authenticated === this._prevAuthenticated
                ),
                tap(
                    (authenticated: boolean) => {
                        this._prevAuthenticated = authenticated;
                    }
                )
            ).subscribe( (a: boolean) => {
                // send application wide reset action if needed
                if (!a) {
                    this.store.dispatch(ClaimsResetAction());
                }
                this._authenticatedSubject.next(a);
            });

        this._lifetimeLeft$ = combineLatest([
            this._timer$,
            this.authenticated$,
            this.idTokenJson$
        ]).pipe(
            skipWhile(
                ([n, a, json]) => !json
            ),
            map( ([n, authenticated, idJson]) => {
                if (!authenticated || (idJson === null)) {
                    return -1;
                } else {
                    return Math.floor(((idJson.exp*1000) - Date.now()) / 1000)
                }
            })
        );
       
    }

    // discoveryDocumentLoaded

    set discoveryDocumentLoaded(discoveryDocumentLoaded: boolean) {
        if (this._prevDiscoveryDocumentLoaded !== discoveryDocumentLoaded) {
            this.store.dispatch(
                ProcessDocumentAction({discoveryDocumentLoaded})
            );
            this._prevDiscoveryDocumentLoaded = discoveryDocumentLoaded;
        }
    }

    get discoveryDocumentLoaded$(): Observable<boolean> {
        return this._discoveryDocumentLoaded$;
    }

    // discoveryDocumentError

    set discoveryDocumentError(discoveryDocumentError: boolean) {
        this.store.dispatch(
            ProcessDocumentErrorAction({discoveryDocumentError})
        );
    }

    get discoveryDocumentError$(): Observable<boolean> {
        return this._discoveryDocumentError$;
    }

    // discoveryDocumentLoadError

    set discoveryDocumentLoadError(discoveryDocumentLoadError: boolean) {
        this.store.dispatch(
            ProcessDocumentLoadErrorAction({discoveryDocumentLoadError})
        );
    }

    get discoveryDocumentLoadError$(): Observable<boolean> {
        return this._discoveryDocumentLoadError$;
    }

    // authenticated

    get authenticated$(): Observable<boolean> {
        return this._authenticated$;
    }

    // idToken

    set idToken(idToken: string) {
        if (idToken !== this._prevId) {        
            this.store.dispatch(
                DataIdTokenAction({idToken})
            );
        }
        this._prevId = idToken;
    }

    get idToken$(): Observable<string> {
        return this._idToken$;
    }

    get idTokenJson$(): Observable<any> {
        return this._idTokenJson$;
    }

    // accessToken

    set accessToken(accessToken: string) {
        if (accessToken !== this._prevAccess) {
            this.store.dispatch(
                DataAccessTokenAction({accessToken})
            );
        }
        this._prevAccess = accessToken;
    }

    get accessToken$(): Observable<any> {
        return this._accessToken$;
    }

    get accessTokenJson$(): Observable<any> {
        return this._accessTokenJson$;
    }

    // refreshToken

    get refreshToken$(): Observable<string> {
        return of(this.oauthStore.getItem('refresh_token') || '');
    }

    // redirect

    set redirect(redirect: string) {
        this.store.dispatch(
            ProcessRedirectAction({redirect})
        );
    }

    get redirect$(): Observable<string> {
        return this._redirect$;
    }

    // info

    set info(info: UserInfo) {
        this.store.dispatch(
            DataInfoAction({info})
        );
    }

    get info$(): Observable<UserInfo> {
        return this._info$;
    }

    // scopes

    set scopes(scopes: Array<string>) {
        if (!!scopes && this.scopesUpdated(scopes)) {
            this.store.dispatch(
                DataScopesAction({scopes})
            );
        }
    }

    get scopes$(): Observable<Array<string>> {
        return this._scopes$;
    }

    private scopesUpdated(scopes: Array<string>): boolean {
        if (this._scopes.length !== scopes.length) {
            return true;
        }
        if (this._scopes.length === 0) {
            return false;
        }
        let equal = true;
        for (let idx = 0; idx < scopes.length; idx++) {
            let hasScope = false;
            for (let old = 0; old < this._scopes.length; old++) {
                if (scopes[idx] === this._scopes[old]) {
                    hasScope = true;
                    break;
                }
            }
            equal = (equal && hasScope);
        }
        return !equal;
    }

    // claims

    set claims(claims: ISSOClaims) {
        if (this.claimsUpdated(claims) ) {
            this.store.dispatch(
                DataClaimsAction({claims})
            );
        }
    }
    
    get claims$(): Observable<ISSOClaims> {
        return this._claims$;
    }

    private claimsUpdated(claims: ISSOClaims): boolean {
        if (!this._claims && !claims) {
            return false;
        }
        if ( (this._claims ? 1 : 0) ^ (claims ? 1 : 0) ) {
            return true;
        }
        let equal = true;
        for(const [key, value] of Object.entries(claims)) {
            equal = equal && (value === (<any>this._claims)[key]);
        }
        return !equal;
    }

    // lifetime

    get lifetime$(): Observable<number> {
        return this._lifetime$;
    }

    get lifetimeLeft$(): Observable<number> {
        return this._lifetimeLeft$;
    }   

    get getLife$(): Observable<[number, number]> {
        return combineLatest([
            this.lifetime$,
            this.lifetimeLeft$
        ])      
    }

    get lifetimePercent$(): Observable<number> {
        return this.getLife$.pipe(
            map(
                ([lifetime, remaining]) => {
                    if (lifetime > 0) {
                        return Math.floor((remaining / lifetime)*2000) / 20;
                    }
                    return 0;
                }
            )
        )
    }

    // 

    resetUser() {
        this.store.dispatch(
            DataResetAction()
        );
    }

}

