import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { map } from 'rxjs/operators';

import { SSOService } from './sso.service';
import { selectRoles, IRoleRequirements, EPermissionCheckResult, ERoles } from '@wobi/sso-state';

const stringArrayContains = (haystack: Array<ERoles>, needle: ERoles): boolean => {
    for (let candidate of haystack || []) {
        if (candidate === needle) {
            return true;
        }
    }
    return false;
};

const hasAnyRoleOf = (anyOf: Array<ERoles> | undefined, roles: Array<ERoles>): boolean => {
    for(let role of roles) {
        if (stringArrayContains(anyOf || [], role)) {
            return true;
        }
    }
    return false;
};

const hasAllRolesOf = (allOf: Array<ERoles> | undefined, roles: Array<ERoles>): boolean => {
    if (!allOf || (allOf.length == 0)) {
        return true;
    }
    let hasAll = true;
    for(let required of allOf) {
        if (!stringArrayContains(roles, required)) {
            hasAll = false;
        }
    }
    return hasAll;
};

@Injectable({
    providedIn: 'root'
})
export class SSOPermissionService {

    constructor(private store: Store, private ssoSvc: SSOService) { } 

    get roles$(): Observable<Array<ERoles>> {
        return this.store.select(selectRoles);  
    }

    allowed(result: EPermissionCheckResult): boolean {
        return (result === EPermissionCheckResult.TokenOK) || (result === EPermissionCheckResult.NoTokenOK);
    }

    evaluate$(requirements: IRoleRequirements): Observable<EPermissionCheckResult> {
        return this.roles$
            .pipe(
                map(
                    (roles: Array<ERoles>): EPermissionCheckResult => this.evaluate(requirements, roles)
                )
            ); 
    }

    evaluate(requirements: IRoleRequirements, roles: Array<ERoles>): EPermissionCheckResult {
        return this.isRBACAllowed(requirements, roles);
    }

    private isRBACAllowed(requirements: IRoleRequirements, roles: Array<ERoles>): EPermissionCheckResult {
        
        let noRoles = !roles || (roles.length === 0);
        if ( noRoles ) {
            // no roles found
            let ok = (requirements.public === true) || (requirements.onlyVisitors === true);
            return ok ? EPermissionCheckResult.NoTokenOK : EPermissionCheckResult.NoTokenErr;
        }

        if (requirements.onlyVisitors === true) {
            // roles present, but only unauthenticated access allowed
            return EPermissionCheckResult.TokenErr;
        }

        if (requirements.public === true) {
            // roles present, public 
            return EPermissionCheckResult.TokenOK;
        }

        let denied = hasAnyRoleOf(requirements.notIn, roles);
        if (denied === true) {
            // some role was explicitly denied
            return EPermissionCheckResult.TokenErr;
        }
        if (requirements.notAll && requirements.notAll.length > 0) {
            denied = hasAllRolesOf(requirements.notAll, roles);
            if (denied === true) {
                // a set of roles was explicitly denied
                return EPermissionCheckResult.TokenErr;
            }
        }


        let allowedByAll = hasAllRolesOf(requirements.all, roles);
        let allowedByIn = hasAnyRoleOf(requirements.in, roles);

        // the allowedByAny block may be invalidated by allowedByAll block
        let allowed = allowedByIn ? (allowedByAll && allowedByIn) : allowedByAll;
        return allowed ? EPermissionCheckResult.TokenOK : EPermissionCheckResult.TokenErr;
    }

}


