import { Injectable, Inject } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, Router } from '@angular/router';
import { Observable, of } from 'rxjs';

import { IRoleRequirements, EPermissionCheckResult, ERoles, IRedirectBehaviour } from '@wobi/sso-state';
import { map } from 'rxjs/operators';
import { SSOPermissionService } from './permission.service';
import { SSOService } from './sso.service';
import { EnvironmentToken, IEnvironment } from '@wobi/core';



const getResolvedUrl = (route: ActivatedRouteSnapshot): string => {
    return '/' + route.pathFromRoot
        .map(v => v.url.map(segment => segment.toString()))
        .filter( s => s.length > 0 )
        .join('/');
};


@Injectable({
    providedIn: 'root'
})
export class RouteGuardService implements CanActivate {

    constructor(
        @Inject(EnvironmentToken) private env: IEnvironment,
        private router: Router,
        private ssoSvc: SSOService,
        private permSvc: SSOPermissionService,
    ) { 
        
    } 
    
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        let requirements: IRoleRequirements = route.data['guard'];
        let behaviour: IRedirectBehaviour = route.data['visitorRedirect'];
        let targetUrl: string = getResolvedUrl(route);
        return this.permSvc.roles$
            .pipe(
                map(
                    (roles: Array<ERoles>): boolean => this.activationLogic(requirements, behaviour, targetUrl, roles)
                ),
            )
    }

    private activationLogic(requirements: IRoleRequirements, behaviour: IRedirectBehaviour, targetUrl: string, roles: Array<ERoles>): boolean {
        const result: EPermissionCheckResult = this.permSvc.evaluate(requirements, roles);
        let allowed: boolean = this.permSvc.allowed(result);

        if (result === EPermissionCheckResult.NoTokenErr) {
            if (behaviour.enabled) {
                if (behaviour.url)  {
                    this.ssoSvc.login(behaviour.url);
                } else {
                    this.ssoSvc.login(targetUrl);
                }
            }
        }  
        if (roles.indexOf(ERoles.Pending) !== -1) {
            this.router.navigateByUrl(this.env.keycloak.postSignup);
            return false;
        }
        if (!allowed) {
            // @TODO should point to an actually existing error site
            this.router.navigateByUrl(this.env.keycloak.permissionError);
        }
        return allowed;
    }
 
 
}


