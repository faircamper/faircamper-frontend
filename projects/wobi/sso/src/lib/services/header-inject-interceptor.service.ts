import { Injectable, Inject } from '@angular/core';
import { HttpRequest, HttpHandler, HttpInterceptor, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';

import { EnvironmentToken, IEnvironment } from '@wobi/core';

import { SSOStateService } from './state.service';


@Injectable()
export class SSOHeaderInjectInterceptor implements HttpInterceptor {
    
    private _token: string;
    private _oidcTokenInjectRegex: Array<RegExp>;

    constructor(
        @Inject(EnvironmentToken) private env: IEnvironment,
        private stateSvc: SSOStateService,
    ) {
        this._token = "";
        this._oidcTokenInjectRegex = Array.from(this.env.keycloak.oidcTokenInjectRegex || []);
        this.stateSvc.accessToken$.subscribe(
            (t: string) => {
                this._token = t;
            }
        );
    }

    intercept (req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (this._token && this.match(`${req.url}`)) {
            if (this._token && (this._token.length > 0)) {
                let authReq: HttpRequest<any> = req.clone({
                    setHeaders: {
                        'Authorization': `Bearer: ${this._token}`
                    }
                });
                return next.handle(authReq);
            }
        }
        return next.handle(req);
    }


    match(fqdnWithPath: string): boolean {
        for(let i = 0; i < this._oidcTokenInjectRegex.length; i++) {
            const matches = fqdnWithPath.match(this._oidcTokenInjectRegex[i]);
            if (matches && (matches.length > 0) && (matches[0].length > 0)) {
                return true;
            }
        }
        return false;
    }

}