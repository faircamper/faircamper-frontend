import { Injectable, Inject } from '@angular/core';
import { Router } from '@angular/router';

import { Observable, combineLatest } from 'rxjs';
import { filter, map, take, skipWhile } from 'rxjs/operators';

import { 
    OAuthService, 
    OAuthEvent, 
    OAuthInfoEvent, 
    OAuthSuccessEvent,
    OAuthErrorEvent, 
    UserInfo, 
} from 'angular-oauth2-oidc';

import { EnvironmentToken, IEnvironment } from '@wobi/core';

import { ISSOClaims, EOauthEvent } from '@wobi/sso-state';
import { SSOStateService } from './state.service';


@Injectable({
    providedIn: 'root'
})
export class SSOEventService {

    private _allEvents$: Observable<OAuthEvent>;
    public successEvents$: Observable<OAuthSuccessEvent>;
    public infoEvents$: Observable<OAuthInfoEvent>;
    public errorEvents$: Observable<OAuthErrorEvent>;

    private _discoveryDocumentLoaded$: Observable<OAuthEvent>;
    private _discoveryDocumentLoadError$: Observable<OAuthEvent>;
    private _discoveryDocumentValidationError$: Observable<OAuthEvent>;

    private _jwksLoadError$: Observable<OAuthEvent>;
    private _invalidNonceInState$: Observable<OAuthEvent>;
    private _codeError$: Observable<OAuthEvent>;

    private _userProfileLoaded$: Observable<OAuthEvent>;
    private _userProfileLoadError$: Observable<OAuthEvent>;

    private _tokenReceived$: Observable<OAuthEvent>;
    private _tokenRefreshed$: Observable<OAuthEvent>;
    private _tokenExpires$: Observable<OAuthEvent>;
    private _tokenError$: Observable<OAuthEvent>;
    private _tokenRefreshError$: Observable<OAuthEvent>;
    private _tokenValidationError$: Observable<OAuthEvent>;
    private _tokenRevokeError$: Observable<OAuthEvent>;
    
    private _silentlyRefreshed$: Observable<OAuthEvent>;
    private _silentRefreshError$: Observable<OAuthEvent>;
    private _silentRefreshTimeout$: Observable<OAuthEvent>;

    private _sessionChanged$: Observable<OAuthEvent>;
    private _sessionTerminated$: Observable<OAuthEvent>;
    private _sessionError$: Observable<OAuthEvent>;

    private _popupBlocked$: Observable<OAuthEvent>;    
    private _popupClosed$: Observable<OAuthEvent>;    
    
    private _logout$: Observable<OAuthEvent>;

    constructor(
        @Inject(EnvironmentToken) private env: IEnvironment,
        private stateSvc: SSOStateService,
        private oauthSvc: OAuthService,
        private router: Router,
    ) { 
        
        this._allEvents$ = this.oauthSvc.events;

        // this._allEvents$.subscribe(console.warn);   

        this._discoveryDocumentLoaded$ = this.getEvent$(EOauthEvent.DiscoveryDocumentLoaded);
        this._discoveryDocumentLoadError$ = this.getEvent$(EOauthEvent.DiscoveryDocumentLoadError);
        this._discoveryDocumentValidationError$ = this.getEvent$(EOauthEvent.DiscoveryDocumentValidationError);

        this._jwksLoadError$ =this.getEvent$(EOauthEvent.JwksLodaError);
        this._invalidNonceInState$ = this.getEvent$(EOauthEvent.InvalidNonceInState);
        this._codeError$ = this.getEvent$(EOauthEvent.CodeError);

        this._userProfileLoaded$ = this.getEvent$(EOauthEvent.UserProfileLoaded);
        this._userProfileLoadError$ = this.getEvent$(EOauthEvent.UserProfileLoadError);

        this._tokenReceived$ = this.getEvent$(EOauthEvent.TokenReceived);
        this._tokenRefreshed$ = this.getEvent$(EOauthEvent.TokenRefreshed);
        this._tokenExpires$ = this.getEvent$(EOauthEvent.TokenExpires);
        this._tokenError$ = this.getEvent$(EOauthEvent.TokenError);
        this._tokenRefreshError$ = this.getEvent$(EOauthEvent.TokenRefreshError);
        this._tokenValidationError$ = this.getEvent$(EOauthEvent.TokenValidationError);
        this._tokenRevokeError$ = this.getEvent$(EOauthEvent.TokenRevokeError);

        this._silentlyRefreshed$ = this.getEvent$(EOauthEvent.SilentlyRefreshed);
        this._silentRefreshError$ = this.getEvent$(EOauthEvent.SilentRefreshError);
        this._silentRefreshTimeout$ = this.getEvent$(EOauthEvent.SilentRefreshTimeout);

        this._sessionChanged$ = this.getEvent$(EOauthEvent.SessionChanged);
        this._sessionTerminated$ = this.getEvent$(EOauthEvent.SessionTerminated);
        this._sessionError$ = this.getEvent$(EOauthEvent.SessionError);

        this._popupBlocked$ = this.getEvent$(EOauthEvent.PopupBlocked);
        this._popupClosed$ = this.getEvent$(EOauthEvent.PopupClosed);

        this._logout$ = this.getEvent$(EOauthEvent.Logout);

        this.successEvents$ = this._allEvents$
            .pipe(
                filter(
                    (e: OAuthEvent) => (e instanceof OAuthSuccessEvent)
                ),
                map(
                    (e: OAuthEvent) => <OAuthSuccessEvent>e
                )
            );

        this.infoEvents$ = this._allEvents$
            .pipe(
                filter(
                    (e: OAuthEvent) => (e instanceof OAuthInfoEvent)
                ),
                map(
                    (e: OAuthEvent) => <OAuthInfoEvent>e
                )
            );

        this.errorEvents$ = this._allEvents$
            .pipe(
                filter(
                    (e: OAuthEvent) => (e instanceof OAuthErrorEvent)
                ),
                map(
                    (e: OAuthEvent) => <OAuthErrorEvent>e
                )
            );

        


        this._allEvents$.subscribe( (e: OAuthEvent) => {
            this.updateTokensValid(e);
        });   


        this._tokenReceived$.subscribe( (e: OAuthEvent) => {
            const redirect = decodeURIComponent(this.oauthSvc.state || '');
            if ( redirect && (redirect.length > 0)) {
                this.oauthSvc.state = "";
                this.stateSvc.redirect = redirect;
            } else {
                this.stateSvc.redirect = "";
            }
        });


        // This is tricky, as it might cause race conditions (where access_token is set in another
        // tab before everything is said and done there.
        // TODO: Improve this setup. See: https://github.com/jeroenheijmans/sample-angular-oauth2-oidc-with-auth-guards/issues/2
        window.addEventListener('storage', (event) => {
            // The `key` is `null` if the event was caused by `.clear()`
            if (event.key !== 'access_token' && event.key !== null) {
                return;
            }
            // console.warn('Noticed changes to access_token (most likely from another tab), updating isAuthenticated');
            
            this.updateTokensValid(null);

            this.stateSvc.authenticated$
                .pipe(take(2), skipWhile( (a: boolean) => !!a))
                .subscribe( (a: boolean) => {
                    this.router.navigateByUrl('/');
                });
        });


        combineLatest([
            this.sessionTerminated$,
            this.sessionError$
        ]).subscribe(
            () => this.router.navigateByUrl('/error/sso')
        );


        this.stateSvc.authenticated$
            .pipe(
                skipWhile( (a: boolean) => !a )
            )
            .subscribe(
               (a: boolean) => {
                    if (a === true) {
                        this.oauthSvc.loadUserProfile().then( (u: object) => {
                            this.stateSvc.info = <UserInfo>u;
                        });
                    } else {
                        this.stateSvc.resetUser();
                    }
               }
            );

        this.discoveryDocumentLoaded$.subscribe( (e: OAuthEvent) => {
            this.stateSvc.discoveryDocumentLoaded = true;
        });

        this.discoveryDocumentValidationError$.subscribe( (e: OAuthEvent) => {
            this.stateSvc.discoveryDocumentError = true;
        });
        
        this.discoveryDocumentLoadError$.subscribe( (e: OAuthEvent) => {
            this.stateSvc.discoveryDocumentLoadError = true;
        });

        combineLatest([
            this.discoveryDocumentLoaded$, 
            this.tokenReceived$,
            this.tokenRefreshed$,
            this.tokenError$,
            this.tokenRefreshError$,
            this.tokenValidationError$
        ]).subscribe( ([a,b,c,d,e,f]) => {
            this.updateTokensValid(null);
        });
    }

    updateTokensValid(e: OAuthEvent | null) {
        let valid = this.oauthSvc.hasValidAccessToken();
        this.stateSvc.accessToken = !!valid ? this.oauthSvc.getAccessToken() : "";
        this.stateSvc.scopes = !!valid ? <Array<string>>this.oauthSvc.getGrantedScopes() : [];
        this.stateSvc.claims = !!valid ? <ISSOClaims>this.oauthSvc.getIdentityClaims() : <ISSOClaims>{};

        valid = this.oauthSvc.hasValidIdToken();
        this.stateSvc.idToken = !!valid ? this.oauthSvc.getIdToken() : "";
    }

    private getEvent$(want: EOauthEvent): Observable<OAuthEvent> {
        return this._allEvents$
            .pipe(
                filter( 
                    (have: OAuthEvent) => have.type === want
                )
            )
    }

    get discoveryDocumentLoaded$(): Observable<OAuthEvent> { return this._discoveryDocumentLoaded$; }
    get discoveryDocumentLoadError$(): Observable<OAuthEvent> { return this._discoveryDocumentLoadError$; }
    get discoveryDocumentValidationError$(): Observable<OAuthEvent> { return this._discoveryDocumentValidationError$; }

    get jwksLoadError$(): Observable<OAuthEvent> { return this._jwksLoadError$; }
    get invalidNonceInState$(): Observable<OAuthEvent> { return this._invalidNonceInState$; }
    get codeError$(): Observable<OAuthEvent> { return this._codeError$; }

    get userProfileLoaded$(): Observable<OAuthEvent> { return this._userProfileLoaded$; }
    get userProfileLoadError$(): Observable<OAuthEvent> { return this._userProfileLoadError$; }

    get tokenReceived$(): Observable<OAuthEvent> { return this._tokenReceived$; }
    get tokenRefreshed$(): Observable<OAuthEvent> { return this._tokenRefreshed$; }
    get tokenExpires$(): Observable<OAuthEvent> { return this._tokenExpires$; }
    get tokenError$(): Observable<OAuthEvent> { return this._tokenError$; }
    get tokenRefreshError$(): Observable<OAuthEvent> { return this._tokenRefreshError$; }
    get tokenValidationError$(): Observable<OAuthEvent> { return this._tokenValidationError$; }
    get tokenRevokeError$(): Observable<OAuthEvent> { return this._tokenRevokeError$; }

    get silentlyRefreshed$(): Observable<OAuthEvent> { return this._silentlyRefreshed$; }
    get silentRefreshError$(): Observable<OAuthEvent> { return this._silentRefreshError$; }
    get silentRefreshTimeout$(): Observable<OAuthEvent> { return this._silentRefreshTimeout$; }

    get sessionChanged$(): Observable<OAuthEvent> { return this._sessionChanged$; }
    get sessionTerminated$(): Observable<OAuthEvent> { return this._sessionTerminated$; }
    get sessionError$(): Observable<OAuthEvent> { return this._sessionError$; }

    get popupBlocked$(): Observable<OAuthEvent> { return this._popupBlocked$; }
    get popupClosed$(): Observable<OAuthEvent> { return this._popupClosed$; }

    get logout$(): Observable<OAuthEvent> { return this._logout$; }
  
}

