import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { ActionsSubject } from '@ngrx/store';
import { Observable, of, from } from 'rxjs';

import { OAuthService, OAuthStorage, TokenResponse } from 'angular-oauth2-oidc';


@Injectable({
    providedIn: 'root'
})
export class SSODebugService {

    constructor(
        protected oauthSvc: OAuthService,
        protected oauthStore: OAuthStorage,
        private http: HttpClient,
        private actions$: ActionsSubject, 
    ) { 
        // this.actions$.subscribe( console.info );
    } 

    refresh(): Observable<null | TokenResponse> {
        let rToken = this.oauthStore.getItem('refresh_token');
        if (rToken) {
            return from(this.oauthSvc.refreshToken());
        }
        return of(null);
    }

    hello() {
        this.http
            .get<any>(`/sso/hello`)
            .subscribe(
                (a: any) => {
                    console.log('MEEP', a);
                }
            ); 
    }


    

}


