import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// module
import { SSORoutingModule } from './sso-routing.module';

import { RedirectComponent } from './components/redirect.component';
import { DebugComponent } from './components/debug.component';
import { DebugIdComponent } from './components/debug-id.component';
import { DebugClaimsComponent } from './components/debug-claims.component';
import { DebugAccessComponent } from './components/debug-access.component';


import { SSOIfDirective } from './directives/sso-if.directive';


@NgModule({
    imports: [
        CommonModule,
       
        SSORoutingModule,
    ],
    declarations: [
        SSOIfDirective, 

        RedirectComponent,

        DebugComponent,
        DebugIdComponent, 
        DebugClaimsComponent,
        DebugAccessComponent,
    ],
    exports: [
        SSOIfDirective,
    ]
})
export class SSOModule {}
