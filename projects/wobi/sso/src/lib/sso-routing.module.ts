import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RedirectComponent } from './components/redirect.component';

import { DebugComponent } from './components/debug.component';
import { DebugIdComponent } from './components/debug-id.component';
import { DebugClaimsComponent } from './components/debug-claims.component';
import { DebugAccessComponent } from './components/debug-access.component';
import { RouteGuardService } from './services/route-guard.service';

const routes: Routes = [
    {
        path: 'de',
        children: [
            {
                path: 'sso',
                children: [
                    {
                        path: 'redirect',
                        component: RedirectComponent,
                        pathMatch: 'full'
                    },
                ],
            }
        ]
    },
    {
        path: 'sso',
        children: [
            {
                path: 'redirect',
                component: RedirectComponent,
                pathMatch: 'full'
            },
            {
                path: 'debug',
                component: DebugComponent,
                children: [
                    {
                        path: 'id',
                        component: DebugIdComponent,
                        pathMatch: 'full'
                    },
                    {
                        path: 'claims',
                        component: DebugClaimsComponent,
                        pathMatch: 'full'
                    },
                    {
                        path: 'access',
                        component: DebugAccessComponent,
                        pathMatch: 'full'
                    },
                    {
                        path: '',
                        pathMatch: 'full',
                        redirectTo: 'access'
                    },
                ],
                canActivate: [RouteGuardService],
                data: {
                    guard: {
                        allOf: ['registered'],
                        redirect: false,
                    }
                }
            },
        ]
    },

];

@NgModule({
    imports: [
        RouterModule.forChild(
            routes,
            // {
            //     scrollPositionRestoration: 'enabled',
            // }
        )
    ],
    exports: [
        RouterModule
    ]
})
export class SSORoutingModule { }
