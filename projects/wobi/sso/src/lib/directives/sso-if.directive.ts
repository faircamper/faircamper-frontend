import { Directive, Input, TemplateRef, ViewContainerRef, OnInit } from '@angular/core';
import { IRoleRequirements, ERoles } from '@wobi/sso-state';
import { SSOPermissionService } from '../services/permission.service';

@Directive({
  selector: '[ssoIf]'
})
export class SSOIfDirective implements OnInit {

    private isHidden: boolean;
    private roles: Array<ERoles>;
    private requirements: IRoleRequirements;

    constructor(
        private templateRef: TemplateRef<any>,
        private viewContainer: ViewContainerRef,
        private permSvc: SSOPermissionService,
    ) {
        this.requirements = <IRoleRequirements>{};
        this.isHidden = true;
        this.roles = [];
    }

    @Input()
    set ssoIf(requirements: IRoleRequirements) {
        this.requirements = requirements;
        this.updateView();
    }

    ngOnInit() {
        this.permSvc.roles$.subscribe( (roles: Array<ERoles>) => {
            this.roles = roles;
            this.updateView();
        });
    }

    private updateView() {
        let result = this.permSvc.evaluate(this.requirements, this.roles);
        if (this.permSvc.allowed(result)) {
            if (this.isHidden) {
                this.viewContainer.createEmbeddedView(this.templateRef);
                this.isHidden = false;
            }
        } else {
            this.isHidden = true;
            this.viewContainer.clear();
        }
    }
}