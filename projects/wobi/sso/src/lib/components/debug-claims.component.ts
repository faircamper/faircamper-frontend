import { Component } from '@angular/core';

import { SSOStateService } from '../services/state.service';
 
@Component({
    selector: 'wobi-sso-debug-claims',
    template: `
    <h3>OIDC claims</h3>
    <pre>{{ stateSvc.claims$ | async | json }}</pre>
    `,
    styles: [`
:host {
    margin: 0 2rem;
    display: block;
}
    `]
})
export class DebugClaimsComponent {

    constructor(public stateSvc: SSOStateService) {}

}
