import { Component } from '@angular/core';

import { SSOStateService } from '../services/state.service';
 
@Component({
    selector: 'wobi-sso-debug-id',
    template: `
    <h3>OIDC ID token</h3>
    <pre>{{ stateSvc.idTokenJson$ | async | json }}</pre>
    `,
    styles: [`
:host {
    margin: 0 2rem;
    display: block;
}
    `]
})
export class DebugIdComponent {

    constructor(public stateSvc: SSOStateService) {}

}
