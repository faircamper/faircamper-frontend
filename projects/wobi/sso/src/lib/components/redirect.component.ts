import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { SSOStateService } from '../services/state.service';
 
const regexp = /^(\/..\/)/;

@Component({
    selector: 'wobi-sso-redirect',
    template: ``,
    styles: [
    ]
})
export class RedirectComponent {

    constructor(
        private router: Router, 
        private stateSvc: SSOStateService,
    ) {
        this.stateSvc.redirect$.subscribe( (redirect: string) => {
            if (!!redirect && (redirect.length > 0) ) {
                const startsWithI18nPrefix = redirect.match(/^\/..\//);
                if (startsWithI18nPrefix) {
                    redirect = redirect.replace(/^(\/..)/,"");
                }
                this.router.navigateByUrl(redirect);
            }
        });
    }

}
