import { Component } from '@angular/core';

import { SSODebugService } from '../services/debug.service';
import { SSOStateService } from '../services/state.service';

@Component({
    selector: 'wobi-sso-debug',
    template: `
    <a [routerLink]="['./', 'access']">Access Token</a>
    <a [routerLink]="['./', 'id']">ID Token</a>
    <a [routerLink]="['./', 'claims']">Claims</a>
    <a *ngIf="stateSvc.authenticated$ | async" href="#" (click)="refresh($event)">Refresh</a>
    <router-outlet></router-outlet>
    `,
    styles: [`
:host {
    margin: 3rem 2rem;
    display: block;
}
a {
    margin-left: 2rem;
}
    `]
})
export class DebugComponent {

    constructor(
        public dbgSvc: SSODebugService,
        public stateSvc: SSOStateService,
    ) {}

    refresh(event: MouseEvent) {
        event.preventDefault();
        event.stopPropagation()
        this.dbgSvc.refresh();
    }

}
