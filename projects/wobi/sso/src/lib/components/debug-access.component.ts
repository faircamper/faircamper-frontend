import { Component } from '@angular/core';

import { SSOStateService } from '../services/state.service';
 
@Component({
    selector: 'wobi-sso-debug-access',
    template: `
    <h3>OIDC access token</h3>
    <pre>{{ stateSvc.accessTokenJson$ | async | json }}</pre>
    `,
    styles: [`
:host {
    margin: 0 2rem;
    display: block;
}
    `]
})
export class DebugAccessComponent {

    constructor(public stateSvc: SSOStateService) {}

}
