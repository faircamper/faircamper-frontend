// import { IEnvironment } from '@wobi/core';
import { UserInfo } from 'angular-oauth2-oidc';

 
export interface ICodeFlowParams {
    state: string;
    code: string;
}

export interface ISSOClaims {
    acr: string;
    at_hash: string;
    aud: string;
    auth_time: number;
    azp: string;
    email: string;
    email_verified: boolean;
    avatar_url: string;    
    exp: number;
    family_name: string;
    given_name: string;
    locale: string;
    phone_number: string;
    phone_number_verified: boolean;
    iat: number;
    iss: string;
    jti: string;
    name: string;
    // nonce: string;
    preferred_username: string;
    session_state: string;
    sub: string;  
    typ: string;
    uid: string;
    uid_updated: number;
}

export interface ISSOProcess {
    discoveryDocumentLoaded: boolean;
    discoveryDocumentLoadError: boolean;
    discoveryDocumentError: boolean;
    redirect: string;
}

export interface ISSOData {
    info: UserInfo;
    scopes: Array<string>;
    claims: ISSOClaims;
    idToken: string;
    accessToken: string;
    process: ISSOProcess;
} 

export interface IAccessTokenRoles {
    roles: Array<ERoles>;    
}

export interface IAccessTokenResources {
    [service: string]: IAccessTokenRoles;
}

export interface IAccessToken {
    exp: number;
    iat: number;
    auth_time: number;
    jti: string;
    iss: string;
    aud: Array<string>;
    sub: string;
    typ: string;
    azp: string;
    // nonce: string;
    // session_state: string;
    acr: string;
    allowed_origins: Array<string>;
    realm_access: IAccessTokenRoles;
    resource_access: IAccessTokenResources;
    scope: string;
    email_verified: boolean;
    name: string;
    preferred_username: string;
    locale: string;
    given_name: string;
    family_name: string;
    email: string;
    phone_number: string;
    phone_number_verified: boolean;
    uid: string;
    uid_updated: number;
}

export enum EOauthEvent {
    DiscoveryDocumentLoaded = 'discovery_document_loaded',
    JwksLodaError = 'jwks_load_error',
    InvalidNonceInState= 'invalid_nonce_in_state',
    DiscoveryDocumentLoadError = 'discovery_document_load_error',
    DiscoveryDocumentValidationError = 'discovery_document_validation_error',
    UserProfileLoaded = 'user_profile_loaded',
    UserProfileLoadError = 'user_profile_load_error',
    TokenReceived = 'token_received',
    TokenError = 'token_error',
    CodeError = 'code_error',
    TokenRefreshed = 'token_refreshed',
    TokenRefreshError = 'token_refresh_error',
    SilentRefreshError = 'silent_refresh_error',
    SilentlyRefreshed =  'silently_refreshed',
    SilentRefreshTimeout = 'silent_refresh_timeout',
    TokenValidationError = 'token_validation_error',
    TokenExpires = 'token_expires',
    SessionChanged = 'session_changed',
    SessionError = 'session_error',
    SessionTerminated = 'session_terminated',
    Logout = 'logout',
    PopupClosed = 'popup_closed',
    PopupBlocked = 'popup_blocked',
    TokenRevokeError = 'token_revoke_error'
}

export enum ERoles {
    Pending = 'pending',
    Registered = 'registered',
    Lessor = 'lessor',
    LessorMod = 'lessor-mod',
    Tenant = 'tenant',
    Admin = 'admin',
}

export interface IRoleRequirements {
    in?: Array<ERoles>;
    notIn?: Array<ERoles>;
    all?: Array<ERoles>;
    notAll?: Array<ERoles>;
    public?: boolean;
    onlyVisitors?: boolean;
}

export enum EPermissionCheckResult {
    NoTokenOK = 'no.token.and.allowed',
    NoTokenErr = 'no.token.and.not.allowed',
    TokenOK = 'token.and.allowed',
    TokenErr = 'token.and.not.allowed',
}

export interface IRedirectBehaviour {
    enabled?: boolean;
    url?: string;
}

export interface ISSORouteGuardData {
    guard?: IRoleRequirements;
    visitorRedirect?: IRedirectBehaviour;
}