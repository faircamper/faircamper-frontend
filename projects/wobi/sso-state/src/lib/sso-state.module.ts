import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// ngrx configuration
import { StoreModule } from '@ngrx/store';
import { moduleReducers } from './ngrx/reducers/module.reducer';
import { initialModuleState } from './ngrx/module.state';


@NgModule({
    imports: [
        CommonModule,
        StoreModule.forFeature(
            '@wobi/sso-state',
            moduleReducers,
            {
                initialState: initialModuleState
            }
        ),
    ],
    declarations: [

    ],
    exports: [

    ]
})
export class SSOModule {}
