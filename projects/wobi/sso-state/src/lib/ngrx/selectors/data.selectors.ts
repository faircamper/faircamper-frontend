import { createSelector } from '@ngrx/store';

import { UserInfo } from 'angular-oauth2-oidc';

import { selectRootState } from '../module.state';
import { ISSOData, ISSOClaims, IAccessToken, IAccessTokenRoles, ERoles } from '../../lib.types';
import { parseToken } from '../../util/jwt.decoder';

const RESOURCE_ACCESS_SERVICE: string = 'fc-app';

export const selectDataState = createSelector(
    selectRootState, 
    (state: any): ISSOData => state.keycloak
);


export const selectUserInfo = createSelector(
    selectDataState,
    (state: ISSOData): UserInfo => state.info
);

export const selectAccessToken = createSelector(
    selectDataState,
    (state: ISSOData): string => state.accessToken
);

export const selectAccessTokenJson = createSelector(
    selectAccessToken,
    (token: string): IAccessToken => parseToken(token)
);

export const selectIdToken = createSelector(
    selectDataState,
    (state: ISSOData): string => state.idToken
);

export const selectIdTokenJson = createSelector(
    selectIdToken,
    (token: string): ISSOClaims => parseToken(token)
);

export const selectClaims = createSelector(
    selectDataState,
    (state: ISSOData): ISSOClaims => state.claims
);

export const selectScopes = createSelector(
    selectDataState,
    (state: ISSOData): Array<string> => state.scopes
);

export const selectLifetime = createSelector(
    selectAccessTokenJson,
    (token: IAccessToken): number => !!token ? (token.exp - token.iat) : -1
);

export const selectRoles = createSelector(
    selectAccessTokenJson,
    (token: IAccessToken): Array<ERoles> => {
        // no token
        if (!token) {
            return [];
        }

        // token present, try get resource
        let resource: IAccessTokenRoles = token.resource_access[RESOURCE_ACCESS_SERVICE];
        if (!resource) {
            // no resource indicates borked token
            return [];
        }

        // return resource roles
        return resource.roles || [];
    }
);



// export const selectAccessTokenValid = createSelector(
//     selectProcessState,
//     (state: ISSOProcess): boolean => state.accessToken != null
// );



// export const selectIdTokenValid = createSelector(
//     selectProcessState,
//     (state: ISSOProcess): boolean => state.idToken !== null
// );
