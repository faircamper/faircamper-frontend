import { createSelector } from '@ngrx/store';

import { selectRootState } from '../module.state';
import { ISSOProcess } from '../../lib.types';



export const selectProcessState = createSelector(
    selectRootState, 
    (state: any): ISSOProcess => state.keycloak.process
);

export const selectRedirect = createSelector(
    selectProcessState,
    (state: ISSOProcess): string => state.redirect
);

export const selectIsDocumentLoaded = createSelector(
    selectProcessState,
    (state: ISSOProcess): boolean => state.discoveryDocumentLoaded
);

export const selectHasDocumentError = createSelector(
    selectProcessState,
    (state: ISSOProcess): boolean => state.discoveryDocumentError
);

export const selectHasDocumentLoadError = createSelector(
    selectProcessState,
    (state: ISSOProcess): boolean => state.discoveryDocumentLoadError
);



