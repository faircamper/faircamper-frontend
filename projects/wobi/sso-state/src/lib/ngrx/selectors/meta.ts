import { createSelector } from '@ngrx/store';

import { selectAccessToken, selectIdToken } from './data.selectors';
import { selectIsDocumentLoaded, selectHasDocumentError, selectHasDocumentLoadError} from './process.selectors';


export const selectAuthenticated = createSelector(
    selectAccessToken,
    selectIdToken,
    selectIsDocumentLoaded,
    selectHasDocumentError,
    selectHasDocumentLoadError,
    (access: string, identity: string, docLoaded: boolean, docParseErr: boolean, docLoadErr: boolean): boolean => 
        (access !== "") && (identity !== "") && docLoaded && !docParseErr && !docLoadErr
);