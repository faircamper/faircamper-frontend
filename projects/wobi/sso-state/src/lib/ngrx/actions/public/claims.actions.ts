import { createAction } from '@ngrx/store';

// app
import { type } from '@wobi/core';

// Category to uniquely identify the actions
const CATEGORY = 'wobi.sso.claims';

/**
 * For each action type in an action group, make a simple
 * enum object for all of this group's action types.
 *
 * The 'type' utility function coerces strings into string
 * literal types and runs a simple check to guarantee all
 * action types in the application are unique.
 */
export interface IClaimsActions {
    RESET: string;
}

export const ClaimsActionTypes: IClaimsActions = {
    RESET: type(`${CATEGORY}.reset`),
};

export const ClaimsResetAction = createAction(
    ClaimsActionTypes.RESET
);
