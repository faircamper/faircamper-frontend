import { createAction, props } from '@ngrx/store';

// app
import { type } from '@wobi/core';

// Category to uniquely identify the actions
const CATEGORY = 'wobi.sso.process';

/**
 * For each action type in an action group, make a simple
 * enum object for all of this group's action types.
 *
 * The 'type' utility function coerces strings into string
 * literal types and runs a simple check to guarantee all
 * action types in the application are unique.
 */
export interface IProcessActions {
    DOCUMENT: string;
    DOCUMENT_ERROR: string;
    DOCUMENT_LOAD_ERROR: string;
    REDIRECT: string;
}

export const ProcessActionTypes: IProcessActions = {
    DOCUMENT: type(`${CATEGORY}.document`),
    DOCUMENT_LOAD_ERROR: type(`${CATEGORY}.document.load.error`),
    DOCUMENT_ERROR: type(`${CATEGORY}.document.error`),
    REDIRECT: type(`${CATEGORY}.redirect`),
};

export const ProcessDocumentAction = createAction(
    ProcessActionTypes.DOCUMENT,
    props<{discoveryDocumentLoaded: boolean}>()
);

export const ProcessDocumentLoadErrorAction = createAction(
    ProcessActionTypes.DOCUMENT_LOAD_ERROR,
    props<{discoveryDocumentLoadError: boolean}>()
);

export const ProcessDocumentErrorAction = createAction(
    ProcessActionTypes.DOCUMENT_ERROR,
    props<{discoveryDocumentError: boolean}>()
);

export const ProcessRedirectAction = createAction(
    ProcessActionTypes.REDIRECT,
    props<{redirect: string}>()
);
