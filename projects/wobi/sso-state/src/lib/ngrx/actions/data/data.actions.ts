import { createAction, props } from '@ngrx/store';

// app
import { type } from '@wobi/core';
import { UserInfo } from 'angular-oauth2-oidc';
import { ISSOData, ISSOClaims } from '../../../lib.types';

// Category to uniquely identify the actions
const CATEGORY = 'wobi.sso.data';

/**
 * For each action type in an action group, make a simple
 * enum object for all of this group's action types.
 *
 * The 'type' utility function coerces strings into string
 * literal types and runs a simple check to guarantee all
 * action types in the application are unique.
 */
export interface IDataActions {
    INFO: string;
    SCOPES: string;
    CLAIMS: string;
    TOKEN_ID: string;
    TOKEN_ACCESS: string;
    SET:   string;
    RESET:   string;
}

export const DataActionTypes: IDataActions = {
    INFO: type(`${CATEGORY}.info`),
    SCOPES: type(`${CATEGORY}.scopes`),
    CLAIMS: type(`${CATEGORY}.claims`),
    TOKEN_ID: type(`${CATEGORY}.token.id`),
    TOKEN_ACCESS: type(`${CATEGORY}.token.access`),
    SET:   type(`${CATEGORY}.set`),
    RESET:   type(`${CATEGORY}.reset`),
};

export const DataSetAction = createAction(
    DataActionTypes.SET,
    props<{newState: ISSOData}>()
);

export const DataInfoAction = createAction(
    DataActionTypes.INFO,
    props<{info: UserInfo}>()
);

export const DataScopesAction = createAction(
    DataActionTypes.SCOPES,
    props<{scopes: Array<string>}>()
);

export const DataClaimsAction = createAction(
    DataActionTypes.CLAIMS,
    props<{claims: ISSOClaims}>()
);

export const DataResetAction = createAction(
    DataActionTypes.RESET
);

export const DataIdTokenAction = createAction(
    DataActionTypes.TOKEN_ID, 
    props<{idToken: string}>()
);

export const DataAccessTokenAction = createAction(
    DataActionTypes.TOKEN_ACCESS,
    props<{accessToken: string}>()
);
