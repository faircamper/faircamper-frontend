import { createFeatureSelector } from '@ngrx/store';

import { ISSOState } from './reducers/module.reducer';

export const initialModuleState: ISSOState = {
//    user: initialUserState,
}; 

export const selectRootState = (state: any) => state; 
export const selectState = createFeatureSelector<ISSOState>('@wobi/sso-state');