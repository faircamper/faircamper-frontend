import { createReducer, on, Action } from '@ngrx/store';
import { UserInfo } from 'angular-oauth2-oidc';

import {
    ProcessDocumentAction,
    ProcessDocumentLoadErrorAction,
    ProcessDocumentErrorAction,
    ProcessRedirectAction,
} from '../actions/process/process.actions';

import {
    DataSetAction,
    DataInfoAction,
    DataClaimsAction,
    DataScopesAction,
    DataResetAction,
    DataIdTokenAction,
    DataAccessTokenAction,
} from '../actions/data/data.actions';

import { ISSOClaims, ISSOData } from '../../lib.types';


export const initialDataState: ISSOData = {
    claims: <ISSOClaims>{},
    scopes: [],
    info: <UserInfo>{},
    idToken: "",
    accessToken: "",
    process: {
        discoveryDocumentLoaded: false,
        discoveryDocumentLoadError: false,
        discoveryDocumentError: false,
        redirect: "",  
    }
};

const reducer = createReducer(
    initialDataState,
    on(
        DataSetAction,
        (state: ISSOData, {newState}) => ({...newState})
    ),
    on(
        DataIdTokenAction,
        (state: ISSOData, {idToken}) => ({...state, ...{idToken}})
    ),
    on(
        DataAccessTokenAction,
        (state: ISSOData, {accessToken}) => ({...state, ...{accessToken}})
    ),
    on(
        DataInfoAction,
        (state: ISSOData, {info}) => ({...state, ...{...info}})
    ),
    on(
        DataScopesAction,
        (state: ISSOData, {scopes}) => ({...state, ...{scopes}})
    ),
    on(
        DataClaimsAction,
        (state: ISSOData, {claims}) => ({...state, ...{claims}})
    ),
    on(
        DataResetAction,
        (state: ISSOData) => ({...initialDataState})
    ),
    on(
        ProcessDocumentAction,
        (state: ISSOData, {discoveryDocumentLoaded}) => {
            let process = {
                ...state.process,
                discoveryDocumentLoaded: discoveryDocumentLoaded,
            };
            return Object.assign({}, state, {process});
        }
    ),
    on(
        ProcessRedirectAction,
        (state: ISSOData, {redirect}) => {
            let process = {
                ...state.process,
                redirect: redirect,
            };
            return Object.assign({}, state, {process});
        }
    ),
    on(
        ProcessDocumentLoadErrorAction,
        (state: ISSOData, {discoveryDocumentLoadError}) => {
            let process = {
                ...state.process,
                discoveryDocumentLoadError: discoveryDocumentLoadError,
            };
            return Object.assign({}, state, {process});
        }
    ),
    on(
        ProcessDocumentErrorAction,
        (state: ISSOData, {discoveryDocumentError}) => {
            let process = {
                ...state.process,
                discoveryDocumentError: discoveryDocumentError,
            };
            return Object.assign({}, state, {process});
        }
    ),
);

export function keycloakReducer(state: ISSOData | undefined, action: Action) {
    return reducer(state, action);
}
