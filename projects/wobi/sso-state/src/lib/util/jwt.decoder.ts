
const urlBase64Decode = (input: string): string => {
    let output = input
        .replace(/-/g, '+')
        .replace(/_/g, '/');
    switch (output.length % 4) {
        case 0: {
            break;
        }
        case 2: {
            output += '==';
            break ;
        }
        case 3: {
            output += '=';
            break;
        }
        default: {
            throw new Error('invalid base64url string');
        }
    }
    return decodeURIComponent(
        encodeURI(
            window.atob(output)
        )
    );
};

export const parseToken = (token: string): any => {
    if (!token) {
        return null;
    }
    const parts = token.split('.');
    if (parts.length !== 3) {
      // throw new Error('invalid JWT string: ' + token);
      return null;
    }
    const decodedFromBase64 = urlBase64Decode(parts[1]);
    if (!decodedFromBase64) {
      // throw new Error('invalid JWT content');
      return null;
    }
    const parsed = JSON.parse(decodedFromBase64);
    if (!parsed) {
        return null;
    }
    return parsed;
}