import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

// module local
import { NotFoundComponent } from './components/error/not-found.component';


@NgModule({
    imports: [
        CommonModule,
        RouterModule,
    ],
    declarations: [
        NotFoundComponent, 
    ],
    exports: [
        NotFoundComponent,
    ]
})
export class CoreModule {}
