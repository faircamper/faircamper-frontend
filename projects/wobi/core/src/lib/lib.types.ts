import { InjectionToken } from '@angular/core';
import { AuthConfig } from 'angular-oauth2-oidc';


// -----------------------------------------------------------------------------------------
// -> helper function for ngrx
//
// typeCache ensures the uniqueness of actions types across the whole project
const typeCache: { [label: string]: boolean } = {};
export function type<T>(label: T | ''): T {
   if (typeCache[<string>label]) {
     throw new Error(`Action type "${label}" is not unqiue"`);
   }
 
   typeCache[<string>label] = true;
 
   return <T>label;
}


// -----------------------------------------------------------------------------------------
// -> helper for uuid db cells

export const UUIDV4Regex = /^[A-F\d]{8}-[A-F\d]{4}-4[A-F\d]{3}-[89AB][A-F\d]{3}-[A-F\d]{12}$/i;
export const UUID_NIL = "00000000-0000-0000-0000-000000000000";

export const uuid = (): string => {
    let seed = Date.now();
    if (window.performance && typeof window.performance.now === 'function') {
        seed += performance.now();
    }

    const uuidStr = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        const r = (seed + Math.random() * 16) % 16 | 0;
        seed = Math.floor(seed / 16);

        return (c === 'x' ? r : r & (0x3 | 0x8)).toString(16);
    });

    return uuidStr;
};

export const isUUIDv4 = (value: string): boolean => {
    return UUIDV4Regex.test(value);
};

// -----------------------------------------------------------------------------------------
// -> typings for environment.ts

export interface IKeycloak {
    accountUrl: string;
    oidcTokenInjectRegex: Array<RegExp>;
    permissionError: string;
    postSignup: string;
}
export interface IEnvironment {
    production: boolean;
    keycloak: IKeycloak;
    oidc: AuthConfig;
}

export const EnvironmentToken: InjectionToken<IEnvironment> = new InjectionToken<IEnvironment>('@wobi/core/token/environment');