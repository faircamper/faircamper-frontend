import { Params, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { RouterStateSerializer, RouterReducerState } from '@ngrx/router-store';


export interface IParamAttribute {
    param: string;
}

export interface IRouterState {
    url: string;
    params: Params;
    queryParams: Params;
    data: any;
}

export const selectRouterFeature = createFeatureSelector<RouterReducerState<IRouterState>>('router');

export const selectRouter = createSelector(
    selectRouterFeature,
    (routeState: RouterReducerState<IRouterState>): IRouterState => routeState.state
);

export const selectQueryParams = createSelector(
    selectRouter,
    (state: IRouterState): Params => state.queryParams
);

export const selectQueryParam = (attr: IParamAttribute) => { 
    return createSelector(
        selectQueryParams,
        (state: Params): any => state[attr.param]
    );
}

export const selectRouteParams = createSelector(
    selectRouter,
    (state: IRouterState): Params => state.params
);

export const selectRouteParam = (attr: IParamAttribute) => { 
    return createSelector(
        selectRouteParams,
        (state: Params): any => state[attr.param]
    );
}

export const selectRouteData = createSelector(
    selectRouter,
    (state: IRouterState): any => state.data
);

export const selectUrl = createSelector(
    selectRouter,
    (state: IRouterState): string => state.url
);
   
export class RouterSerializer implements RouterStateSerializer<IRouterState> {
    serialize(routerState: RouterStateSnapshot): IRouterState {
        const {
            url,
            root: { queryParams },
        } = routerState;
        let currentRoute: ActivatedRouteSnapshot = routerState.root;
        let params: Params = {};
        while (currentRoute?.firstChild) {
            currentRoute = currentRoute.firstChild;
            params = {
                ...params,
                ...currentRoute.params,
            };
        }
        const data = currentRoute.data;
        return { url, params, queryParams, data };
    }
}
