import { Component } from '@angular/core';
import { Router } from '@angular/router';


@Component({
    selector: 'wobi-core-error-not-found',
    template: `
<h2>HTTP Status 404 <small>page not found</small></h2>
    `,
    styles: [`
:host {
    margin: 3rem 2rem;
    display: block;
}
    `]
})
export class NotFoundComponent {

    constructor(
        private router: Router,
    ) {
        console.log(this.router.url);
        console.log(this.router.routerState);

    }


}
