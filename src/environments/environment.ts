// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  karma: false,
  defaultCurrency: 'EUR',
  apiURL: 'http://localhost:8080/api/',
  // apiURL: 'https://api.testing.wobi.lan/',
  retries: 1,
  timeout: 3000000,
  keycloak: {
    accountUrl: 'https://wobi-test.de/auth/realms/fc-test/account/',
    oidcTokenInjectRegex: [/api.wobi-test.de/, /localhost:8080/],
  },
  oidc: {
    // Url of the Identity Provider
    issuer: 'https://wobi-test.de/auth/realms/fc-test',

    // URL of the SPA to redirect the user to after login
    redirectUri: 'http://localhost:4200/$1/sso/redirect',

    postLogoutRedirectUri: 'http://localhost:4200/',

    // The SPA's id. The SPA is registerd with this id at the auth-server
    // clientId: 'server.code',
    clientId: 'fc-app',

    // Just needed if your auth server demands a secret. In general, this
    // is a sign that the auth server is not configured with SPAs in mind
    // and it might not enforce further best practices vital for security
    // such applications.
    // dummyClientSecret: 'secret',

    responseType: 'code',

    // set the scope for the permissions the client should request
    // The first four are defined by OIDC.
    // Important: Request offline_access to get a refresh token
    // The api scope is a usecase specific one
    scope: 'openid profile email offline_access phone',

    showDebugInformation: false,
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
