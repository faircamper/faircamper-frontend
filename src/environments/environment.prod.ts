export const environment = {
  production: true,
  karma: false,
  defaultCurrency: 'EUR',
  apiURL: '',
  retries: 3,
  timeout: 3000,
  auth: {
    tokenEndpoint: '',
    userinfoEndpoint: '',
    clientId: '', 
    clientSecret: '',
    scope: '',
    showDebugInformation: true,
    timeoutFactor: 0.01,
    requireHttps: false,
    oidc: false,
  },

  keycloak: {
    accountUrl: 'https://testing.wobi.lan/auth/realms/fc-test/account/',
    oidcTokenInjectRegex: [
      /\/https:\/\/api.testing.wobi.lan/,
    ],
    permissionError: '/errors/permission',
    postSignup: '/lessor/signup'
  },
  oidc: {
      // Url of the Identity Provider
      issuer: 'https://testing.wobi.lan/auth/realms/fc-test',

      // URL of the SPA to redirect the user to after login
      redirectUri: 'https://testing.wobi.lan/$1/sso/redirect',

      postLogoutRedirectUri: 'https://testing.wobi.lan/',

      // The SPA's id. The SPA is registerd with this id at the auth-server
      // clientId: 'server.code',
      clientId: 'fc-app',

      // Just needed if your auth server demands a secret. In general, this
      // is a sign that the auth server is not configured with SPAs in mind
      // and it might not enforce further best practices vital for security
      // such applications.
      // dummyClientSecret: 'secret',

      responseType: 'code',

      // set the scope for the permissions the client should request
      // The first four are defined by OIDC.
      // Important: Request offline_access to get a refresh token
      // The api scope is a usecase specific one
      scope: 'openid profile email offline_access phone',

      showDebugInformation: false,
  }
};
