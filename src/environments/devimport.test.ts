import { StoreDevtoolsModule } from '@ngrx/store-devtools';

export const DevelopmentImports = [
  StoreDevtoolsModule.instrument({
    maxAge: 50, // Retains last 50 states
    logOnly: false, // Restrict extension to log-only mode
  }),
];
