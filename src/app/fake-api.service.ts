import { Injectable } from '@angular/core';
import {
  InMemoryDbService,
  ParsedRequestUrl,
  RequestInfoUtilities,
  RequestInfo,
  ResponseOptions,
  STATUS,
} from 'angular-in-memory-web-api';
import { FakeLessorApiService } from './lessor/fake-api/fake-lessor-api.service';
import { FakeTenantApiService } from './tenant/fake-api/fake-tenant-api.service';
import { FakeSignupTenantApiService } from './signup/fake-api/fake-signup-api.service';
import { FakeCalendarApiService } from './lessor/fake-api/fake-calendar-api.service';
import { Availability } from './tenant/models/availability/availability';
import { FakeEquipmentApiService } from './lessor/fake-api/fake-equipment-api.service';

@Injectable({
  providedIn: 'root',
})
export class FakeApiService implements InMemoryDbService {
  constructor(
    private fakeLessorApiService: FakeLessorApiService,
    private fakeTenantApiService: FakeTenantApiService,
    private fakeSignupTenantApiService: FakeSignupTenantApiService,
    private fakeCalendarApiService: FakeCalendarApiService,
    private fakeOptionalEquipmentApiService: FakeEquipmentApiService
  ) {}

  createDb() {
    let db = {} as any;
    db = { ...db, ...this.fakeLessorApiService.createDb() };
    db = { ...db, ...this.fakeTenantApiService.createDb() };
    db = { ...db, ...this.fakeSignupTenantApiService.createDb() };
    db = { ...db, ...this.fakeCalendarApiService.createDb() };
    db = { ...db, ...this.fakeOptionalEquipmentApiService.createDb() };
    return { ...db };
  }
  get(reqInfo: RequestInfo) {
    if (
      !reqInfo.id &&
      reqInfo.collection &&
      reqInfo.collectionName === 'availability'
    ) {
      // filter for availibilty
      const from = reqInfo.query.get('from');
      let to = reqInfo.query.get('to');
      if (from && to) {
        const fromDate = new Date(
          +from[0].split('-')[0],
          +from[0].split('-')[1] - 1
        );
        const toDate = new Date(+to[0].split('-')[0], +to[0].split('-')[1] - 1);
        const data = reqInfo.collection.filter((e: Availability) => {
          const eDate = new Date(e.year, e.month);
          return (
            eDate.getTime() >= fromDate.getTime() &&
            eDate.getTime() <= toDate.getTime()
          );
        });
        return reqInfo.utils.createResponse$(() => {
          const options: ResponseOptions = {
            body: { data },
            status: STATUS.OK,
          };
          return options;
        });
      }
    }
    // filter on id
    if (reqInfo.id && reqInfo.collection) {
      return reqInfo.utils.createResponse$(() => {
        const data = (reqInfo.collection as any[]).find((e) => {
          return e.id == reqInfo.id;
        });
        const options: ResponseOptions = {
          body: { data },
          status: STATUS.OK,
        };
        return options;
      });
    }
    // send whole collection
    if (!reqInfo.id && reqInfo.collection) {
      return reqInfo.utils.createResponse$(() => {
        const options: ResponseOptions = {
          body: { data: reqInfo.collection },
          status: STATUS.OK,
        };
        return options;
      });
    }
    return undefined;
  }
  parseRequestUrl(url: string, utils: RequestInfoUtilities): ParsedRequestUrl {
    url = this.fakeLessorApiService.parseRequestUrl(url, utils);
    url = this.fakeTenantApiService.parseRequestUrl(url, utils);
    url = this.fakeSignupTenantApiService.parseRequestUrl(url, utils);
    url = this.fakeCalendarApiService.parseRequestUrl(url, utils);
    url = this.fakeOptionalEquipmentApiService.parseRequestUrl(url, utils);
    return utils.parseRequestUrl(url);
  }
}
