import { CurrencyPipe } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import {
  Inject,
  LOCALE_ID,
  NgModule,
  APP_INITIALIZER,
  Injector,
} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FcLibModule } from 'faircamper-lib/projects/fc-lib/src/public-api';

import { environment } from '../environments/environment';
import { DevelopmentImports } from '../environments/devimport';

// ##################################################################
// auth

import { OAuthModule, OAuthStorage } from 'angular-oauth2-oidc';

// ##################################################################
// ngrx

import { StoreModule } from '@ngrx/store';
import { routerReducer, RouterReducerState } from '@ngrx/router-store';

import { ActionReducerMap } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

// ##################################################################
// wobi sso

import {
  SSOModule,
  SSOHeaderInjectInterceptor,
  SSOInitializerTrigger,
} from '@wobi/sso';

import { ISSOData, keycloakReducer, initialDataState } from '@wobi/sso-state';

// ##################################################################
// basics

import { CoreModule, EnvironmentToken, IRouterState } from '@wobi/core';

// ##################################################################
// project local

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FakeTenantApiService } from './tenant/fake-api/fake-tenant-api.service';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { FakeLessorApiService } from './lessor/fake-api/fake-lessor-api.service';
import { FakeApiService } from './fake-api.service';

// ##################################################################
// file-local

interface IRootState {
  keycloak: ISSOData;
  router: RouterReducerState<IRouterState> | null;
}

const initialRootState: IRootState = {
  keycloak: initialDataState,
  router: null,
};

const rootReducers: ActionReducerMap<any> = {
  keycloak: keycloakReducer,
  router: routerReducer,
};

const storageFactory = (): OAuthStorage => {
  return localStorage;
};

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(FakeApiService, {
      delay: 600,
      passThruUnknownUrl: true,
      apiBase: 'api',
    }),
    StoreModule.forRoot(rootReducers, {
      initialState: initialRootState,
      runtimeChecks: {
        strictStateImmutability: true,
        strictActionImmutability: true,
        strictStateSerializability: false,
        strictActionSerializability: false,
      },
    }),
    EffectsModule.forRoot([]),
    CoreModule,
    SSOModule,
    OAuthModule.forRoot(),
    AppRoutingModule,
    FcLibModule,
    ...DevelopmentImports,
  ],
  providers: [
    CurrencyPipe,
    {
      provide: EnvironmentToken,
      useValue: environment,
    },
    {
      provide: APP_INITIALIZER,
      useFactory: SSOInitializerTrigger,
      deps: [Injector],
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: SSOHeaderInjectInterceptor,
      multi: true,
    },
    {
      provide: OAuthStorage,
      useFactory: storageFactory,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
  constructor(@Inject(LOCALE_ID) locale: string) {
    console.log('running with locale:', locale);
  }
}
