import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { NotFoundComponent } from '@wobi/core';

const routes: Routes = [
  {
    path: 'lessor',
    loadChildren: () =>
      import('./lessor/lessor.module').then((mod) => mod.LessorModule),
  },
  {
    path: 'signup',
    loadChildren: () =>
      import('./signup/signup.module').then((mod) => mod.SignupModule),
  },
  {
    path: '',
    loadChildren: () =>
      import('./tenant/tenant.module').then((mod) => mod.TenantModule),
  },
  {
    path: '**',
    pathMatch: 'full',
    component: NotFoundComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
