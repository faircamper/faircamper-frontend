import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Subscription, EMPTY } from 'rxjs';
import { ERoles, IRoleRequirements } from '@wobi/sso-state';
import { SSOService } from '@wobi/sso';
import { Message, MessagesService } from '../lessor/services/message/messages.service';


 
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
})
export class SignupComponent implements OnInit {

  public loginRequirements: IRoleRequirements = {
    onlyVisitors: true,
  };

  // public logoutRequirements: IRoleRequirements = {
  //   in: [ERoles.Pending, ERoles.Registered]
  // };

  // isOpen = true;
  // expanded = false;
   msgSubscription: Subscription = EMPTY.subscribe();
   url = '';
  constructor(
    private messages: MessagesService, private router: Router, private ssoSvc: SSOService) {
    router.events.subscribe((data) => {
      if (data instanceof NavigationEnd) {
        this.url = data.url;
      }
    });
  }

  ngOnInit(): void {
    this.msgSubscription = this.messages.getMessages().subscribe((msg) => {
      this.displayMessage(msg);
    });

    this.url = this.router.url;
  }

  public displayMessage(msg: Message): void {
    const errCode = msg.errorCode === undefined ? '' : msg.errorCode;
    // Open Message System here
  }


}
