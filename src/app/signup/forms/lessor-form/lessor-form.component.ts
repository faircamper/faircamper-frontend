import { formatDate } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { accountRoles, businessForms, LessorAccount,} from 'src/app/lessor/models/account/lessor';
@Component({
  selector: 'app-lessor-form',
  templateUrl: './lessor-form.component.html',
  styleUrls: ['./lessor-form.component.css']
})
export class LessorFormComponent implements OnInit {
  signupForm!: FormGroup;
  @Input() min: string = '';//format '2022-05-22'
  @Input() max: string = formatDate(new Date(), 'YYYY-MM-dd', 'en_Us');//todays date
  @Input() disabledDate: boolean = false;
  @Output() outputSignup = new EventEmitter<LessorAccount>();

  accountRoles = accountRoles;
  businessForms = businessForms;
  
  constructor(public fb: FormBuilder) {}

  ngOnInit(): void {
    this.signupForm = this.fb.group({
      company: this.fb.group({
        name: ['', [Validators.required]],
        businessFormId: ['', [Validators.required]],
        street: ['', [Validators.required]],
        streetNumber: ['', [Validators.required]],
        postal_code: ['', [Validators.required]],
        city: ['', [Validators.required]],
        country: ['', [Validators.required]],
        preTaxDeductable: [false, [Validators.required]],
        website: ['', [Validators.required]],
      }),
      person: this.fb.group({
        name: ['', [Validators.required]],
        surname: ['', [Validators.required]],
        accountRoleId: ['', [Validators.required]],
        email: ['', [Validators.required, Validators.email]],
        username: ['', [Validators.required]],
        birthday: ['', [Validators.required]],
        mobilePhone: ['', [Validators.required]],
        phone: ['', [Validators.required]],
      }),
    });
    console.log(accountRoles);
  }

  get getPerson(): FormGroup {
    return this.signupForm.get('person') as FormGroup;
  }
  get getCompany(): FormGroup {
    return this.signupForm.get('company') as FormGroup;
  }

  submit() {
    const companyFormValue = this.signupForm.get('company')?.value;
    const personFormValue = this.signupForm.get('person')?.value;
    const newLessorAccount: LessorAccount = {
      id: 'd69d071d-d3f8-4fed-8f23-095bdce4b252',
      keycloakId: '312ewqewqdsa',
      email: 'dsadasda@dasdas.de',
      isEmailVerified: true,
      username: 'dasda',
      name: 'fadfsdf',
      surname: 'fsdfdsfsd',
      accountType: 'lessor',
      company: { ...companyFormValue },
      persons: [{ ...personFormValue }],
      documents: [],
    };
    console.log(newLessorAccount);
    //this.outputSignup.emit(newLessorAccount);
  }
}

