import { formatDate } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EMPTY, Subscription } from 'rxjs';
import { TenantAccountService } from 'src/app/tenant/fake-api/account/account/tenant-account.service';

import { TenantAccount } from '../../../tenant/models/Account/tenant';

@Component({
  selector: 'app-tenant-form',
  templateUrl: './tenant-form.component.html',
  styleUrls: ['./tenant-form.component.css']
})
export class TenantFormComponent implements OnInit {

  signupForm!: FormGroup;
  @Input() min: string = '';//format '2022-05-22'
  @Input() max: string = formatDate(new Date(), 'YYYY-MM-dd', 'en_Us');//todays date
  @Input() disabledDate: boolean = false;
  @Output() outputSignup = new EventEmitter<TenantAccount>();
  @Input() isSignupPage = true;
  accountSubscription: Subscription = EMPTY.subscribe();
  public accountData: TenantAccount | null = null;
  constructor(public fb: FormBuilder, public accountService: TenantAccountService) { }

  ngOnInit(): void {
    this.signupForm = this.fb.group({
      tenantAccount: this.fb.group({
        street: ['', [Validators.required]],
        streetNumber: ['', [Validators.required]],
        postalCode: ['', [Validators.required]],
        city: ['', [Validators.required]],
        country: ['', [Validators.required]],
        birthday: ['',[Validators.required]],
        mobilePhone: ['', [Validators.required]],
        phone: [''],
      })
    });

    this.accountSubscription = this.accountService.get<TenantAccount>().subscribe((data) => {
      this.accountData = data
    });
  }

  get getAccount(): FormGroup {
    return this.signupForm.get('tenantAccount') as FormGroup;
  }

  submit() {
    const tenantFormValue = this.signupForm.get('tenantAccount')?.value;
    const newTenantAccount: TenantAccount = {
      ...tenantFormValue,
    };
    this.outputSignup.emit(newTenantAccount);
  }

  editPersonalData(){
    
    console.log("Subscription",this.accountData);
    const personalDataForm = this.signupForm.get('tenantAccount')?.value;
    const newPersonalData: TenantAccount = {
      ...personalDataForm,
    };
    //this.outputSignup.emit(newTenantAccount);
    console.log(newPersonalData);  
  }

}
