import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SignupPageComponent } from './signup-page/signup-page.component';
import { SignupComponent } from './signup.component';

const routes: Routes = [
  {
    path: '',
    component: SignupComponent,
    children: [
      {
        path: '',
        component: SignupPageComponent,
        // canActivate: [RouteGuardService],
        // data: <ISSORouteGuardData>{
        //   guard: {
        //     in: [ERoles.Lessor, ERoles.LessorMod],
        //   },
        //   visitorRedirect: {
        //     enabled: true,
        //   },
        // },
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SignupRoutingModule {}
