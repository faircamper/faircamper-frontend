import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SignupPageComponent } from './signup-page/signup-page.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { FcLibModule } from 'faircamper-lib/projects/fc-lib/src/lib/fc-lib.module';
import { SSOModule } from '@wobi/sso';
import { SignupRoutingModule } from './signup-routing.module';
import { SignupComponent } from './signup.component';
import { LessorFormComponent } from './forms/lessor-form/lessor-form.component';
import { TenantFormComponent } from './forms/tenant-form/tenant-form.component';


@NgModule({
  declarations: [
    SignupComponent,
    SignupPageComponent,
    LessorFormComponent,
    TenantFormComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    SignupRoutingModule,
    RouterModule,
    FcLibModule,
    SSOModule,
  ],
  exports: [
    TenantFormComponent,
  ]
})
export class SignupModule { }
