import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { EMPTY, Subscription } from 'rxjs';
import { TenantAccount } from 'src/app/tenant/models/Account/tenant';
import { TenantAccountService } from 'src/app/tenant/services/http/account/tenant-account.service';

@Component({
  selector: 'app-signup-page',
  templateUrl: './signup-page.component.html',
  styleUrls: ['./signup-page.component.css'],
})
export class SignupPageComponent implements OnInit, OnDestroy {
  showTenantForm = false;
  showLessorForm = false;
  showForm = false;
  tenantAccountServiceSub: Subscription = EMPTY.subscribe();

  constructor(protected tenantAccountService: TenantAccountService) {}

  ngOnInit(): void {}

  ngOnDestroy(): void {
    this.tenantAccountServiceSub.unsubscribe();
  }

  showChoosenForm(choosenForm = ''): void {
    if (choosenForm == 'lessor') {
      this.showLessorForm = !this.showLessorForm;
      this.showForm = !this.showForm;
    } else if (choosenForm == 'tenant') {
      this.showTenantForm = !this.showTenantForm;
      this.showForm = !this.showForm;
    }
  }

  onTenantSignup(account: TenantAccount): void {
    this.tenantAccountServiceSub = this.tenantAccountService
      .create<TenantAccount>(account)
      .subscribe(() => {});
  }
}
