import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';

import signupTenantAccountJson from './mock-data/signupTenant_GET.json';

import {
  InMemoryDbService,
  ParsedRequestUrl,
  RequestInfo,
  RequestInfoUtilities,
  ResponseOptions,
  STATUS,
} from 'angular-in-memory-web-api';

enum Collections {
  Account,
}
@Injectable({
  providedIn: 'root',
})
export class FakeSignupTenantApiService {
  enabledUrls: number[] = [Collections.Account];

  createDb() {
    let db = {} as any;
    if (environment.karma) {
      db = this.createTestDB();
    } else {
      db = this.createDevDB();
    }
    return { ...db };
  }

  createTestDB() {
    let db = {} as any;
    if (this.enabledUrls.find((e) => e === Collections.Account) !== undefined) {
      db.signupTenantAccount = signupTenantAccountJson;
    }
    return db;
  }
  createDevDB() {
    let db = {} as any;
    db.signupTenantAccount = signupTenantAccountJson;
    return db;
  }
  parseRequestUrl(url: string, utils: RequestInfoUtilities): string {
    const newUrl = url.replace(/\/signup\//, '/signupTenantAccount');
    return newUrl;
  }
}
