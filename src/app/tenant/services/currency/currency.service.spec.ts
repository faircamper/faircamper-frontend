import { TestBed } from '@angular/core/testing';

import { CurrencyService } from './currency.service';

describe('CurrencyService', () => {
	let service: CurrencyService;

	beforeEach(() => {
		TestBed.configureTestingModule({});
		service = TestBed.inject(CurrencyService);
	});

	describe('toCent()', () => {
		it('service should be created', () => {
			expect(service).toBeTruthy();
		});

		it('should trim spaces and transform " 10" to 1000', () => {
			const value = service.toCent(' 10');
			expect(value).toBe(1000);
		});

		it('should trim spaces and transform " 100 " to 1000', () => {
			const value = service.toCent(' 10 ');
			expect(value).toBe(1000);
		});

		it('should trim spaces and transform "10 " to 1000', () => {
			const value = service.toCent('10 ');
			expect(value).toBe(1000);
		});

		it('should transform 100 to 10000', () => {
			const value = service.toCent('100');
			expect(value).toBe(10000);
		});

		it('should transform €100.0 to 10000', () => {
			const value = service.toCent('€100.0');
			expect(value).toBe(10000);
		});

		it('should transform €100,0 to 10000', () => {
			const value = service.toCent('€100,0');
			expect(value).toBe(10000);
		});

		it('should transform €100,00 to 10000', () => {
			const value = service.toCent('€100,00');
			expect(value).toBe(10000);
		});
		it('should transform €100.00 to 10000', () => {
			const value = service.toCent('€100.00');
			expect(value).toBe(10000);
		});
		it('should transform €100.000,00 to 10000000', () => {
			const value = service.toCent('€100.000,00');
			expect(value).toBe(10000000);
		});
		it('should transform €100,000.00 to 10000000', () => {
			const value = service.toCent('€100,000.00');
			expect(value).toBe(10000000);
		});
		it('should transform €100 000,00 to 10000000', () => {
			const value = service.toCent('€100 000,00');
			expect(value).toBe(10000000);
		});
		it('should transform €1.000,5 to 100050', () => {
			const value = service.toCent('€1.000,5');
			expect(value).toBe(100050);
		});
		it('should transform 1.000,50€ to 100050', () => {
			const value = service.toCent('1.000,50€');
			expect(value).toBe(100050);
		});
		// Morocco special case
		it('should transform the morocco special case 1,234.56 .د.م. to 123456', () => {
			const value = service.toCent('1,234.56 .د.م.');
			expect(value).toBe(123456);
		});
	});

	//
	//
	//

	describe('toEuro()', () => {
		it('service should be created', () => {
			expect(service).toBeTruthy();
		});

		it('should transform 100 to 10', () => {
			const value = service.toEuro(1000);
			expect(value).toBe(10);
		});

		it('should transform 10044 to 100.44', () => {
			const value = service.toEuro(10044);
			expect(value).toBe(100.44);
		});

		it('should transform 104 to 1.04', () => {
			const value = service.toEuro(104);
			expect(value).toBe(1.04);
		});

		it('should transform 7 to 0.07', () => {
			const value = service.toEuro(7);
			expect(value).toBe(0.07);
		});

		it('should transform 17 to 0.17', () => {
			const value = service.toEuro(17);
			expect(value).toBe(0.17);
		});

		it('should transform 33333 to 333.33', () => {
			const value = service.toEuro(33333);
			expect(value).toBe(333.33);
		});

		it('should transform 20059605932 to 200596059.32', () => {
			const value = service.toEuro(20059605932);
			expect(value).toBe(200596059.32);
		});
	});

	//
	//
	//

	describe('cleanCurrencyFormat()', () => {
		it('service should be created', () => {
			expect(service).toBeTruthy();
		});

		it('should transform 100 to 100', () => {
			const value = service.cleanCurrencyFormat('100');
			expect(value).toBe(100);
		});

		it('should transform €1000,00 to 1000', () => {
			const value = service.cleanCurrencyFormat('€1000,00');
			expect(value).toBe(1000);
		});

		it('should transform €1000.00 to 1000', () => {
			const value = service.cleanCurrencyFormat('€1000.00');
			expect(value).toBe(1000);
		});

		it('should transform $1000.00 to 1000', () => {
			const value = service.cleanCurrencyFormat('$1000.00');
			expect(value).toBe(1000);
		});

		it('should transform $1000.0 to 1000', () => {
			const value = service.cleanCurrencyFormat('$1000.0');
			expect(value).toBe(1000);
		});

		it('should transform €100.000,00 to 100000', () => {
			const value = service.cleanCurrencyFormat('€100.000,00');
			expect(value).toBe(100000);
		});
		it('should transform €100,000.00 to 100000', () => {
			const value = service.cleanCurrencyFormat('€100,000.00');
			expect(value).toBe(100000);
		});
		it('should transform €100 000,00 to 100000', () => {
			const value = service.cleanCurrencyFormat('€100 000,00');
			expect(value).toBe(100000);
		});
		it('should transform €1.000,5 to 1000.5', () => {
			const value = service.cleanCurrencyFormat('€1.000,5');
			expect(value).toBe(1000.5);
		});
		it('should transform 1.000,50€ to 1000.5', () => {
			const value = service.cleanCurrencyFormat('1.000,50€');
			expect(value).toBe(1000.5);
		});

		it('should trim spaces and transform " 10" to 10', () => {
			const value = service.cleanCurrencyFormat(' 10');
			expect(value).toBe(10);
		});

		it('should trim spaces and transform " 100 " to 10', () => {
			const value = service.cleanCurrencyFormat(' 10 ');
			expect(value).toBe(10);
		});

		it('should trim spaces and transform "10 " to 10', () => {
			const value = service.cleanCurrencyFormat('10 ');
			expect(value).toBe(10);
		});

		// Morocco special case
		it('should transform the morocco special case 1,234.56 .د.م. to 1234.56', () => {
			const value = service.cleanCurrencyFormat('1,234.56 .د.م.');
			expect(value).toBe(1234.56);
		});
	});
});
