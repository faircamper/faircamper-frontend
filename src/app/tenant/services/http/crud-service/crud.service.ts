import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable, ObservableInput, throwError } from 'rxjs';
import { catchError, map, retry, timeout } from 'rxjs/operators';
import { MessagesService } from 'src/app/tenant/services/message/messages.service';
import { environment } from 'src/environments/environment';

const baseURL = environment.apiURL;
// const headers = new HttpHeaders({ 'Content-Type': 'application/json; charset=utf-8', 'Accept-Language': 'en' });
const headers = new HttpHeaders({
  'Content-Type': 'application/json; charset=utf-8',
  'Accept-Language': 'de',
});
// const headers = new HttpHeaders({ 'Content-Type': 'application/json; charset=utf-8' });
@Injectable({
  providedIn: 'root',
})
export abstract class CrudService {
  constructor(
    protected httpClient: HttpClient,
    @Inject(String) protected entityURL: string,
    protected messages: MessagesService
  ) {}

  get<T>(id?: number | string | undefined): Observable<T> {
    let url = baseURL + this.entityURL;
    console.log(url);
    if (this.entityURL !== 'lessors') {
      if (id) {
        url += '/' + id;
      } else {
        this.messages.send({
          text: ' No id set on GET method for ' + this.entityURL,
        });
      }
    }
    return this.httpClient.get<ItemResponse<T>>(url, { headers }).pipe(
      timeout(environment.timeout),
      retry(environment.retries),
      catchError(handleError(this.messages)),
      map((response) => {
        return response.data;
      })
    );
  }

  list<T>(): Observable<T[]> {
    return this.httpClient
      .get<ListResponse<T>>(baseURL + this.entityURL, { headers })
      .pipe(
        timeout(environment.timeout),
        retry(environment.retries),
        catchError(handleError(this.messages)),
        map((response) => response.data)
      );
  }

  create<T>(obj: T): Observable<T> {
    return this.httpClient
      .post<ItemResponse<T>>(baseURL + this.entityURL, obj, { headers })
      .pipe(
        timeout(environment.timeout),
        retry(environment.retries),
        catchError(handleError(this.messages)),
        map((response) => response.data)
      );
  }

  update<T extends { id?: number }>(obj: T): Observable<T> {
    if (this.entityURL !== 'lessors') {
      if (!obj.id) {
        this.messages.send({
          text: ' No id set on PUT method for ' + this.entityURL,
        });
      }
    }
    var idStr = obj.id ? '/' + obj.id : '';
    return this.httpClient
      .put<ItemResponse<T>>(baseURL + this.entityURL + idStr, obj, { headers })
      .pipe(
        timeout(environment.timeout),
        retry(environment.retries),
        catchError(handleError(this.messages)),
        map((response) => response.data)
      );
  }

  delete<T extends { id?: number }>(obj: T): any {
    if (this.entityURL !== 'lessors') {
      if (!obj.id) {
        this.messages.send({
          text: ' No id set on PUT method for ' + this.entityURL,
        });
      }
    }
    var idStr = obj.id ? '/' + obj.id : '';
    return this.httpClient
      .delete<ItemResponse<T>>(baseURL + this.entityURL + idStr, { headers })
      .pipe(
        timeout(environment.timeout),
        retry(environment.retries),
        catchError(handleError(this.messages)),
        map((response) => response.data)
      );
  }
}

function handleError(msg: MessagesService) {
  return <T>(
    err: any,
    caught: Observable<ItemResponse<T>>
  ): ObservableInput<any> => {
    msg.send({ errorCode: err.status, text: 'something went wrong' });
    return throwError(err);
  };
}

interface ItemResponse<T> {
  errors: any | null;
  events: any | null;
  data: T;
}

interface ListResponse<T> {
  errors: any | null;
  events: any | null;
  data: T[];
}
