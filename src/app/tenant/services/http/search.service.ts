import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable, ObservableInput, throwError } from 'rxjs';
import { catchError, filter, map, retry, timeout } from 'rxjs/operators';
import { MessagesService } from 'src/app/tenant/services/message/messages.service';
import { environment } from 'src/environments/environment';
import { Vehicle, VehicleSearchObject } from '../../models/vehicle/vehicle';

const baseURL = environment.apiURL;
// const headers = new HttpHeaders({ 'Content-Type': 'application/json; charset=utf-8', 'Accept-Language': 'en' });
const headers = new HttpHeaders({
  'Content-Type': 'application/json; charset=utf-8',
  'Accept-Language': 'de',
});

@Injectable({
  providedIn: 'root',
})
export class SearchService {
  constructor(
    private httpClient: HttpClient,
    protected messages: MessagesService
  ) {}

  search(filters: string): Observable<VehicleSearchObject[]> {
    return this.httpClient
      .get<ListResponse<VehicleSearchObject>>(baseURL + 'search?' + filters, {
        headers,
      })
      .pipe(
        timeout(environment.timeout),
        retry(environment.retries),
        catchError(handleError(this.messages)),
        map((response) => response.data)
      );
  }
}

function handleError(msg: MessagesService) {
  return <T>(
    err: any,
    caught: Observable<ItemResponse<T>>
  ): ObservableInput<any> => {
    msg.send({ errorCode: err.status, text: 'something went wrong' });
    return throwError(err);
  };
}

interface ItemResponse<T> {
  errors: any | null;
  events: any | null;
  data: T;
}

interface ListResponse<T> {
  errors: any | null;
  events: any | null;
  data: T[];
}
