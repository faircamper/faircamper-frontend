import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CrudService } from 'src/app/lessor/services/http/crud-service/crud.service';
import { MessagesService } from '../../message/messages.service';

@Injectable({
  providedIn: 'root',
})
export class BookingService extends CrudService {
  constructor(httpClient: HttpClient, messagesService: MessagesService) {
    super(httpClient, 'bookings', messagesService);
  }
}
