import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, ObservableInput, of, throwError } from 'rxjs';
import { timeout, retry, catchError, map } from 'rxjs/operators';
import { Availability } from 'src/app/tenant/models/availability/availability';
import { TimePeriod } from 'src/app/tenant/models/availability/timeperiod';
import { MessagesService } from 'src/app/tenant/services/message/messages.service';
import { environment } from 'src/environments/environment';
import { CrudService } from '../crud-service/crud.service';

const baseURL = environment.apiURL;
const headers = new HttpHeaders({
  'Content-Type': 'application/json; charset=utf-8',
  'Accept-Language': 'de',
});

@Injectable({
  providedIn: 'root',
})
export class AvailabilityService extends CrudService {
  timeperiod: null | TimePeriod[] = null;
  constructor(
    override httpClient: HttpClient,
    override messages: MessagesService
  ) {
    super(httpClient, 'availability', messages);
  }

  listAvailability(id: any, filter: string): Observable<Availability[]> {
    const url = baseURL + 'public/vehicle/' + id + '/availability?' + filter;
    return this.httpClient
      .get<ListResponse<Availability>>(url, { headers })
      .pipe(
        timeout(environment.timeout),
        retry(environment.retries),
        catchError(handleError(this.messages)),
        map((response) => {
          return response.data;
        })
      );
  }

  timePeriod(id: any, fr: any, to: any): Observable<TimePeriod[]> {
    // const url = baseURL + 'vehicle/'+id+'/price?fr='+fr+'&to='+to;
    const url =
      baseURL +
      'vehicle/49ad023c-d3e3-4043-9e3e-c1f50056a140/price?fr=' +
      fr +
      '&to=' +
      to;

    return this.httpClient.get<ListResponse<TimePeriod>>(url, { headers }).pipe(
      timeout(environment.timeout),
      retry(environment.retries),
      catchError(handleError(this.messages)),
      map((response) => {
        return response;
      })
    );
  }
}
interface ListResponse<T> {
  errors: any | null;
  events: any | null;
  data: T[];
}

function handleError(msg: MessagesService) {
  return <T>(
    err: any,
    caught: Observable<ItemResponse<T>>
  ): ObservableInput<any> => {
    msg.send({ errorCode: err.status, text: 'something went wrong' });
    return throwError(err);
  };
}

interface ItemResponse<T> {
  errors: any | null;
  events: any | null;
  data: T;
}
