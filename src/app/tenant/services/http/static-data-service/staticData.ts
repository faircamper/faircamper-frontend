export interface StaticData {
  vehicleModels: IdValue[];
  fuelValues: IdValue[];
  consumptions: IdValue[];
  vehicleTypes: IdValue[];
  bedTypes: IdValue[];
  gears: IdValue[];
  weights: IdValue[];
  equipmentPriceTypes: IdValue[];
}

export interface IdValue {
  id: number;
  name: string;
}
