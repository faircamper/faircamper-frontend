import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, ObservableInput, of, throwError } from 'rxjs';
import { timeout, retry, catchError, map } from 'rxjs/operators';
import {
  Equipment,
  EquipmentCategory,
  EquipmentSubCategory,
} from 'src/app/tenant/models/equipment/equipment';
import { MessagesService } from 'src/app/tenant/services/message/messages.service';
import { environment } from 'src/environments/environment';
import { CrudService } from '../crud-service/crud.service';

const baseURL = environment.apiURL;
// const headers = new HttpHeaders({ 'Content-Type': 'application/json; charset=utf-8', 'Accept-Language': 'en' });
const headers = new HttpHeaders({
  'Content-Type': 'application/json; charset=utf-8',
  'Accept-Language': 'de',
});
// const headers = new HttpHeaders({ 'Content-Type': 'application/json; charset=utf-8' });

@Injectable({
  providedIn: 'root',
})
export class EquipmentService extends CrudService {
  equipments: null | Equipment[] = null;
  equipmentCategories: null | EquipmentCategory[] = null;
  equipmentSubCategories: null | Map<number, EquipmentSubCategory> = null;

  constructor(
    override httpClient: HttpClient,
    override messages: MessagesService
  ) {
    super(httpClient, 'equipment', messages);
  }

  listEquipment(
    filter?: 'category' | 'subCategory' | 'station',
    filterValue?: number
  ): Observable<Equipment[]> {
    return this.httpClient
      .get<ListResponse<Equipment>>(baseURL + this.entityURL, { headers })
      .pipe(
        timeout(environment.timeout),
        retry(environment.retries),
        catchError(handleError(this.messages)),
        map((response) => {
          if (filter && filterValue) {
            if (filter === 'category') {
              return (response.data as Equipment[]).filter(
                (e) => e.categoryId === filterValue
              );
            }
            if (filter === 'subCategory') {
              return (response.data as Equipment[]).filter(
                (e) => e.subCategoryId === filterValue
              );
            }
            if (filter === 'station') {
              return (response.data as Equipment[]).filter(
                (e) => e.stationId === filterValue
              );
            }
          }
          return response.data;
        })
      );
  }

  listEquipmentCategories(): Observable<EquipmentCategory[]> {
    const url = baseURL + 'equipment/categories';
    if (this.equipmentCategories === null) {
      return this.httpClient
        .get<ListResponse<EquipmentCategory>>(url, { headers })
        .pipe(
          timeout(environment.timeout),
          retry(environment.retries),
          catchError(handleError(this.messages)),
          map((response) => {
            this.equipmentCategories = response.data;
            this.createSubCatArray(response.data);
            return response.data;
          })
        );
    } else {
      return of(this.equipmentCategories);
    }
  }

  createSubCatArray(equipmentCategories: EquipmentCategory[]): void {
    this.equipmentSubCategories = new Map();
    for (const cat of equipmentCategories) {
      for (const sub of cat.children) {
        this.equipmentSubCategories.set(sub.id, sub);
      }
    }
  }

  getSubCategoryById(id: number): EquipmentSubCategory | undefined {
    return this.equipmentSubCategories?.get(id);
  }
  getCategoryById(id: number): EquipmentCategory | undefined {
    return this.equipmentCategories?.find((e) => e.id === id);
  }
}

interface ListResponse<T> {
  errors: any | null;
  events: any | null;
  data: T[];
}

function handleError(msg: MessagesService) {
  return <T>(
    err: any,
    caught: Observable<ItemResponse<T>>
  ): ObservableInput<any> => {
    msg.send({ errorCode: err.status, text: 'something went wrong' });
    return throwError(err);
  };
}

interface ItemResponse<T> {
  errors: any | null;
  events: any | null;
  data: T;
}
