import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { VehicleDetails } from '../../models/vehicle/publicVehicleDetails';
import { environment } from 'src/environments/environment';

const baseURL = environment.apiURL;

@Injectable({
  providedIn: 'root'
})
export class PublicVehicleDetailService {


  constructor(private http: HttpClient) {
  }

  public getPublicVehicleDetails(id: string): Observable<VehicleDetails> {

    return this.http.get<VehicleDetails>(baseURL + "public/vehicle/" + id);
  }

}
