import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { EMPTY, Subscription } from 'rxjs';
import { TenantAccountService } from './fake-api/account/account/tenant-account.service';
import { TenantAccount } from './models/Account/tenant';
import { Message, MessagesService } from './services/message/messages.service';
import { ERoles, IRoleRequirements } from '@wobi/sso-state';
import { SSOService } from '@wobi/sso';

@Component({
  selector: 'app-tenant',
  templateUrl: './tenant.component.html',
  styleUrls: ['./tenant.component.css'],
})
export class TenantComponent implements OnInit {
  public loginRequirements: IRoleRequirements = {
    onlyVisitors: true,
  };

  public logoutRequirements: IRoleRequirements = {
    in: [ERoles.Pending, ERoles.Registered],
  };

  tenantAccountData: TenantAccount | null = null;
  accountSubscription: Subscription = EMPTY.subscribe();

  isOpen = true;
  expanded = false;
  msgSubscription: Subscription = EMPTY.subscribe();
  url = '';
  locale = 'de';
  constructor(
    private messages: MessagesService,
    private router: Router,
    private ssoSvc: SSOService,
    public accountService: TenantAccountService,
    @Inject(LOCALE_ID) locale: string
  ) {
    this.locale = locale;
    router.events.subscribe((data) => {
      if (data instanceof NavigationEnd) {
        this.url = data.url;
      }
    });
  }

  ngOnInit(): void {
    this.accountSubscription = this.accountService
      .get<TenantAccount>()
      .subscribe((data) => {
        this.tenantAccountData = data;
      });

    this.msgSubscription = this.messages.getMessages().subscribe((msg) => {
      this.displayMessage(msg);
    });

    this.url = this.router.url;
  }
  /**
    displayMessage
  **/
  public displayMessage(msg: Message): void {
    const errCode = msg.errorCode === undefined ? '' : msg.errorCode;
    // Open Message System here
  }

  ngOnDestroy(): void {
    this.msgSubscription.unsubscribe();
  }

  isUrl(url: string): boolean {
    return this.url === url;
  }

  doLogin() {
    this.ssoSvc.login(`/${this.locale}/`);
  }

  doLogout() {
    this.ssoSvc.logout();
  }
}
