import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RouteGuardService } from '@wobi/sso';
import { ERoles, ISSORouteGuardData } from '@wobi/sso-state';
import { AccountPageTenantComponent } from './components/account/pages/account-page-tenant/account-page-tenant.component';
import { CheckoutPageComponent } from './components/checkout/pages/checkout-page/checkout-page.component';
import { VehicleDetailPageComponent } from './components/vehicle/pages/vehicle-detail-page/vehicle-detail-page.component';
import { VehicleListComponent } from './components/vehicle/pages/vehicle-list/vehicle-list.component';
import { TenantComponent } from './tenant.component';

const routes: Routes = [
  {
    path: '',
    component: TenantComponent,
    children: [
      {
        path: '',
        component: VehicleListComponent,
      },
      {
        path: ':id',
        component: VehicleDetailPageComponent,
      },
      {
        path: 'tenant/account',
        component: AccountPageTenantComponent,
        // canActivate: [RouteGuardService],
        // data: <ISSORouteGuardData>{
        //     guard: {
        //         in: [ERoles.Tenant, ERoles.Registered],
        //     },
        //     visitorRedirect: {
        //       enabled: true,
        //     }
        // },
      },
      {
        path: 'checkout/:id',
        component: CheckoutPageComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TenantRoutingModule {}
