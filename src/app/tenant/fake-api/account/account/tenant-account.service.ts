import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CrudService } from '../../../../tenant/services/http/crud-service/crud.service';
import { MessagesService } from '../../../../tenant/services/message/messages.service';

@Injectable({
  providedIn: 'root',
})
export class TenantAccountService extends CrudService {
  constructor(
    override httpClient: HttpClient,
    override messages: MessagesService
  ) {
    super(httpClient, 'account/tenant', messages);
  }
}
