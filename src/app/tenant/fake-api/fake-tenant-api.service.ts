import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';

import tenantAccountJson from './mock-data/tenantAccount_GET.json';
import availabilityJson from './mock-data/availability_GET.json';
import bookingJson from './mock-data/booking_GET.json';

import { RequestInfoUtilities } from 'angular-in-memory-web-api';

enum Collections {
  Account,
  Availability,
  Booking,
}
@Injectable({
  providedIn: 'root',
})
export class FakeTenantApiService {
  enabledUrls: number[] = [
    Collections.Account,
    Collections.Availability,
    Collections.Booking,
  ];

  createDb() {
    let db = {} as any;
    if (environment.karma) {
      db = this.createTestDB();
    } else {
      db = this.createDevDB();
    }
    return { ...db };
  }

  createDevDB() {
    let db = {} as any;
    if (this.enabledUrls.find((e) => e === Collections.Account) !== undefined) {
      db.tenantAccount = tenantAccountJson;
    }
    if (
      this.enabledUrls.find((e) => e === Collections.Availability) !== undefined
    ) {
      db.availability = availabilityJson;
    }
    if (this.enabledUrls.find((e) => e === Collections.Booking) !== undefined) {
      db.bookings = bookingJson;
    }
    return db;
  }
  createTestDB() {
    let db = {} as any;
    db.tenantAccount = tenantAccountJson;
    db.availability = availabilityJson;
    db.bookings = bookingJson;
    return db;
  }
  parseRequestUrl(url: string, utils: RequestInfoUtilities): string {
    console.log(url);
    let newUrl = url.replace(/\/account\/tenant/, '/tenantAccount');
    newUrl = url.replace(
      /\/public\/vehicle\/.*\/availability/,
      '/availability/'
    );
    return newUrl;
  }
}
