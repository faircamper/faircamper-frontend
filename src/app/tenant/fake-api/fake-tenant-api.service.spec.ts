import { TestBed } from '@angular/core/testing';
import { FakeTenantApiService } from './fake-tenant-api.service';


describe('FakeApiService', () => {
  let service: FakeTenantApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FakeTenantApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
