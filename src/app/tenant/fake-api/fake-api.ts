import { Injectable } from '@angular/core';
import {
  InMemoryDbService,
  ParsedRequestUrl,
  RequestInfoUtilities,
} from 'angular-in-memory-web-api';

@Injectable({
  providedIn: 'root',
})
export class FakeApi implements InMemoryDbService {
  // parseRequestUrl override
  // Do this to manipulate the request URL or the parsed result
  // into something your data store can handle.
  // This example turns a request for `/foo/heroes` into just `/heroes`.
  // It leaves other URLs untouched and forwards to the default parser.
  // It also logs the result of the default parser.
  parseRequestUrl(url: string, utils: RequestInfoUtilities): ParsedRequestUrl {
    console.log('Replacing ' + url);

    let newUrl = url.replace('api/equipment/categories', 'api/categories');
    newUrl = newUrl.replace('api/static', 'api/staatic');
    newUrl = newUrl.replace(
      'api/search?long=54.1231&lat=10.56&p=0',
      'api/search'
    );
    newUrl = newUrl.replace(
      'api/public/vehicle/abc4077b-3a4e-4a61-bb45-85bc8fc1af62/availability',
      'api/availability'
    );
    newUrl = newUrl.replace(
      'api/public/vehicle/abc4077b-3a4e-4a61-bb45-85bc8fc1af62',
      'api/vehicle'
    );
    newUrl = newUrl.replace(
      '/api/vehicle/49ad023c-d3e3-4043-9e3e-c1f50056a140/price?fr=2022-02-21&to=2022-02-28',
      'api/falseSmallBusiness'
    );
    newUrl = newUrl.replace(
      '/api/vehicle/49ad023c-d3e3-4043-9e3e-c1f50056a140/price?fr=2022-03-20&to=2022-03-27',
      'api/trueSmallBusiness'
    );
    newUrl = newUrl.replace(
      '/api/vehicle/49ad023c-d3e3-4043-9e3e-c1f50056a140/price?fr=2022-03-22&to=2022-03-29',
      'api/bookingDurationTooLow'
    );

    return utils.parseRequestUrl(newUrl);
  }

  createDb() {
    // {base}/categories
    const categories = categoriesData;
    const staatic = staticData;
    const search = searchData;
    const vehicle = vehicleData;
    const availability = availabilityData;
    const falseSmallBusiness = falseSmallBusinessData;
    const trueSmallBusiness = trueSmallBusinessData;
    const bookingDurationTooLow = bookingDurationTooLowData;

    return {
      categories,
      staatic,
      search,
      vehicle,
      availability,
      falseSmallBusiness,
      trueSmallBusiness,
      bookingDurationTooLow,
    };
  }
}

const bookingDurationTooLowData = {
  vehicleId: 'abc4077b-3a4e-4a61-bb45-85bc8fc1af62',
  aggregatedPrice: 592700,
  meanPrice: 8300,
  currency: '€',
  fee: 25600,
  isSmallBusiness: true,
  bookingDurationTooLow: true,
};

const trueSmallBusinessData = {
  vehicleId: 'abc4077b-3a4e-4a61-bb45-85bc8fc1af62',
  aggregatedPrice: 592700,
  meanPrice: 8300,
  currency: '€',
  fee: 25600,
  isSmallBusiness: true,
  bookingDurationTooLow: false,
};

const falseSmallBusinessData = {
  vehicleId: 'abc4077b-3a4e-4a61-bb45-85bc8fc1af62',
  aggregatedPrice: 134500,
  meanPrice: 7200,
  currency: '€',
  fee: 13500,
  isSmallBusiness: false,
  bookingDurationTooLow: false,
};

const availabilityData = {
  unavailabilities: [
    {
      from: '2022-02-05',
      to: '2022-02-10',
    },
    {
      from: '2022-02-12',
      to: '2022-02-13',
    },
    {
      from: '2022-03-05',
      to: '2022-03-10',
    },
    {
      from: '2022-03-16',
      to: '2022-03-18',
    },
  ],
  minBookingDuration: 7,
};

const vehicleData = {
  errors: null,
  events: null,
  data: {
    id: 'abc4077b-3a4e-4a61-bb45-85bc8fc1af62',
    lessorId: 42,
    minPrice: 9367,
    stationId: '483fb586-7fcf-4bd6-9501-fb1f5fe57a38',
    seasonCollectionId: 14,
    integratedEquipmentIds: [
      164, 14, 11, 21, 173, 97, 157, 50, 74, 40, 146, 9, 70, 55, 184, 75, 46,
      159, 177, 35, 59, 161, 114, 54, 2, 88, 127, 91, 103, 10, 26, 73, 176, 83,
      84, 181, 119, 155, 180, 86, 118, 139, 102, 129, 122, 30, 175, 31, 49, 134,
      147, 79, 93, 178, 165, 28, 7, 128, 145, 172, 57, 185, 183, 95, 131, 138,
      174, 19, 13, 23,
    ],
    optionalEquipmentIds: [
      '9d3c3ef2-42ff-4a41-b17c-7ff1bcddecfa',
      '6c3b020b-5aea-40d7-b5af-d4630829f593',
      'f4fc9107-88f2-4f80-9419-57a2eecf2ca3',
      '9777c742-073d-43b1-9150-d407452f7b0b',
      '9e7a8109-0dfd-4c17-993f-a06c20f27600',
      '1647d42f-0f4f-4e82-a7ba-73100ca0da65',
      '4f7e34ef-a6d6-4b43-92c4-c1a0f1230974',
      'f2dd83af-f61b-4bbe-8174-c840b896682a',
      'c1cda815-f209-427f-aba0-6e186f1acf9a',
      'ddab35eb-283a-4ff3-9edf-53cde65f700d',
      'de51f6d8-9fb8-4692-b1c9-f83f44fc585e',
      '39b6af19-6ef2-462c-832c-2203147487ba',
      'c77ad580-63e4-4fb6-86de-fd64d174333c',
      'bf788d88-9526-4dde-9ed5-811d0b3d8438',
      '935a9671-e8dc-4afe-81a9-2bbf79125dea',
      '551ede5b-452b-4ef7-a2c7-e35bc8e96bbe',
      '8ff603ec-abcd-4ce2-9ba4-d9ccb0e9f116',
      'b525506f-7b13-471f-b20b-8123be655f3e',
      '720868e0-4320-4de2-8c2d-a381549ffd85',
      '14ed220d-e202-437f-8860-d316cf8ec3ff',
    ],
    prices: [
      {
        priceLevelId: 1,
        amount: 6650,
        currency: 'EUR',
        minBookingDuration: 4,
      },
      {
        priceLevelId: 2,
        amount: 10279,
        currency: 'EUR',
        minBookingDuration: 2,
      },
      {
        priceLevelId: 3,
        amount: 11038,
        currency: 'EUR',
        minBookingDuration: 2,
      },
      {
        priceLevelId: 4,
        amount: 7824,
        currency: 'EUR',
        minBookingDuration: 4,
      },
    ],
    images: [
      {
        url: 'http://test.faircamperintern.de/api/images/3eWKVPCSOaE33zd9TrucogQ8s1TJahhjRkpAMg5qOnY=',
      },
      {
        url: 'http://test.faircamperintern.de/api/images/iyeH-vzdw6WNq81qgses2okBa106zsKJq52qYl-eA94=',
      },
      {
        url: 'http://test.faircamperintern.de/api/images/lRm74mYopYmY13bQjccYPwBR4SIJYiY8cwEprXt4r1A=',
      },
      {
        url: 'http://test.faircamperintern.de/api/images/zW-qsmIsyMd5FLbOojVoXYIW-IAzh1ONvrvrLJo3tTo=',
      },
      {
        url: 'http://test.faircamperintern.de/api/images/OUhiFbYrBfeIy4fVxR8G6dVeY0aQ_hMoyaytQnl0ucE=',
      },
      {
        url: 'http://test.faircamperintern.de/api/images/1PsTrDRTwOHmoQJ3_8CekA6U93EeAziYIxWYFvV-doE=',
      },
      {
        url: 'http://test.faircamperintern.de/api/images/7iMb3jx0WwV6_i0P4mvsm9og-6vxEXW9SwDhJAmHsqA=',
      },
      {
        url: 'http://test.faircamperintern.de/api/images/32m2cT_giIY6B7To1wMSEQ8Vw8EVDPWERy823zctZ5w=',
      },
    ],
    mileage: {
      costPerKm: 86,
      isFree: false,
      freeAfterRentalDays: 82,
      customPackages: [
        {
          km: 2500,
          discount: 2,
        },
      ],
      predefinedPackages: [
        {
          km: 500,
          discount: 5,
          id: 1,
          isSelected: true,
        },
        {
          km: 1000,
          discount: 10,
          id: 2,
          isSelected: true,
        },
        {
          km: 1500,
          discount: 15,
          id: 3,
          isSelected: false,
        },
      ],
    },
    rentalTerms: {
      minDriverAge: 26,
      drivingLicensePossession: 1,
      additionalDriversAllowed: 'allowedHaveToBeNamed',
      chargeForExtraDriver: false,
      feePerDriver: 7646,
      deductiblePartialCover: 8222,
      deductibleComprehensiveInsurance: 7485,
      europeWideProtectionLetter: false,
      depositAmount: 5964,
      payInAdvanceViaBankTransfer: true,
      daysInAdvance: 6,
      depositReduction: false,
      payOnSiteVia: ['ec', 'visa', 'mastercard'],
      smallAnimalsAllowed: true,
      smallAnimalPrice: 12,
      smallAnimalsPriceModell: 'perBooking',
      numberOfSmallAnimals: 4,
      smokingAllowed: false,
      excludeCountries: ['Norwegen'],
      includeCountries: ['Monaco'],
      region: ['euCountries'],
    },
    title: 'Ergonomic 5thgeneration Graphic Interface-NL-U-594',
    description:
      'Iste beatae voluptas nisi excepturi cum. Molestiae labore at accusamus aspernatur. Dolorem voluptatem numquam nulla soluta.\nFuga quaerat ratione delectus quaerat. Exercitationem asperiores optio neque odit nisi.\nNeque nihil consequuntur reprehenderit officiis saepe. Quo architecto ipsam recusandae itaque eveniet praesentium quos.\nExercitationem similique nostrum at. Quae reprehenderit consectetur aspernatur optio. Sint nostrum similique.\nMagni temporibus qui fuga fugiat accusamus. Quidem quasi repellat exercitationem.\nQuam minima numquam soluta. Beatae doloribus eligendi incidunt. Aspernatur quia fugiat libero.\nIste pariatur atque voluptas. Eveniet quo minus dolores quidem. Commodi voluptates modi.\nUnde at atque velit. Sapiente quaerat quibusdam eos velit quos. Voluptatibus mollitia consectetur ipsam.\nPariatur sint sed quam occaecati cum earum dicta. Quia fugiat nihil est vero repellendus eaque. Facilis alias aliquid enim cumque laudantium.',
    externalId: 'NL-U-594',
    vehicleTypeId: 5,
    weightId: 1,
    manufacturer: 'GMC',
    modelId: 110,
    constructionYear: 1977,
    beds: [
      {
        designation: 1,
        length: 183,
        width: 136,
      },
      {
        designation: 1,
        length: 218,
        width: 107,
      },
      {
        designation: 1,
        length: 218,
        width: 100,
      },
      {
        designation: 1,
        length: 197,
        width: 138,
      },
      {
        designation: 1,
        length: 199,
        width: 121,
      },
      {
        designation: 1,
        length: 181,
        width: 122,
      },
      {
        designation: 1,
        length: 214,
        width: 106,
      },
      {
        designation: 1,
        length: 220,
        width: 134,
      },
    ],
    numSeatsOverall: 3,
    numSeats3Point: 3,
    numSeatsLap: 0,
    numSeatsIsofix: 0,
    numBedsOverall: 4,
    numBedsComfort: 4,
    numBedsComfortKids: 0,
    youtube: 'https://www.youtube.com/watch?v=MgyDZN8Mu8A',
    hasBestPrice: false,
    hasLiveCalendar: false,
    hasInstantBooking: true,
    power: 159,
    powerUnitId: 0,
    payload: 1382,
    fuelId: 4,
    consumptionId: 7,
    gearId: 0,
    length: 725,
    height: 352,
    width: 259,
    onlineStatusId: 1,
    wobiStatusId: 2,
    version: 1,
    station: {
      id: '483fb586-7fcf-4bd6-9501-fb1f5fe57a38',
      name: 'Austermühle GmbH & Co. KG - Stade',
      description: 'extend web-enabled functionalities',
      city: 'Stade',
      organization: 'Austermühle GmbH & Co. KG',
      optional: 'Proactive 3rdgeneration toolset',
      street: 'Mechthild-Jähn-Gasse 9/3',
      postalCode: '69056',
      country: 'Europe/Berlin',
      fee: 50,
      geoLocation: {
        longitude: 9.47629,
        latitude: 53.59337,
      },
      openingHours: [
        {
          handover: 'OPEN',
          checkin: {
            from: '09:00',
            to: '16:30',
          },
          checkout: {
            from: '10:00',
            to: '18:00',
          },
          isFee: false,
        },
        {
          handover: 'OPEN',
          checkin: {
            from: '10:00',
            to: '13:00',
          },
          checkout: {
            from: '07:30',
            to: '20:00',
          },
          isFee: false,
        },
        {
          handover: 'OPEN',
          checkin: {
            from: '07:30',
            to: '13:30',
          },
          checkout: {
            from: '08:30',
            to: '12:30',
          },
          isFee: false,
        },
        {
          handover: 'CONSULTATION',
          checkin: {
            from: '00:00',
            to: '00:00',
          },
          checkout: {
            from: '00:00',
            to: '00:00',
          },
          isFee: true,
        },
        {
          handover: 'CLOSED',
          checkin: {
            from: '00:00',
            to: '00:00',
          },
          checkout: {
            from: '00:00',
            to: '00:00',
          },
          isFee: false,
        },
        {
          handover: 'CLOSED',
          checkin: {
            from: '00:00',
            to: '00:00',
          },
          checkout: {
            from: '00:00',
            to: '00:00',
          },
          isFee: false,
        },
        {
          handover: 'OPEN',
          checkin: {
            from: '09:00',
            to: '15:00',
          },
          checkout: {
            from: '08:30',
            to: '15:00',
          },
          isFee: false,
        },
      ],
      equipment: null,
      bufferDays: 0,
      version: 1,
    },
    equipmentItems: [
      {
        id: '14ed220d-e202-437f-8860-d316cf8ec3ff',
        description: 'exploit frictionless e-business',
        stationId: '483fb586-7fcf-4bd6-9501-fb1f5fe57a38',
        stationName: 'Austermühle GmbH & Co. KG - Stade',
        categoryId: 1,
        subCategoryId: 4,
        price: 1328,
        priceTypeId: 1,
        maxPrice: 1643,
        version: 1,
        calculationType: 'perPiece',
      },
      {
        id: '720868e0-4320-4de2-8c2d-a381549ffd85',
        description: 'utilize distributed experiences',
        stationId: '483fb586-7fcf-4bd6-9501-fb1f5fe57a38',
        stationName: 'Austermühle GmbH & Co. KG - Stade',
        categoryId: 1,
        subCategoryId: 6,
        price: 702,
        priceTypeId: 1,
        maxPrice: 1025,
        version: 1,
        calculationType: 'perNight',
      },
      {
        id: 'bf788d88-9526-4dde-9ed5-811d0b3d8438',
        description: 'generate front-end ROI',
        stationId: '483fb586-7fcf-4bd6-9501-fb1f5fe57a38',
        stationName: 'Austermühle GmbH & Co. KG - Stade',
        categoryId: 1,
        subCategoryId: 15,
        price: 670,
        priceTypeId: 4,
        maxPrice: 1125,
        version: 1,
        calculationType: 'perPieceAndNight',
      },
      {
        id: '9d3c3ef2-42ff-4a41-b17c-7ff1bcddecfa',
        description: 'repurpose distributed interfaces',
        stationId: '483fb586-7fcf-4bd6-9501-fb1f5fe57a38',
        stationName: 'Austermühle GmbH & Co. KG - Stade',
        categoryId: 18,
        subCategoryId: 20,
        price: 256,
        priceTypeId: 2,
        maxPrice: 1296,
        version: 1,
        calculationType: 'perBooking',
      },
      {
        id: 'f2dd83af-f61b-4bbe-8174-c840b896682a',
        description: 'enhance impactful e-commerce',
        stationId: '483fb586-7fcf-4bd6-9501-fb1f5fe57a38',
        stationName: 'Austermühle GmbH & Co. KG - Stade',
        categoryId: 18,
        subCategoryId: 21,
        price: 10,
        priceTypeId: 2,
        maxPrice: 940,
        version: 1,
        calculationType: 'inclusive',
      },
      {
        id: '39b6af19-6ef2-462c-832c-2203147487ba',
        description: 'target world-class paradigms',
        stationId: '483fb586-7fcf-4bd6-9501-fb1f5fe57a38',
        stationName: 'Austermühle GmbH & Co. KG - Stade',
        categoryId: 18,
        subCategoryId: 25,
        price: 543,
        priceTypeId: 2,
        maxPrice: 1044,
        version: 1,
        calculationType: 'perPiece',
      },
      {
        id: '935a9671-e8dc-4afe-81a9-2bbf79125dea',
        description: 'architect world-class experiences',
        stationId: '483fb586-7fcf-4bd6-9501-fb1f5fe57a38',
        stationName: 'Austermühle GmbH & Co. KG - Stade',
        categoryId: 29,
        subCategoryId: 31,
        price: 836,
        priceTypeId: 1,
        maxPrice: 1512,
        version: 1,
        calculationType: 'perNight',
      },
      {
        id: '551ede5b-452b-4ef7-a2c7-e35bc8e96bbe',
        description: 'disintermediate mission-critical content',
        stationId: '483fb586-7fcf-4bd6-9501-fb1f5fe57a38',
        stationName: 'Austermühle GmbH & Co. KG - Stade',
        categoryId: 29,
        subCategoryId: 33,
        price: 591,
        priceTypeId: 4,
        maxPrice: 1829,
        version: 1,
        calculationType: 'perPieceAndNight',
      },
      {
        id: 'c1cda815-f209-427f-aba0-6e186f1acf9a',
        description: 'empower turn-key initiatives',
        stationId: '483fb586-7fcf-4bd6-9501-fb1f5fe57a38',
        stationName: 'Austermühle GmbH & Co. KG - Stade',
        categoryId: 29,
        subCategoryId: 34,
        price: 690,
        priceTypeId: 2,
        maxPrice: 845,
        version: 1,
        calculationType: 'perBooking',
      },
      {
        id: 'de51f6d8-9fb8-4692-b1c9-f83f44fc585e',
        description: 'orchestrate intuitive communities',
        stationId: '483fb586-7fcf-4bd6-9501-fb1f5fe57a38',
        stationName: 'Austermühle GmbH & Co. KG - Stade',
        categoryId: 29,
        subCategoryId: 34,
        price: 228,
        priceTypeId: 4,
        maxPrice: 791,
        version: 1,
        calculationType: 'inclusive',
      },
      {
        id: 'c77ad580-63e4-4fb6-86de-fd64d174333c',
        description: 'benchmark cutting-edge ROI',
        stationId: '483fb586-7fcf-4bd6-9501-fb1f5fe57a38',
        stationName: 'Austermühle GmbH & Co. KG - Stade',
        categoryId: 37,
        subCategoryId: 40,
        price: 1345,
        priceTypeId: 0,
        maxPrice: 1901,
        version: 1,
        calculationType: 'perPiece',
      },
      {
        id: '4f7e34ef-a6d6-4b43-92c4-c1a0f1230974',
        description: 'repurpose next-generation relationships',
        stationId: '483fb586-7fcf-4bd6-9501-fb1f5fe57a38',
        stationName: 'Austermühle GmbH & Co. KG - Stade',
        categoryId: 37,
        subCategoryId: 42,
        price: 475,
        priceTypeId: 2,
        maxPrice: 1166,
        version: 1,
        calculationType: 'perPiece',
      },
      {
        id: '9e7a8109-0dfd-4c17-993f-a06c20f27600',
        description: 'revolutionize e-business applications',
        stationId: '483fb586-7fcf-4bd6-9501-fb1f5fe57a38',
        stationName: 'Austermühle GmbH & Co. KG - Stade',
        categoryId: 37,
        subCategoryId: 46,
        price: 82,
        priceTypeId: 0,
        maxPrice: 875,
        version: 1,
        calculationType: 'perPiece',
      },
      {
        id: '1647d42f-0f4f-4e82-a7ba-73100ca0da65',
        description: 'strategize 24/7 users',
        stationId: '483fb586-7fcf-4bd6-9501-fb1f5fe57a38',
        stationName: 'Austermühle GmbH & Co. KG - Stade',
        categoryId: 37,
        subCategoryId: 48,
        price: 1350,
        priceTypeId: 4,
        maxPrice: 1384,
        version: 1,
        calculationType: 'perPiece',
      },
      {
        id: 'ddab35eb-283a-4ff3-9edf-53cde65f700d',
        description: 'synergize one-to-one architectures',
        stationId: '483fb586-7fcf-4bd6-9501-fb1f5fe57a38',
        stationName: 'Austermühle GmbH & Co. KG - Stade',
        categoryId: 52,
        subCategoryId: 55,
        price: 60,
        priceTypeId: 4,
        maxPrice: 1684,
        version: 1,
        calculationType: 'perPiece',
      },
      {
        id: 'b525506f-7b13-471f-b20b-8123be655f3e',
        description: 'mesh user-centric supply-chains',
        stationId: '483fb586-7fcf-4bd6-9501-fb1f5fe57a38',
        stationName: 'Austermühle GmbH & Co. KG - Stade',
        categoryId: 52,
        subCategoryId: 56,
        price: 325,
        priceTypeId: 2,
        maxPrice: 917,
        version: 1,
        calculationType: 'perPiece',
      },
      {
        id: '8ff603ec-abcd-4ce2-9ba4-d9ccb0e9f116',
        description: 'monetize granular methodologies',
        stationId: '483fb586-7fcf-4bd6-9501-fb1f5fe57a38',
        stationName: 'Austermühle GmbH & Co. KG - Stade',
        categoryId: 67,
        subCategoryId: 68,
        price: 531,
        priceTypeId: 4,
        maxPrice: 1587,
        version: 1,
        calculationType: 'perPiece',
      },
      {
        id: '9777c742-073d-43b1-9150-d407452f7b0b',
        description: 'incentivize innovative schemas',
        stationId: '483fb586-7fcf-4bd6-9501-fb1f5fe57a38',
        stationName: 'Austermühle GmbH & Co. KG - Stade',
        categoryId: 67,
        subCategoryId: 71,
        price: 838,
        priceTypeId: 2,
        maxPrice: 962,
        version: 1,
        calculationType: 'perPiece',
      },
      {
        id: 'f4fc9107-88f2-4f80-9419-57a2eecf2ca3',
        description: 'deploy enterprise initiatives',
        stationId: '483fb586-7fcf-4bd6-9501-fb1f5fe57a38',
        stationName: 'Austermühle GmbH & Co. KG - Stade',
        categoryId: 87,
        subCategoryId: 88,
        price: 280,
        priceTypeId: 2,
        maxPrice: 687,
        version: 1,
        calculationType: 'perPiece',
      },
      {
        id: '6c3b020b-5aea-40d7-b5af-d4630829f593',
        description: 'unleash customized web services',
        stationId: '483fb586-7fcf-4bd6-9501-fb1f5fe57a38',
        stationName: 'Austermühle GmbH & Co. KG - Stade',
        categoryId: 104,
        subCategoryId: 148,
        price: 1567,
        priceTypeId: 3,
        maxPrice: 1656,
        version: 1,
        calculationType: 'perPiece',
      },
    ],
  },
};

const categoriesData = {
  errors: null,
  events: null,
  data: [
    {
      id: 1,
      name: 'Fahrerhaus',
      parentId: 0,
      children: [
        {
          id: 16,
          name: 'Funkfernbedienung',
          parentId: 1,
        },
        {
          id: 12,
          name: 'CD',
          parentId: 1,
        },
        {
          id: 15,
          name: 'USB Anschluss',
          parentId: 1,
        },
        {
          id: 5,
          name: 'Außenspiegel elektrisch',
          parentId: 1,
        },
        {
          id: 7,
          name: 'Armlehnen',
          parentId: 1,
        },
        {
          id: 4,
          name: 'Fenster elektrisch',
          parentId: 1,
        },
        {
          id: 17,
          name: 'Lenkradfernbedienung',
          parentId: 1,
        },
        {
          id: 8,
          name: 'Sitze drehbar ',
          parentId: 1,
        },
        {
          id: 2,
          name: 'Navigation',
          parentId: 1,
        },
        {
          id: 9,
          name: 'Zentralverriegelung',
          parentId: 1,
        },
        {
          id: 13,
          name: 'Bluetooth',
          parentId: 1,
        },
        {
          id: 10,
          name: 'Verdunklung',
          parentId: 1,
        },
        {
          id: 11,
          name: 'Radio',
          parentId: 1,
        },
        {
          id: 3,
          name: 'Klimaanlage ',
          parentId: 1,
        },
        {
          id: 14,
          name: 'MP3',
          parentId: 1,
        },
        {
          id: 6,
          name: 'Pilotensitze',
          parentId: 1,
        },
      ],
    },
    {
      id: 18,
      name: 'Küche',
      parentId: 0,
      children: [
        {
          id: 24,
          name: 'Spüle',
          parentId: 18,
        },
        {
          id: 22,
          name: 'Mikrowelle',
          parentId: 18,
        },
        {
          id: 27,
          name: 'Warmwasser',
          parentId: 18,
        },
        {
          id: 28,
          name: 'Dunstabzugshaube',
          parentId: 18,
        },
        {
          id: 19,
          name: 'Kochstelle',
          parentId: 18,
        },
        {
          id: 20,
          name: 'Kochstelle Mehr-Flammig',
          parentId: 18,
        },
        {
          id: 26,
          name: 'Gefrierschrank',
          parentId: 18,
        },
        {
          id: 25,
          name: 'Kühlschrank',
          parentId: 18,
        },
        {
          id: 23,
          name: 'Kaffeemaschine/Presse',
          parentId: 18,
        },
        {
          id: 21,
          name: 'Backofen',
          parentId: 18,
        },
      ],
    },
    {
      id: 29,
      name: 'Bad',
      parentId: 0,
      children: [
        {
          id: 30,
          name: 'Waschbecken',
          parentId: 29,
        },
        {
          id: 34,
          name: 'Toilette',
          parentId: 29,
        },
        {
          id: 33,
          name: 'Dusche außen',
          parentId: 29,
        },
        {
          id: 36,
          name: 'Badfenster',
          parentId: 29,
        },
        {
          id: 32,
          name: 'Dusche separat von Toilette',
          parentId: 29,
        },
        {
          id: 35,
          name: 'Warmwasser',
          parentId: 29,
        },
        {
          id: 31,
          name: 'Dusche innen',
          parentId: 29,
        },
      ],
    },
    {
      id: 37,
      name: 'Wohnraum',
      parentId: 0,
      children: [
        {
          id: 45,
          name: 'Wohnraumbatterie',
          parentId: 37,
        },
        {
          id: 43,
          name: 'LED Beleuchtung',
          parentId: 37,
        },
        {
          id: 41,
          name: 'Mediaplayer',
          parentId: 37,
        },
        {
          id: 50,
          name: 'Elektrisches Hubbett',
          parentId: 37,
        },
        {
          id: 51,
          name: 'Internet / WLAN',
          parentId: 37,
        },
        {
          id: 48,
          name: 'Verdunklung',
          parentId: 37,
        },
        {
          id: 38,
          name: 'Klimaanlage (230V)',
          parentId: 37,
        },
        {
          id: 44,
          name: 'Spannungswandler 12V-230V',
          parentId: 37,
        },
        {
          id: 46,
          name: 'Panoramadach',
          parentId: 37,
        },
        {
          id: 40,
          name: 'SAT-Anlage ',
          parentId: 37,
        },
        {
          id: 49,
          name: 'Tür Schlafbereich',
          parentId: 37,
        },
        {
          id: 39,
          name: 'Fernseher',
          parentId: 37,
        },
        {
          id: 47,
          name: 'Fliegengitter',
          parentId: 37,
        },
        {
          id: 42,
          name: 'USB Anschluss',
          parentId: 37,
        },
      ],
    },
    {
      id: 52,
      name: 'Fahrzeug',
      parentId: 0,
      children: [
        {
          id: 53,
          name: 'Markise',
          parentId: 52,
        },
        {
          id: 58,
          name: 'Fahrradmitnahme (4 Fahrräder)',
          parentId: 52,
        },
        {
          id: 55,
          name: 'Tempomat',
          parentId: 52,
        },
        {
          id: 62,
          name: 'Abwassertank',
          parentId: 52,
        },
        {
          id: 63,
          name: 'Netzstrom-Anschluss (230V)',
          parentId: 52,
        },
        {
          id: 59,
          name: 'Heckgarage',
          parentId: 52,
        },
        {
          id: 65,
          name: 'Solar-Anlage',
          parentId: 52,
        },
        {
          id: 56,
          name: 'Fahrradmitnahme (2 Fahrräder)',
          parentId: 52,
        },
        {
          id: 60,
          name: 'Anhängerkupplung',
          parentId: 52,
        },
        {
          id: 66,
          name: 'Elektrische Trittstufe',
          parentId: 52,
        },
        {
          id: 57,
          name: 'Fahrradmitnahme (3 Fahrräder)',
          parentId: 52,
        },
        {
          id: 61,
          name: 'Frischwassertank',
          parentId: 52,
        },
        {
          id: 54,
          name: 'Servolenkung',
          parentId: 52,
        },
        {
          id: 64,
          name: 'Wasseranschluss',
          parentId: 52,
        },
      ],
    },
    {
      id: 67,
      name: 'Sicherheit',
      parentId: 0,
      children: [
        {
          id: 72,
          name: 'Alarmanlage',
          parentId: 67,
        },
        {
          id: 73,
          name: 'Feuerlöscher',
          parentId: 67,
        },
        {
          id: 75,
          name: 'Rauchmelder',
          parentId: 67,
        },
        {
          id: 70,
          name: 'ABS',
          parentId: 67,
        },
        {
          id: 76,
          name: 'Warnwesten',
          parentId: 67,
        },
        {
          id: 68,
          name: 'Airbag Fahrer',
          parentId: 67,
        },
        {
          id: 71,
          name: 'ESP',
          parentId: 67,
        },
        {
          id: 74,
          name: 'CO-Melder',
          parentId: 67,
        },
        {
          id: 69,
          name: 'Airbag Beifahrer',
          parentId: 67,
        },
        {
          id: 77,
          name: 'Rückfahrkamera',
          parentId: 67,
        },
      ],
    },
    {
      id: 78,
      name: 'Winterbetrieb',
      parentId: 0,
      children: [
        {
          id: 80,
          name: 'Standheizung',
          parentId: 78,
        },
        {
          id: 83,
          name: 'Heizung Abwassertank',
          parentId: 78,
        },
        {
          id: 86,
          name: 'Gas-Außensteckdose',
          parentId: 78,
        },
        {
          id: 82,
          name: 'Doppelboden',
          parentId: 78,
        },
        {
          id: 79,
          name: 'Wintertauglich',
          parentId: 78,
        },
        {
          id: 84,
          name: 'Winterreifen',
          parentId: 78,
        },
        {
          id: 81,
          name: 'Fußbodenheizung',
          parentId: 78,
        },
        {
          id: 85,
          name: 'Schneeketten',
          parentId: 78,
        },
      ],
    },
    {
      id: 87,
      name: 'Inklusivleistung ohne Aufpreis',
      parentId: 0,
      children: [
        {
          id: 95,
          name: 'Ausführliche Einweisung',
          parentId: 87,
        },
        {
          id: 100,
          name: 'Stellplatzführer',
          parentId: 87,
        },
        {
          id: 98,
          name: '230V Stromkabel',
          parentId: 87,
        },
        {
          id: 103,
          name: 'PKW Parkplatz',
          parentId: 87,
        },
        {
          id: 91,
          name: 'Gasflasche',
          parentId: 87,
        },
        {
          id: 90,
          name: 'Endreinigung (innen)',
          parentId: 87,
        },
        {
          id: 88,
          name: 'Campingmöbel-Set',
          parentId: 87,
        },
        {
          id: 92,
          name: 'CEE Adapter',
          parentId: 87,
        },
        {
          id: 93,
          name: 'Toileten-Chemie',
          parentId: 87,
        },
        {
          id: 97,
          name: 'Frischwasserschlauch',
          parentId: 87,
        },
        {
          id: 102,
          name: 'Kochzubehör Töpfe, Pfannen',
          parentId: 87,
        },
        {
          id: 94,
          name: 'Spezielles Toilettenpapier',
          parentId: 87,
        },
        {
          id: 96,
          name: 'Schutzbrief',
          parentId: 87,
        },
        {
          id: 89,
          name: 'Endreinigung (außen)',
          parentId: 87,
        },
        {
          id: 99,
          name: 'Auffahrkeile',
          parentId: 87,
        },
        {
          id: 101,
          name: 'Geschirr, Besteck',
          parentId: 87,
        },
      ],
    },
    {
      id: 104,
      name: 'Optionales Zubehör & Dienstleistungen',
      parentId: 0,
      children: [
        {
          id: 136,
          name: 'Einwegmiete',
          parentId: 104,
        },
        {
          id: 112,
          name: 'Campingstühle Set (alle Personen)',
          parentId: 104,
        },
        {
          id: 124,
          name: 'Grill Einweg',
          parentId: 104,
        },
        {
          id: 161,
          name: 'Fahrradträger 3 Räder',
          parentId: 104,
        },
        {
          id: 163,
          name: 'Fahrrad Erwachsener',
          parentId: 104,
        },
        {
          id: 169,
          name: 'Außenspiegel für Wohnwagen',
          parentId: 104,
        },
        {
          id: 156,
          name: 'Zusatz-Gasflasche 5kg',
          parentId: 104,
        },
        {
          id: 176,
          name: 'Kilometer-Paket 500 extra Freikilometer (Ãœbetrag aus Mietbedingungen)',
          parentId: 104,
        },
        {
          id: 118,
          name: 'Kaffeemaschine',
          parentId: 104,
        },
        {
          id: 117,
          name: 'Küchenausstattung Set (z.B. Töpfe, Pfannen, usw.)',
          parentId: 104,
        },
        {
          id: 151,
          name: 'Klimaanlage (mobil)',
          parentId: 104,
        },
        {
          id: 152,
          name: 'Stromgenerator',
          parentId: 104,
        },
        {
          id: 116,
          name: 'Geschirr & Besteck Set (1 Person)',
          parentId: 104,
        },
        {
          id: 150,
          name: 'PKW Parkplatz',
          parentId: 104,
        },
        {
          id: 167,
          name: 'Surfbrett',
          parentId: 104,
        },
        {
          id: 108,
          name: 'Endreinigung (Toilette)',
          parentId: 104,
        },
        {
          id: 123,
          name: 'Grill Gas',
          parentId: 104,
        },
        {
          id: 142,
          name: 'Winterreifen',
          parentId: 104,
        },
        {
          id: 147,
          name: 'Sat-Anlage',
          parentId: 104,
        },
        {
          id: 177,
          name: 'Kilometer-Paket 1.000 extra Freikilometer (Ãœbetrag aus Mietbedingungen)',
          parentId: 104,
        },
        {
          id: 153,
          name: 'Navigationsgerät',
          parentId: 104,
        },
        {
          id: 184,
          name: 'Premium-Paket II',
          parentId: 104,
        },
        {
          id: 122,
          name: 'Grill Kohle',
          parentId: 104,
        },
        {
          id: 120,
          name: 'Backofen (mobil)',
          parentId: 104,
        },
        {
          id: 149,
          name: 'Wechselrichter',
          parentId: 104,
        },
        {
          id: 166,
          name: 'E-Bike',
          parentId: 104,
        },
        {
          id: 174,
          name: 'Dachgepäckträger',
          parentId: 104,
        },
        {
          id: 182,
          name: 'Komfor-Paket III',
          parentId: 104,
        },
        {
          id: 106,
          name: 'Endreinigung (innen)',
          parentId: 104,
        },
        {
          id: 180,
          name: 'Komfor-Paket I',
          parentId: 104,
        },
        {
          id: 119,
          name: 'Toaster',
          parentId: 104,
        },
        {
          id: 168,
          name: 'Sup-Board',
          parentId: 104,
        },
        {
          id: 128,
          name: 'Handtücher Set (alle Personen)',
          parentId: 104,
        },
        {
          id: 157,
          name: 'Gasflasche 11kg',
          parentId: 104,
        },
        {
          id: 173,
          name: 'Anhängerkupplung',
          parentId: 104,
        },
        {
          id: 114,
          name: 'Campingtisch',
          parentId: 104,
        },
        {
          id: 113,
          name: 'Campingstuhl',
          parentId: 104,
        },
        {
          id: 115,
          name: 'Geschirr & Besteck Set (alle Personen)',
          parentId: 104,
        },
        {
          id: 138,
          name: 'Kinder-Sitzerhöhung',
          parentId: 104,
        },
        {
          id: 105,
          name: 'Endreinigung (außen)',
          parentId: 104,
        },
        {
          id: 137,
          name: 'Kindersitz',
          parentId: 104,
        },
        {
          id: 143,
          name: 'Schneeketten',
          parentId: 104,
        },
        {
          id: 165,
          name: 'Fahrrad Kleinkind',
          parentId: 104,
        },
        {
          id: 127,
          name: 'Bettwäsche Set 1 Person',
          parentId: 104,
        },
        {
          id: 135,
          name: 'Rückgabe außerhalb der Öffnungszeiten',
          parentId: 104,
        },
        {
          id: 134,
          name: 'Abholung außerhalb der Öffnungszeiten',
          parentId: 104,
        },
        {
          id: 162,
          name: 'Fahrradträger 4 Räder',
          parentId: 104,
        },
        {
          id: 164,
          name: 'Fahrrad Kind',
          parentId: 104,
        },
        {
          id: 141,
          name: 'Kinder-Zusatzbett Fahrerhaus',
          parentId: 104,
        },
        {
          id: 183,
          name: 'Premium-Paket I',
          parentId: 104,
        },
        {
          id: 139,
          name: 'Kinder-Hochstuhl',
          parentId: 104,
        },
        {
          id: 148,
          name: 'WLAN',
          parentId: 104,
        },
        {
          id: 110,
          name: 'Endreinigung Leerung Abwassertank',
          parentId: 104,
        },
        {
          id: 146,
          name: 'TV',
          parentId: 104,
        },
        {
          id: 111,
          name: 'Camping-Möbel Set Stühle & Tisch (alle Personen)',
          parentId: 104,
        },
        {
          id: 155,
          name: 'Gasflasche 5kg',
          parentId: 104,
        },
        {
          id: 179,
          name: 'Unbegrenzte Kilometer',
          parentId: 104,
        },
        {
          id: 131,
          name: 'Transfer Bahnhof',
          parentId: 104,
        },
        {
          id: 159,
          name: 'Camping-Toilette (mobil)',
          parentId: 104,
        },
        {
          id: 132,
          name: 'Service Fahrzeuglieferung',
          parentId: 104,
        },
        {
          id: 140,
          name: 'Kinder-Zusatzbett',
          parentId: 104,
        },
        {
          id: 145,
          name: 'Heizlüfter',
          parentId: 104,
        },
        {
          id: 158,
          name: 'Zusatz-Gasflasche 11kg',
          parentId: 104,
        },
        {
          id: 185,
          name: 'Premium-Paket III',
          parentId: 104,
        },
        {
          id: 121,
          name: 'Kocher (mobil)',
          parentId: 104,
        },
        {
          id: 125,
          name: 'Kühbox',
          parentId: 104,
        },
        {
          id: 160,
          name: 'Fahrradträger 2 Räder',
          parentId: 104,
        },
        {
          id: 178,
          name: 'Kilometer-Paket 1.500 extra Freikilometer (Ãœbetrag aus Mietbedingungen)',
          parentId: 104,
        },
        {
          id: 109,
          name: 'Endreinigung Leerung Toilette',
          parentId: 104,
        },
        {
          id: 130,
          name: 'Transfer Flughafen',
          parentId: 104,
        },
        {
          id: 181,
          name: 'Komfor-Paket II',
          parentId: 104,
        },
        {
          id: 133,
          name: 'Service Fahrzeugabholung',
          parentId: 104,
        },
        {
          id: 171,
          name: 'Markise',
          parentId: 104,
        },
        {
          id: 172,
          name: 'Sonnensegel',
          parentId: 104,
        },
        {
          id: 170,
          name: 'Vorzelt',
          parentId: 104,
        },
        {
          id: 175,
          name: 'Dachbox',
          parentId: 104,
        },
        {
          id: 126,
          name: 'Bettwäsche Set (alle Personen)',
          parentId: 104,
        },
        {
          id: 129,
          name: 'Handtücher Set (1 Person)',
          parentId: 104,
        },
        {
          id: 154,
          name: 'Außendusche',
          parentId: 104,
        },
        {
          id: 107,
          name: 'Endreinigung (Bad)',
          parentId: 104,
        },
        {
          id: 144,
          name: 'Winterabdeckung Fahrerhaus',
          parentId: 104,
        },
      ],
    },
  ],
};

const staticData = {
  errors: null,
  events: null,
  data: {
    bedTypes: [
      {
        id: 0,
        name: 'Alkoven',
      },
      {
        id: 1,
        name: 'Hubbett',
      },
      {
        id: 2,
        name: 'Seitensofa',
      },
      {
        id: 3,
        name: 'Umbau Essecke',
      },
      {
        id: 4,
        name: 'Etagenbetten',
      },
      {
        id: 5,
        name: 'Einzelbett längs',
      },
      {
        id: 6,
        name: 'Doppelbett quer',
      },
      {
        id: 7,
        name: 'Doppelbett längs',
      },
      {
        id: 8,
        name: 'Queensbett',
      },
    ],
    consumptions: [
      {
        id: 0,
        name: '6-8l',
      },
      {
        id: 1,
        name: '8-10l',
      },
      {
        id: 2,
        name: '10-12l',
      },
      {
        id: 3,
        name: '12-14l',
      },
      {
        id: 4,
        name: '14-16l',
      },
      {
        id: 5,
        name: '16-18l',
      },
      {
        id: 6,
        name: '18-20l',
      },
      {
        id: 7,
        name: '20-22l',
      },
    ],
    equipmentPriceTypes: [
      {
        id: 0,
        name: 'pro Stück',
      },
      {
        id: 1,
        name: 'pro Nacht',
      },
      {
        id: 2,
        name: 'pro Stück und Nacht',
      },
      {
        id: 3,
        name: 'pro Buchung',
      },
      {
        id: 4,
        name: 'kostenlos',
      },
    ],
    fuelValues: [
      {
        id: 0,
        name: 'Diesel',
      },
      {
        id: 1,
        name: 'Diesel/Gas',
      },
      {
        id: 2,
        name: 'Benzin',
      },
      {
        id: 3,
        name: 'Diesel',
      },
    ],
    gears: [
      {
        id: 0,
        name: 'Schaltung',
      },
      {
        id: 1,
        name: 'Automatik',
      },
    ],
    vehicleModels: [
      {
        id: 0,
        name: '2in1-Mobil',
      },
      {
        id: 1,
        name: 'ABI',
      },
      {
        id: 2,
        name: 'Adria',
      },
      {
        id: 3,
        name: 'Affinity',
      },
      {
        id: 4,
        name: 'Ahorn',
      },
      {
        id: 5,
        name: 'Airstream',
      },
      {
        id: 6,
        name: 'Alpha',
      },
      {
        id: 7,
        name: 'Arca',
      },
      {
        id: 8,
        name: 'Atlas',
      },
      {
        id: 9,
        name: 'Autoroller',
      },
      {
        id: 10,
        name: 'Autostar',
      },
      {
        id: 11,
        name: 'Avento',
      },
      {
        id: 24,
        name: 'Bürstner',
      },
      {
        id: 12,
        name: 'Bavaria',
      },
      {
        id: 13,
        name: 'Bavaria Camp',
      },
      {
        id: 14,
        name: 'Bawemo',
      },
      {
        id: 15,
        name: 'Beisl',
      },
      {
        id: 16,
        name: 'Bela',
      },
      {
        id: 17,
        name: 'Benimar',
      },
      {
        id: 18,
        name: 'Beyerland',
      },
      {
        id: 19,
        name: 'Bimobil',
      },
      {
        id: 20,
        name: 'BK Bluebird',
      },
      {
        id: 21,
        name: 'Bravia',
      },
      {
        id: 22,
        name: 'Bresler',
      },
      {
        id: 23,
        name: 'Burow',
      },
      {
        id: 25,
        name: 'Ca-Mo-Car',
      },
      {
        id: 26,
        name: 'Carado',
      },
      {
        id: 27,
        name: 'Caravelair',
      },
      {
        id: 28,
        name: 'Caretta',
      },
      {
        id: 29,
        name: 'Carnaby Caravans',
      },
      {
        id: 30,
        name: 'Caro',
      },
      {
        id: 31,
        name: 'Carthago',
      },
      {
        id: 32,
        name: 'Challenger',
      },
      {
        id: 33,
        name: 'Chateau',
      },
      {
        id: 34,
        name: 'Chausson',
      },
      {
        id: 35,
        name: 'Chrysler',
      },
      {
        id: 36,
        name: 'CI International',
      },
      {
        id: 37,
        name: 'Citroen',
      },
      {
        id: 38,
        name: 'Clever',
      },
      {
        id: 39,
        name: 'CNP',
      },
      {
        id: 40,
        name: 'Coachmen',
      },
      {
        id: 41,
        name: 'Concorde',
      },
      {
        id: 42,
        name: 'Cristall',
      },
      {
        id: 43,
        name: 'Crosscamp',
      },
      {
        id: 44,
        name: 'CS-Reisemobile',
      },
      {
        id: 45,
        name: 'Damon',
      },
      {
        id: 46,
        name: 'Dehler',
      },
      {
        id: 47,
        name: 'Delta',
      },
      {
        id: 48,
        name: 'Dethleffs',
      },
      {
        id: 49,
        name: 'Domo',
      },
      {
        id: 50,
        name: 'Dopfer',
      },
      {
        id: 51,
        name: 'Dreamer',
      },
      {
        id: 52,
        name: 'Due Erre',
      },
      {
        id: 53,
        name: 'Eifelland',
      },
      {
        id: 54,
        name: 'Elnagh',
      },
      {
        id: 55,
        name: 'Esterel',
      },
      {
        id: 56,
        name: 'Etrusco',
      },
      {
        id: 57,
        name: 'Eura Mobil',
      },
      {
        id: 58,
        name: 'Euro Liner',
      },
      {
        id: 59,
        name: 'EVM',
      },
      {
        id: 60,
        name: 'eXtreme',
      },
      {
        id: 61,
        name: 'Fendt',
      },
      {
        id: 62,
        name: 'FFB - Tabbert',
      },
      {
        id: 63,
        name: 'Fiat',
      },
      {
        id: 64,
        name: 'Fischer',
      },
      {
        id: 65,
        name: 'Fleetwood',
      },
      {
        id: 66,
        name: 'Fleurette',
      },
      {
        id: 67,
        name: 'Florium',
      },
      {
        id: 68,
        name: 'Ford',
      },
      {
        id: 69,
        name: 'Ford / Reimo',
      },
      {
        id: 70,
        name: 'Forster',
      },
      {
        id: 71,
        name: 'Four-Wheel Camper',
      },
      {
        id: 72,
        name: 'FR Mobil',
      },
      {
        id: 73,
        name: 'Frankia',
      },
      {
        id: 74,
        name: 'Free Living',
      },
      {
        id: 75,
        name: 'General Motors',
      },
      {
        id: 76,
        name: 'Gigant',
      },
      {
        id: 77,
        name: 'GiottiLine',
      },
      {
        id: 80,
        name: 'Glücksmobil',
      },
      {
        id: 79,
        name: 'Globe-Traveller',
      },
      {
        id: 78,
        name: 'Globecar',
      },
      {
        id: 81,
        name: 'Granduca',
      },
      {
        id: 82,
        name: 'Hehn',
      },
      {
        id: 83,
        name: 'Heku',
      },
      {
        id: 84,
        name: 'Hobby',
      },
      {
        id: 85,
        name: 'Holiday Rambler',
      },
      {
        id: 86,
        name: 'Home-Car',
      },
      {
        id: 87,
        name: 'HRZ',
      },
      {
        id: 88,
        name: 'Hymer / Eriba',
      },
      {
        id: 89,
        name: 'Hymercar',
      },
      {
        id: 90,
        name: 'ICF',
      },
      {
        id: 91,
        name: 'Ilusion',
      },
      {
        id: 92,
        name: 'Itineo',
      },
      {
        id: 93,
        name: 'Iveco',
      },
      {
        id: 94,
        name: 'Joint',
      },
      {
        id: 95,
        name: 'Kabe',
      },
      {
        id: 96,
        name: 'Karmann',
      },
      {
        id: 97,
        name: 'Kentucky',
      },
      {
        id: 98,
        name: 'Kip',
      },
      {
        id: 99,
        name: 'Knaus',
      },
      {
        id: 100,
        name: 'La Mancelle',
      },
      {
        id: 101,
        name: 'La Marca',
      },
      {
        id: 102,
        name: 'La Strada',
      },
      {
        id: 103,
        name: 'Lacet',
      },
      {
        id: 104,
        name: 'Laika',
      },
      {
        id: 105,
        name: 'Le Voyager',
      },
      {
        id: 106,
        name: 'Linne-Liner',
      },
      {
        id: 107,
        name: 'LMC',
      },
      {
        id: 108,
        name: 'M+M Mobile',
      },
      {
        id: 109,
        name: 'Ma-Bu',
      },
      {
        id: 110,
        name: 'Maesss',
      },
      {
        id: 111,
        name: 'Malibu',
      },
      {
        id: 112,
        name: 'MAN',
      },
      {
        id: 113,
        name: 'Mazda',
      },
      {
        id: 114,
        name: 'McLouis',
      },
      {
        id: 115,
        name: 'MegaMobil',
      },
      {
        id: 116,
        name: 'Mercedes-Benz',
      },
      {
        id: 117,
        name: 'Miller',
      },
      {
        id: 118,
        name: 'Mirage',
      },
      {
        id: 119,
        name: 'Mitsubishi',
      },
      {
        id: 120,
        name: 'Mobilvetta',
      },
      {
        id: 121,
        name: 'Monaco',
      },
      {
        id: 122,
        name: 'Moncayo',
      },
      {
        id: 123,
        name: 'Mooveo',
      },
      {
        id: 124,
        name: 'Morelo',
      },
      {
        id: 126,
        name: 'Neo-Traveller',
      },
      {
        id: 125,
        name: 'Neotec',
      },
      {
        id: 127,
        name: 'Niesmann & Bischoff',
      },
      {
        id: 128,
        name: 'Niewadow',
      },
      {
        id: 129,
        name: 'NobelART',
      },
      {
        id: 130,
        name: 'Nordstar',
      },
      {
        id: 131,
        name: 'Notin',
      },
      {
        id: 132,
        name: 'Opel',
      },
      {
        id: 133,
        name: 'OrangeCamp',
      },
      {
        id: 134,
        name: 'Ormocar',
      },
      {
        id: 135,
        name: 'P.L.A.',
      },
      {
        id: 142,
        name: 'Pössl',
      },
      {
        id: 136,
        name: 'Paul & Paula',
      },
      {
        id: 137,
        name: 'Pemberton',
      },
      {
        id: 138,
        name: 'Peugeot',
      },
      {
        id: 139,
        name: 'Phoenix',
      },
      {
        id: 140,
        name: 'Pilote',
      },
      {
        id: 141,
        name: 'PNC',
      },
      {
        id: 143,
        name: 'Procab',
      },
      {
        id: 144,
        name: 'Raclet',
      },
      {
        id: 145,
        name: 'Rapido',
      },
      {
        id: 146,
        name: 'Reimo',
      },
      {
        id: 147,
        name: 'Reisemobile Beier',
      },
      {
        id: 148,
        name: 'Renault',
      },
      {
        id: 149,
        name: 'Reydam',
      },
      {
        id: 150,
        name: 'Rimor',
      },
      {
        id: 151,
        name: 'Riva',
      },
      {
        id: 152,
        name: 'Riviera',
      },
      {
        id: 153,
        name: 'RMB',
      },
      {
        id: 154,
        name: 'Roadcar',
      },
      {
        id: 155,
        name: 'Roadtrek',
      },
      {
        id: 156,
        name: 'Robel-Mobil',
      },
      {
        id: 157,
        name: 'Robeta',
      },
      {
        id: 158,
        name: 'Roller Team',
      },
      {
        id: 159,
        name: 'Schwabenmobil',
      },
      {
        id: 160,
        name: 'SEA',
      },
      {
        id: 161,
        name: 'Seitz',
      },
      {
        id: 195,
        name: 'Selbstbau',
      },
      {
        id: 162,
        name: 'Six-Pac',
      },
      {
        id: 163,
        name: 'Sloop',
      },
      {
        id: 194,
        name: 'Sonstige',
      },
      {
        id: 164,
        name: 'Sprite',
      },
      {
        id: 165,
        name: 'Stephex Motorhomes',
      },
      {
        id: 166,
        name: 'Sterckeman',
      },
      {
        id: 167,
        name: 'Sun Living',
      },
      {
        id: 168,
        name: 'Sunlight',
      },
      {
        id: 169,
        name: 'Sunroller',
      },
      {
        id: 170,
        name: 'Swift',
      },
      {
        id: 171,
        name: 'T@B',
      },
      {
        id: 172,
        name: 'Tabbert',
      },
      {
        id: 173,
        name: 'TEC',
      },
      {
        id: 174,
        name: 'Tischer',
      },
      {
        id: 175,
        name: 'Tourne',
      },
      {
        id: 176,
        name: 'Trigano',
      },
      {
        id: 177,
        name: 'Triple E',
      },
      {
        id: 178,
        name: 'TSL Landsberg/Rockwood',
      },
      {
        id: 179,
        name: 'Ultra',
      },
      {
        id: 180,
        name: 'VANTourer',
      },
      {
        id: 181,
        name: 'Vario-Mobil',
      },
      {
        id: 182,
        name: 'Victory',
      },
      {
        id: 183,
        name: 'Volkner',
      },
      {
        id: 184,
        name: 'Volkswagen',
      },
      {
        id: 185,
        name: 'Weinsberg',
      },
      {
        id: 186,
        name: 'Weippert',
      },
      {
        id: 187,
        name: 'Westfalia',
      },
      {
        id: 188,
        name: 'Wilk',
      },
      {
        id: 189,
        name: 'Willerby',
      },
      {
        id: 190,
        name: 'Wingamm',
      },
      {
        id: 191,
        name: 'Winnebago',
      },
      {
        id: 192,
        name: 'Woelcke',
      },
      {
        id: 193,
        name: 'XGO',
      },
    ],
    vehicleTypes: [
      {
        id: 0,
        name: 'Alkoven',
      },
      {
        id: 1,
        name: 'Teilintegriert',
      },
      {
        id: 2,
        name: 'Bus-Klasse',
      },
      {
        id: 3,
        name: 'Kastenwagen',
      },
      {
        id: 4,
        name: 'Integriert',
      },
      {
        id: 5,
        name: 'Wohnwagen',
      },
      {
        id: 6,
        name: 'Sonstiges',
      },
    ],
    weights: [
      {
        id: 0,
        name: '< 3,5t',
      },
      {
        id: 1,
        name: '>3,5t',
      },
    ],
  },
};

const searchData = {
  errors: null,
  events: null,
  data: [
    {
      vehicleId: 'abc4077b-3a4e-4a61-bb45-85bc8fc1af62',
      externalId: 'PF-B-4596',
      title: 'Team-oriented empowering Graphical User Interface-PF-B-4596',
      modelId: 184,
      manufacturer: 'GMC',
      lat: 51.47805,
      long: 6.8625,
      distance: 385.68082,
      imageUrl:
        'http://test.faircamperintern.de/api/images/3eWKVPCSOaE33zd9TrucogQ8s1TJahhjRkpAMg5qOnY=',
      altText: '',
      minPrice: 4049,
      euroCentOverall: 340116,
      smallPetsAllowed: true,
      numBedsOverall: 4,
      vehicleTypeId: 2,
      hasBestPrice: true,
      hasLiveCalendar: false,
      hasInstantBooking: false,
      equipmentCategoryIds: [1, 18, 29, 37, 52, 67, 78, 87, 104],
      EquipmentSubCategoryIds: [
        7, 9, 11, 12, 14, 22, 24, 26, 27, 31, 32, 33, 35, 39, 46, 47, 50, 51,
        62, 64, 66, 68, 69, 75, 76, 81, 82, 83, 94, 127, 128, 155, 166,
      ],
      weightId: 0,
      currency: '',
    },
    {
      vehicleId: '2e481d9a-561b-45d1-98d9-0e9d9a4977aa',
      externalId: 'DEG-F-69',
      title: 'Upgradable disintermediate algorithm-DEG-F-69',
      modelId: 81,
      manufacturer: 'Volkswagen',
      lat: 51.47805,
      long: 6.8625,
      distance: 385.68082,
      imageUrl:
        'http://test.faircamperintern.de/api/images/3eWKVPCSOaE33zd9TrucogQ8s1TJahhjRkpAMg5qOnY=',
      altText: '',
      minPrice: 5320,
      euroCentOverall: 446880,
      smallPetsAllowed: false,
      numBedsOverall: 2,
      vehicleTypeId: 2,
      hasBestPrice: false,
      hasLiveCalendar: false,
      hasInstantBooking: false,
      equipmentCategoryIds: [1, 18, 29, 37, 52, 67, 78, 87, 104],
      EquipmentSubCategoryIds: [
        7, 9, 11, 12, 14, 22, 24, 26, 27, 31, 32, 33, 35, 39, 46, 47, 50, 51,
        62, 64, 66, 68, 69, 75, 76, 81, 82, 83, 94, 127, 128, 155, 166,
      ],
      weightId: 1,
      currency: '',
    },
    {
      vehicleId: '2687c7aa-bbdc-410a-8d98-44bc3194e309',
      externalId: 'SLF-Z-8',
      title: 'Up-sized optimal pricing structure-SLF-Z-8',
      modelId: 51,
      manufacturer: 'Subaru',
      lat: 51.47805,
      long: 6.8625,
      distance: 385.68082,
      imageUrl:
        'http://test.faircamperintern.de/api/images/3eWKVPCSOaE33zd9TrucogQ8s1TJahhjRkpAMg5qOnY=',
      altText: '',
      minPrice: 4616,
      euroCentOverall: 521892,
      smallPetsAllowed: false,
      numBedsOverall: 2,
      vehicleTypeId: 5,
      hasBestPrice: false,
      hasLiveCalendar: false,
      hasInstantBooking: false,
      equipmentCategoryIds: [1, 18, 29, 37, 52, 67, 78, 87, 104],
      EquipmentSubCategoryIds: [
        7, 9, 11, 12, 14, 22, 24, 26, 27, 31, 32, 33, 35, 39, 46, 47, 50, 51,
        62, 64, 66, 68, 69, 75, 76, 81, 82, 83, 94, 127, 128, 155, 166,
      ],
      weightId: 0,
      currency: '',
    },
    {
      vehicleId: '876a33f6-179c-45bc-9dc6-854d4dcd2b8e',
      externalId: 'FR-N-35',
      title: 'Down-sized bi-directional model-FR-N-35',
      modelId: 98,
      manufacturer: 'Eagle',
      lat: 51.47805,
      long: 6.8625,
      distance: 385.68082,
      imageUrl:
        'http://test.faircamperintern.de/api/images/3eWKVPCSOaE33zd9TrucogQ8s1TJahhjRkpAMg5qOnY=',
      altText: '',
      minPrice: 4049,
      euroCentOverall: 659400,
      smallPetsAllowed: true,
      numBedsOverall: 4,
      vehicleTypeId: 0,
      hasBestPrice: true,
      hasLiveCalendar: false,
      hasInstantBooking: false,
      equipmentCategoryIds: [1, 18, 29, 37, 52, 67, 78, 87, 104],
      EquipmentSubCategoryIds: [
        7, 9, 11, 12, 14, 22, 24, 26, 27, 31, 32, 33, 35, 39, 46, 47, 50, 51,
        62, 64, 66, 68, 69, 75, 76, 81, 82, 83, 94, 127, 128, 155, 166,
      ],
      weightId: 1,
      currency: '',
    },
    {
      vehicleId: '860f9424-8641-4a58-b56f-ce44f8fd8606',
      externalId: 'HRO-K-7',
      title: 'Public-key zero administration circuit-HRO-K-7',
      modelId: 29,
      manufacturer: 'Lexus',
      lat: 50.80904,
      long: 8.77069,
      distance: 388.27527,
      imageUrl:
        'http://test.faircamperintern.de/api/images/OvowLw9SFP3bgNKpIDlF5JqotAZGRv2gfRa3VUFRmKs=',
      altText: '',
      minPrice: 4431,
      euroCentOverall: 384972,
      smallPetsAllowed: true,
      numBedsOverall: 2,
      vehicleTypeId: 0,
      hasBestPrice: false,
      hasLiveCalendar: false,
      hasInstantBooking: false,
      equipmentCategoryIds: [1, 18, 29, 37, 52, 67, 78, 87, 104],
      EquipmentSubCategoryIds: [
        5, 7, 8, 9, 10, 11, 13, 15, 16, 17, 19, 20, 21, 24, 33, 34, 35, 38, 46,
        51, 53, 56, 60, 68, 79, 80, 82, 83, 86, 97, 98, 101, 102, 127, 138, 176,
      ],
      weightId: 0,
      currency: '',
    },
    {
      vehicleId: 'b5c68cc0-f116-4ff1-81c1-5a206604be8b',
      externalId: 'MK-X-5354',
      title: 'Profit-focused needs-based open system-MK-X-5354',
      modelId: 78,
      manufacturer: 'Ford',
      lat: 50.80904,
      long: 8.77069,
      distance: 388.27527,
      imageUrl:
        'http://test.faircamperintern.de/api/images/OvowLw9SFP3bgNKpIDlF5JqotAZGRv2gfRa3VUFRmKs=',
      altText: '',
      minPrice: 5818,
      euroCentOverall: 939792,
      smallPetsAllowed: false,
      numBedsOverall: 3,
      vehicleTypeId: 0,
      hasBestPrice: true,
      hasLiveCalendar: false,
      hasInstantBooking: false,
      equipmentCategoryIds: [1, 18, 29, 37, 52, 67, 78, 87, 104],
      EquipmentSubCategoryIds: [
        5, 7, 8, 9, 10, 11, 13, 15, 16, 17, 19, 20, 21, 24, 33, 34, 35, 38, 46,
        51, 53, 56, 60, 68, 79, 80, 82, 83, 86, 97, 98, 101, 102, 127, 138, 176,
      ],
      weightId: 0,
      currency: '',
    },
    {
      vehicleId: '23800bac-eaa8-4714-bde1-ad81f155b832',
      externalId: 'COC-B-1',
      title: 'Business-focused systemic solution-COC-B-1',
      modelId: 1,
      manufacturer: 'GMC',
      lat: 50.82709,
      long: 6.9747,
      distance: 440.1765,
      imageUrl:
        'http://test.faircamperintern.de/api/images/ZFZhz6sngmndce5SEogypguvFls0rDy0tG1mZ6YAiBs=',
      altText: '',
      minPrice: 4437,
      euroCentOverall: 600936,
      smallPetsAllowed: true,
      numBedsOverall: 2,
      vehicleTypeId: 5,
      hasBestPrice: true,
      hasLiveCalendar: false,
      hasInstantBooking: false,
      equipmentCategoryIds: [1, 18, 29, 37, 52, 67, 78, 87, 104],
      EquipmentSubCategoryIds: [
        8, 14, 20, 21, 24, 30, 32, 33, 36, 39, 44, 50, 54, 57, 61, 62, 63, 64,
        65, 71, 73, 75, 76, 79, 80, 83, 86, 94, 95, 101, 109, 129, 165, 180,
        185,
      ],
      weightId: 0,
      currency: '',
    },
    {
      vehicleId: 'a4fe1ea7-c047-41ca-8500-0b55a4583c23',
      externalId: 'LIP-F-351',
      title: 'Polarized exuding software-LIP-F-351',
      modelId: 100,
      manufacturer: 'Cadillac',
      lat: 50.82709,
      long: 6.9747,
      distance: 440.1765,
      imageUrl:
        'http://test.faircamperintern.de/api/images/ZFZhz6sngmndce5SEogypguvFls0rDy0tG1mZ6YAiBs=',
      altText: '',
      minPrice: 4674,
      euroCentOverall: 611352,
      smallPetsAllowed: true,
      numBedsOverall: 3,
      vehicleTypeId: 1,
      hasBestPrice: true,
      hasLiveCalendar: false,
      hasInstantBooking: false,
      equipmentCategoryIds: [1, 18, 29, 37, 52, 67, 78, 87, 104],
      EquipmentSubCategoryIds: [
        8, 14, 20, 21, 24, 30, 32, 33, 36, 39, 44, 50, 54, 57, 61, 62, 63, 64,
        65, 71, 73, 75, 76, 79, 80, 83, 86, 94, 95, 101, 109, 129, 165, 180,
        185,
      ],
      weightId: 0,
      currency: '',
    },
    {
      vehicleId: 'cb68c6b5-3819-40c5-85f6-4dca893a6f85',
      externalId: 'HU-QY-537',
      title: 'Universal systematic interface-HU-QY-537',
      modelId: 30,
      manufacturer: 'Chevrolet',
      lat: 50.82709,
      long: 6.9747,
      distance: 440.1765,
      imageUrl:
        'http://test.faircamperintern.de/api/images/ZFZhz6sngmndce5SEogypguvFls0rDy0tG1mZ6YAiBs=',
      altText: '',
      minPrice: 5671,
      euroCentOverall: 626136,
      smallPetsAllowed: true,
      numBedsOverall: 3,
      vehicleTypeId: 2,
      hasBestPrice: false,
      hasLiveCalendar: false,
      hasInstantBooking: false,
      equipmentCategoryIds: [1, 18, 29, 37, 52, 67, 78, 87, 104],
      EquipmentSubCategoryIds: [
        8, 14, 20, 21, 24, 30, 32, 33, 36, 39, 44, 50, 54, 57, 61, 62, 63, 64,
        65, 71, 73, 75, 76, 79, 80, 83, 86, 94, 95, 101, 109, 129, 165, 180,
        185,
      ],
      weightId: 1,
      currency: '',
    },
    {
      vehicleId: 'c2697201-90be-4aa5-911e-9a096141e2ea',
      externalId: 'GL-D-89',
      title: 'Exclusive secondary Graphic Interface-GL-D-89',
      modelId: 123,
      manufacturer: 'GMC',
      lat: 50.82709,
      long: 6.9747,
      distance: 440.1765,
      imageUrl:
        'http://test.faircamperintern.de/api/images/ZFZhz6sngmndce5SEogypguvFls0rDy0tG1mZ6YAiBs=',
      altText: '',
      minPrice: 8318,
      euroCentOverall: 698712,
      smallPetsAllowed: true,
      numBedsOverall: 7,
      vehicleTypeId: 5,
      hasBestPrice: false,
      hasLiveCalendar: false,
      hasInstantBooking: false,
      equipmentCategoryIds: [1, 18, 29, 37, 52, 67, 78, 87, 104],
      EquipmentSubCategoryIds: [
        8, 14, 20, 21, 24, 30, 32, 33, 36, 39, 44, 50, 54, 57, 61, 62, 63, 64,
        65, 71, 73, 75, 76, 79, 80, 83, 86, 94, 95, 101, 109, 129, 165, 180,
        185,
      ],
      weightId: 0,
      currency: '',
    },
    {
      vehicleId: 'fc11ebf7-267b-4f03-a72d-c0e62fd5d232',
      externalId: 'VS-AT-4',
      title: 'Enterprise-wide user-facing algorithm-VS-AT-4',
      modelId: 168,
      manufacturer: 'FIAT',
      lat: 50.82709,
      long: 6.9747,
      distance: 440.1765,
      imageUrl:
        'http://test.faircamperintern.de/api/images/ZFZhz6sngmndce5SEogypguvFls0rDy0tG1mZ6YAiBs=',
      altText: '',
      minPrice: 5004,
      euroCentOverall: 753816,
      smallPetsAllowed: false,
      numBedsOverall: 4,
      vehicleTypeId: 2,
      hasBestPrice: false,
      hasLiveCalendar: false,
      hasInstantBooking: false,
      equipmentCategoryIds: [1, 18, 29, 37, 52, 67, 78, 87, 104],
      EquipmentSubCategoryIds: [
        8, 14, 20, 21, 24, 30, 32, 33, 36, 39, 44, 50, 54, 57, 61, 62, 63, 64,
        65, 71, 73, 75, 76, 79, 80, 83, 86, 94, 95, 101, 109, 129, 165, 180,
        185,
      ],
      weightId: 1,
      currency: '',
    },
    {
      vehicleId: '442778eb-bc58-496f-a31a-470e4aa667f8',
      externalId: 'UE-SO-689',
      title: 'Reduced real-time encoding-UE-SO-689',
      modelId: 129,
      manufacturer: 'Ford',
      lat: 50.82709,
      long: 6.9747,
      distance: 440.1765,
      imageUrl:
        'http://test.faircamperintern.de/api/images/ZFZhz6sngmndce5SEogypguvFls0rDy0tG1mZ6YAiBs=',
      altText: '',
      minPrice: 6007,
      euroCentOverall: 972888,
      smallPetsAllowed: true,
      numBedsOverall: 4,
      vehicleTypeId: 1,
      hasBestPrice: true,
      hasLiveCalendar: false,
      hasInstantBooking: false,
      equipmentCategoryIds: [1, 18, 29, 37, 52, 67, 78, 87, 104],
      EquipmentSubCategoryIds: [
        8, 14, 20, 21, 24, 30, 32, 33, 36, 39, 44, 50, 54, 57, 61, 62, 63, 64,
        65, 71, 73, 75, 76, 79, 80, 83, 86, 94, 95, 101, 109, 129, 165, 180,
        185,
      ],
      weightId: 1,
      currency: '',
    },
    {
      vehicleId: 'ea879977-a7fd-4d2f-9fac-83fa4b2fc23a',
      externalId: 'ESW-QX-66',
      title: 'Team-oriented zero-defect emulation-ESW-QX-66',
      modelId: 148,
      manufacturer: 'Chevrolet',
      lat: 50.82709,
      long: 6.9747,
      distance: 440.1765,
      imageUrl:
        'http://test.faircamperintern.de/api/images/ZFZhz6sngmndce5SEogypguvFls0rDy0tG1mZ6YAiBs=',
      altText: '',
      minPrice: 4248,
      euroCentOverall: 1000020,
      smallPetsAllowed: false,
      numBedsOverall: 7,
      vehicleTypeId: 5,
      hasBestPrice: false,
      hasLiveCalendar: false,
      hasInstantBooking: false,
      equipmentCategoryIds: [1, 18, 29, 37, 52, 67, 78, 87, 104],
      EquipmentSubCategoryIds: [
        8, 14, 20, 21, 24, 30, 32, 33, 36, 39, 44, 50, 54, 57, 61, 62, 63, 64,
        65, 71, 73, 75, 76, 79, 80, 83, 86, 94, 95, 101, 109, 129, 165, 180,
        185,
      ],
      weightId: 0,
      currency: '',
    },
    {
      vehicleId: 'b4515c3e-e76f-4543-90b4-027e97015715',
      externalId: 'OSL-Z-5447',
      title: 'Reactive 3rdgeneration emulation-OSL-Z-5447',
      modelId: 4,
      manufacturer: 'Acura',
      lat: 49.68369,
      long: 8.61839,
      distance: 511.6575,
      imageUrl:
        'http://test.faircamperintern.de/api/images/PVw3RCFJbx8-ra8rQW2H9QiDHpBtDSXWhcIA2DHwhEQ=',
      altText: '',
      minPrice: 4142,
      euroCentOverall: 399420,
      smallPetsAllowed: false,
      numBedsOverall: 4,
      vehicleTypeId: 0,
      hasBestPrice: true,
      hasLiveCalendar: false,
      hasInstantBooking: false,
      equipmentCategoryIds: [1, 18, 29, 37, 52, 67, 78, 87, 104],
      EquipmentSubCategoryIds: [
        3, 7, 8, 10, 11, 16, 23, 27, 30, 31, 35, 36, 40, 44, 48, 54, 56, 59, 60,
        62, 64, 68, 81, 82, 83, 84, 85, 92, 97, 98, 112, 122, 145, 155, 176,
        185,
      ],
      weightId: 0,
      currency: '',
    },
    {
      vehicleId: 'ce21e4fc-4b55-4e53-99a8-f3fdc65be773',
      externalId: 'SÖM-SQ-49',
      title: 'Diverse neutral architecture-SÖM-SQ-49',
      modelId: 178,
      manufacturer: 'Chevrolet',
      lat: 49.68369,
      long: 8.61839,
      distance: 511.6575,
      imageUrl:
        'http://test.faircamperintern.de/api/images/PVw3RCFJbx8-ra8rQW2H9QiDHpBtDSXWhcIA2DHwhEQ=',
      altText: '',
      minPrice: 4442,
      euroCentOverall: 827148,
      smallPetsAllowed: false,
      numBedsOverall: 5,
      vehicleTypeId: 4,
      hasBestPrice: true,
      hasLiveCalendar: false,
      hasInstantBooking: false,
      equipmentCategoryIds: [1, 18, 29, 37, 52, 67, 78, 87, 104],
      EquipmentSubCategoryIds: [
        3, 7, 8, 10, 11, 16, 23, 27, 30, 31, 35, 36, 40, 44, 48, 54, 56, 59, 60,
        62, 64, 68, 81, 82, 83, 84, 85, 92, 97, 98, 112, 122, 145, 155, 176,
        185,
      ],
      weightId: 1,
      currency: '',
    },
    {
      vehicleId: '0d1a27f1-e157-4b0f-916d-37050782d46a',
      externalId: 'SIM-ME-73',
      title: 'Vision-oriented fresh-thinking complexity-SIM-ME-73',
      modelId: 97,
      manufacturer: 'Hyundai',
      lat: 48.35149,
      long: 8.96317,
      distance: 651.6732,
      imageUrl:
        'http://test.faircamperintern.de/api/images/aFbG1_oslWnWbw-Tceb9caI-VUhHt9ZheWY84zfhneg=',
      altText: '',
      minPrice: 5092,
      euroCentOverall: 701148,
      smallPetsAllowed: true,
      numBedsOverall: 3,
      vehicleTypeId: 1,
      hasBestPrice: false,
      hasLiveCalendar: false,
      hasInstantBooking: false,
      equipmentCategoryIds: [1, 18, 29, 37, 52, 67, 78, 87, 104],
      EquipmentSubCategoryIds: [
        4, 7, 8, 9, 14, 19, 20, 21, 23, 24, 28, 34, 35, 36, 43, 57, 60, 61, 64,
        68, 73, 75, 80, 82, 86, 88, 89, 90, 92, 93, 102, 148, 184,
      ],
      weightId: 1,
      currency: '',
    },
    {
      vehicleId: '42355432-a49d-477d-a5d0-91772d1407dd',
      externalId: 'SM-NI-66',
      title: 'User-friendly tertiary portal-SM-NI-66',
      modelId: 97,
      manufacturer: 'Lexus',
      lat: 48.35149,
      long: 8.96317,
      distance: 651.6732,
      imageUrl:
        'http://test.faircamperintern.de/api/images/aFbG1_oslWnWbw-Tceb9caI-VUhHt9ZheWY84zfhneg=',
      altText: '',
      minPrice: 8436,
      euroCentOverall: 735840,
      smallPetsAllowed: false,
      numBedsOverall: 5,
      vehicleTypeId: 1,
      hasBestPrice: false,
      hasLiveCalendar: false,
      hasInstantBooking: false,
      equipmentCategoryIds: [1, 18, 29, 37, 52, 67, 78, 87, 104],
      EquipmentSubCategoryIds: [
        4, 7, 8, 9, 14, 19, 20, 21, 23, 24, 28, 34, 35, 36, 43, 57, 60, 61, 64,
        68, 73, 75, 80, 82, 86, 88, 89, 90, 92, 93, 102, 148, 184,
      ],
      weightId: 0,
      currency: '',
    },
    {
      vehicleId: 'ce4a6647-25ca-45fc-a774-e29a86a0b998',
      externalId: 'RPL-U-16',
      title: 'Ameliorated exuding time-frame-RPL-U-16',
      modelId: 6,
      manufacturer: 'Mitsubishi',
      lat: 48.35149,
      long: 8.96317,
      distance: 651.6732,
      imageUrl:
        'http://test.faircamperintern.de/api/images/aFbG1_oslWnWbw-Tceb9caI-VUhHt9ZheWY84zfhneg=',
      altText: '',
      minPrice: 5483,
      euroCentOverall: 947772,
      smallPetsAllowed: true,
      numBedsOverall: 5,
      vehicleTypeId: 0,
      hasBestPrice: false,
      hasLiveCalendar: false,
      hasInstantBooking: false,
      equipmentCategoryIds: [1, 18, 29, 37, 52, 67, 78, 87, 104],
      EquipmentSubCategoryIds: [
        4, 7, 8, 9, 14, 19, 20, 21, 23, 24, 28, 34, 35, 36, 43, 57, 60, 61, 64,
        68, 73, 75, 80, 82, 86, 88, 89, 90, 92, 93, 102, 148, 184,
      ],
      weightId: 1,
      currency: '',
    },
  ],
};
