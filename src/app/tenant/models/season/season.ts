export interface SeasonCollection {
	id: number;
	name: string;
	seasons: Season[];
	toJSON?: { (key: string): any };
}

export interface Season {
	id?: number;
	priceLevel: PriceLevel;
	from: Date;
	to: Date;
}
export interface PriceLevel {
	id: number;
	name: string;
	color: string;
	version?: number;
}

export interface Daterange {
	from: Date;
	to: Date;
}
