import * as moment from 'moment';
import { Daterange } from './season';

export class Day {
	posX: number;
	day: number;
	date: Date;
	constructor(positionX: number, day: number, date: Date) {
		this.posX = positionX;
		this.day = day;
		this.date = date;
	}
}
export const defaultHeight = 60;
export const defaultColor = 'pink';
export class SeasonShape {
	name: string;
	from: Date;
	to: Date;
	color: string;
	heightPx: number;
	shape: Shape;
	selected: boolean;

	constructor(
		name: string,
		from: Date,
		to: Date,
		heightPx: number,
		conversionFunc: (date: Date) => number,
		color: string
	) {
		this.name = name;
		this.color = color;
		this.heightPx = heightPx === undefined ? defaultHeight : heightPx;

		const x0 = conversionFunc(from);
		const x1 = conversionFunc(to);
		this.shape = new Shape(x0, x1, this.heightPx, 2.5);
		this.from = from;
		this.to = to;
		this.selected = false;
	}

	select(): void {
		this.selected = true;
	}

	unSelect(): void {
		this.selected = false;
	}

	seasonColor(): string {
		return this.color;
	}

	// fromDate(): Date | undefined {
	// 	return this.from;
	// }

	// toDate(): Date | undefined {
	// 	return this.to;
	// }

	// updateShape(): void {
	// 	// the form emits null dates, when user did not select for instance the end date
	// 	if (this.to === null || this.from === null) {
	// 		return;
	// 	}
	// 	const x0 = xCoord(moment(this.from).dayOfYear(), 365);
	// 	const x1 = xCoord(moment(this.to).dayOfYear(), 365);

	// 	this.shape = new Shape(x0, x1, this.heightPx, 5);
	// }

	equals(season: SeasonShape): boolean {
		return season.from === this.from && season.to === this.to;
	}
}
export function xCoord(day: number, timelineInPx: number): number {
	return (timelineInPx * day) / (2 * 365);
}

export class Shape {
	points: Point[];

	constructor(x0: number, x1: number, height: number, marginY: number) {
		this.points = [
			new Point(x0, height + marginY),
			new Point(x0, 0 + marginY),
			new Point(x1, 0 + marginY),
			new Point(x1, height + marginY),
		];
	}

	toString(): string {
		let pointsAsStr = '';
		this.points.forEach((point) => {
			pointsAsStr = `${pointsAsStr} ${point}`;
		});
		return pointsAsStr;
	}

	x0(): number {
		if (this.points.length > 0) {
			return this.points[0].x;
		}
		return 0;
	}
}

export class Point {
	y: number;
	x: number;

	constructor(x: number, y: number) {
		this.x = x;
		this.y = y;
	}
	toString(): string {
		return `${this.x},${this.y}`;
	}
}

export class SeasonColor {
	selected: string;
	notSelected: string;

	constructor(selected: string, notSelected: string) {
		this.selected = selected;
		this.notSelected = notSelected;
	}

	colorSelected(): string {
		return this.selected;
	}

	color(): string {
		return this.notSelected;
	}
}
