export interface Availability {
  month: number;
  year: number;
  days: AvailabilityDay[];
}
export interface AvailabilityDay {
  day: number;
  month: number;
  year: number;
  available: boolean;
  checkout: boolean;
  checkin: boolean;
  price: number;
  originalPrice: number;
  minBookingDuration: number;
}
