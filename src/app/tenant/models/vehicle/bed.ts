export interface Bed {
  designation: number;
  length: number;
  width: number;
}
