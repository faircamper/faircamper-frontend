export interface RentalTerms {
  id: string
  hasAllFreeKm: boolean
  kmFreePerNight: number
  kmFreeTotal: number
  extraFeePerKm: number
  noFeeFromRentalDays: number
  minimumDriverAge: number
  minimumDrivingLicenseYears: number
  allowsExtraDriver: boolean
  allowsExtraDriverWithName: boolean
  allowsExtraDriverWithCosts: boolean
  feeExtraDriver: number
  deductiblePartialCover: number
  deductibleComprehensiveInsurance: number
  europeProtectionCover: boolean
  deposit: number
  depositPaymentMethodLocalId: number
  depositPaymentMethodAdvanceId: number
  depositBankTransfer: boolean
  depositDaysPaymentBeforeRental: number
  depositReduction: boolean
  smokingAllowed: boolean
  smallPetsAllowed: boolean
  smallPetsFee: number
  smallPetsPriceModellId: number
  smallPetsNumber: number
  foreignTravel: ForeignTravel[]
  version: number
}

export interface ForeignTravel {

  country: string
  isAllowed: boolean
}
