import { Image } from 'src/app/tenant/models/image';
import { Equipment } from '../equipment/equipment';
import { EquipmentItem } from '../equipment/station-equipment';
import { SeasonCollection } from '../season/season';
import { Station } from '../station/station';
import { Bed } from './bed';
import { Price } from './price';
import { RentalTerms } from './rentalTerms';

export interface Vehicle {
  id: number;
  vehicleId?: string;
  version?: number;
  station: Station | null;
  stationId: number;
  images: Image[];
  equipment: EquipmentItem[];
  equipmentItems: Equipment[];
  prices: Price[];
  seasonCollection: SeasonCollection;
  isApproved: boolean; // unlocked by faircamper
  isActive: boolean; // lessor can deactivate, so that the vehicle is not visble to end user
  isDeleted: boolean; // not visible for lessor

  rentalTerms: RentalTerms;

  // Metadata
  externalId?: string;
  title: string;
  description: string;
  constructionYear: number;
  model: string;
  vehicleTypeId: number;
  youtube: string;

  //techDetails
  manufacturer: string;
  power: number;
  payload: number;
  consumptionId: number;
  gearId: number;
  fuelId: number;
  height: number;
  length: number;
  width: number;
  weightId: number;
  statusId: number;

  numSeatsOverall: number;
  numSeats3Point: number;
  numSeatsLap: number;
  numSeatsIsofix: number;
  // beds
  numBedsOverall: number;
  numBedsComfort: number;
  numBedsComfortKids: number;
  beds: Bed[];
  minPrice: number;
}
export interface VehicleSearchObject {
  altText: string;
  distance: number;
  euroCentOverall: number;
  externalId: string;
  imageUrl: string;
  lat: number;
  long: number;
  manufacturer: string;
  minPrice: number;
  model: string;
  numBedsOverall: number;
  smallPetsAllowed: boolean;
  title: string;
  vehicleId: string;
  vehicleTypeId: number;
  weightId: number;
  hasBestPrice: boolean;
  hasLiveCalendar: boolean;
  hasInstantBooking: boolean;
}

export function consumptionVals(): string[] {
  return [
    '6-8l',
    '8-10l',
    '10-12l',
    '12-14l',
    '14-16l',
    '16-18l',
    '18-20l',
    '20-22l',
    '20-22l',
  ];
}

export enum Weight {
  Over35t = '>3,5t',
  Under35t = '< 3,5t',
}
