import { Money } from 'src/app/tenant/models/money';
import { PriceLevel } from '../season/season';

export interface Price {
  amount: any;
  currency: any;
  seasonCalendarId?: string;
  price: Money;
  season: PriceLevel;
}
