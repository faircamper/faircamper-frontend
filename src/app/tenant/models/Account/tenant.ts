export interface TenantAccount {
    id?: string;
    keycloakId: string;
    email: string;
    isEmailVerified: boolean;
    username: string;
    name: string;
    surname: string;
    accountType: string;
    street: string;
    streetNumber: string;
    postalCode: string;
    city: string;
    country: string;
    birthday: string;
    mobilePhone: string;
    phone: string;
  }

  export interface AccountInit {
    id?: string;
    keycloakId: string;
    email: string;
    isEmailVerified: boolean;
    username: string;
    name: string;
    surname: string;
    accountType: string;
  }