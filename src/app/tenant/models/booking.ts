import { BookingEquipmentCategory } from './equipment/booking_equipment';

export interface Booking {
  id: string | undefined;
  state: 'open' | 'pending' | 'accepted' | 'insurance' | 'finished';
  lessorResponseExpected: string;
  kmPackages: KmPackage[];
  equipment: BookingEquipmentCategory[];
}
export interface KmPackage {
  id: string | undefined;
  km: number;
  discount: number;
  price: number;
  amountPicked: number;
}
