export interface OpeningHours {
	handover: Handover;
	checkin?: {
		from: string;
		to: string;
	};
	checkout?: {
		from: string;
		to: string;
	};
	isFee: boolean;
}

export enum Day {
	SUNDAY = 0,
	MONDAY,
	TUESDAY,
	WEDNESDAY,
	THURSDAY,
	FRIDAY,
	SATURDAY,
}

export enum Handover {
	OPEN = 'OPEN',
	CLOSED = 'CLOSED',
	CONSULTATION = 'CONSULTATION',
}

export const weekdays = new Map<Day, string>([
	[Day.SUNDAY, $localize`:weekday|:Sunday`],
	[Day.MONDAY, $localize`:weekday|:Monday`],
	[Day.TUESDAY, $localize`:weekday|:Tuesday`],
	[Day.WEDNESDAY, $localize`:weekday|:Wednesday`],
	[Day.THURSDAY, $localize`:weekday|:Thursday`],
	[Day.FRIDAY, $localize`:weekday|:Friday`],
	[Day.SATURDAY, $localize`:weekday|:Saturday`],
]);

export const defaultOpeningHours: OpeningHours[] =
	[
		{
			checkin: { from: '12:00', to: '12:00' },
			checkout: { from: '12:00', to: '12:00' },
			handover: Handover.CLOSED,
			isFee: false,
		},
		{
			checkin: { from: '12:00', to: '12:00' },
			checkout: { from: '12:00', to: '12:00' },
			handover: Handover.CLOSED,
			isFee: false,
		},
		{
			checkin: { from: '12:00', to: '12:00' },
			checkout: { from: '12:00', to: '12:00' },
			handover: Handover.CLOSED,
			isFee: false,
		},
		{
			checkin: { from: '12:00', to: '12:00' },
			checkout: { from: '12:00', to: '12:00' },
			handover: Handover.CLOSED,
			isFee: false,
		},
		{
			checkin: { from: '12:00', to: '12:00' },
			checkout: { from: '12:00', to: '12:00' },
			handover: Handover.CLOSED,
			isFee: false,
		},
		{
			checkin: { from: '12:00', to: '12:00' },
			checkout: { from: '12:00', to: '12:00' },
			handover: Handover.CLOSED,
			isFee: false,
		},
		{
			checkin: { from: '12:00', to: '12:00' },
			checkout: { from: '12:00', to: '12:00' },
			handover: Handover.CLOSED,
			isFee: false,
		},
	];
