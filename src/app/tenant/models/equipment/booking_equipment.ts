export interface BookingEquipment {
  id: number;
  categoryId: number;
  name: string;
  description: string;
  price: number;
  priceTypeId: number;
  maxPrice: number;
  amount: number;
  checked: boolean;
}

export interface BookingEquipmentCategory {
  categoryId: string;
  categoryName: string;
  equipments: BookingEquipment[];
}
