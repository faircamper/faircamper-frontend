export interface Equipment {
  calculationType: string;
	id: number;
	categoryId: number;
	subCategoryId: number;
	name: string;
	description: string;
	price: {
		amount: number;
		currency: string;
	};
	priceTypeId: number;
	maxPrice: number;
	stationId: number;
	stationName: string;
}

export interface EquipmentSubCategory {
	id: number;
	name: string;
	parentId: number;
	children: Equipment[];
}

export interface EquipmentCategory {
	id: number;
	name: string;
	parentId: number;
	children: EquipmentSubCategory[];
}
