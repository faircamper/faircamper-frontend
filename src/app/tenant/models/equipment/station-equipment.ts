import { Money } from 'src/app/tenant/models/money';

export interface EquipmentItem {
  id: number | null;
  name: string;
  path: string[];
  categoryId: number | null;
  userDescription: string;
  price: Money;
  priceMax?: Money;
  priceType: PriceType;
}

export enum PriceType {
  Free = 'Free',
  Included = 'Included',
  PerBooking = 'PerBooking',
  PerItem = 'PerItem',
  PerNight = 'PerNight',
  PerItemAndNight = 'PerItemAndNight',
}

export enum Category {
  Cab = 'Cab',
  Kitchen = 'Kitchen',
  Bath = 'Bath',
  Vehicle = 'Vehicle',
  Security = 'Security',
  Winter = 'Winter',
  FreeLessorService = 'Free Lessor Service',
  Default = 'Default',
}

const Equpiment = {
  cab: [
    'Navigation',
    'Klimaanlage',
    'Fenster elektrisch',
    'Außenspiegel elektrisch',
    'Pilotensitze',
    'Armlehnen',
    'Sitze drehbar',
    'Zentralverriegelung',
    'Verdunklung',
    'Radio',
    'CD',
    'Bluetooth',
    'MP3',
    'USB Anschluss',
    'iPhone/iPod Anschluss',
    'Funkfernbedienung',
    'Lenkradfernbedienung',
  ],
  kitchen: [
    'Kochstelle',
    'Kochstelle Mehr-Flammig',
    'Backofen',
    'Spüle',
    'Kühlschrank Gas/12V/230V',
    'Gefriermöglichkeit',
    'Warmwasser',
  ],
  bath: [
    'Waschbecken',
    'Dusche',
    'Dusche separat von Toilette',
    'Zweite Dusche außen',
    'Toilette',
    'Warmwasser',
    'Badfenster',
  ],
  livingSpace: [
    'Klimaanlage (230V)',
    'Fernseher',
    'SAT-Anlage',
    'Mediaplayer',
    'DVD Player',
    'USB Anschluss',
    'LED Beleuchtung',
    'Spannungswandler 12V-230V',
    'Wohnraumbatterie',
    'Panoramadach',
    'Fliegengitter',
    'Verdunklung',
    'Tür Schlafbereich',
    'Elektrisches Hubbett',
    'Getrennte Betten',
  ],
};

const CabItems = [
  'Navigation',
  'Klimaanlage',
  'Fenster elektrisch',
  'Außenspiegel elektrisch',
  'Pilotensitze',
  'Armlehnen',
  'Sitze drehbar',
  'Zentralverriegelung',
  'Verdunklung',
  'Radio',
  'CD',
  'Bluetooth',
  'MP3',
  'USB Anschluss',
  'iPhone/iPod Anschluss',
  'Funkfernbedienung',
  'Lenkradfernbedienung',
];
const KitchenItems = [
  'Kochstelle',
  'Kochstelle Mehr-Flammig',
  'Backofen',
  'Spüle',
  'Kühlschrank Gas/12V/230V',
  'Gefriermöglichkeit',
  'Warmwasser',
];
const BathItems = [
  'Waschbecken',
  'Dusche',
  'Dusche separat von Toilette',
  'Zweite Dusche außen',
  'Toilette',
  'Warmwasser',
  'Badfenster',
];

const LivingSpaceItems = [
  'Klimaanlage (230V)',
  'Fernseher',
  'SAT-Anlage',
  'Mediaplayer',
  'DVD Player',
  'USB Anschluss',
  'LED Beleuchtung',
  'Spannungswandler 12V-230V',
  'Wohnraumbatterie',
  'Panoramadach',
  'Fliegengitter',
  'Verdunklung',
  'Tür Schlafbereich',
  'Elektrisches Hubbett',
  'Getrennte Betten',
];
