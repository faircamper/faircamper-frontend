export interface Unavailability {
	from:Date;
    to:Date;
}