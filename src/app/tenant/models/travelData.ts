export interface TravelData {
  startDate: Date | undefined;
  endDate: Date | undefined;
}
