export interface SearchData {
  startDate: Date | undefined;
  endDate: Date | undefined;
}
