// import { IUser } from '@wobi/account';
import { ActionReducerMap } from '@ngrx/store';
import { Booking } from '../../models/booking';
import { SearchData } from '../../models/searchData';
import { TravelData } from '../../models/travelData';
import { bookingReducer } from './booking.reducer';
import { searchDataReducer } from './searchData.reducer';

import { travelDataReducer } from './travelData.reducer';

export interface ITenantState {
  travelData: TravelData;
  searchData: SearchData;
  booking: Booking;
}

export const moduleReducers: ActionReducerMap<ITenantState> = {
  travelData: travelDataReducer,
  searchData: searchDataReducer,
  booking: bookingReducer,
};
