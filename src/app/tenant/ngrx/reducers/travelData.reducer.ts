import { Action, createReducer, on } from '@ngrx/store';
import { TravelData } from '../../models/travelData';
import {
  setTravelDataAction,
  setTravelEndDataAction,
  setTravelStartDataAction,
} from '../actions/travelData.actions';

export const initialTravelDataState: TravelData = {
  startDate: undefined,
  endDate: undefined,
};

const reducer = createReducer(
  initialTravelDataState,
  on(setTravelDataAction, (state: TravelData, { travelData }) => ({
    ...travelData,
  })),
  on(setTravelStartDataAction, (state: TravelData, { startDate }) => ({
    ...state,
    ...startDate,
  })),
  on(setTravelEndDataAction, (state: TravelData, { endDate }) => ({
    ...state,
    ...endDate,
  }))
);

export function travelDataReducer(
  state: TravelData | undefined,
  action: Action
) {
  return reducer(state, action);
}
