import { Action, createReducer, on } from '@ngrx/store';
import { Booking } from '../../models/booking';
import {
  bookingLoadAction,
  bookingLoadSuccessAction,
} from '../actions/booking.actions ';

export const initialBookingState: Booking = {
  id: undefined,
  state: 'open',
  lessorResponseExpected: '',
  kmPackages: [],
  equipment: [],
};

const reducer = createReducer(
  initialBookingState,
  on(bookingLoadAction, (state: Booking) => ({
    ...state,
  })),
  on(bookingLoadSuccessAction, (state: Booking, { booking }) => ({
    ...booking,
  }))
);

export function bookingReducer(state: Booking | undefined, action: Action) {
  return reducer(state, action);
}
