import { Action, createReducer, on } from '@ngrx/store';
import { SearchData } from '../../models/searchData';
import { TravelData } from '../../models/travelData';
import {
  searchDataSetAction,
  searchDataSetEndDateAction,
  searchDataSetStartDateAction,
} from '../actions/searchData.actions ';

export const initialSearchDataState: SearchData = {
  startDate: undefined,
  endDate: undefined,
};

const reducer = createReducer(
  initialSearchDataState,
  on(searchDataSetAction, (state: SearchData, { searchData }) => ({
    ...searchData,
  })),
  on(searchDataSetStartDateAction, (state: SearchData, { startDate }) => ({
    ...state,
    ...startDate,
  })),
  on(searchDataSetEndDateAction, (state: SearchData, { endDate }) => ({
    ...state,
    ...endDate,
  }))
);

export function searchDataReducer(
  state: SearchData | undefined,
  action: Action
) {
  return reducer(state, action);
}
