import { createAction, props } from '@ngrx/store';

import { type } from '@wobi/core';
import { Booking } from '../../models/booking';
// Category to uniquely identify the actions
const CATEGORY = 'wobi.tenant.booking';

/**
 * For each action type in an action group, make a simple
 * enum object for all of this group's action types.
 *
 * The 'type' utility function coerces strings into string
 * literal types and runs a simple check to guarantee all
 * action types in the application are unique.
 */
export interface TenantBookingActions {
  LOAD: string;
  LOAD_SUCCESS: string;
  UPDATE: string;
  UPDATE_COMPANY: string;
  UPDATE_SUCCESS: string;
}

export const BookingActionTypes: TenantBookingActions = {
  LOAD: type(`${CATEGORY}.load`),
  LOAD_SUCCESS: type(`${CATEGORY}.loadSuccess`),
  UPDATE: type(`${CATEGORY}.update`),
  UPDATE_COMPANY: type(`${CATEGORY}.company`),
  UPDATE_SUCCESS: type(`${CATEGORY}.updateSuccess`),
};

export const bookingLoadAction = createAction(
  BookingActionTypes.LOAD,
  props<{ id: string }>()
);

export const bookingLoadSuccessAction = createAction(
  BookingActionTypes.LOAD_SUCCESS,
  props<{ booking: Booking }>()
);

export const accountUpdateSuccessAction = createAction(
  BookingActionTypes.UPDATE_SUCCESS,
  props<{ booking: Booking }>()
);
