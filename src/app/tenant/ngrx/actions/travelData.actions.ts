import { createAction, props } from '@ngrx/store';

import { type } from '@wobi/core';
import { TravelData } from '../../models/travelData';
// Category to uniquely identify the actions
const CATEGORY = 'wobi.tenant.travel-data';

/**
 * For each action type in an action group, make a simple
 * enum object for all of this group's action types.
 *
 * The 'type' utility function coerces strings into string
 * literal types and runs a simple check to guarantee all
 * action types in the application are unique.
 */
export interface ITenantTravelDataActions {
  SET: string;
  SETSTART: string;
  SETEND: string;
}

export const TravelDataActionTypes: ITenantTravelDataActions = {
  SET: type(`${CATEGORY}.set`),
  SETSTART: type(`${CATEGORY}.setStart`),
  SETEND: type(`${CATEGORY}.setEnd`),
};

export const setTravelDataAction = createAction(
  TravelDataActionTypes.SET,
  props<{ travelData: TravelData }>()
);
export const setTravelStartDataAction = createAction(
  TravelDataActionTypes.SETSTART,
  props<{ startDate: Date }>()
);
export const setTravelEndDataAction = createAction(
  TravelDataActionTypes.SETEND,
  props<{ endDate: Date }>()
);
