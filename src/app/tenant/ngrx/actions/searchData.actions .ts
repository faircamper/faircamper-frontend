import { createAction, props } from '@ngrx/store';

import { type } from '@wobi/core';
import { SearchData } from '../../models/searchData';
import { TravelData } from '../../models/travelData';
// Category to uniquely identify the actions
const CATEGORY = 'wobi.tenant.search-data';

/**
 * For each action type in an action group, make a simple
 * enum object for all of this group's action types.
 *
 * The 'type' utility function coerces strings into string
 * literal types and runs a simple check to guarantee all
 * action types in the application are unique.
 */
export interface TenantSearchDataActions {
  SET: string;
  SETSTART: string;
  SETEND: string;
}

export const SearchDataActionTypes: TenantSearchDataActions = {
  SET: type(`${CATEGORY}.set`),
  SETSTART: type(`${CATEGORY}.setStart`),
  SETEND: type(`${CATEGORY}.setEnd`),
};

export const searchDataSetAction = createAction(
  SearchDataActionTypes.SET,
  props<{ searchData: SearchData }>()
);
export const searchDataSetStartDateAction = createAction(
  SearchDataActionTypes.SETSTART,
  props<{ startDate: Date }>()
);
export const searchDataSetEndDateAction = createAction(
  SearchDataActionTypes.SETEND,
  props<{ endDate: Date }>()
);
