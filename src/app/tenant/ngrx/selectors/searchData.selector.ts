import { createSelector } from '@ngrx/store';
import { SearchData } from '../../models/searchData';
import { selectState } from '../module.state';

import { ITenantState } from '../reducers/module.reducer';

export const selectSearchData = createSelector(
  selectState,
  (state: ITenantState): SearchData => state.searchData
);
