import { createSelector } from '@ngrx/store';
import { Booking, KmPackage } from '../../models/booking';
import { BookingEquipmentCategory } from '../../models/equipment/booking_equipment';
import { selectState } from '../module.state';

import { ITenantState } from '../reducers/module.reducer';

export const selectBooking = createSelector(
  selectState,
  (state: ITenantState): Booking => state.booking
);
export const selectBookingKmPackages = createSelector(
  selectState,
  (state: ITenantState): KmPackage[] => state.booking.kmPackages
);

export const selectBookingEquipment = createSelector(
  selectState,
  (state: ITenantState): BookingEquipmentCategory[] => state.booking.equipment
);
