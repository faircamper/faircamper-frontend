import { createFeatureSelector } from '@ngrx/store';
import { initialBookingState } from './reducers/booking.reducer';
import { ITenantState } from './reducers/module.reducer';
import { initialSearchDataState } from './reducers/searchData.reducer';
import { initialTravelDataState } from './reducers/travelData.reducer';

export const initialModuleState: ITenantState = {
  travelData: initialTravelDataState,
  searchData: initialSearchDataState,
  booking: initialBookingState,
};

export const selectState = createFeatureSelector<ITenantState>('@wobi/tenant');
