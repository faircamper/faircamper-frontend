import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Action, Store } from '@ngrx/store';
import { Observable, switchMap, map } from 'rxjs';
import { Booking } from '../../models/booking';
import { BookingService } from '../../services/http/booking/booking.service';
import {
  bookingLoadAction,
  bookingLoadSuccessAction,
} from '../actions/booking.actions ';

@Injectable()
export class BookingEffects {
  constructor(
    private actions$: Actions,
    private bookingService: BookingService,
    private store: Store
  ) {}

  loadBooking$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(bookingLoadAction),
      switchMap((data) =>
        this.bookingService
          .get<Booking>(data.id)
          .pipe(
            map((booking) => bookingLoadSuccessAction({ booking: booking }))
          )
      )
    )
  );
}
