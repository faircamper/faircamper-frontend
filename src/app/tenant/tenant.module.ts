import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SSOModule } from '@wobi/sso';
import { Store, StoreModule } from '@ngrx/store';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';

import { FcLibModule } from 'fc-lib';
import { AccountPageTenantComponent } from './components/account/pages/account-page-tenant/account-page-tenant.component';
import { TenantAccountDataComponent } from './components/account/tenant-account-data/tenant-account-data.component';
import { TenantAccountFormComponent } from './components/account/tenant-account-form/tenant-account-form.component';
import { TenantOverviewComponent } from './components/account/tenant-overview/tenant-overview.component';
import { ImageGalleryComponent } from './components/image-gallery/image-gallery.component';
import { BookingBoxComponent } from './components/vehicle/booking-box/booking-box.component';
import { CalendarOverlayComponent } from './components/vehicle/calendar-overlay/calendar-overlay.component';
import { VehicleDetailPageComponent } from './components/vehicle/pages/vehicle-detail-page/vehicle-detail-page.component';
import { VehicleListComponent } from './components/vehicle/pages/vehicle-list/vehicle-list.component';
import { VehicleDescriptionComponent } from './components/vehicle/vehicle-description/vehicle-description.component';
import { VehicleEquipmentComponent } from './components/vehicle/vehicle-equipment/vehicle-equipment.component';
import { VehicleExtraDetailsComponent } from './components/vehicle/vehicle-extra-details/vehicle-extra-details.component';
import { VehicleFilterBarComponent } from './components/vehicle/vehicle-filter-bar/vehicle-filter-bar.component';
import { VehicleHeadlineComponent } from './components/vehicle/vehicle-headline/vehicle-headline.component';
import { VehicleIconBarComponent } from './components/vehicle/vehicle-icon-bar/vehicle-icon-bar.component';
import { VehicleSearchResultPreviewCardComponent } from './components/vehicle/vehicle-search-result-preview-card/vehicle-search-result-preview-card.component';
import { VehicleTechDetailsComponent } from './components/vehicle/vehicle-tech-details/vehicle-tech-details.component';
import { FakeTenantApiService } from './fake-api/fake-tenant-api.service';
import { TenantRoutingModule } from './tenant-routing.module';
import { TenantComponent } from './tenant.component';
import { CalendarMonthViewComponent } from './components/vehicle/calendar-overlay/calendar-month-view/calendar-month-view.component';
import { initialModuleState } from './ngrx/module.state';
import { moduleReducers } from './ngrx/reducers/module.reducer';
import { CheckoutPageComponent } from './components/checkout/pages/checkout-page/checkout-page.component';
import { BookingEffects } from './ngrx/effects/booking.effects';
import { EffectsModule } from '@ngrx/effects';
import { CheckoutStatusBarComponent } from './components/checkout/checkout-status-bar/checkout-status-bar.component';
import { CheckoutBookingComponent } from './components/checkout/checkout-booking/checkout-booking.component';
import { CheckoutEquipmentComponent } from './components/checkout/checkout-booking/checkout-equipment/checkout-equipment.component';
import { CheckoutKilometerPackagesComponent } from './components/checkout/checkout-booking/checkout-kilometer-packages/checkout-kilometer-packages.component';
import { CheckoutKilometerPackagesFormComponent } from './components/checkout/checkout-booking/checkout-kilometer-packages-form/checkout-kilometer-packages-form.component';
import { CheckoutPandingPageComponent } from './components/checkout/pages/checkout-panding-page/checkout-panding-page.component';
import { CheckoutPersonalDataComponent } from './components/checkout/checkout-booking/checkout-personal-data/checkout-personal-data.component';
import { TenantFormComponent } from '../signup/forms/tenant-form/tenant-form.component';
import { SignupModule } from '../signup/signup.module';

@NgModule({
  declarations: [
    VehicleDetailPageComponent,
    VehicleListComponent,
    ImageGalleryComponent,
    BookingBoxComponent,
    VehicleHeadlineComponent,
    VehicleDescriptionComponent,
    VehicleEquipmentComponent,
    VehicleIconBarComponent,
    VehicleFilterBarComponent,
    VehicleSearchResultPreviewCardComponent,
    CalendarOverlayComponent,
    VehicleTechDetailsComponent,
    VehicleExtraDetailsComponent,
    AccountPageTenantComponent,
    TenantAccountDataComponent,
    TenantAccountFormComponent,
    TenantOverviewComponent,
    TenantComponent,
    CalendarMonthViewComponent,
    CheckoutPageComponent,
    CheckoutStatusBarComponent,
    CheckoutBookingComponent,
    CheckoutEquipmentComponent,
    CheckoutKilometerPackagesComponent,
    CheckoutKilometerPackagesFormComponent,
    CheckoutPandingPageComponent,
    CheckoutPersonalDataComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule,
    TenantRoutingModule,
    FcLibModule,
    SSOModule,
    HttpClientModule,
    SignupModule,
    StoreModule.forFeature('@wobi/tenant', moduleReducers, {
      initialState: initialModuleState,
    }),
    EffectsModule.forFeature([BookingEffects]),
  ],
})
export class TenantModule {}
