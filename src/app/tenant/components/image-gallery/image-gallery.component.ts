import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Image } from 'src/app/tenant/models/image';

@Component({
  selector: 'app-image-gallery',
  templateUrl: './image-gallery.component.html',
  styleUrls: ['./image-gallery.component.css'],
})
export class ImageGalleryComponent implements OnInit {
  currentIndex = 0;
  @Input() images: Image[] = [];
  @Output() closeOverlay = new EventEmitter();
  constructor() {}

  ngOnInit(): void {}

  previous(): void {
    if (this.currentIndex > 0) {
      this.currentIndex = this.currentIndex - 1;
    } else {
      this.currentIndex = this.images.length - 1;
    }
  }

  forward(): void {
    if (this.currentIndex + 1 < this.images.length) {
      this.currentIndex = this.currentIndex + 1;
    } else {
      this.currentIndex = 0;
    }
  }

  close(): void {
    this.closeOverlay.emit();
  }
}
