import { Component, Input, OnInit } from '@angular/core';
import { IdValue } from 'src/app/tenant/services/http/static-data-service/staticData';
import { Station } from 'src/app/tenant/models/station/station';
import { Vehicle } from 'src/app/tenant/models/vehicle/vehicle';

@Component({
  selector: 'app-vehicle-headline',
  templateUrl: './vehicle-headline.component.html',
  styleUrls: ['./vehicle-headline.component.css'],
})
export class VehicleHeadlineComponent implements OnInit {
  @Input() vehicle: Vehicle | null = null;
  @Input() station: Station | null = null;
  @Input() vehicleModels: IdValue[] = [];
  constructor() {}

  ngOnInit(): void {}

  findVehicleModel(id: number): IdValue {
    const model = this.vehicleModels.find((e) => e.id === id);
    if (model) {
      return model;
    } else {
      return { id: 0, name: '' };
    }
  }
}
