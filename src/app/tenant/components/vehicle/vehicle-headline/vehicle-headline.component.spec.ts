import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleHeadlineComponent } from './vehicle-headline.component';

describe('VehicleHeadlineComponent', () => {
  let component: VehicleHeadlineComponent;
  let fixture: ComponentFixture<VehicleHeadlineComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VehicleHeadlineComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleHeadlineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
