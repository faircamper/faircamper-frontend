import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-vehicle-description',
  templateUrl: './vehicle-description.component.html',
  styleUrls: ['./vehicle-description.component.css'],
})
export class VehicleDescriptionComponent implements OnInit {
  @Input() description: string = '';
  showMore = false;
  constructor() {}

  ngOnInit(): void {}
}
