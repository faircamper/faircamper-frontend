import { Component, Input, OnInit } from '@angular/core';
import {
  Vehicle,
  VehicleSearchObject,
} from 'src/app/tenant/models/vehicle/vehicle';

@Component({
  selector: 'app-vehicle-search-result-preview-card',
  templateUrl: './vehicle-search-result-preview-card.component.html',
  styleUrls: ['./vehicle-search-result-preview-card.component.css'],
})
export class VehicleSearchResultPreviewCardComponent implements OnInit {
  @Input() vehicle: VehicleSearchObject | null = null;
  @Input() datesSelected = false;

  constructor() {}

  ngOnInit(): void {}
}
