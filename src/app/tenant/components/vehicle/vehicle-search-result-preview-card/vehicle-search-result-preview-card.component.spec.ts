import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleSearchResultPreviewCardComponent } from './vehicle-search-result-preview-card.component';

describe('VehicleSearchResultPreviewCardComponent', () => {
  let component: VehicleSearchResultPreviewCardComponent;
  let fixture: ComponentFixture<VehicleSearchResultPreviewCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VehicleSearchResultPreviewCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleSearchResultPreviewCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
