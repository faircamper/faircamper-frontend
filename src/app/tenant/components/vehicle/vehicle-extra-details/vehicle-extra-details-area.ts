export interface Area {

  name: string
  icon: string
  optionalExtras: OptionalExtras[]
}

export class OptionalExtras {

  currentAmount = 0
  maxAmount = 1
  name = ""
  price = 0
  priceUnit = "pro Buchung"
  quantity = 0
  calculationType = ""
  amount :any = ""
}
