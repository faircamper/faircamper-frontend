import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleExtraDetailsComponent } from './vehicle-extra-details.component';

describe('VehicleExtraDetailsComponent', () => {
  let component: VehicleExtraDetailsComponent;
  let fixture: ComponentFixture<VehicleExtraDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VehicleExtraDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleExtraDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
