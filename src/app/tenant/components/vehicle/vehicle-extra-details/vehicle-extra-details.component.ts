import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { Vehicle } from 'src/app/tenant/models/vehicle/vehicle';
import { OptionalExtras } from './vehicle-extra-details-area';

@Component({
  selector: 'app-vehicle-extra-details',
  templateUrl: './vehicle-extra-details.component.html',
  styleUrls: ['./vehicle-extra-details.component.css'],
})
export class VehicleExtraDetailsComponent implements OnInit, OnChanges {
  @Input() vehicle: Vehicle | undefined;
  @Input() dayLength: any | undefined;
  extraData: any;
  tooltip_status = -1;
  totalAmount: number = 0;
  @Output() totalExtraAmount = new EventEmitter<number>();
  message: string = '';
  previousDays: any;
  constructor() {}

  ngOnInit(): void {
    if (this.vehicle == undefined) return;

    let extras: OptionalExtras[] = [];
    this.vehicle.equipmentItems?.forEach((item) => {
      let extra: OptionalExtras = new OptionalExtras();
      extra.name = item.description;
      extra.calculationType = item.calculationType;
      extra.price = Number(item.price);
      extra.maxAmount = Number(item.maxPrice);
      extra.amount = parseInt(
        String(Number(item.maxPrice) / Number(item.price))
      );
      extras.push(extra);
    });
    this.extraData = {
      areas: [
        {
          name: 'Allgemeines',
          icon: 'star',
          optionalExtras: [extras[0], extras[1]],
        },
        {
          name: 'Fahrerkabine',
          icon: 'driverCabin',
          optionalExtras: [extras[2], extras[9]],
        },
        {
          name: 'Küche',
          icon: 'forkKnife',
          optionalExtras: [extras[3]],
        },
        {
          name: 'Bad',
          icon: 'shower',
          optionalExtras: [extras[4]],
        },
        {
          name: 'Wohnraum',
          icon: 'sofa',
          optionalExtras: [extras[5]],
        },
        {
          name: 'Fahrzeug',
          icon: 'motorhome',
          optionalExtras: [extras[6]],
        },
        {
          name: 'Sicherheit',
          icon: 'shield',
          optionalExtras: [extras[7]],
        },
        {
          name: 'Winterbetrieb',
          icon: 'snowflake',
          optionalExtras: [extras[8]],
        },
      ],
    };
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['dayLength'].previousValue !== this.previousDays) {
      this.daysChanged(changes['dayLength'].previousValue, this.dayLength);
      this.previousDays = changes['dayLength'].previousValue;
    }
  }

  daysChanged(prevDays: any, nowDays: any) {
    this.totalAmount = 0;
    let dropDowns = ['perPiece', 'perPieceAndNight'];
    let checkbox = ['perNight', 'perBooking', 'inclusive'];
    for (let i = 0; i < this.extraData.areas.length; i++) {
      for (let j = 0; j < this.extraData.areas[i].optionalExtras.length; j++) {
        let item = this.extraData.areas[i].optionalExtras[j];
        if (item.calculationType == 'inclusive') {
          item.quantity = 1;
        }
        if (dropDowns.includes(item.calculationType)) {
          if (item.quantity > 0) {
            let total = item.quantity * item.price;
            if (item.calculationType == 'perPieceAndNight') {
              total = total * nowDays;
            }
            item.currentAmount = total;
            this.totalAmount = this.totalAmount + total;
          }
        } else {
          if (item.quantity > 0 && item.calculationType != 'inclusive') {
            let total = item.quantity * item.price * nowDays;
            if (item.calculationType == 'perBooking') {
              total = item.price;
            }
            item.currentAmount = total;
            this.totalAmount = this.totalAmount + total;
          }
        }
        this.extraData.areas[i].optionalExtras[j] = item;
        this.totalExtraAmount.emit(this.totalAmount);
      }
    }
  }

  calculateTotalDropdown(index: any, arrayIndex: any, item: any, $event: any) {
    let amount = 0;
    if ($event.target.value > 0) {
      if (item.calculationType == 'perPieceAndNight') {
        let total = $event.target.value * item.price * this.dayLength;
        if (
          this.extraData.areas[arrayIndex].optionalExtras[index].quantity > 0
        ) {
          let previousTotal =
            this.extraData.areas[arrayIndex].optionalExtras[index].quantity *
            item.price *
            this.dayLength;
          this.totalAmount = amount = this.totalAmount + total - previousTotal;
        } else {
          this.totalAmount = amount = this.totalAmount + total;
        }
        this.extraData.areas[arrayIndex].optionalExtras[index].currentAmount =
          total;
        this.extraData.areas[arrayIndex].optionalExtras[index].quantity =
          $event.target.value;
      } else {
        let total = $event.target.value * item.price;
        if (
          this.extraData.areas[arrayIndex].optionalExtras[index].quantity > 0
        ) {
          let previousTotal =
            this.extraData.areas[arrayIndex].optionalExtras[index].quantity *
            item.price;
          this.totalAmount = amount = this.totalAmount + total - previousTotal;
        } else {
          this.totalAmount = amount = this.totalAmount + total;
        }
        this.extraData.areas[arrayIndex].optionalExtras[index].currentAmount =
          total;
        this.extraData.areas[arrayIndex].optionalExtras[index].quantity =
          $event.target.value;
      }
    } else {
      if (item.calculationType == 'perPieceAndNight') {
        amount = this.totalAmount =
          this.totalAmount -
          this.extraData.areas[arrayIndex].optionalExtras[index].quantity *
            item.price *
            this.dayLength;
      } else {
        amount = this.totalAmount =
          this.totalAmount -
          this.extraData.areas[arrayIndex].optionalExtras[index].quantity *
            item.price;
      }
      this.extraData.areas[arrayIndex].optionalExtras[index].currentAmount = 0;
      this.extraData.areas[arrayIndex].optionalExtras[index].quantity = 0;
    }
    this.totalExtraAmount.emit(this.totalAmount);
  }

  calculateTotalCheckbox(index: any, arrayIndex: any, item: any, $event: any) {
    if (item.calculationType == 'inclusive') {
      if ($event.target.checked) {
        this.extraData.areas[arrayIndex].optionalExtras[index].quantity = 1;
      } else {
        this.extraData.areas[arrayIndex].optionalExtras[index].quantity = 0;
      }
      return;
    }
    let amount = 0;
    if ($event.target.checked) {
      let total: any;
      if (item.calculationType == 'perBooking') {
        total = item.price;
      } else {
        total = this.dayLength * item.price;
      }
      if (this.extraData.areas[arrayIndex].optionalExtras[index].quantity > 0) {
        let previousTotal: any;
        if (item.calculationType == 'perBooking') {
          previousTotal = item.price;
        } else {
          previousTotal = this.dayLength * item.price;
        }
        this.totalAmount = amount = this.totalAmount + total - previousTotal;
      } else {
        if (this.dayLength > 0 || item.calculationType == 'perBooking') {
          this.totalAmount = amount = this.totalAmount + total;
        }
      }
      this.extraData.areas[arrayIndex].optionalExtras[index].currentAmount =
        total;
      this.extraData.areas[arrayIndex].optionalExtras[index].quantity = 1;
    } else {
      if (this.extraData.areas[arrayIndex].optionalExtras[index].quantity > 0) {
        let previousTotal: any;
        if (item.calculationType == 'perBooking') {
          previousTotal = item.price;
        } else {
          previousTotal = this.dayLength * item.price;
        }
        this.extraData.areas[arrayIndex].optionalExtras[
          index
        ].currentAmount = 0;
        this.extraData.areas[arrayIndex].optionalExtras[index].quantity = 0;
        this.totalAmount = amount = this.totalAmount - previousTotal;
      }
    }
    this.totalExtraAmount.emit(this.totalAmount);
  }

  setCalculationType(type: any) {
    if (type == 'perPiece') {
      return 'pro Stück';
    } else if (type == 'perNight') {
      return 'pro Nacht';
    } else if (type == 'perPieceAndNight') {
      return 'pro Stück und Nacht';
    } else if (type == 'perBooking') {
      return 'pro Buchung';
    } else if (type == 'inclusive') {
      return 'inklusive';
    } else {
      return '';
    }
  }

  createArray(time: any) {
    if (time > 0) {
      let arr = [];
      for (let i = 0; i < time; i++) {
        arr.push(i + 1);
      }
      return arr;
    } else {
      return [1];
    }
  }
}
