import { Component, Input, OnInit } from '@angular/core';
import { StaticDataService } from 'src/app/tenant/services/http/static-data-service/static-data.service';
import { Vehicle } from 'src/app/tenant/models/vehicle/vehicle';

@Component({
  selector: 'app-vehicle-tech-details',
  templateUrl: './vehicle-tech-details.component.html',
  styleUrls: ['./vehicle-tech-details.component.css'],
})
export class VehicleTechDetailsComponent implements OnInit {
  @Input() vehicle: Vehicle | undefined;
  constructor(private staticDataService: StaticDataService) {}

  ngOnInit(): void {}

  getBedsDesignation(id: number) {
    return this.staticDataService.bedTypeIdToName(id);
  }

  getGearName(id: number) {
    return this.staticDataService.gearTypeIdToName(id);
  }
  getConsumptionName(id: number) {
    return this.staticDataService.consumptionTypeIdToName(id);
  }

  getModelName(id: number) {
    return this.staticDataService.modelTypeIdToName(id);
  }
  getFuelName(id: number) {
    return this.staticDataService.fuelTypeIdToName(id);
  }
  getWeightName(id: number) {
    return this.staticDataService.weightTypeIdToName(id);
  }
}
