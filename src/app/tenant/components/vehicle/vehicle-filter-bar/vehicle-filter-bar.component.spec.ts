import { CurrencyPipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import {
  ActivatedRoute,
  convertToParamMap,
  ParamMap,
  Params,
} from '@angular/router';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

import { VehicleFilterBarComponent } from './vehicle-filter-bar.component';

/********************************/
/********************************/
/********************************/
//  testing setFilters function
/********************************/
/********************************/
/********************************/

describe('VehicleFilterBarComponent - setFilters()', () => {
  let component: VehicleFilterBarComponent;
  let fixture: ComponentFixture<VehicleFilterBarComponent>;
  const fakeActivatedRoute = {
    queryParams: of(convertToParamMap({})),
  } as unknown as ActivatedRoute;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [VehicleFilterBarComponent],
      imports: [HttpClientTestingModule],
      providers: [
        CurrencyPipe,
        { provide: ActivatedRoute, useValue: fakeActivatedRoute },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleFilterBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('no filter is entered should generate long=54.1231&lat=10.56', async () => {
    component.setFilters().subscribe((data) => {
      const expectedResult = 'long=54.1231&lat=10.56';
      const result = data;
      expect(result).toEqual(expectedResult);
    });
  });
});

describe('VehicleFilterBarComponent - setFilters() - long = 10 & lat = 10', () => {
  let component: VehicleFilterBarComponent;
  let fixture: ComponentFixture<VehicleFilterBarComponent>;
  const fakeActivatedRoute = {
    queryParams: of({ long: 10, lat: 10 }),
  } as unknown as ActivatedRoute;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [VehicleFilterBarComponent],
      providers: [
        CurrencyPipe,
        { provide: ActivatedRoute, useValue: fakeActivatedRoute },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleFilterBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should generate long=10&lat=10', async () => {
    component.setFilters().subscribe((data) => {
      const expectedResult = 'long=10&lat=10';
      const result = data;
      expect(result).toEqual(expectedResult);
    });
  });
});

describe('VehicleFilterBarComponent - setFilters() - spa = true', () => {
  let component: VehicleFilterBarComponent;
  let fixture: ComponentFixture<VehicleFilterBarComponent>;
  const fakeActivatedRoute = {
    queryParams: of({ spa: true }),
  } as unknown as ActivatedRoute;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [VehicleFilterBarComponent],
      providers: [
        CurrencyPipe,
        { provide: ActivatedRoute, useValue: fakeActivatedRoute },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleFilterBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('spa should be true and generate long=54.1231&lat=10.56&spa=true', async () => {
    component.setFilters().subscribe((data) => {
      const expectedResult = 'long=54.1231&lat=10.56&spa=true';
      const result = data;
      expect(result).toEqual(expectedResult);
    });
  });
});

describe('VehicleFilterBarComponent - setFilters() - fr=2021-11-23&to=2021-11-28', () => {
  let component: VehicleFilterBarComponent;
  let fixture: ComponentFixture<VehicleFilterBarComponent>;
  const fakeActivatedRoute = {
    queryParams: of({ fr: '2021-11-23', to: '2021-11-28' }),
  } as unknown as ActivatedRoute;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [VehicleFilterBarComponent],
      providers: [
        CurrencyPipe,
        { provide: ActivatedRoute, useValue: fakeActivatedRoute },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleFilterBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should generate long=54.1231&lat=10.56&fr=2021-11-23&to=2021-11-28', async () => {
    component.setFilters().subscribe((data) => {
      const expectedResult =
        'long=54.1231&lat=10.56&fr=2021-11-23&to=2021-11-28';
      const result = data;
      expect(result).toEqual(expectedResult);
    });
  });
});

describe('VehicleFilterBarComponent - setFilters() - pf=1&pt=99', () => {
  let component: VehicleFilterBarComponent;
  let fixture: ComponentFixture<VehicleFilterBarComponent>;
  const fakeActivatedRoute = {
    queryParams: of({ pf: '1', pt: '99' }),
  } as unknown as ActivatedRoute;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [VehicleFilterBarComponent],
      providers: [
        CurrencyPipe,
        { provide: ActivatedRoute, useValue: fakeActivatedRoute },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleFilterBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should generate long=54.1231&lat=10.56&pf=100&pt=9900', async () => {
    component.setFilters().subscribe((data) => {
      const expectedResult = 'long=54.1231&lat=10.56&pf=100&pt=9900';
      const result = data;
      expect(result).toEqual(expectedResult);
    });
  });
});

describe('VehicleFilterBarComponent - setFilters() - np=3', () => {
  let component: VehicleFilterBarComponent;
  let fixture: ComponentFixture<VehicleFilterBarComponent>;
  const fakeActivatedRoute = {
    queryParams: of({ np: '3' }),
  } as unknown as ActivatedRoute;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [VehicleFilterBarComponent],
      providers: [
        CurrencyPipe,
        { provide: ActivatedRoute, useValue: fakeActivatedRoute },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleFilterBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should generate long=54.1231&lat=10.56&np=3', async () => {
    component.setFilters().subscribe((data) => {
      const expectedResult = 'long=54.1231&lat=10.56&np=3';
      const result = data;
      expect(result).toEqual(expectedResult);
    });
  });
});

describe('VehicleFilterBarComponent - setFilters() - vt=1', () => {
  let component: VehicleFilterBarComponent;
  let fixture: ComponentFixture<VehicleFilterBarComponent>;
  const fakeActivatedRoute = {
    queryParams: of({ vt: [1] }),
  } as unknown as ActivatedRoute;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [VehicleFilterBarComponent],
      providers: [
        CurrencyPipe,
        { provide: ActivatedRoute, useValue: fakeActivatedRoute },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleFilterBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should generate long=54.1231&lat=10.56&vt=1', async () => {
    component.setFilters().subscribe((data) => {
      const expectedResult = 'long=54.1231&lat=10.56&vt=1';
      const result = data;
      expect(result).toEqual(expectedResult);
    });
  });
});

describe('VehicleFilterBarComponent - setFilters() - eq=[11,12,13]', () => {
  let component: VehicleFilterBarComponent;
  let fixture: ComponentFixture<VehicleFilterBarComponent>;
  const fakeActivatedRoute = {
    queryParams: of({ eq: [11, 12, 13] }),
  } as unknown as ActivatedRoute;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [VehicleFilterBarComponent],
      providers: [
        CurrencyPipe,
        { provide: ActivatedRoute, useValue: fakeActivatedRoute },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleFilterBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should generate long=54.1231&lat=10.56&eq=11&eq=12&eq=13', async () => {
    component.setFilters().subscribe((data) => {
      const expectedResult = 'long=54.1231&lat=10.56&eq=11&eq=12&eq=13';
      const result = data;
      expect(result).toEqual(expectedResult);
    });
  });
});

/********************************/
/********************************/
/********************************/
//  testing setQuerryParams function
/********************************/
/********************************/
/********************************/

describe('VehicleFilterBarComponent - setQuerryParams() - none', () => {
  let component: VehicleFilterBarComponent;
  let fixture: ComponentFixture<VehicleFilterBarComponent>;
  const fakeActivatedRoute = {
    queryParams: of({}),
  } as unknown as ActivatedRoute;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [VehicleFilterBarComponent],
      providers: [
        CurrencyPipe,
        { provide: ActivatedRoute, useValue: fakeActivatedRoute },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleFilterBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should generate QuerryParams - none triggering fallback {long: 54.1231, lat: 10.56}', async () => {
    const result = component.setQuerryParams();
    const expectedResult = { long: 54.1231, lat: 10.56 };
    expect(result).toEqual(expectedResult);
  });

  it('should generate QuerryParams - lat=11 long=12 ending in: {long: 12, lat: 11}', async () => {
    component.geoControl.controls['long'].patchValue(12);
    component.geoControl.controls['lat'].patchValue(11);
    const result = component.setQuerryParams();
    const expectedResult = { long: 12, lat: 11 };
    expect(result).toEqual(expectedResult);
  });

  it('should generate QuerryParams - fr=2021-12-01 to=2021-12-14 ending in:  {long: 54.1231, lat: 10.56, fr: 2021-12-01, to: 2021-12-14}', async () => {
    component.dateControl.controls['from'].patchValue('2021-12-01');
    component.dateControl.controls['to'].patchValue('2021-12-14');
    const result = component.setQuerryParams();
    const expectedResult = {
      long: 54.1231,
      lat: 10.56,
      fr: '2021-12-01',
      to: '2021-12-14',
    };
    expect(result).toEqual(expectedResult);
  });

  it('should generate QuerryParams - np=4 ending in:  {long: 54.1231, lat: 10.56, np:4 }', async () => {
    component.numberOfPassengers = 4;
    const result = component.setQuerryParams();
    const expectedResult = {
      long: 54.1231,
      lat: 10.56,
      np: 4,
    };
    expect(result).toEqual(expectedResult);
  });

  it('should generate QuerryParams - spa=true ending in:  {long: 54.1231, lat: 10.56, spa:true }', async () => {
    component.pets = true;
    const result = component.setQuerryParams();
    const expectedResult = {
      long: 54.1231,
      lat: 10.56,
      spa: true,
    };
    expect(result).toEqual(expectedResult);
  });

  it('should generate QuerryParams - eq=21 eq=22 eq=23 ending in:  {long: 54.1231, lat: 10.56, eq:[21,22,23] }', async () => {
    component.checkedEquipments = [21, 22, 23];
    const result = component.setQuerryParams();
    const expectedResult = {
      long: 54.1231,
      lat: 10.56,
      eq: [21, 22, 23],
    };
    expect(result).toEqual(expectedResult);
  });

  it('should generate QuerryParams - vt=0 vt=5 vt=4 ending in:  {long: 54.1231, lat: 10.56, vt:[0,5,4] }', async () => {
    component.activeVehicleTypes = [0, 5, 4];
    const result = component.setQuerryParams();
    const expectedResult = {
      long: 54.1231,
      lat: 10.56,
      vt: [0, 5, 4],
    };
    expect(result).toEqual(expectedResult);
  });
});
