import { CurrencyPipe } from '@angular/common';
import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { distinctUntilChanged, filter, map } from 'rxjs/operators';
import { IdValue } from 'src/app/tenant/services/http/static-data-service/staticData';
import { EquipmentCategory } from 'src/app/tenant/models/equipment/equipment';
import { CurrencyService } from 'src/app/tenant/services/currency/currency.service';
import { Store } from '@ngrx/store';
import { setTravelDataAction } from 'src/app/tenant/ngrx/actions/travelData.actions';
import { searchDataSetAction } from 'src/app/tenant/ngrx/actions/searchData.actions ';

@Component({
  selector: 'app-vehicle-filter-bar',
  templateUrl: './vehicle-filter-bar.component.html',
  styleUrls: ['./vehicle-filter-bar.component.css'],
})
export class VehicleFilterBarComponent implements OnInit, OnDestroy {
  @Input() vehicleTypes: IdValue[] = [];
  @Input() equipmentCategories: EquipmentCategory[] = [];
  @Output() filtersChanged = new EventEmitter();
  @Output() initialFiltersSet = new EventEmitter();
  pets = false;
  numberOfPassengers = 0;
  checkedEquipments: number[] = [];
  eqCatsToSHowAll: number[] = [];
  activeVehicleTypes: number[] = [];
  dateControl = new FormGroup({
    from: new FormControl(
      new Date().getFullYear() +
        '-' +
        this.toTwoDigits(new Date().getMonth() + 1) +
        '-' +
        this.toTwoDigits(new Date().getDate())
    ),
    to: new FormControl(''),
  });
  geoControl = new FormGroup({
    long: new FormControl(54.1231),
    lat: new FormControl(10.56),
  });
  priceControl = new FormGroup({
    from: new FormControl(''),
    to: new FormControl(''),
  });

  filters: string = '';
  dateSubs: Subscription = new Subscription();
  geoSubs: Subscription = new Subscription();
  priceSubs: Subscription = new Subscription();
  filtersub: Subscription = new Subscription();
  searchSub: Subscription = new Subscription();

  get fromPrice(): any {
    return this.priceControl?.get('from');
  }
  get toPrice(): any {
    return this.priceControl?.get('to');
  }
  constructor(
    private activatedRoute: ActivatedRoute,
    private currencyPipe: CurrencyPipe,
    private currencyService: CurrencyService,
    private store: Store
  ) {}

  ngOnInit(): void {
    this.dateSubs = this.dateControl.valueChanges.subscribe((data) => {
      if (data.from && data.to) {
        this.reload();
      }
    });
    this.geoSubs = this.geoControl.valueChanges
      .pipe(distinctUntilChanged())
      .subscribe((data) => {
        if (data.long && data.lat) {
          this.reload();
        }
      });
    this.priceSubs = this.priceControl.valueChanges.subscribe((data) => {
      if (data.from && data.to) {
        this.reload();
      }
    });
    this.filtersub = this.setFilters().subscribe((filters) => {
      this.formatFromPrice();
      this.formatToPrice();
      this.initialFiltersSet.emit(filters);
    });
  }

  formatFromPrice(): void {
    const inputStr = this.currencyService.cleanCurrencyFormat(
      this.fromPrice.value
    );
    this.fromPrice.patchValue(this.currencyPipe.transform(inputStr, 'EUR'));
  }
  formatToPrice(): void {
    const inputStr = this.currencyService.cleanCurrencyFormat(
      this.toPrice.value
    );
    this.toPrice.patchValue(this.currencyPipe.transform(inputStr, 'EUR'));
  }
  ngOnDestroy(): void {
    this.priceSubs.unsubscribe();
    this.dateSubs.unsubscribe();
    this.geoSubs.unsubscribe();
    this.filtersub.unsubscribe();
    this.searchSub.unsubscribe();
  }
  reload(): void {
    this.filtersChanged.emit(this.setQuerryParams());
  }
  toTwoDigits(num: number | string): string {
    num += '';
    if ((num as string).length === 2) {
      return num as string;
    } else if ((num as string).length > 2) {
      return '-1';
    } else {
      return '0' + num;
    }
  }

  toggleEq(id: number): any {
    const i = this.checkedEquipments.indexOf(id);
    if (i > -1) {
      this.checkedEquipments.splice(i, 1);
    } else {
      this.checkedEquipments.push(id);
    }
    this.reload();
  }

  checkActiveEq(id: number): boolean {
    const i = this.checkedEquipments.indexOf(id);
    if (i > -1) {
      return true;
    } else {
      return false;
    }
  }

  checkShowAllOfEquipmentsCategory(id: number): boolean {
    const i = this.eqCatsToSHowAll.indexOf(id);
    if (i > -1) {
      return true;
    } else {
      return false;
    }
  }

  showAllOfThisEquipmentCategory(id: number): void {
    const i = this.eqCatsToSHowAll.indexOf(id);
    if (i > -1) {
      this.eqCatsToSHowAll.splice(i, 1);
    } else {
      this.eqCatsToSHowAll.push(id);
    }
  }

  toggleVehicleType(id: number) {
    const i = this.activeVehicleTypes.indexOf(id);
    if (i > -1) {
      this.activeVehicleTypes.splice(i, 1);
    } else {
      this.activeVehicleTypes.push(id);
    }
    this.reload();
  }

  checkVehicleTypeFilter(id: number): boolean {
    const i = this.activeVehicleTypes.indexOf(id);
    if (i > -1) {
      return true;
    } else {
      return false;
    }
  }

  setQuerryParams(): any {
    const querryParams: any = {};
    if (
      this.geoControl.controls['long'].value &&
      this.geoControl.controls['lat'].value
    ) {
      querryParams.long = this.geoControl.controls['long'].value;
      querryParams.lat = this.geoControl.controls['lat'].value;
    }
    if (
      this.dateControl.controls['from'].value &&
      this.dateControl.controls['to'].value
    ) {
      const from = new Date(this.dateControl.controls['from'].value).getTime();
      const to = new Date(this.dateControl.controls['to'].value).getTime();
      this.store.dispatch(
        searchDataSetAction({
          searchData: { startDate: new Date(from), endDate: new Date(to) },
        })
      );
      if (from <= to) {
        querryParams.fr = this.dateControl.controls['from'].value;
        querryParams.to = this.dateControl.controls['to'].value;
      }
    }
    const from = this.currencyService.cleanCurrencyFormat(
      this.priceControl.controls['from'].value
    );
    const to = this.currencyService.cleanCurrencyFormat(
      this.priceControl.controls['to'].value
    );
    if (from !== 0 || to !== 0) {
      if (!from && to) {
        querryParams.pf = 0;
        querryParams.pt = to;
      } else if (from && to) {
        const cleanFrom = from;
        const cleanTo = to;
        if (cleanFrom <= cleanTo) {
          querryParams.pf = cleanFrom;
          querryParams.pt = cleanTo;
        }
      } else if (from && !to) {
        querryParams.pf = from;
      }
    }

    if (this.numberOfPassengers > 0) {
      querryParams.np = this.numberOfPassengers;
    }
    if (this.pets) {
      querryParams.spa = true;
    }
    if (this.checkedEquipments.length > 0) {
      querryParams.eq = this.checkedEquipments;
    }
    if (this.activeVehicleTypes.length > 0) {
      let vtIds = [];
      for (let index = 0; index < this.activeVehicleTypes.length; index++) {
        vtIds.push(this.activeVehicleTypes[index]);
      }
      querryParams.vt = vtIds;
    }
    return querryParams;
  }

  setFilters(): Observable<string> {
    return this.activatedRoute.queryParams.pipe(
      map((params) => {
        if (params['long']) {
          this.geoControl.get('long')?.setValue(params['long']);
        }
        if (params['lat']) {
          this.geoControl.get('lat')?.setValue(params['lat']);
        }
        if (params['fr']) {
          this.dateControl.get('from')?.setValue(params['fr']);
        }
        if (params['to']) {
          this.dateControl.get('to')?.setValue(params['to']);
        }
        if (params['pf']) {
          this.priceControl.get('from')?.setValue(+params['pf']);
        }
        if (params['pt']) {
          this.priceControl.get('to')?.setValue(+params['pt']);
        }
        if (params['np']) {
          this.numberOfPassengers = +params['np'];
        }
        if (params['spa']) {
          this.pets = params['spa'];
        }
        if (params['eq']) {
          if (typeof params['eq'] === 'string') {
            this.checkedEquipments.push(+params['eq']);
          } else {
            this.checkedEquipments = params['eq'].map((entry: string) =>
              Number(entry)
            );
          }
        }
        if (params['vt']) {
          if (typeof params['vt'] === 'string') {
            this.activeVehicleTypes.push(+params['vt']);
          } else {
            this.activeVehicleTypes = params['vt'].map((entry: string) =>
              Number(entry)
            );
          }
        }
        this.filters =
          (params['long'] ? 'long=' + params['long'] : 'long=54.1231') +
          (params['lat'] ? '&lat=' + params['lat'] : '&lat=10.56') +
          (params['fr'] ? '&fr=' + params['fr'] : '') +
          (params['to'] ? '&to=' + params['to'] : '') +
          (params['pf'] ? '&pf=' + params['pf'] * 100 : '') +
          (params['pt'] ? '&pt=' + params['pt'] * 100 : '') +
          (params['np'] ? '&np=' + params['np'] : '') +
          (params['spa'] ? '&spa=' + params['spa'] : '');
        if (params['vt']) {
          params['vt'].forEach((element: string) => {
            this.filters += '&vt=' + element;
          });
        }
        if (params['eq']) {
          params['eq'].forEach((element: string) => {
            this.filters += '&eq=' + element;
          });
        }
        return this.filters;
      })
    );
  }
}
