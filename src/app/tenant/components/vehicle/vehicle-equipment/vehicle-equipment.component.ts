import { Component, Input, OnInit } from '@angular/core';
import { EquipmentService } from 'src/app/tenant/services/http/equipment/equipment.service';
import {
  Equipment,
  EquipmentCategory,
} from 'src/app/tenant/models/equipment/equipment';

@Component({
  selector: 'app-vehicle-equipment',
  templateUrl: './vehicle-equipment.component.html',
  styleUrls: ['./vehicle-equipment.component.css'],
})
export class VehicleEquipmentComponent implements OnInit {
  @Input() equipments: Equipment[] = [];
  equipmentCategories: EquipmentCategory[] = [];
  vehicleEquipmentCategories: EquipmentCategory[] = [];
  showMore = false;
  additionalExtras: any = [];
  constructor(private equipmentService: EquipmentService) {}

  ngOnInit(): void {
    this.equipmentService.listEquipmentCategories().subscribe((data) => {
      for (let i = 0; i < data.length; i++) {
        for (let v = 0; v < data[i].children.length; v++) {
          this.additionalExtras.push(data[i].children[v]);
        }
      }
      this.equipmentCategories = data;
      this.sortCategories();
    });
  }

  moveToExtra() {
    if (location.href.includes('#extrasPage')) {
      location.href = location.href;
    } else {
      location.href = location.href + '#extrasPage';
    }
  }

  sortCategories() {
    this.equipmentCategories.forEach((element) => {
      this.equipments.forEach((e) => {
        if (
          e.categoryId === element.id &&
          !this.vehicleEquipmentCategories.find((d) => d.id === e.categoryId)
        ) {
          this.vehicleEquipmentCategories.push(element);
        }
      });
    });
  }
  getCatgeory(id: number): EquipmentCategory | undefined {
    return this.equipmentCategories.find((e) => e.id === id);
  }
}
