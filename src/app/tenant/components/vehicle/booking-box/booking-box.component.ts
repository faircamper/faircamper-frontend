import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, EMPTY } from 'rxjs';
import { StaticDataService } from 'src/app/tenant/services/http/static-data-service/static-data.service';
import { IdValue } from 'src/app/tenant/services/http/static-data-service/staticData';
import { VehicleService } from 'src/app/tenant/services/http/vehicle/vehicle.service';
import { OpeningHours } from 'src/app/tenant/models/station/station-opening-hours';
import { Vehicle } from 'src/app/tenant/models/vehicle/vehicle';
import { AvailabilityDay } from 'src/app/tenant/models/availability/availability';

@Component({
  selector: 'app-booking-box',
  templateUrl: './booking-box.component.html',
  styleUrls: ['./booking-box.component.css'],
})
export class BookingBoxComponent {
  vehicle$: Observable<Vehicle> = EMPTY;
  vehicleModels: IdValue[] = [];
  @Input() totalExtraAmount = 0;
  @Output() updateDays: EventEmitter<any> = new EventEmitter<any>();
  nights = 0;
  totalAmount = 0;
  tooltip_status = -1;
  tooltip_status1 = -1;
  selectedDates: string | undefined = '';
  title = 'Reisezeitraum wählen';
  calendarOverlayIsOpen = false;
  isShowDiv = true;
  textBeforePrice = 'ab';
  @Input() openings?: OpeningHours[];
  apiLoaded: any;

  constructor(
    public router: Router,
    private vehicleService: VehicleService,
    private activatedRoute: ActivatedRoute,
    private staticDataService: StaticDataService
  ) {}
  vehicle: any = [];
  ngOnInit(): void {
    const vehicleId = this.activatedRoute.snapshot.paramMap.get('id');
    if (vehicleId) {
      this.vehicle$ = this.vehicleService.get(vehicleId);
      this.vehicleService.get<Vehicle>(vehicleId).subscribe((data) => {
        this.vehicle = data;
      });
    }
  }
  toggleDateModal() {
    this.calendarOverlayIsOpen = !this.calendarOverlayIsOpen;
  }

  toggleDisplayDiv() {
    this.isShowDiv = !this.isShowDiv;
  }

  updateData(data: { days: AvailabilityDay[] }) {
    const days = data.days;
    console.log(days.length);
    let aggregatedPrice = 0;
    days.forEach((e) => {
      aggregatedPrice += e.price;
    });
    let daysLength = days.length;
    const meanPrice = aggregatedPrice / days.length;
    this.nights = days.length;
    this.updateDays.emit(daysLength);
    if (aggregatedPrice) {
      this.apiLoaded = 1;
      this.vehicle.minPrice = meanPrice;
      this.vehicle.aggregatedPrice = aggregatedPrice / 100;
      // this.vehicle.fee = fee / 100;
      this.vehicle.fee = 100;
      // this.vehicle.isSmallBusiness = data.isSmallBusiness;
      this.vehicle.isSmallBusiness = false;
      this.textBeforePrice = 'Ø';
      this.totalAmount =
        this.vehicle.aggregatedPrice + this.vehicle.fee + this.totalExtraAmount;
    } else {
      this.apiLoaded = 0;
      this.totalAmount = 0;
    }
  }
}
