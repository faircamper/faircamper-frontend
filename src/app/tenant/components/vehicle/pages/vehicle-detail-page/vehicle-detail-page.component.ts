import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EMPTY, Observable } from 'rxjs';
import { StaticDataService } from 'src/app/tenant/services/http/static-data-service/static-data.service';
import { IdValue } from 'src/app/tenant/services/http/static-data-service/staticData';
import { StationService } from 'src/app/tenant/services/http/station/station.service';
import { VehicleService } from 'src/app/tenant/services/http/vehicle/vehicle.service';
import { Station } from 'src/app/tenant/models/station/station';
import { Vehicle } from 'src/app/tenant/models/vehicle/vehicle';

@Component({
  selector: 'app-vehicle-detail-page',
  templateUrl: './vehicle-detail-page.component.html',
  styleUrls: ['./vehicle-detail-page.component.css'],
})
export class VehicleDetailPageComponent implements OnInit {
  vehicle$: Observable<Vehicle> = EMPTY;
  dayLength: any = 0;

  vehicleModels: IdValue[] = [];
  station: Station | null = null;
  overlay = false;
  totalExtraAmount: number = 0;
  constructor(
    private vehicleService: VehicleService,
    private activatedRoute: ActivatedRoute,
    private staticDataService: StaticDataService,
    private stationService: StationService
  ) {}
  imageObject: any = [];
  ngOnInit(): void {
    const vehicleId = this.activatedRoute.snapshot.paramMap.get('id');
    if (vehicleId) {
      this.vehicle$ = this.vehicleService.get(vehicleId);
      this.vehicleService.get<Vehicle>(vehicleId).subscribe((data) => {
        for (const [key, value] of Object.entries(data.images)) {
          let image: any = {
            image: value.url,
            thumbImage: value.url,
          };
          this.imageObject.push(image);
        }

        this.station = data.station;
        // this.stationService
        //   .get<Station>(data.stationId)
        //   .subscribe((station) => {
        //     console.log("Station:");
        //     console.log(station);
        //     this.station = station;
        //   });
      });
    }
    this.staticDataService.getVehicleModels().subscribe((data) => {
      this.vehicleModels = data;
    });
  }

  findVehicleModel(id: number): IdValue {
    const model = this.vehicleModels.find((e) => e.id === id);
    if (model) {
      return model;
    } else {
      return { id: 0, name: '' };
    }
  }

  showOverlay() {
    this.overlay = true;
  }
  nextprevfunction(id: number, num: number) {
    for (let i = 1; i <= num; i++) {
      let targetElement = <HTMLElement>document.querySelector('.control-' + i);
      let targetElements = <HTMLElement>(
        document.querySelector('.controls-' + i)
      );
      targetElement.style.display = 'none';
      targetElements.style.display = 'none';
      let targetindicator = <HTMLElement>(
        document.querySelector('#indicator-' + i)
      );
      targetindicator.style.color = '#fff';
    }
    let targetElement = <HTMLElement>document.querySelector('.control-' + id);
    let targetElements = <HTMLElement>document.querySelector('.controls-' + id);
    targetElement.style.display = 'block';
    targetElements.style.display = 'block';
    let targetindicator = <HTMLElement>(
      document.querySelector('#indicator-' + id)
    );
    targetindicator.style.color = '#ffb317';
  }

  daysUpdated($event: any) {
    this.dayLength = $event;
  }
}
