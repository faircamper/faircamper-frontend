import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { EquipmentService } from 'src/app/tenant/services/http/equipment/equipment.service';
import { SearchService } from 'src/app/tenant/services/http/search.service';
import { StaticDataService } from 'src/app/tenant/services/http/static-data-service/static-data.service';
import { IdValue } from 'src/app/tenant/services/http/static-data-service/staticData';
import { EquipmentCategory } from 'src/app/tenant/models/equipment/equipment';
import { VehicleSearchObject } from 'src/app/tenant/models/vehicle/vehicle';

@Component({
  selector: 'app-vehicle-list',
  templateUrl: './vehicle-list.component.html',
  styleUrls: ['./vehicle-list.component.css'],
})
export class VehicleListComponent implements OnInit {
  page = 0;
  vehicles: VehicleSearchObject[] = [];
  reloadDebouncer = new Subject<any>();
  equipmentCategories: EquipmentCategory[] = [];
  vehicleTypes: IdValue[] = [];
  filters: string = '';
  selectedDate = false;
  constructor(
    private searchService: SearchService,
    private equipmentService: EquipmentService,
    private staticDataService: StaticDataService,
    private router: Router
  ) {
    this.reloadDebouncer.pipe(debounceTime(500)).subscribe((querryParams) => {
      this.page = 0;
      this.router.navigate([''], {
        queryParams: querryParams,
      });
    });
  }

  ngOnInit(): void {
    this.equipmentService.listEquipmentCategories().subscribe((data) => {
      this.equipmentCategories = data;
      const toDelete = this.equipmentCategories.findIndex((e) => e.id === 104);
      this.equipmentCategories.splice(toDelete, 1);
    });
    this.staticDataService.getVehicleTypes().subscribe((data: IdValue[]) => {
      this.vehicleTypes = data;
    });
  }

  reload(querryParams: any): void {
    if (querryParams.fr || querryParams.to) {
      this.selectedDate = true;
    } else {
      this.selectedDate = false;
    }
    this.reloadDebouncer.next(querryParams);
  }
  initialLoad(filters: string) {
    this.filters = filters;
    const searchSub = this.searchService
      .search(filters + '&p=' + this.page)
      .subscribe((data) => {
        this.vehicles = data;
        searchSub.unsubscribe();
      });
  }
  loadMore() {
    this.page++;
    const searchSub = this.searchService
      .search(this.filters + '&p=' + this.page)
      .subscribe((data) => {
        data.forEach((d) => {
          this.vehicles.push(d);
        });
        searchSub.unsubscribe();
      });
  }
}
