import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleIconBarComponent } from './vehicle-icon-bar.component';

describe('VehicleIconBarComponent', () => {
  let component: VehicleIconBarComponent;
  let fixture: ComponentFixture<VehicleIconBarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VehicleIconBarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleIconBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
