import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-vehicle-icon-bar',
  templateUrl: './vehicle-icon-bar.component.html',
  styleUrls: ['./vehicle-icon-bar.component.css'],
})
export class VehicleIconBarComponent implements OnInit {
  @Input() vehicleTypeId = 0;
  @Input() seats = 1;
  @Input() beds = 1;
  @Input() pets = true;
  @Input() weightId = 0;
  @Input() hasAllFreeKm = false;
  constructor() {}

  ngOnInit(): void {}
}
