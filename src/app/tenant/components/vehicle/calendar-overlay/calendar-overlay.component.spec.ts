import {
  async,
  ComponentFixture,
  TestBed,
  waitForAsync,
} from '@angular/core/testing';
import {
  HttpClient,
  HttpClientModule,
  HttpHandler,
} from '@angular/common/http';

import { CalendarOverlayComponent } from './calendar-overlay.component';
import { ActivatedRoute } from '@angular/router';
import { AvailabilityService } from 'src/app/tenant/services/http/calendar/availability.service';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { FakeApiService } from 'src/app/fake-api.service';
describe('CalendarOverlayComponent', () => {
  let component: CalendarOverlayComponent;
  let fixture: ComponentFixture<CalendarOverlayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: {
                get: () => 123,
              },
            },
          },
        },
        AvailabilityService,
      ],
      imports: [
        HttpClientModule,
        HttpClientInMemoryWebApiModule.forRoot(FakeApiService, { delay: 25 }),
      ],
      declarations: [CalendarOverlayComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarOverlayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('getAvailability() should receive testData', waitForAsync(() => {
    component.calenderRangeStart = '2022-06-01';
    component.calenderRangeEnd = '2022-07-01';
    component.getAvailability().subscribe((data) => {
      expect(data.length).toBe(2);
    });
  }));

  it('getDateString() should return date as correct string', () => {
    expect(component.getDateString(new Date(2022, 5, 3))).toBe('03.06.2022');
    expect(component.getDateString(new Date(2022, 11, 3))).toBe('03.12.2022');
    expect(component.getDateString(new Date(2022, 11, 22))).toBe('22.12.2022');
    expect(component.getDateString(new Date(2022, 5, 22))).toBe('22.06.2022');
  });

  it('days() should return date as correct string', waitForAsync(() => {
    component.calenderRangeStart = '2022-06-01';
    component.calenderRangeEnd = '2022-07-01';
    component.year = 2022;
    component.getAvailability().subscribe((data) => {
      component.availability = data;
      expect(data.length).toBe(2);
      const c = component.days(5);
      expect(component.days(5).length).toEqual(42);
      expect(component.days(5)).toEqual(testDays);
    });
  }));

  it('days() should return date as correct string', waitForAsync(() => {
    component.calenderRangeStart = '2022-06-01';
    component.calenderRangeEnd = '2022-07-01';
    component.year = 2022;
    component.getAvailability().subscribe((data) => {
      component.availability = data;
      expect(data.length).toBe(2);
      const c = component.days(5);
      expect(component.days(5).length).toEqual(42);
      expect(component.days(5)).toEqual(testDays);
    });
  }));

  it('choseDate() should return date as correct string', waitForAsync(() => {
    component.calenderRangeStart = '2022-06-01';
    component.calenderRangeEnd = '2022-07-01';
    component.year = 2022;
    component.getAvailability().subscribe((data) => {
      component.availability = data;
      // expect(data.length).toBe(2);
      // const c = component.days(5);
      // expect(component.days(5).length).toEqual(42);
      // expect(component.days(5)).toEqual(testDays);
    });
  }));

  it('setHoverDate() should set the Hover Date correctly and deleteHoverDate() should set it to undefined', waitForAsync(() => {
    component.setHoverDate({
      day: -1,
      month: 5,
      year: 2022,
      available: false,
      checkout: false,
      checkin: false,
      price: 0,
      originalPrice: 0,
      minBookingDuration: 0,
    });
    expect(component.hoverDate).toBe(undefined);
    component.setHoverDate({
      day: 16,
      month: 5,
      year: 2022,
      available: true,
      checkout: true,
      checkin: true,
      price: 3500,
      originalPrice: 3500,
      minBookingDuration: 7,
    });
    expect(component.hoverDate).toEqual(new Date(2022, 5, 16));
    component.deleteHoverDate();
    expect(component.hoverDate).toBe(undefined);
  }));

  it('wouldStartDateTurnunpickable() should set startDateWouldTurnUnpickable to true when a given day would cause the startdate to be unpickable. e.g. going back in time so the start date becomes the end date but does not allow checkouts', waitForAsync(() => {
    component.calenderRangeStart = '2022-06-01';
    component.calenderRangeEnd = '2022-07-01';
    component.year = 2022;
    component.getAvailability().subscribe((data) => {
      component.availability = data;
      component.startDay = component.availability[0].days[12];
      component.wouldStartDateTurnunpickable(component.availability[0].days[3]);
      expect(component.startDateWouldTurnUnpickable).toBe(true);
      component.wouldStartDateTurnunpickable(
        component.availability[0].days[18]
      );
      expect(component.startDateWouldTurnUnpickable).toBe(false);
    });
  }));

  it('isDateRangeCorrect() should check if min Booking duration is met', waitForAsync(() => {
    component.calenderRangeStart = '2022-06-01';
    component.calenderRangeEnd = '2022-07-01';
    component.year = 2022;
    component.getAvailability().subscribe((data) => {
      component.availability = data;
      component.startDay = data[0].days[12];
      component.endDay = data[0].days[16];
      expect(component.isDateRangeCorrect()).toBe(false);
      component.endDay = data[0].days[26];
      expect(component.isDateRangeCorrect()).toBe(true);
    });
  }));

  it('changeMonth() should check if min Booking duration is met', waitForAsync(() => {
    component.calenderRangeStart = '2022-06-01';
    component.calenderRangeEnd = '2022-07-01';
    component.year = 2022;
    component.getAvailability().subscribe((data) => {
      component.availability = data;
      component.startDay = data[0].days[12];
      component.endDay = data[0].days[16];
      expect(component.isDateRangeCorrect()).toBe(false);
      component.endDay = data[0].days[26];
      expect(component.isDateRangeCorrect()).toBe(true);
    });
  }));

  it('checkAndCorrectMonth() should check if month is btw. 0-11 and if not correct it', () => {
    expect(component.checkAndCorrectMonth(2)).toBe(2);
    expect(component.checkAndCorrectMonth(0)).toBe(0);
    expect(component.checkAndCorrectMonth(11)).toBe(11);
    expect(component.checkAndCorrectMonth(12)).toBe(0);
    expect(component.checkAndCorrectMonth(-1)).toBe(11);
    expect(component.checkAndCorrectMonth(-5)).toBe(7);
    expect(component.checkAndCorrectMonth(17)).toBe(5);
  });

  it('checkAndCorrectYear() should check if month is btw. 0-11 and if not correct the year and return it', () => {
    expect(component.checkAndCorrectYear(2, 2022)).toBe(2022);
    expect(component.checkAndCorrectYear(0, 2022)).toBe(2022);
    expect(component.checkAndCorrectYear(11, 2022)).toBe(2022);
    expect(component.checkAndCorrectYear(12, 2022)).toBe(2023);
    expect(component.checkAndCorrectYear(-1, 2022)).toBe(2021);
    expect(component.checkAndCorrectYear(-5, 2022)).toBe(2021);
    expect(component.checkAndCorrectYear(17, 2022)).toBe(2023);
  });

  it('getDaysBetweenRange() should return days btw. start and end date', waitForAsync(() => {
    component.calenderRangeStart = '2022-06-01';
    component.calenderRangeEnd = '2022-07-01';
    component.year = 2022;
    component.getAvailability().subscribe((data) => {
      component.availability = data;
      expect(component.getDaysBetweenRange()).toEqual([]);
      component.startDay = {
        day: 13,
        month: 5,
        year: 2022,
        available: true,
        checkout: false,
        checkin: true,
        price: 3500,
        originalPrice: 3500,
        minBookingDuration: 7,
      };
      expect(component.getDaysBetweenRange()).toEqual([]);
      component.endDay = {
        day: 22,
        month: 5,
        year: 2022,
        available: true,
        checkout: true,
        checkin: true,
        price: 4000,
        originalPrice: 4500,
        minBookingDuration: 7,
      };
      const comparableResut = [
        component.availability[0].days[14],
        component.availability[0].days[15],
        component.availability[0].days[16],
        component.availability[0].days[17],
        component.availability[0].days[18],
        component.availability[0].days[19],
        component.availability[0].days[20],
        component.availability[0].days[21],
        component.availability[0].days[22],
        component.availability[0].days[23],
      ];
      expect(component.getDaysBetweenRange().length).toEqual(9);
    });
  }));
});

const testDays = [
  {
    day: -1,
    month: 5,
    year: 2022,
    available: false,
    checkout: false,
    checkin: false,
    price: 0,
    originalPrice: 0,
    minBookingDuration: 0,
  },
  {
    day: -1,
    month: 5,
    year: 2022,
    available: false,
    checkout: false,
    checkin: false,
    price: 0,
    originalPrice: 0,
    minBookingDuration: 0,
  },
  {
    day: 1,
    month: 5,
    year: 2022,
    available: false,
    checkout: true,
    checkin: true,
    price: 2650,
    originalPrice: 2650,
    minBookingDuration: 5,
  },
  {
    day: 2,
    month: 5,
    year: 2022,
    available: false,
    checkout: true,
    checkin: true,
    price: 2650,
    originalPrice: 2650,
    minBookingDuration: 5,
  },
  {
    day: 3,
    month: 5,
    year: 2022,
    available: false,
    checkout: true,
    checkin: true,
    price: 2650,
    originalPrice: 2650,
    minBookingDuration: 5,
  },
  {
    day: 4,
    month: 5,
    year: 2022,
    available: false,
    checkout: true,
    checkin: true,
    price: 2650,
    originalPrice: 2650,
    minBookingDuration: 5,
  },
  {
    day: 5,
    month: 5,
    year: 2022,
    available: false,
    checkout: true,
    checkin: true,
    price: 2650,
    originalPrice: 2650,
    minBookingDuration: 5,
  },
  {
    day: 6,
    month: 5,
    year: 2022,
    available: false,
    checkout: true,
    checkin: true,
    price: 2650,
    originalPrice: 2650,
    minBookingDuration: 5,
  },
  {
    day: 7,
    month: 5,
    year: 2022,
    available: false,
    checkout: true,
    checkin: true,
    price: 2650,
    originalPrice: 2650,
    minBookingDuration: 5,
  },
  {
    day: 8,
    month: 5,
    year: 2022,
    available: false,
    checkout: true,
    checkin: true,
    price: 2650,
    originalPrice: 2650,
    minBookingDuration: 5,
  },
  {
    day: 9,
    month: 5,
    year: 2022,
    available: false,
    checkout: true,
    checkin: true,
    price: 2650,
    originalPrice: 2650,
    minBookingDuration: 5,
  },
  {
    day: 10,
    month: 5,
    year: 2022,
    available: false,
    checkout: true,
    checkin: true,
    price: 2650,
    originalPrice: 2650,
    minBookingDuration: 5,
  },
  {
    day: 11,
    month: 5,
    year: 2022,
    available: true,
    checkout: true,
    checkin: false,
    price: 3500,
    originalPrice: 3500,
    minBookingDuration: 7,
  },
  {
    day: 12,
    month: 5,
    year: 2022,
    available: true,
    checkout: false,
    checkin: false,
    price: 3500,
    originalPrice: 3500,
    minBookingDuration: 7,
  },
  {
    day: 13,
    month: 5,
    year: 2022,
    available: true,
    checkout: false,
    checkin: true,
    price: 3500,
    originalPrice: 3500,
    minBookingDuration: 7,
  },
  {
    day: 14,
    month: 5,
    year: 2022,
    available: true,
    checkout: true,
    checkin: true,
    price: 3500,
    originalPrice: 3500,
    minBookingDuration: 7,
  },
  {
    day: 15,
    month: 5,
    year: 2022,
    available: true,
    checkout: true,
    checkin: true,
    price: 3500,
    originalPrice: 3500,
    minBookingDuration: 7,
  },
  {
    day: 16,
    month: 5,
    year: 2022,
    available: true,
    checkout: true,
    checkin: true,
    price: 3500,
    originalPrice: 3500,
    minBookingDuration: 7,
  },
  {
    day: 17,
    month: 5,
    year: 2022,
    available: true,
    checkout: true,
    checkin: true,
    price: 3500,
    originalPrice: 3500,
    minBookingDuration: 7,
  },
  {
    day: 18,
    month: 5,
    year: 2022,
    available: true,
    checkout: true,
    checkin: false,
    price: 4000,
    originalPrice: 4500,
    minBookingDuration: 7,
  },
  {
    day: 19,
    month: 5,
    year: 2022,
    available: true,
    checkout: false,
    checkin: false,
    price: 4000,
    originalPrice: 4500,
    minBookingDuration: 7,
  },
  {
    day: 20,
    month: 5,
    year: 2022,
    available: true,
    checkout: false,
    checkin: true,
    price: 4000,
    originalPrice: 4500,
    minBookingDuration: 7,
  },
  {
    day: 21,
    month: 5,
    year: 2022,
    available: true,
    checkout: true,
    checkin: true,
    price: 4000,
    originalPrice: 4500,
    minBookingDuration: 7,
  },
  {
    day: 22,
    month: 5,
    year: 2022,
    available: true,
    checkout: true,
    checkin: true,
    price: 4000,
    originalPrice: 4500,
    minBookingDuration: 7,
  },
  {
    day: 23,
    month: 5,
    year: 2022,
    available: true,
    checkout: true,
    checkin: true,
    price: 4000,
    originalPrice: 4500,
    minBookingDuration: 7,
  },
  {
    day: 24,
    month: 5,
    year: 2022,
    available: true,
    checkout: true,
    checkin: true,
    price: 4000,
    originalPrice: 4500,
    minBookingDuration: 7,
  },
  {
    day: 25,
    month: 5,
    year: 2022,
    available: true,
    checkout: true,
    checkin: false,
    price: 4000,
    originalPrice: 4500,
    minBookingDuration: 7,
  },
  {
    day: 26,
    month: 5,
    year: 2022,
    available: true,
    checkout: false,
    checkin: false,
    price: 4000,
    originalPrice: 4500,
    minBookingDuration: 7,
  },
  {
    day: 27,
    month: 5,
    year: 2022,
    available: true,
    checkout: false,
    checkin: true,
    price: 4000,
    originalPrice: 4500,
    minBookingDuration: 7,
  },
  {
    day: 28,
    month: 5,
    year: 2022,
    available: true,
    checkout: true,
    checkin: true,
    price: 4000,
    originalPrice: 4500,
    minBookingDuration: 7,
  },
  {
    day: 29,
    month: 5,
    year: 2022,
    available: true,
    checkout: true,
    checkin: true,
    price: 4000,
    originalPrice: 4500,
    minBookingDuration: 7,
  },
  {
    day: 30,
    month: 5,
    year: 2022,
    available: true,
    checkout: true,
    checkin: true,
    price: 4000,
    originalPrice: 4500,
    minBookingDuration: 7,
  },
  {
    day: -1,
    month: 5,
    year: 2022,
    available: false,
    checkout: false,
    checkin: false,
    price: 0,
    originalPrice: 0,
    minBookingDuration: 0,
  },
  {
    day: -1,
    month: 5,
    year: 2022,
    available: false,
    checkout: false,
    checkin: false,
    price: 0,
    originalPrice: 0,
    minBookingDuration: 0,
  },
  {
    day: -1,
    month: 5,
    year: 2022,
    available: false,
    checkout: false,
    checkin: false,
    price: 0,
    originalPrice: 0,
    minBookingDuration: 0,
  },
  {
    day: -1,
    month: 5,
    year: 2022,
    available: false,
    checkout: false,
    checkin: false,
    price: 0,
    originalPrice: 0,
    minBookingDuration: 0,
  },
  {
    day: -1,
    month: 5,
    year: 2022,
    available: false,
    checkout: false,
    checkin: false,
    price: 0,
    originalPrice: 0,
    minBookingDuration: 0,
  },
  {
    day: -1,
    month: 5,
    year: 2022,
    available: false,
    checkout: false,
    checkin: false,
    price: 0,
    originalPrice: 0,
    minBookingDuration: 0,
  },
  {
    day: -1,
    month: 5,
    year: 2022,
    available: false,
    checkout: false,
    checkin: false,
    price: 0,
    originalPrice: 0,
    minBookingDuration: 0,
  },
  {
    day: -1,
    month: 5,
    year: 2022,
    available: false,
    checkout: false,
    checkin: false,
    price: 0,
    originalPrice: 0,
    minBookingDuration: 0,
  },
  {
    day: -1,
    month: 5,
    year: 2022,
    available: false,
    checkout: false,
    checkin: false,
    price: 0,
    originalPrice: 0,
    minBookingDuration: 0,
  },
  {
    day: -1,
    month: 5,
    year: 2022,
    available: false,
    checkout: false,
    checkin: false,
    price: 0,
    originalPrice: 0,
    minBookingDuration: 0,
  },
];
