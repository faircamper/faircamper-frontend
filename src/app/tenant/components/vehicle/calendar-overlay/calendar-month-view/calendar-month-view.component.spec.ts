import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import monthTestDataJson from './pureTestData_AvailebilityDays.json';
import { FakeApiService } from 'src/app/fake-api.service';
import {
  Availability,
  AvailabilityDay,
} from 'src/app/tenant/models/availability/availability';

import { CalendarMonthViewComponent } from './calendar-month-view.component';

describe('CalendarMonthViewComponent', () => {
  let component: CalendarMonthViewComponent;
  let fixture: ComponentFixture<CalendarMonthViewComponent>;
  let http: HttpClient;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CalendarMonthViewComponent],
      imports: [
        HttpClientModule,
        HttpClientInMemoryWebApiModule.forRoot(FakeApiService, { delay: 50 }),
      ],
    }).compileComponents();

    http = TestBed.get(HttpClient);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarMonthViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('can get Availability', waitForAsync(() => {
    http
      .get<{ data: Availability[] }>(
        'api/public/vehicle/123456789/availability?from=2022-06-01&to=2022-07-2022'
      )
      .subscribe((availability) => {
        console.log(availability);
        expect(availability.data.length).toBe(2);
      });
  }));

  it('.monthToDate() should return correct Date', () => {
    component.year = 2022;
    component.month = 5;
    expect(component.monthToDate()).toEqual(new Date(2022, 5));
  });

  it('.getDaysByWeek() should return correct week', () => {
    component.days = monthTestDataJson;
    let week = component.getDaysByWeek(1, 5);
    expect(week.length).toBe(7);
    expect(week[0].day).toBe(-1);
    expect(week[1].day).toBe(-1);
    expect(week[2].day).toBe(1);
    expect(week[3].day).toBe(2);
    expect(week[4].day).toBe(3);
    expect(week[5].day).toBe(4);
    expect(week[6].day).toBe(5);
    week = component.getDaysByWeek(2, 5);
    expect(week.length).toBe(7);
    expect(week[0].day).toBe(6);
    expect(week[1].day).toBe(7);
    expect(week[2].day).toBe(8);
    expect(week[3].day).toBe(9);
    expect(week[4].day).toBe(10);
    expect(week[5].day).toBe(11);
    expect(week[6].day).toBe(12);
    week = component.getDaysByWeek(3, 5);
    expect(week.length).toBe(7);
    expect(week[0].day).toBe(13);
    expect(week[1].day).toBe(14);
    expect(week[2].day).toBe(15);
    expect(week[3].day).toBe(16);
    expect(week[4].day).toBe(17);
    expect(week[5].day).toBe(18);
    expect(week[6].day).toBe(19);
    week = component.getDaysByWeek(4, 5);
    expect(week.length).toBe(7);
    expect(week[0].day).toBe(20);
    expect(week[1].day).toBe(21);
    expect(week[2].day).toBe(22);
    expect(week[3].day).toBe(23);
    expect(week[4].day).toBe(24);
    expect(week[5].day).toBe(25);
    expect(week[6].day).toBe(26);
    week = component.getDaysByWeek(5, 5);
    expect(week.length).toBe(7);
    expect(week[0].day).toBe(27);
    expect(week[1].day).toBe(28);
    expect(week[2].day).toBe(29);
    expect(week[3].day).toBe(30);
    expect(week[4].day).toBe(-1);
    expect(week[5].day).toBe(-1);
    expect(week[6].day).toBe(-1);
  });

  it('.isDatePicked() should return correct values', () => {
    component.days = monthTestDataJson;
    expect(component.isDatePicked(component.days[2])).toBeFalse();
    component.startDay = component.days[2];
    expect(component.isDatePicked(component.days[0])).toBeFalse();
    expect(component.isDatePicked(component.days[3])).toBeFalse();
    expect(component.isDatePicked(component.days[2])).toBeTrue();
    component.endDay = component.days[14];
    expect(component.isDatePicked(component.days[1])).toBeFalse();
    expect(component.isDatePicked(component.days[8])).toBeFalse();
    expect(component.isDatePicked(component.days[14])).toBeTrue();
  });

  it('.isDatePickable() should return correct values', () => {
    component.days = monthTestDataJson;
    //false because day.day should be -1
    expect(component.isDatePickable(component.days[0])).toBeFalse();
    //false because day.available is false
    expect(component.isDatePickable(component.days[2])).toBeFalse();
    //false because day.checkin is false
    expect(component.isDatePickable(component.days[12])).toBeFalse();
    //true because day.available and day.checkin are true and no start date was picked yet
    expect(component.isDatePickable(component.days[21])).toBeTrue();
    component.startDay = component.days[23];
    //false because checkout is false
    expect(component.isDatePickable(component.days[28])).toBeFalse();
    //true because we are going back in time, so start and end switch
    expect(component.isDatePickable(component.days[14])).toBeTrue();
    component.endDay = component.days[14];
    //true because day.available and day.checkin are true and a start date + end date was picked
    expect(component.isDatePickable(component.days[21])).toBeTrue();
    //false because day.checkin is false and a start date + end date
    expect(component.isDatePickable(component.days[12])).toBeFalse();
  });

  it('.onStartMonth() should return true only when displayed month matches current month', () => {
    component.year = new Date().getFullYear();
    component.month = new Date().getMonth();
    expect(component.onStartMonth()).toBeTrue();
    component.year--;
    expect(component.onStartMonth()).toBeFalse();
    component.year++;
    component.month = new Date(component.year, component.month - 1).getMonth();
    expect(component.onStartMonth()).toBeFalse();
  });

  it('.checkinColor() should return true when day should be displayed with checkin color', () => {
    component.days = monthTestDataJson;
    expect(component.checkinColor(component.days[0])).toBeFalse();
    expect(component.checkinColor(component.days[3])).toBeFalse();
    expect(component.checkinColor(component.days[12])).toBeFalse();
    expect(component.checkinColor(component.days[13])).toBeFalse();
    expect(component.checkinColor(component.days[15])).toBeFalse();
    expect(component.checkinColor(component.days[14])).toBeTrue();
  });

  it('.checkoutColor() should return true when day should be displayed with checkout color', () => {
    component.days = monthTestDataJson;
    expect(component.checkoutColor(component.days[0])).toBeFalse();
    expect(component.checkoutColor(component.days[3])).toBeFalse();
    expect(component.checkoutColor(component.days[13])).toBeFalse();
    expect(component.checkoutColor(component.days[15])).toBeFalse();
    expect(component.checkoutColor(component.days[14])).toBeFalse();
    expect(component.checkoutColor(component.days[12])).toBeTrue();
  });
  it('.disabledColor() should return true when day should be displayed with disabled Color', () => {
    component.days = monthTestDataJson;
    expect(component.disabledColor(component.days[3])).toBeTrue();
    expect(component.disabledColor(component.days[13])).toBeTrue();
    expect(component.disabledColor(component.days[0])).toBeFalse();
    expect(component.disabledColor(component.days[15])).toBeFalse();
    expect(component.disabledColor(component.days[14])).toBeFalse();
    expect(component.disabledColor(component.days[12])).toBeFalse();
  });
  it('.orangeColor() should return true when day should be displayed with orange color', () => {
    component.days = monthTestDataJson;
    expect(component.orangeColor(component.days[3])).toBeFalse();
    expect(component.orangeColor(component.days[13])).toBeFalse();
    expect(component.orangeColor(component.days[0])).toBeFalse();
    expect(component.orangeColor(component.days[15])).toBeFalse();
    expect(component.orangeColor(component.days[14])).toBeFalse();
    expect(component.orangeColor(component.days[12])).toBeFalse();
    // set Picked Date
    component.startDay = component.days[21];
    expect(component.orangeColor(component.days[21])).toBeTrue();
    component.startDateWouldTurnUnpickable = true;
    expect(component.orangeColor(component.days[21])).toBeFalse();
    component.startDateWouldTurnUnpickable = false;
    component.endDay = component.days[31];
    expect(component.orangeColor(component.days[31])).toBeTrue();
    component.minDurationError = true;
    expect(component.orangeColor(component.days[31])).toBeFalse();
  });
  it('.redColor() should return true when day should be displayed with red color', () => {
    component.days = monthTestDataJson;
    expect(component.redColor(component.days[3])).toBeFalse();
    expect(component.redColor(component.days[13])).toBeFalse();
    expect(component.redColor(component.days[0])).toBeFalse();
    expect(component.redColor(component.days[15])).toBeFalse();
    expect(component.redColor(component.days[14])).toBeFalse();
    expect(component.redColor(component.days[12])).toBeFalse();
    // set Picked Date
    component.startDay = component.days[21];
    expect(component.redColor(component.days[21])).toBeFalse();
    component.startDateWouldTurnUnpickable = true;
    expect(component.redColor(component.days[21])).toBeTrue();
    component.startDateWouldTurnUnpickable = false;
    component.endDay = component.days[31];
    expect(component.redColor(component.days[31])).toBeFalse();
    component.minDurationError = true;
    expect(component.redColor(component.days[31])).toBeTrue();
    expect(component.redColor(component.days[30])).toBeTrue();
    expect(component.redColor(component.days[29])).toBeTrue();
    expect(component.redColor(component.days[28])).toBeTrue();
    expect(component.redColor(component.days[27])).toBeTrue();
    expect(component.redColor(component.days[26])).toBeTrue();
    expect(component.redColor(component.days[25])).toBeTrue();
    expect(component.redColor(component.days[24])).toBeTrue();
    expect(component.redColor(component.days[23])).toBeTrue();
    expect(component.redColor(component.days[22])).toBeTrue();
    expect(component.redColor(component.days[21])).toBeTrue();
    expect(component.redColor(component.days[20])).toBeFalse();
  });

  it('.lightOrangeColor() should return true when day should be displayed with red color', () => {
    component.days = monthTestDataJson;
    expect(component.lightOrangeColor(component.days[3])).toBeFalse();
    expect(component.lightOrangeColor(component.days[13])).toBeFalse();
    expect(component.lightOrangeColor(component.days[0])).toBeFalse();
    expect(component.lightOrangeColor(component.days[15])).toBeFalse();
    expect(component.lightOrangeColor(component.days[14])).toBeFalse();
    expect(component.lightOrangeColor(component.days[12])).toBeFalse();
    // set Picked Date
    component.startDay = component.days[21];
    expect(component.lightOrangeColor(component.days[21])).toBeFalse();
    component.hoverDate = new Date(2022, 5, 25);
    expect(component.lightOrangeColor(component.days[22])).toBeTrue();
    expect(component.lightOrangeColor(component.days[23])).toBeTrue();
    expect(component.lightOrangeColor(component.days[24])).toBeTrue();
    expect(component.lightOrangeColor(component.days[25])).toBeTrue();
    expect(component.lightOrangeColor(component.days[26])).toBeFalse();
    component.endDay = component.days[26];
    expect(component.lightOrangeColor(component.days[21])).toBeFalse();
    expect(component.lightOrangeColor(component.days[22])).toBeTrue();
    expect(component.lightOrangeColor(component.days[23])).toBeTrue();
    expect(component.lightOrangeColor(component.days[24])).toBeTrue();
    expect(component.lightOrangeColor(component.days[25])).toBeTrue();
    expect(component.lightOrangeColor(component.days[26])).toBeFalse();
    component.minDurationError = true;
    expect(component.lightOrangeColor(component.days[21])).toBeFalse();
    expect(component.lightOrangeColor(component.days[22])).toBeFalse();
    expect(component.lightOrangeColor(component.days[23])).toBeFalse();
    expect(component.lightOrangeColor(component.days[24])).toBeFalse();
    expect(component.lightOrangeColor(component.days[25])).toBeFalse();
    expect(component.lightOrangeColor(component.days[26])).toBeFalse();
  });

  it('.isDateInBetween() should return correct Date', () => {
    component.days = monthTestDataJson;
    expect(component.isDateInBetween(component.days[0])).toBeFalse();
    expect(component.isDateInBetween(component.days[15])).toBeFalse();
    component.startDay = component.days[21];
    component.hoverDate = new Date(2022, 5, 23);
    expect(component.isDateInBetween(component.days[21])).toBeFalse();
    expect(component.isDateInBetween(component.days[22])).toBeTrue();
    expect(component.isDateInBetween(component.days[23])).toBeTrue();
    expect(component.isDateInBetween(component.days[24])).toBeFalse();
    expect(component.isDateInBetween(component.days[10])).toBeFalse();
    expect(component.isDateInBetween(component.days[30])).toBeFalse();
    expect(component.isDateInBetween(component.days[0])).toBeFalse();
    component.endDay = component.days[26];
    expect(component.isDateInBetween(component.days[21])).toBeFalse();
    expect(component.isDateInBetween(component.days[22])).toBeTrue();
    expect(component.isDateInBetween(component.days[23])).toBeTrue();
    expect(component.isDateInBetween(component.days[24])).toBeTrue();
    expect(component.isDateInBetween(component.days[25])).toBeTrue();
    expect(component.isDateInBetween(component.days[26])).toBeFalse();
    expect(component.isDateInBetween(component.days[10])).toBeFalse();
    expect(component.isDateInBetween(component.days[30])).toBeFalse();
    expect(component.isDateInBetween(component.days[0])).toBeFalse();
    // component.isDateInBetween();
  });
});
