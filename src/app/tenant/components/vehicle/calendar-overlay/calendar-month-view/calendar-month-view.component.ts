import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AvailabilityDay } from 'src/app/tenant/models/availability/availability';

@Component({
  selector: 'tenant-calendar-month-view',
  templateUrl: './calendar-month-view.component.html',
  styleUrls: ['./calendar-month-view.component.css'],
})
export class CalendarMonthViewComponent implements OnInit {
  @Input() year: number = new Date().getFullYear();
  @Input() month: number = new Date().getMonth();
  @Input() days: AvailabilityDay[] = [];
  @Input() startDay: undefined | AvailabilityDay = undefined;
  @Input() endDay: undefined | AvailabilityDay = undefined;
  @Input() hoverDate: undefined | Date = undefined;
  @Input() startDateWouldTurnUnpickable = false;
  @Input() minDurationError = false;
  @Input() secondCalendar = false;
  @Output() choseDay = new EventEmitter();
  @Output() setHoverDate = new EventEmitter();
  @Output() deleteHoverDate = new EventEmitter();
  @Output() changedMonth = new EventEmitter();
  constructor() {}

  ngOnInit(): void {}

  monthToDate() {
    return new Date(this.year, this.month);
  }

  getDaysByWeek(week: number, month: number): AvailabilityDay[] {
    let days = this.days;
    days = days.slice((week - 1) * 7, week * 7);
    return days;
  }

  isDatePicked(day: AvailabilityDay): boolean {
    if (day.day === -1 || !this.startDay) {
      return false;
    }
    const date = new Date(day.year, day.month, day.day);
    const startDate = new Date(
      this.startDay.year,
      this.startDay.month,
      this.startDay.day
    );
    let endDate = undefined;
    if (this.endDay) {
      endDate = new Date(this.endDay.year, this.endDay.month, this.endDay.day);
    }
    if (
      date.getTime() === startDate?.getTime() ||
      date.getTime() === endDate?.getTime()
    )
      return true;
    return false;
  }
  isDatePickable(day: AvailabilityDay): boolean {
    if (day.day === -1) {
      return false;
    }
    if (!day.available) {
      return false;
    }
    const newDate = new Date(day.year, day.month, day.day);
    // // Start selected => Set end date

    if (this.startDay && !this.endDay) {
      const startDate = new Date(
        this.startDay.year,
        this.startDay.month,
        this.startDay.day
      );
      if (newDate.getTime() >= (startDate as Date).getTime()) {
        if (!day.checkout) return false;
      } else if (newDate.getTime() < (startDate as Date).getTime()) {
        if (!day.checkin) return false;
      }
    }
    if (!day.checkin && !day.checkout) {
      return false;
    }
    if (!day.checkin && !this.startDay) {
      return false;
    }
    if (!day.checkin && this.startDay && this.endDay) {
      return false;
    }
    return true;
  }

  clickedOnDay(day: AvailabilityDay) {
    if (this.isDatePickable(day)) {
      this.choseDay.emit(day);
    }
  }

  onStartMonth(): boolean {
    return (
      this.month === new Date().getMonth() &&
      this.year === new Date().getFullYear()
    );
  }

  checkinColor(day: AvailabilityDay): boolean {
    return (
      day.checkin &&
      !day.checkout &&
      day.day !== -1 &&
      !this.isDateInBetween(day) &&
      !(this.startDateWouldTurnUnpickable && this.isDatePicked(day)) &&
      !this.isDatePicked(day)
    );
  }

  checkoutColor(day: AvailabilityDay): boolean {
    return (
      day.checkout &&
      !day.checkin &&
      day.day !== -1 &&
      !this.isDateInBetween(day) &&
      !(this.startDateWouldTurnUnpickable && this.isDatePicked(day)) &&
      !this.isDatePicked(day)
    );
  }

  disabledColor(day: AvailabilityDay): boolean {
    return (!day.available || (!day.checkin && !day.checkout)) && day.day > 0;
  }

  orangeColor(day: AvailabilityDay): boolean {
    return (
      this.isDatePicked(day) &&
      !this.startDateWouldTurnUnpickable &&
      !this.minDurationError
    );
  }
  redColor(day: AvailabilityDay): boolean {
    return (
      (this.startDateWouldTurnUnpickable && this.isDatePicked(day)) ||
      (this.minDurationError && this.isDatePicked(day)) ||
      this.isDateInBetween(day) ||
      ((this.isDatePicked(day) || this.isDateInBetween(day)) && !day.available)
    );
  }
  lightOrangeColor(day: AvailabilityDay): boolean {
    return (
      !this.minDurationError &&
      day.available &&
      this.isDateInBetween(day) &&
      !this.isDatePicked(day)
    );
  }

  isDateInBetween(day: AvailabilityDay): boolean {
    if (day.day === -1) {
      return false;
    }
    const startDay = this.startDay;
    const endDay = this.endDay;
    const hoverDate = this.hoverDate;
    const date = new Date(day.year, day.month, day.day);
    if (startDay && endDay) {
      const startDate = new Date(startDay.year, startDay.month, startDay.day);
      const endDate = new Date(endDay.year, endDay.month, endDay.day);
      return date.getTime() > startDate.getTime() &&
        date.getTime() < endDate.getTime()
        ? true
        : false;
    } else if (startDay && hoverDate) {
      const startDate = new Date(startDay.year, startDay.month, startDay.day);
      if (hoverDate.getTime() > startDate.getTime()) {
        return date.getTime() > startDate?.getTime() &&
          date.getTime() < hoverDate.getTime()
          ? true
          : false;
      } else if (hoverDate.getTime() < startDate.getTime()) {
        return date.getTime() < startDate?.getTime() &&
          date.getTime() > hoverDate.getTime()
          ? true
          : false;
      }
    }
    return false;
  }
}
