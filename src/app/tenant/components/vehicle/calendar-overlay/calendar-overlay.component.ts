import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AvailabilityService } from 'src/app/tenant/services/http/calendar/availability.service';
import {
  Availability,
  AvailabilityDay,
} from 'src/app/tenant/models/availability/availability';
import { EMPTY, Observable } from 'rxjs';
import { formatDate } from '@angular/common';
import { Store } from '@ngrx/store';
import { selectSearchData } from 'src/app/tenant/ngrx/selectors/searchData.selector';

@Component({
  selector: 'tenant-calendar-overlay',
  templateUrl: './calendar-overlay.component.html',
  styleUrls: ['./calendar-overlay.component.css'],
})
export class CalendarOverlayComponent implements OnInit, OnDestroy {
  year: number = new Date().getFullYear();
  month: number = new Date().getMonth();
  calenderRangeStart: string = formatDate(
    new Date(this.year, this.month, 1),
    'yyyy-MM-dd',
    'en-US'
  );
  calenderRangeEnd: string = formatDate(
    new Date(this.year, this.month + 1, 1),
    'yyyy-MM-dd',
    'en-US'
  );
  startDay: undefined | AvailabilityDay = undefined;
  endDay: undefined | AvailabilityDay = undefined;
  minDuration = 0;
  minDurationError = false;
  startDateWouldTurnUnpickable = false;
  hoverDate: undefined | Date = undefined;
  availabilitySub = EMPTY.subscribe();
  storeSub = EMPTY.subscribe();
  @Input() isOpen = false;
  @Output() closed: EventEmitter<string> = new EventEmitter<string>();
  @Output() updateData: EventEmitter<any> = new EventEmitter<any>();
  @Output() selectedDate = new EventEmitter<string>();
  availability: Availability[] = [];
  constructor(
    private availabilityService: AvailabilityService,
    private activatedRoute: ActivatedRoute,
    private store: Store
  ) {}

  ngOnInit(): void {
    this.storeSub = this.store.select(selectSearchData).subscribe((data) => {
      if (data.endDate && data.startDate) {
        const end = data.endDate;
        const start = data.startDate;
        this.calenderRangeStart = formatDate(
          new Date(start.getFullYear(), start.getMonth(), 1),
          'yyyy-MM-dd',
          'en-US'
        );
        const adjustEnd =
          start.getMonth() === end.getMonth() &&
          start.getFullYear() === end.getFullYear()
            ? 1
            : 0;
        const adjustedEnd = new Date(
          end.getFullYear(),
          end.getMonth() + adjustEnd,
          1
        );
        this.calenderRangeEnd = formatDate(
          new Date(adjustedEnd.getFullYear(), adjustedEnd.getFullYear(), 1),
          'yyyy-MM-dd',
          'en-US'
        );
        this.year = start.getFullYear();
        this.month = start.getMonth();
        this.availabilitySub = this.getAvailability().subscribe((data) => {
          this.availability = data;
          this.setInitialDateRangeToSearchData(start, end);
        });
      } else {
        this.availabilitySub = this.getAvailability().subscribe((data) => {
          this.availability = data;
        });
      }
    });
  }

  setInitialDateRangeToSearchData(start: Date, end: Date): void {
    this.availability.forEach((e) => {
      const startYear = start.getFullYear();
      const startMonth = start.getMonth();
      const startDay = start.getDate();
      const endYear = end.getFullYear();
      const endMonth = end.getMonth();
      const endDay = end.getDate();
      if (e.year === startYear && e.month === startMonth) {
        e.days.forEach((day) => {
          if (
            day.day == startDay &&
            day.month == startMonth &&
            day.year == startYear
          ) {
            this.startDay = day;
          }
        });
      }
      if (e.year === endYear && e.month === endMonth) {
        e.days.forEach((day) => {
          if (
            day.day == endDay &&
            day.month == endMonth &&
            day.year == endYear
          ) {
            this.endDay = day;
          }
        });
      }
    });
  }

  ngOnDestroy(): void {
    this.availabilitySub.unsubscribe();
    this.storeSub.unsubscribe();
  }

  getAvailability(): Observable<Availability[]> {
    // const vehicleId = this.activatedRoute.snapshot.paramMap.get('id');
    const vehicleId = this.activatedRoute.snapshot.paramMap.get('id');
    let filter =
      'from=' + this.calenderRangeStart + '&to=' + this.calenderRangeEnd;
    return this.availabilityService.listAvailability(vehicleId, filter);
  }

  getDateString(date: any) {
    let month = date.getMonth() + 1;
    return (
      (date.getDate() < 10 ? '0' + date.getDate() : date.getDate()) +
      '.' +
      (month < 10 ? '0' + month : month) +
      '.' +
      date.getFullYear()
    );
  }

  days(month: number): AvailabilityDay[] {
    // eine 0 als Tag einzutragen versetzt das New Date auf den letzten Tag des VORHERIGEN Monats, deswegen das +1
    const daysInMonth = new Date(this.year, month + 1, 0).getDate();
    const startDay = new Date(this.year, month, 0).getDay();
    const year = this.checkAndCorrectYear(month, this.year);
    month = this.checkAndCorrectMonth(month);
    const availability = this.availability.find((e) => {
      return e.month === month && e.year === year;
    });
    const days = [];
    if (!availability) {
      return [];
    }

    month = availability.month;
    let dayCount = 1;
    for (let i = 0; i < 42; i++) {
      if (i >= startDay && dayCount <= daysInMonth) {
        days.push(availability.days[dayCount - 1]);
        dayCount++;
      } else {
        days.push({
          day: -1,
          month: month,
          year: this.year,
          available: false,
          checkout: false,
          checkin: false,
          price: 0,
          originalPrice: 0,
          minBookingDuration: 0,
        });
      }
    }
    return days;
  }

  choseDate(day: AvailabilityDay) {
    const newDate = new Date(day.year, day.month, day.day);

    // Both selected => Set start date and reset endDay
    if (this.startDay && this.endDay) {
      this.startDay = day;
      this.endDay = undefined;
    }

    // // Start selected => Set end date
    else if (this.startDay && !this.endDay) {
      const startDate = new Date(
        this.startDay.year,
        this.startDay.month,
        this.startDay.day
      );
      if (newDate.getTime() >= (startDate as Date).getTime()) {
        if (!day.checkout) return;
        this.endDay = day;
      } else if (newDate.getTime() < (startDate as Date).getTime()) {
        if (!day.checkin) return;
        if (!this.startDay?.checkout) return;
        this.endDay = this.startDay;
        this.startDay = day;
      }
    }
    // None selected => Set start date
    else if (!this.startDay && !this.endDay) {
      this.startDay = day;
    }
  }

  setHoverDate(day: AvailabilityDay) {
    if (day.day === -1) {
      return;
    }
    this.hoverDate = new Date(day.year, day.month, day.day);
  }

  deleteHoverDate() {
    this.hoverDate = undefined;
  }

  wouldStartDateTurnunpickable(day: AvailabilityDay): void {
    this.startDateWouldTurnUnpickable = false;
    const newDate = new Date(day.year, day.month, day.day);
    if (this.startDay && !this.endDay) {
      const startDate = new Date(
        this.startDay.year,
        this.startDay.month,
        this.startDay.day
      );
      if (newDate.getTime() < startDate.getTime()) {
        if (!this.startDay.checkout) {
          this.startDateWouldTurnUnpickable = true;
        }
      }
    }
  }

  isDateRangeCorrect(): boolean {
    this.minDurationError = false;
    if (!this.startDay || !this.endDay) return false;
    const startDate = new Date(
      this.startDay.year,
      this.startDay.month,
      this.startDay.day
    );
    const endDate = new Date(
      this.endDay.year,
      this.endDay.month,
      this.endDay.day
    );
    if (startDate.getTime() > endDate.getTime()) return false;

    const months = this.availability.filter((e) => {
      if (!this.startDay) return false;
      if (!this.endDay) return false;
      return (
        e.month >= this.startDay.month &&
        e.month <= this.endDay.month &&
        e.year >= this.startDay.year &&
        e.year <= this.endDay.year
      );
    });

    const days: AvailabilityDay[] = [];
    months.forEach((element) => {
      days.push(...element.days);
    });
    const unaivableDays = days.filter((e) => {
      if (!this.startDay) return false;
      if (!this.endDay) return false;
      const currentDate = new Date(e.year, e.month, e.day);
      const startDate = new Date(
        this.startDay.year,
        this.startDay.month,
        this.startDay.day
      );
      const endDate = new Date(
        this.endDay.year,
        this.endDay.month,
        this.endDay.day
      );
      return (
        currentDate.getTime() >= startDate.getTime() &&
        currentDate.getTime() <= endDate.getTime() &&
        !e.available
      );
    });
    if (unaivableDays && unaivableDays.length > 0) {
      return false;
    }

    const minBookingdurations = days.map((e) => {
      return e.minBookingDuration;
    });

    if (minBookingdurations) {
      this.minDuration = Math.max(...minBookingdurations);
    } else {
      return false;
    }

    const dayInMilliSeconds = 86400000;
    const durationInDays = Math.round(
      (endDate.getTime() - startDate.getTime()) / dayInMilliSeconds
    );

    if (durationInDays < this.minDuration) {
      this.minDurationError = true;
      return false;
    }

    return true;
  }

  changeMonth(change: number) {
    this.month += change;
    this.year = this.checkAndCorrectYear(this.month, this.year);
    this.month = this.checkAndCorrectMonth(this.month);
    if (change === -1) {
      const currentCalendarStartMonth = +this.calenderRangeStart.split('-')[1];
      const currentCalendarStartYear = +this.calenderRangeStart.split('-')[0];
      if (
        new Date(this.year, this.month).getTime() <
        new Date(currentCalendarStartYear, currentCalendarStartMonth).getTime()
      ) {
        this.calenderRangeStart = formatDate(
          new Date(this.year, this.month, 1),
          'yyyy-MM-dd',
          'en-US'
        );
      }
    } else {
      const currentCalendarEndMonth = +this.calenderRangeEnd.split('-')[1];
      const currentCalendarEndYear = +this.calenderRangeEnd.split('-')[0];
      if (
        new Date(this.year, this.month + 1).getTime() >=
        new Date(currentCalendarEndYear, currentCalendarEndMonth).getTime()
      ) {
        const m = this.checkAndCorrectMonth(this.month + 1);
        const y = this.checkAndCorrectYear(this.month + 1, this.year);
        this.calenderRangeEnd = formatDate(
          new Date(y, m, 1),
          'yyyy-MM-dd',
          'en-US'
        );
      }
    }
    this.availabilitySub.unsubscribe();
    this.availabilitySub = this.getAvailability().subscribe((data) => {
      this.availability = data;
    });
  }
  checkAndCorrectMonth(month: number) {
    return month > 11 ? month - 12 : month < 0 ? month + 12 : month;
  }
  checkAndCorrectYear(month: number, year: number) {
    return month > 11 ? year + 1 : month < 0 ? year - 1 : year;
  }

  close() {
    this.closed.emit('closed');
    this.updateData.emit({ data: [], days: 0 });
    this.selectedDate.emit('');
  }
  submit() {
    if (this.startDay && this.endDay) {
      const startDate = new Date(
        this.startDay.year,
        this.startDay.month,
        this.startDay.day
      );
      const endDate = new Date(
        this.endDay.year,
        this.endDay.month,
        this.endDay.day
      );
      this.selectedDate.emit(
        this.getDateString(startDate) + ' - ' + this.getDateString(endDate)
      );
      this.closed.emit('closed');
      const days = this.getDaysBetweenRange();
      this.updateData.emit({ days });
    }
  }

  getDaysBetweenRange(): AvailabilityDay[] {
    if (!this.startDay || !this.endDay) {
      return [];
    }
    const availability = this.availability.filter((e) => {
      if (!this.startDay) {
        return;
      }
      if (!this.endDay) {
        return;
      }
      return (
        e.month >= this.startDay.month &&
        e.year >= this.startDay.year &&
        e.month <= this.endDay.month &&
        e.year <= this.endDay.year
      );
    });

    const days: AvailabilityDay[] = [];
    availability.forEach((e) => {
      e.days.forEach((day) => {
        if (!this.startDay) {
          return;
        }
        if (!this.endDay) {
          return;
        }
        if (
          day.day >= this.startDay.day &&
          day.month >= this.startDay.month &&
          day.year >= this.startDay.year &&
          day.day < this.endDay.day &&
          day.month <= this.endDay.month &&
          day.year <= this.endDay.year
        ) {
          days.push(day);
        }
      });
    });
    return days;
  }
}
