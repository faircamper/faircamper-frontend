import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Subject } from 'rxjs';
import { TenantAccount } from 'src/app/tenant/models/Account/tenant';

@Component({
  selector: 'app-tenant-overview',
  templateUrl: './tenant-overview.component.html',
  styleUrls: ['./tenant-overview.component.css']
})
export class TenantOverviewComponent implements OnInit {

  @Input() tenant: TenantAccount | null = null;
  @Output() tenantOut = new EventEmitter<TenantAccount>();
  edit: boolean = false;
  saveEvent: Subject<void> = new Subject<void>();

  constructor() {}

  ngOnInit(): void {}

  saveTenant(): void {
    this.saveEvent.next();
    this.edit = !this.edit;
  }

  editTenant(): void {
    this.edit = !this.edit;
  }

  onTenantUpdate(t: TenantAccount): void {
    this.tenantOut.emit(t);
  }

}
