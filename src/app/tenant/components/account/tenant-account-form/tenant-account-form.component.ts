import { formatDate } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EMPTY, Observable, Subscription } from 'rxjs';
import { TenantAccount } from '../../../../tenant/models/Account/tenant';

@Component({
  selector: 'app-tenant-account-form',
  templateUrl: './tenant-account-form.component.html',
  styleUrls: ['./tenant-account-form.component.css']
})

export class TenantAccountFormComponent implements OnInit {

  editTenantForm: FormGroup = this.fb.group({});
  @Input() tenant: TenantAccount | null = null;
  @Input() min: string = '';//format '2022-05-22'
  @Input() max: string = formatDate(new Date(), 'YYYY-MM-dd', 'en_Us');//todays date
  @Input() disabledDate: boolean = false;
  @Input() saveEvent: Observable<void> = EMPTY;
  saveEventSubscription: Subscription = EMPTY.subscribe();
  @Output() tenantFormOut = new EventEmitter<TenantAccount>();

  constructor(public fb: FormBuilder) {
    this.editTenantForm = this.fb.group({
        street: ['', [Validators.required]],
        streetNumber: ['', [Validators.required]],
        postalCode: ['', [Validators.required]],
        city: ['', [Validators.required]],
        country: ['', [Validators.required]],
        birthday: ['',[Validators.required]],
        mobilePhone: ['', [Validators.required]],
        phone: [''],
    });
  }

  ngOnInit(): void {
    this.saveEventSubscription = this.saveEvent.subscribe(() => {
      const newtenant: TenantAccount = {
        ...this.editTenantForm.value,
      };
      if (this.editTenantForm.touched) {
        console.log(this.editTenantForm);
        //this.tenantFormOut.emit(newtenant);
      }
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.tenant) {
      this.editTenantForm.patchValue(this.tenant);
    }
  }

  ngOnDestroy(): void {
    this.saveEventSubscription.unsubscribe();
  }

  get getAccount(): FormGroup {
    return this.editTenantForm as FormGroup;
  }

}
