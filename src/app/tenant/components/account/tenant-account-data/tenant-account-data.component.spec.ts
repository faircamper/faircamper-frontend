import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TenantAccountDataComponent } from './tenant-account-data.component';

describe('TenantAccountDataComponent', () => {
  let component: TenantAccountDataComponent;
  let fixture: ComponentFixture<TenantAccountDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TenantAccountDataComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TenantAccountDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
