import { Component, Input, OnInit } from '@angular/core';
import { TenantAccount } from '../../../models/Account/tenant';

@Component({
  selector: 'app-tenant-account-data',
  templateUrl: './tenant-account-data.component.html',
  styleUrls: ['./tenant-account-data.component.css']
})
export class TenantAccountDataComponent implements OnInit {
  @Input() tenantAccountData: TenantAccount | null = null;

  constructor() { }

  ngOnInit(): void {
  }

}
