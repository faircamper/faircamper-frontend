import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { SSOStateService } from 'projects/wobi/sso/src/lib/services/state.service';
import { EMPTY, Subscription } from 'rxjs';
import { TenantAccount } from 'src/app/tenant/models/Account/tenant';
import { TenantAccountService } from 'src/app/tenant/services/http/account/tenant-account.service';

@Component({
  selector: 'app-account-page-tenant',
  templateUrl: './account-page-tenant.component.html',
  styleUrls: ['./account-page-tenant.component.css']
})

export class AccountPageTenantComponent implements OnInit, OnDestroy {
  account: TenantAccount | null = null;
  accountSubscription: Subscription = EMPTY.subscribe();
  
  constructor(
    public accountService: TenantAccountService,
    public fb: FormBuilder,
    protected sso: SSOStateService
  ) {}

  ngOnInit(): void {
    this.accountSubscription = this.accountService
      .get<TenantAccount>()
      .subscribe((data) => {
        this.account = data;
      });
  }

  onTenantUpdate(a: TenantAccount): void {
    console.log(a);
    if (this.account !== null) {
      this.account = a;
      this.updateAccount();
    }
  }

  ngOnDestroy(): void {
    this.accountSubscription.unsubscribe();
  }

  private updateAccount() {
    // if (this.account !== null) {
    //   this.accountService
    //     .update<TenantAccount>(this.account)
    //     .subscribe((acc) => {
    //       this.account = acc;
    //     });
    // }
    console.log(this.account);
  }
}
