import { Component, Input, OnInit } from '@angular/core';
import * as dayjs from 'dayjs';

@Component({
  selector: 'tenant-checkout-panding-page',
  templateUrl: './checkout-panding-page.component.html',
  styleUrls: ['./checkout-panding-page.component.css']
})
export class CheckoutPandingPageComponent implements OnInit {
  @Input() responseExpected: string = '';
  constructor() { }

  ngOnInit(): void {
    if(this.responseExpected !== ""){
      this.responseExpected = dayjs(new Date(this.responseExpected)).locale('de').format("DD.MM.YYYY");  
    }
    
  }

}
