import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckoutPandingPageComponent } from './checkout-panding-page.component';

describe('CheckoutPandingPageComponent', () => {
  let component: CheckoutPandingPageComponent;
  let fixture: ComponentFixture<CheckoutPandingPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CheckoutPandingPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckoutPandingPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
