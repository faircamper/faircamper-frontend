import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { EMPTY, Observable } from 'rxjs';
import { Booking } from 'src/app/tenant/models/booking';
import { bookingLoadAction } from 'src/app/tenant/ngrx/actions/booking.actions ';
import { selectBooking } from 'src/app/tenant/ngrx/selectors/booking.selector';

@Component({
  selector: 'app-checkout-page',
  templateUrl: './checkout-page.component.html',
  styleUrls: ['./checkout-page.component.css'],
})
export class CheckoutPageComponent implements OnInit {
  booking$: Observable<Booking> = EMPTY;
  constructor(private store: Store, private activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    const id = this.activatedRoute.snapshot.paramMap.get('id');
    console.log(id);
    console.log(id);
    console.log(id);
    console.log(id);
    if (id) {
      this.store.dispatch(bookingLoadAction({ id }));
    }
    this.booking$ = this.store.select(selectBooking);
  }
}
