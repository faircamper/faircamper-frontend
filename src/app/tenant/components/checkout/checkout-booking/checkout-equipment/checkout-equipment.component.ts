import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormBuilder } from '@angular/forms';
import { Store } from '@ngrx/store';
import { EMPTY, Observable } from 'rxjs';
import {
  BookingEquipment,
  BookingEquipmentCategory,
} from 'src/app/tenant/models/equipment/booking_equipment';
import { selectBookingEquipment } from 'src/app/tenant/ngrx/selectors/booking.selector';

@Component({
  selector: 'tenant-checkout-equipment',
  templateUrl: './checkout-equipment.component.html',
  styleUrls: ['./checkout-equipment.component.css'],
})
export class CheckoutEquipmentComponent implements OnInit {
  equipmentCategories$: Observable<BookingEquipmentCategory[]> = EMPTY;
  equipmentCategories: BookingEquipmentCategory[] = [];
  @Input() nights = 14;
  constructor(private store: Store) {}

  ngOnInit(): void {
    this.equipmentCategories$ = this.store.select(selectBookingEquipment);
    this.equipmentCategories$.subscribe((data) => {
      this.equipmentCategories = data;
    });
  }

  calculateSum(equipment: BookingEquipment): number {
    if (equipment.priceTypeId === 0) {
      return equipment.amount * equipment.price;
    } else if (equipment.priceTypeId === 1) {
      const v = this.nights * equipment.price;
      return v > 0 && v > equipment.maxPrice ? equipment.maxPrice : v;
    } else if (equipment.priceTypeId === 2) {
      const v = this.nights * equipment.price * equipment.amount;
      return v > 0 && v > equipment.maxPrice ? equipment.maxPrice : v;
    } else if (equipment.priceTypeId === 3) {
      return equipment.price;
    } else if (equipment.priceTypeId === 4) {
      return 0;
    }

    return 0;
  }

  test(eq: BookingEquipment, n: number) {
    console.log(eq);
    console.log(n);
    eq.amount = n;
  }
}
