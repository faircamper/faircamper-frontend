import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckoutPersonalDataComponent } from './checkout-personal-data.component';

describe('CheckoutPersonalDataComponent', () => {
  let component: CheckoutPersonalDataComponent;
  let fixture: ComponentFixture<CheckoutPersonalDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CheckoutPersonalDataComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckoutPersonalDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
