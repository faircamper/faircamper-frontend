import { Component, OnInit } from '@angular/core';
import { ERoles, IRoleRequirements } from '@wobi/sso-state';
@Component({
  selector: 'tenant-checkout-personal-data',
  templateUrl: './checkout-personal-data.component.html',
  styleUrls: ['./checkout-personal-data.component.css']
})
export class CheckoutPersonalDataComponent implements OnInit {

  
  public reqLoggedOut: IRoleRequirements = {
    onlyVisitors: true, 
  }
  public reqLoggedIn: IRoleRequirements = {
    all: [ERoles.Registered], 
  }

  constructor() { }
  
  ngOnInit(): void {

  }

}


