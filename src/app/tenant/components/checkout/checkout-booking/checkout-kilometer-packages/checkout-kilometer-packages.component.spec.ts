import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckoutKilometerPackagesComponent } from './checkout-kilometer-packages.component';

describe('CheckoutKilometerPackagesComponent', () => {
  let component: CheckoutKilometerPackagesComponent;
  let fixture: ComponentFixture<CheckoutKilometerPackagesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CheckoutKilometerPackagesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckoutKilometerPackagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
