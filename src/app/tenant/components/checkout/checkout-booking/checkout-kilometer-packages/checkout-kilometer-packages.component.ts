import { Component, EventEmitter, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Observable, EMPTY } from 'rxjs';
import { KmPackage } from 'src/app/tenant/models/booking';
import { selectBookingKmPackages } from 'src/app/tenant/ngrx/selectors/booking.selector';

@Component({
  selector: 'tenant-checkout-kilometer-packages',
  templateUrl: './checkout-kilometer-packages.component.html',
  styleUrls: ['./checkout-kilometer-packages.component.css']
})
export class CheckoutKilometerPackagesComponent implements OnInit {

  kmPackages$: Observable<KmPackage[]> = EMPTY;

  constructor(private store: Store,public fb: FormBuilder) { }

  ngOnInit(): void {
    this.kmPackages$ = this.store.select(selectBookingKmPackages);
  }
}
