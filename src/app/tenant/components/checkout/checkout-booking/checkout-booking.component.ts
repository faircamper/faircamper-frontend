import { Component, Input, OnInit } from '@angular/core';
import { Booking } from 'src/app/tenant/models/booking';

@Component({
  selector: 'tenant-checkout-booking',
  templateUrl: './checkout-booking.component.html',
  styleUrls: ['./checkout-booking.component.css'],
})
export class CheckoutBookingComponent implements OnInit {
  @Input() booking: Booking | undefined = undefined;
  constructor() {}

  ngOnInit(): void {}
}
