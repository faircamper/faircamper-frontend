import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckoutKilometerPackagesFormComponent } from './checkout-kilometer-packages-form.component';

describe('CheckoutKilometerPackagesFormComponent', () => {
  let component: CheckoutKilometerPackagesFormComponent;
  let fixture: ComponentFixture<CheckoutKilometerPackagesFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CheckoutKilometerPackagesFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckoutKilometerPackagesFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
