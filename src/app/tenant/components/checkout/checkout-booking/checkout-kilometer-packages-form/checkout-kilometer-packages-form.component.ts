import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-checkout-kilometer-packages-form',
  templateUrl: './checkout-kilometer-packages-form.component.html',
  styleUrls: ['./checkout-kilometer-packages-form.component.css']
})
export class CheckoutKilometerPackagesFormComponent implements OnInit {

  @Input() package:any

  form!: FormGroup;
  amount: number = 0;
  
  constructor(public fb: FormBuilder) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      id:[this.package.id],
      amountPicked:[this.package.amountPicked]
    });
  }

  get getAmountPicked(): FormControl{
    return this.form.get('amountPicked') as FormControl
  }
  
  callOnChange(e:Event): void{
    this.form.value.amountPicked;
    console.log(this.form.value);    
  }

}
