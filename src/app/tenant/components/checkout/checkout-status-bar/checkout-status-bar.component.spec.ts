import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckoutStatusBarComponent } from './checkout-status-bar.component';

describe('CheckoutStatusBarComponent', () => {
  let component: CheckoutStatusBarComponent;
  let fixture: ComponentFixture<CheckoutStatusBarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CheckoutStatusBarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckoutStatusBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
