import { Component, Input, OnInit } from '@angular/core';
@Component({
  selector: 'app-checkout-status-bar',
  templateUrl: './checkout-status-bar.component.html',
  styleUrls: ['./checkout-status-bar.component.css'],
})
export class CheckoutStatusBarComponent implements OnInit {

  @Input() bookingState:
    | 'open'
    | 'pending'
    | 'accepted'
    | 'insurance'
    | 'finished' = 'open';
  constructor() {}

  ngOnInit(): void {}
}
