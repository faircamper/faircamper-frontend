import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { EffectsModule } from '@ngrx/effects';
// ngrx configuration
import { Store, StoreModule } from '@ngrx/store';
import { SSOModule } from '@wobi/sso';
import { FcLibModule } from 'fc-lib';
import { ImageCropperModule } from 'ngx-image-cropper';
import { CompanyDataComponent } from './components/account/company-details/company-details.component';
import { CompanyDataFormComponent } from './components/account/company-form/company-form.component';
import { CompanyComponent } from './components/account/company/company.component';
import { DocumentListComponent } from './components/account/document-list/document-list.component';
import { DocumentUploadFormComponent } from './components/account/document-upload-form/document-upload-form.component';
import { DocumentsComponent } from './components/account/documents/documents.component';
import { AccountPageComponent } from './components/account/pages/account-page/account-page.component';
import { PersonFormComponent } from './components/account/person-form/person-form.component';
import { PersonListComponent } from './components/account/person-list/person-list.component';
import { PersonalDataComponent } from './components/account/personal-data/personal-data.component';
import { PersonsComponent } from './components/account/persons/persons.component';
import { DashboardComponent } from './components/dashboard/pages/dashboard/dashboard.component';
import { CalendarTimelineComponent } from './components/seasons/calendar-timeline/calendar-timeline.component';
import { SeasonsPageComponent } from './components/seasons/pages/seasons-page/seasons-page.component';
import { PriceLevelFormComponent } from './components/seasons/price-level-form/price-level-form.component';
import { PriceLevelListComponent } from './components/seasons/price-level-list/price-level-list.component';
import { SeasonCollectionFormComponent } from './components/seasons/season-collection-form/season-collection-form.component';
import { YearSelectorComponent } from './components/seasons/season-collection-selector/season-collection-selector.component';
import { SeasonFormComponent } from './components/seasons/season-form/season-form.component';
import { StationCreateComponent } from './components/station/pages/station-create/station-create.component';
import { StationEditComponent } from './components/station/pages/station-edit/station-edit.component';
import { StationFormComponent } from './components/station/station-form/station-form.component';
import { StationListComponent } from './components/station/station-list/station-list.component';
import { StationMetadataFormComponent } from './components/station/station-metadata-form/station-metadata-form.component';
import { StationsOpeningTimesComponent } from './components/station/station-opening-hours/station-opening-hours.component';
import { StationsComponent } from './components/station/stations/stations.component';
import { ImageUploadGalleryComponent } from './components/vehicle/image-upload-gallery/image-upload-gallery.component';
import { ImageUploadComponent } from './components/vehicle/image-upload/image-upload.component';
import { VehicleCreateComponent } from './components/vehicle/pages/vehicle-create/vehicle-create.component';
import { VehicleEditComponent } from './components/vehicle/pages/vehicle-edit/vehicle-edit.component';
import { VehicleListComponent } from './components/vehicle/pages/vehicle-list/vehicle-list.component';
import { VehicleBasisDataFormComponent } from './components/vehicle/vehicle-basis-data-form/vehicle-basis-data-form.component';
import { VehicleBedsAndSeatsFormComponent } from './components/vehicle/vehicle-beds-and-seats-form/vehicle-beds-and-seats-form.component';
import { VehicleCategoryFormComponent } from './components/vehicle/vehicle-category-form/vehicle-category-form.component';
import { VehicleFormComponent } from './components/vehicle/vehicle-form/vehicle-form.component';
import { VehicleMileageFormComponent } from './components/vehicle/vehicle-mileage-form/vehicle-mileage-form.component';
import { VehicleNameDescriptionFormComponent } from './components/vehicle/vehicle-name-description-form/vehicle-name-description-form.component';
import { VehiclePricesFormComponent } from './components/vehicle/vehicle-prices-form/vehicle-prices-form.component';
import { VehicleRentalTermsFormComponent } from './components/vehicle/vehicle-rental-terms-form/vehicle-rental-terms-form.component';
import { LessorRoutingModule } from './lessor-routing.module';
import { LessorComponent } from './lessor.component';
import { accountLoadAction } from './ngrx/actions/account.actions';
import { AccountEffects } from './ngrx/effects/account.effects';
import { initialModuleState } from './ngrx/module.state';
import { moduleReducers } from './ngrx/reducers/module.reducer';

import { BookingCalendarPageHeaderComponent } from './components/booking-calendar-page/booking-calendar-header/booking-calendar-page-header.component';
import { BookingCalendarPageComponent } from './components/booking-calendar-page/booking-calendar-page.component';
import { BookingCalendarTimelineComponent } from './components/booking-calendar-timeline/booking-calendar-timeline.component';
import { EquipmentItemFormComponent } from './components/vehicle/equipment-item-form/equipment-item-form.component';
import { EquipmentLinkSelectComponent } from './components/vehicle/equipment-link-select/equipment-link-select.component';
import { EquipmentLinkComponent } from './components/vehicle/equipment-link/equipment-link.component';
import { EquipmentComponent } from './components/vehicle/equipment/equipment.component';
import { OptionalEquipmentFormComponent } from './components/vehicle/optional-equipment-form/optional-equipment-form.component';
import { OptionalEquipmentListComponent } from './components/vehicle/optional-equipment-list/optional-equipment-list.component';
import { OptionalEquipmentComponent } from './components/vehicle/optional-equipment/optional-equipment.component';

@NgModule({
  declarations: [
    LessorComponent,
    AccountPageComponent,
    VehicleCreateComponent,
    VehicleFormComponent,
    VehicleBasisDataFormComponent,
    VehicleNameDescriptionFormComponent,
    VehicleCategoryFormComponent,
    VehicleBedsAndSeatsFormComponent,
    ImageUploadGalleryComponent,
    ImageUploadComponent,
    VehicleMileageFormComponent,
    VehiclePricesFormComponent,
    VehicleRentalTermsFormComponent,
    VehicleCreateComponent,
    DashboardComponent,
    DocumentUploadFormComponent,
    PersonalDataComponent,
    CompanyDataComponent,
    CompanyDataFormComponent,
    CompanyComponent,
    DocumentsComponent,
    DocumentListComponent,
    PersonsComponent,
    PersonListComponent,
    PersonFormComponent,
    VehicleListComponent,
    StationListComponent,
    StationEditComponent,
    StationCreateComponent,
    StationFormComponent,
    StationsOpeningTimesComponent,
    StationMetadataFormComponent,
    StationsComponent,
    VehicleEditComponent,
    SeasonsPageComponent,
    CalendarTimelineComponent,
    PriceLevelFormComponent,
    PriceLevelListComponent,
    YearSelectorComponent,
    SeasonFormComponent,
    SeasonCollectionFormComponent,
    BookingCalendarPageComponent,
    BookingCalendarTimelineComponent,
    BookingCalendarPageHeaderComponent,
    EquipmentComponent,
    OptionalEquipmentComponent,
    OptionalEquipmentListComponent,
    OptionalEquipmentFormComponent,
    EquipmentLinkComponent,
    EquipmentLinkSelectComponent,
    EquipmentItemFormComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule,
    LessorRoutingModule,
    FcLibModule,
    SSOModule,
    ImageCropperModule,
    StoreModule.forFeature('@wobi/lessor', moduleReducers, {
      initialState: initialModuleState,
    }),
    EffectsModule.forFeature([AccountEffects]),
  ],
})
export class LessorModule {
  constructor(store: Store) {
    store.dispatch(accountLoadAction());
  }
}
