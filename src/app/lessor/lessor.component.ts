import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Subscription, EMPTY } from 'rxjs';
import { MessagesService, Message } from './services/message/messages.service';
import { ERoles, IRoleRequirements } from '@wobi/sso-state';
import { SSOService } from '@wobi/sso';

@Component({
  selector: 'app-lessor',
  templateUrl: './lessor.component.html',
  styleUrls: ['./lessor.component.css'],
})
export class LessorComponent implements OnInit {
  public loginRequirements: IRoleRequirements = {
    onlyVisitors: true,
  };

  public logoutRequirements: IRoleRequirements = {
    in: [ERoles.Pending, ERoles.Registered],
  };

  isOpen = true;
  expanded = false;
  msgSubscription: Subscription = EMPTY.subscribe();
  url = '';
  locale = '';
  constructor(
    private messages: MessagesService,
    private router: Router,
    private ssoSvc: SSOService,
    @Inject(LOCALE_ID) locale: string
  ) {
    this.locale = locale;
    router.events.subscribe((data) => {
      if (data instanceof NavigationEnd) {
        this.url = data.url;
      }
    });
  }

  ngOnInit(): void {
    this.msgSubscription = this.messages.getMessages().subscribe((msg) => {
      this.displayMessage(msg);
    });

    this.url = this.router.url;
  }

  /**
    displayMessage
  **/
  public displayMessage(msg: Message): void {
    const errCode = msg.errorCode === undefined ? '' : msg.errorCode;
    // Open Message System here
  }

  ngOnDestroy(): void {
    this.msgSubscription.unsubscribe();
  }

  isUrl(url: string): boolean {
    return this.url === url;
  }

  doLogin() {
    this.ssoSvc.login(`/${this.locale}/lessor`);
  }

  doLogout() {
    this.ssoSvc.logout();
  }
}
