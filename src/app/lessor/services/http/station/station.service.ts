import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MessagesService } from '../../message/messages.service';
import { CrudService } from '../crud-service/crud.service';

@Injectable({
  providedIn: 'root',
})
export class StationService extends CrudService {
  constructor(private http: HttpClient, override messages: MessagesService) {
    super(http, 'stations', messages);
  }
}
