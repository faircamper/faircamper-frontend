import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MessagesService } from 'src/app/shared/services/message/messages.service';
import { environment } from 'src/environments/environment';
import { CrudService } from '../crud-service/crud.service';

const baseURL = environment.apiURL;
const headers = new HttpHeaders({ 'Content-Type': 'application/json; charset=utf-8', 'Accept-Language': 'en' });
@Injectable({
    providedIn: 'root',
})
export class MileageService extends CrudService {
    constructor(protected httpClient: HttpClient, protected messages: MessagesService) {
        super(httpClient, 'vehicles/mileagepacks', messages);
    }
}
