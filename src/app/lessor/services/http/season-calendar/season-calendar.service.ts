import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { Season, SeasonCollection } from 'src/app/lessor/models/seasons/season';
import { MessagesService } from '../../message/messages.service';
import { CrudService } from '../crud-service/crud.service';

@Injectable({
  providedIn: 'root',
})
export class SeasonCollectionService extends CrudService {
  constructor(httpClient: HttpClient, messages: MessagesService) {
    super(httpClient, 'seasoncollection', messages);
  }

  /**
   * name
   */
  public override get<T>(id?: number | string | undefined): Observable<T> {
    return super.get<T>(id).pipe(
      map((sc: T) => {
        <SeasonCollection>(<any>sc).seasons.forEach((season: Season) => {
          season.from = new Date(season.from);
          season.to = new Date(season.to);
        });
        return sc;
      })
    );
  }
}
