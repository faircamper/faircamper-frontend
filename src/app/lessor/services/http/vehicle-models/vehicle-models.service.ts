import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MessagesService } from '../../message/messages.service';
import { CrudService } from '../crud-service/crud.service';

@Injectable({
	providedIn: 'root',
})
export class VehicleModelsService extends CrudService {
	constructor(protected override httpClient: HttpClient, protected override messages: MessagesService) {
		super(httpClient, 'vehicles/models', messages);
	}
}
