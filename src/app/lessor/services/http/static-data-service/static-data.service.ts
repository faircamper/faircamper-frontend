import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EMPTY, Observable, of, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { MessagesService } from '../../message/messages.service';
import { CrudService } from '../crud-service/crud.service';
import { StaticData, IdValue } from './staticData';

@Injectable({
  providedIn: 'root',
})
export class StaticDataService extends CrudService {
  staticData: StaticData | undefined;
  staticSub: Observable<StaticData> = EMPTY;
  loadingStaticData = false;
  constructor(
    override httpClient: HttpClient,
    override messages: MessagesService
  ) {
    super(httpClient, 'static', messages);
  }

  preloadStaticData(): void {
    this.get<StaticData>().subscribe((data) => {});
  }
  getBedTypes(): Observable<IdValue[]> {
    if (!this.staticData) {
      if (!this.loadingStaticData) {
        this.staticSub = this.get<StaticData>();
        this.loadingStaticData = true;
      }
      return this.staticSub.pipe(
        map((data) => {
          this.loadingStaticData = false;
          this.staticData = data;
          return data.bedTypes;
        })
      );
    } else {
      return of(this.staticData.bedTypes);
    }
  }

  bedTypeIdToName(id: number): string {
    if (!this.staticData) {
      if (!this.loadingStaticData) {
        this.loadingStaticData = true;
        const sub = this.get<StaticData>().subscribe((data) => {
          this.staticData = data;
          this.loadingStaticData = false;
          sub.unsubscribe();
        });
      }
    } else {
      const priceTypes = this.staticData.bedTypes;
      const name = priceTypes.find((e) => e.id === id)?.name;
      if (!name) {
        return 'Unknown Price Type';
      }
      return name;
    }
    return 'Unknown Price Type';
  }

  getVehicleFuelTypes(): Observable<IdValue[]> {
    if (!this.staticData) {
      if (!this.loadingStaticData) {
        this.staticSub = this.get<StaticData>();
        this.loadingStaticData = true;
      }
      return this.staticSub.pipe(
        map((data) => {
          this.loadingStaticData = false;
          this.staticData = data;
          return data.fuelValues;
        })
      );
    } else {
      return of(this.staticData.fuelValues);
    }
  }

  getWeightValues(): Observable<IdValue[]> {
    if (!this.staticData) {
      if (!this.loadingStaticData) {
        this.staticSub = this.get<StaticData>();
        this.loadingStaticData = true;
      }
      return this.staticSub.pipe(
        map((data) => {
          this.loadingStaticData = false;
          this.staticData = data;
          return data.weights;
        })
      );
    } else {
      return of(this.staticData.weights);
    }
  }

  getGearTypes(): Observable<IdValue[]> {
    if (!this.staticData) {
      if (!this.loadingStaticData) {
        this.staticSub = this.get<StaticData>();
        this.loadingStaticData = true;
      }
      return this.staticSub.pipe(
        map((data) => {
          this.loadingStaticData = false;
          this.staticData = data;
          return data.gears;
        })
      );
    } else {
      return of(this.staticData.gears);
    }
  }

  getConsumptionValues(): Observable<IdValue[]> {
    if (!this.staticData) {
      if (!this.loadingStaticData) {
        this.staticSub = this.get<StaticData>();
        this.loadingStaticData = true;
      }
      return this.staticSub.pipe(
        map((data) => {
          this.loadingStaticData = false;
          this.staticData = data;
          return data.consumptions;
        })
      );
    } else {
      return of(this.staticData.consumptions);
    }
  }

  getVehicleModels(): Observable<IdValue[]> {
    if (!this.staticData) {
      if (!this.loadingStaticData) {
        this.staticSub = this.get<StaticData>();
        this.loadingStaticData = true;
      }
      return this.staticSub.pipe(
        map((data) => {
          this.loadingStaticData = false;
          this.staticData = data;
          return data.vehicleModels;
        })
      );
    } else {
      return of(this.staticData.vehicleModels);
    }
  }

  getVehicleTypes(): Observable<IdValue[]> {
    if (!this.staticData) {
      if (!this.loadingStaticData) {
        this.staticSub = this.get<StaticData>();
        this.loadingStaticData = true;
      }
      return this.staticSub.pipe(
        map((data) => {
          this.loadingStaticData = false;
          this.staticData = data;
          return data.vehicleTypes;
        })
      );
    } else {
      return of(this.staticData.vehicleTypes);
    }
  }

  getEquipmentPriceTypes(): Observable<IdValue[]> {
    if (!this.staticData) {
      if (!this.loadingStaticData) {
        this.staticSub = this.get<StaticData>();
        this.loadingStaticData = true;
      }
      return this.staticSub.pipe(
        map((data) => {
          this.loadingStaticData = false;
          this.staticData = data;
          return data.equipmentPriceTypes;
        })
      );
    } else {
      return of(this.staticData.equipmentPriceTypes);
    }
  }

  equipmentPriceTypeIdToName(id: number): string {
    if (!this.staticData) {
      if (!this.loadingStaticData) {
        this.loadingStaticData = true;
        const sub = this.get<StaticData>().subscribe((data) => {
          this.staticData = data;
          this.loadingStaticData = false;
          sub.unsubscribe();
        });
      }
    } else {
      const priceTypes = this.staticData.equipmentPriceTypes;
      const name = priceTypes.find((e) => e.id === id)?.name;
      if (!name) {
        return 'Unknown Price Type';
      }
      return name;
    }
    return 'Unknown Price Type';
  }
}
