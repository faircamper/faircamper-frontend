import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, ObservableInput, throwError } from 'rxjs';
import { timeout, retry, catchError, map } from 'rxjs/operators';
import { MessagesService } from 'src/app/shared/services/message/messages.service';
import { environment } from 'src/environments/environment';
import { CrudService } from '../crud-service/crud.service';

const baseURL = environment.apiURL;
const headers = new HttpHeaders({ 'Content-Type': 'application/json; charset=utf-8', 'Accept-Language': 'en' });
@Injectable({
	providedIn: 'root',
})
export class LessorService extends CrudService {
	constructor(protected httpClient: HttpClient, protected messages: MessagesService) {
		super(httpClient, 'lessors', messages);
	}
}
// 	get<T>(): Observable<T> {
// 		let url = baseURL + this.entityURL;
// 		return this.httpClient
// 			.get<ItemResponse<T>>(url, { headers })
// 			.pipe(
// 				timeout(environment.timeout),
// 				retry(environment.retries),
// 				catchError(handleError(this.messages)),
// 				map((response: any) => {
// 					return response.data;
// 				})
// 			);
// 	}
// }

// function handleError(msg: MessagesService) {
// 	return <T>(err: any, caught: Observable<ItemResponse<T>>): ObservableInput<any> => {
// 		msg.send({ errorCode: err.status, text: 'something went wrong' });
// 		return throwError(err);
// 	};
// }

// interface ItemResponse<T> {
// 	errors: any | null;
// 	events: any | null;
// 	data: T;
// }
