import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MessagesService } from '../../message/messages.service';
import { CrudService } from '../crud-service/crud.service';

@Injectable({
  providedIn: 'root',
})
export class VehicleService extends CrudService {
  constructor(
    override httpClient: HttpClient,
    override messages: MessagesService
  ) {
    super(httpClient, 'vehicles', messages);
  }
}
