/* tslint:disable:no-unused-variable */

import { HttpClient, HttpHandler } from '@angular/common/http';
import { TestBed, async, inject } from '@angular/core/testing';
import { UrlHelperService } from 'angular-oauth2-oidc';
import { EquipmentService } from './equipment.service';

describe('Service: Equipment', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [EquipmentService, HttpClient, HttpHandler, UrlHelperService],
		});
	});

	it('should ...', inject([EquipmentService], (service: EquipmentService) => {
		expect(service).toBeTruthy();
	}));
});
