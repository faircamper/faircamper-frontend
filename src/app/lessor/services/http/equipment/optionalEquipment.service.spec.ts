import { HttpClient, HttpHandler } from '@angular/common/http';
import { TestBed, async, inject } from '@angular/core/testing';
import { UrlHelperService } from 'angular-oauth2-oidc';
import { OptionalEquipmentService } from './optionalEquipment.service';

describe('Service: OptionalEquipment', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [OptionalEquipmentService, HttpClient, HttpHandler, UrlHelperService],
		});
	});

	it('should ...', inject([OptionalEquipmentService], (service: OptionalEquipmentService) => {
		expect(service).toBeTruthy();
	}));
});