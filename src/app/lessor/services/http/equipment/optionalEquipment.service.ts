import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { OptionalEquipmentCategory } from 'src/app/lessor/models/equipment/equipment';
import { MessagesService } from '../../message/messages.service';
import {
  CrudService
} from '../crud-service/crud.service';

@Injectable({
  providedIn: 'root',
})
export class OptionalEquipmentService extends CrudService {
  constructor(
    override httpClient: HttpClient,
    override messages: MessagesService
  ) {
    super(httpClient, 'optionalequipment', messages);
  }

  categoriesUrl = 'categories';
  /**
   * get optional equipment categories
   */
  public getCategories(): Observable<OptionalEquipmentCategory[]> {
    return this.list([], this.categoriesUrl);
  }
}
