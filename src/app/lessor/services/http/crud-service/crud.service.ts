import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable, ObservableInput, throwError } from 'rxjs';
import { catchError, map, retry, timeout } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { MessagesService } from '../../message/messages.service';

export const baseURL = environment.apiURL;
const headers = new HttpHeaders({
  'Content-Type': 'application/json; charset=utf-8',
  'Accept-Language': 'de',
});
@Injectable({
  providedIn: 'root',
})
export abstract class CrudService {
  constructor(
    protected httpClient: HttpClient,
    @Inject(String) protected entityURL: string,
    protected messages: MessagesService
  ) {}

  get<T>(id?: number | string | undefined): Observable<T> {
    let url = baseURL + this.entityURL;
    if (this.entityURL !== 'lessors' && this.entityURL !== 'static') {
      if (id) {
        url += '/' + id;
      }
    }
    return this.httpClient.get<ItemResponse<T>>(url, { headers }).pipe(
      timeout(environment.timeout),
      retry(environment.retries),
      catchError(handleError(this.messages)),
      map((response) => {
        return response.data;
      })
    );
  }

  list<T>(parameters?: string[], urlSegment?: string): Observable<T[]> {
    const params = parameters ? '?' + parameters.join('&') : '';
    let url = '';
    if (urlSegment) {
      url = `${baseURL}${this.entityURL}/${urlSegment}${params}`;
    } else {
      url = `${baseURL}${this.entityURL}${params}`;
    }
    return this.httpClient.get<ListResponse<T>>(url, { headers }).pipe(
      timeout(environment.timeout),
      retry(environment.retries),
      catchError(handleError(this.messages)),
      map((response) => response.data)
    );
  }

  create<T>(obj: T): Observable<T> {
    return this.httpClient
      .post<ItemResponse<T>>(baseURL + this.entityURL, obj, { headers })
      .pipe(
        timeout(environment.timeout),
        retry(environment.retries),
        catchError(handleError(this.messages)),
        map((response) => response.data)
      );
  }

  update<T extends { id?: number | string }>(obj: T): Observable<T> {
    if (!obj.id) {
      this.messages.send({
        text: ' No id set on PUT method for ' + this.entityURL,
      });
    }
    var idStr = obj.id ? '/' + obj.id : '';
    return this.httpClient
      .put<ItemResponse<T>>(baseURL + this.entityURL + idStr, obj, { headers })
      .pipe(
        timeout(environment.timeout),
        retry(environment.retries),
        catchError(handleError(this.messages)),
        map((response) => response.data)
      );
  }

  delete<T extends { id?: number | string }>(obj: T): any {
    if (this.entityURL !== 'lessors') {
      if (!obj.id) {
        this.messages.send({
          text: ' No id set on PUT method for ' + this.entityURL,
        });
      }
    }
    var idStr = obj.id ? '/' + obj.id : '';
    return this.httpClient
      .delete<ItemResponse<T>>(baseURL + this.entityURL + idStr, { headers })
      .pipe(
        timeout(environment.timeout),
        retry(environment.retries),
        catchError(handleError(this.messages)),
        map((response) => response.data)
      );
  }
}

function handleError(msg: MessagesService) {
  return <T>(
    err: any,
    caught: Observable<ItemResponse<T>>
  ): ObservableInput<any> => {
    msg.send({ errorCode: err.status, text: 'something went wrong' });
    return throwError(() => err);
  };
}

interface ItemResponse<T> {
  errors: any | null;
  events: any | null;
  data: T;
}

export interface ListResponse<T> {
  errors: any | null;
  events: any | null;
  data: T[];
}
