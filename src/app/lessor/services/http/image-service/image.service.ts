import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
	providedIn: 'root',
})
export class ImageService {
	constructor(protected httpClient: HttpClient) {}
	add(image: any): Observable<any> {
		return this.httpClient.post(environment.apiURL + 'images', image);
	}
}
