import { Injectable } from '@angular/core';

@Injectable({
	providedIn: 'root',
})
export class CurrencyService {
	constructor() {}

	cleanCurrencyFormat(priceString: string): number {
		priceString = '' + priceString;
		priceString = priceString.replace(/[^0-9$.,]/g, '');
		if (priceString[priceString.length - 3] === '.' || priceString[priceString.length - 3] === ',') {
			priceString = priceString.replace(/\D/g, '');
			const position = priceString.length - 2;
			const output = [priceString.slice(0, position), '.', priceString.slice(position)].join('');
			return +output;
		} else if (priceString[priceString.length - 2] === '.' || priceString[priceString.length - 2] === ',') {
			priceString = priceString.replace(/\D/g, '');
			const position = priceString.length - 1;
			const output = [priceString.slice(0, position), '.', priceString.slice(position)].join('');
			return +output;
		} else {
			return +priceString.replace(/\D/g, '');
		}
	}

	toCent(priceString: string): number {
		priceString = '' + priceString;
		priceString = priceString.replace(/[^0-9$.,]/g, '');
		// Morocco Specialcase:
		priceString = priceString.replace(/.د.م./g, '');
		if (priceString[priceString.length - 3] === '.' || priceString[priceString.length - 3] === ',') {
			priceString = priceString.replace(/\D/g, '');
			return +priceString;
		} else if (priceString[priceString.length - 2] === '.' || priceString[priceString.length - 2] === ',') {
			priceString = (priceString + '0').replace(/\D/g, '');
			return +priceString;
		} else {
			priceString = (priceString + '00').replace(/\D/g, '');
			return +priceString;
		}
	}

	toEuro(cents: number): number {
		return Math.round((cents + Number.EPSILON) * 100) / 10000;
	}
}
