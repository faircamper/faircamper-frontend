import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { DocumentResponse } from 'src/app/lessor/models/account/lessor';
import { ServerResponse as Resp } from 'src/app/lessor/models/server-response';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class DocumentService {
  constructor(public client: HttpClient) {}

  /**
   * uploadDocument
   */
  public uploadDocument(
    formData: FormData
  ): Observable<Resp<DocumentResponse>> {
    return this.client.post<Resp<DocumentResponse>>(
      `${environment.apiURL}account/documents`,
      formData
    );
  }
}
