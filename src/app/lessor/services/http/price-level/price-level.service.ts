import { HttpClient } from '@angular/common/http';
import { CrudService } from '../crud-service/crud.service';
import { Injectable } from '@angular/core';
import { MessagesService } from '../../message/messages.service';

@Injectable({
  providedIn: 'root',
})
export class PriceLevelService extends CrudService {
  constructor(
    httpClient: HttpClient,
    messages: MessagesService
  ) {
    super(httpClient, 'pricelevel', messages);
  }
}
