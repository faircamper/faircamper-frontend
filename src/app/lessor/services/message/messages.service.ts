import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MessagesService {

  messagesSubject: Subject<Message> = new Subject<Message>()
  constructor() {
  }

  /**
   * send
   */
  public send(msg: Message): void {
    this.messagesSubject.next(msg);
  }


  /**
   * getMessageSubject
   */
  public getMessages(): Subject<Message> {
    return this.messagesSubject;
  }


}


export interface Message {
  text: string;
  errorCode?: number;
  httpError?: HttpErrorResponse;
}
