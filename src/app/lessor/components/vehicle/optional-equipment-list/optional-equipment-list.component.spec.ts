import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OptionalEquipmentListComponent } from './optional-equipment-list.component';

describe('OptionalEquipmentListComponent', () => {
  let component: OptionalEquipmentListComponent;
  let fixture: ComponentFixture<OptionalEquipmentListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OptionalEquipmentListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OptionalEquipmentListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
