import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { OptionalEquipment } from 'src/app/lessor/models/equipment/equipment';

@Component({
  selector: 'app-optional-equipment-list',
  templateUrl: './optional-equipment-list.component.html',
  styleUrls: ['./optional-equipment-list.component.css'],
})
export class OptionalEquipmentListComponent implements OnInit, OnChanges {
  @Input() optionalEquipment: OptionalEquipment[] | null = [];

  @Input() selectedOptionEquipmentIds: string[] | undefined = [];

  @Output() optionalEquipmentSelect = new EventEmitter<string[]>();

  @Output() optionalEquipmentEdit = new EventEmitter<OptionalEquipment>();

  optionalEquipmentForm: FormGroup = this.fb.group({});
  columns = [
    {
      name: $localize`:@@fc-station-list:Kategorie`,
      sortable: true,
      sort: 'externalId',
    },
    {
      name: $localize`:@@fc-station-list:Name`,
      sortable: true,
      sort: 'externalId',
    },
    {
      name: $localize`:@@fc-station-list:Beschreibung`,
      sortable: true,
      sort: 'externalId',
    },
  ];

  constructor(private fb: FormBuilder) {}
  ngOnChanges(changes: SimpleChanges): void {
    if (!this.optionalEquipment) {
      return;
    }
    this.optionalEquipmentForm = this.equipmentListToFormGroup(
      this.optionalEquipment
    );
  }

  private equipmentListToFormGroup(list: OptionalEquipment[]): FormGroup {
    return this.fb.group({
      optionalFormArray: this.fb.array(
        list.map((optionalEquipment) =>
          this.fb.group({
            isChecked: this.selectedOptionEquipmentIds?.find(
              (e) => optionalEquipment.id === e
            ),
            id: optionalEquipment.id,
          })
        )
      ),
    });
  }

  ngOnInit(): void {}

  editOptionalEquipment(item: OptionalEquipment): void {
    this.optionalEquipmentEdit.emit(item);
  }

  deleteOptionalEquipment() {
    console.log('DELETE');
  }

  onSelectOptionalEquipment(item: OptionalEquipment): void {
    const rawFormValue = this.optionalEquipmentForm.get('optionalFormArray')
      ?.value as result[];
    const selectedIds = rawFormValue
      .filter((e) => e.isChecked && e.id)
      .map((e: result) => e.id);

    this.optionalEquipmentSelect.emit(selectedIds);
  }
}

interface result {
  id: string;
  isChecked: boolean | null;
}
