import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { EMPTY } from 'rxjs';

@Component({
  selector: 'lessor-vehicle-rental-terms-form',
  templateUrl: './vehicle-rental-terms-form.component.html',
  styleUrls: ['./vehicle-rental-terms-form.component.scss'],
})
export class VehicleRentalTermsFormComponent implements OnDestroy, OnInit {
  @Input() rentaltermsId: string | number | undefined;
  @Input() isEdit = false;
  @Input() vehicleForm: FormGroup | null = null;
  rentalServicePostSub = EMPTY.subscribe();
  rentalServicePutSub = EMPTY.subscribe();

  // TODO: [WOBI-1003] European Countries in DB
  europeanCountries = [
    'Albanien',
    'Andorra',
    'Belarus',
    'Belgien',
    'Bosnien und Herzegowina',
    'Bulgarien',
    'Dänemark',
    'Deutschland',
    'Estland',
    'Finnland',
    'Frankreich',
    'Griechenland',
    'Irland',
    'Island',
    'Italien',
    'Kasachstan',
    'Kosovo',
    'Kroatien',
    'Lettland',
    'Liechtenstein',
    'Litauen',
    'Luxemburg',
    'Malta',
    'Moldau',
    'Monaco',
    'Montenegro',
    'Niederlande',
    'Nordmazedonien',
    'Norwegen',
    'Österreich',
    'Polen',
    'Portugal',
    'Rumänien',
    'Russland',
    'San Marino',
    'Schweden',
    'Schweiz',
    'Serbien',
    'Slowakei',
    'Slowenien',
    'Spanien',
    'Tschechien',
    'Türkei',
    'Ukraine',
    'Ungarn',
    'Vatikanstadt',
    'Vereinigtes Königreich',
  ];

  // Called from template (to prevent ExpressionChangedAfterItHasBeenCheckedError)
  range1830 = this.range(18, 30);
  range15 = this.range(1, 5);

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {}

  ngOnDestroy(): void {
    this.rentalServicePostSub.unsubscribe();
    this.rentalServicePutSub.unsubscribe();
  }

  get rentalTermsForm(): FormGroup {
    return this.vehicleForm?.get('rentalTermsForm') as FormGroup;
  }
  get excludeCountries(): FormControl {
    return this.rentalTermsForm?.get('excludeCountries') as FormControl;
  }

  get includeCountries(): FormControl {
    return this.rentalTermsForm?.get('includeCountries') as FormControl;
  }
  get minDriverAge(): FormControl {
    return this.rentalTermsForm?.get('minDriverAge') as FormControl;
  }
  get drivingLicensePossession(): FormControl {
    return this.rentalTermsForm?.get('drivingLicensePossession') as FormControl;
  }
  get additionalDriversAllowed(): FormControl {
    return this.rentalTermsForm?.get('additionalDriversAllowed') as FormControl;
  }
  get chargeForExtraDriver(): FormControl {
    return this.rentalTermsForm?.get('chargeForExtraDriver') as FormControl;
  }
  get region(): FormControl {
    return this.rentalTermsForm?.get('region') as FormControl;
  }

  range(begin: number, end: number): number[] {
    const nums: number[] = [];
    for (let i = begin; i <= end; i++) {
      nums.push(i);
    }
    return nums;
  }

  checkOtherList(country: string, otherList: string): boolean {
    if (!this.rentalTermsForm) {
      return false;
    }

    const array: string[] = this.rentalTermsForm.get(otherList)?.value;
    return array.indexOf(country) > -1;
  }

  showDoAdditionalDriverCost(): boolean {
    return (
      this.rentalTermsForm.get('additionalDriversAllowed')?.value ===
      'allowedHaveToBeNamed'
    );
  }

  showAdditionalDriverCost(): boolean {
    return (
      this.rentalTermsForm.get('additionalDriversAllowed')?.value ==
        'allowedHaveToBeNamed' &&
      this.rentalTermsForm.get('chargeForExtraDriver')?.value == true
    );
  }
}
