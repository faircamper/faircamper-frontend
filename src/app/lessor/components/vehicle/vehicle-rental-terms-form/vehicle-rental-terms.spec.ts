import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { VehicleRentalTermsFormComponent } from './vehicle-rental-terms-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

describe('VehicleRentalTerms', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule,
      ],
      declarations: [VehicleRentalTermsFormComponent],
    }).compileComponents();
  });

  describe('range()', () => {
    it(`should include the first and last provided number`, () => {
      const fixture = TestBed.createComponent(VehicleRentalTermsFormComponent);
      const component = fixture.componentInstance;
      fixture.detectChanges();

      let range = component.range(18, 30);

      expect(range[0]).toBe(18);
      expect(range[range.length - 1]).toBe(30);

      expect(range.length).toBe(13);
    });
  });
});
