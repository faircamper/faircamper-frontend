import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EquipmentItemFormComponent } from './equipment-item-form.component';

describe('EquipmentItemFormComponent', () => {
  let component: EquipmentItemFormComponent;
  let fixture: ComponentFixture<EquipmentItemFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EquipmentItemFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EquipmentItemFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
