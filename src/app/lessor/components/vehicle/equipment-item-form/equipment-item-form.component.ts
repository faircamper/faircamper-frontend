import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import {
  Equipment,
  OptionalEquipment,
} from 'src/app/lessor/models/equipment/equipment';

@Component({
  selector: 'app-equipment-item-form',
  templateUrl: './equipment-item-form.component.html',
  styleUrls: ['./equipment-item-form.component.css'],
})
export class EquipmentItemFormComponent implements OnInit {
  @Input() equipment: EquipmentFormState | null = null;
  @Output() equipmentSelect = new EventEmitter<EquipmentFormState>();
  @Output() optionalEquipmentLink = new EventEmitter<EquipmentFormState>();

  form: FormGroup = new FormGroup({});
  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    if (this.equipment) {
      this.form = this.fb.group(this.equipment);
    }
  }
  onChange(): void {
    const formVal = this.form.value;
    const state: EquipmentFormState = { ...formVal };
    this.equipmentSelect.emit(state);
  }

  onChargeableActive(): void {
    this.optionalEquipmentLink.emit({ ...this.form.value });
  }
  get isChargeableActive(): boolean {
    return this.form.get('isChargeableActive')?.value;
  }
}

export interface EquipmentFormState extends Equipment {
  isSelected: boolean;
  isChargeableActive: boolean;
  optionalEquipmentId: string | null;
  optionalEquipment?: OptionalEquipment;
}
