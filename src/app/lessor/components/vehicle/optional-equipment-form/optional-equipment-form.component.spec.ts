import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OptionalEquipmentFormComponent } from './optional-equipment-form.component';

describe('OptionalEquipmentFormComponent', () => {
  let component: OptionalEquipmentFormComponent;
  let fixture: ComponentFixture<OptionalEquipmentFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OptionalEquipmentFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OptionalEquipmentFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
