import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { EMPTY, Observable } from 'rxjs';
import { IdValue } from 'src/app/lessor/models/account/lessor';
import {
  OptionalEquipmentCategory,
  OptionalEquipment,
} from 'src/app/lessor/models/equipment/equipment';

enum OptionalEquipmentFormMode {
  EditWithCategories = 'EditWithCategories',
  EditNoCategories = 'EditNoCategories',
  CreateWithCategories = 'CreateWithCategories',
  CreateCategoriesPreselected = 'CreateCategoriesPreselected',
}

export const isEditMode = (mode: OptionalEquipmentFormMode): boolean => {
  return (
    mode === OptionalEquipmentFormMode.EditWithCategories ||
    mode === OptionalEquipmentFormMode.EditNoCategories
  );
};

export const modes = OptionalEquipmentFormMode;
@Component({
  selector: 'app-optional-equipment-form',
  templateUrl: './optional-equipment-form.component.html',
  styleUrls: ['./optional-equipment-form.component.css'],
})
export class OptionalEquipmentFormComponent implements OnInit, OnChanges {
  @Input() selectedStationId: string | null | undefined = null;
  @Input() optionalEquipment: OptionalEquipment | null = null;
  @Input() mode:
    | OptionalEquipmentFormMode.EditWithCategories
    | OptionalEquipmentFormMode.EditNoCategories
    | OptionalEquipmentFormMode.CreateWithCategories
    | OptionalEquipmentFormMode.CreateCategoriesPreselected =
    OptionalEquipmentFormMode.CreateWithCategories;
  @Input() optionalEquipmentCategories: OptionalEquipmentCategory[] | null = [];
  @Output() submitOptionalEquipment = new EventEmitter<OptionalEquipment>();

  placeholder = 'Wähle eine Kategorie';

  categoryNames: IdValue[] = [];

  priceTypes: IdValue[] = [
    { id: 0, name: 'pro Stück' },
    { id: 1, name: 'pro Nacht' },
    { id: 2, name: 'pro Stück und Nacht' },
    { id: 3, name: 'pro Buchung' },
    { id: 4, name: 'kostenlos' },
  ];

  optionalEquipmentForm = this.fb.group({
    categoryId: [''],
    description: [null, [Validators.required]],
    nameId: [''],
    price: [null, [Validators.required]],
    priceTypeId: ['', [Validators.required]],
    maxPrice: [null, [Validators.required]],
    id: [''],
    version: [0],
    stationId: [''],
  });

  constructor(private fb: FormBuilder) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (this.optionalEquipment) {
      this.optionalEquipmentForm.patchValue(this.optionalEquipment);
    }
    this.onChangeCategory(this.optionalEquipment?.categoryId);
    console.log(this);
  }

  ngOnInit(): void {}

  get getDescription(): FormControl {
    return this.optionalEquipmentForm?.get('description') as FormControl;
  }
  get getPrice(): FormControl {
    return this.optionalEquipmentForm?.get('price') as FormControl;
  }
  get getMaxPrice(): FormControl {
    return this.optionalEquipmentForm?.get('maxPrice') as FormControl;
  }
  get getPriceTypeId(): FormControl {
    return this.optionalEquipmentForm?.get('priceTypeId') as FormControl;
  }
  get getCategoryId(): FormControl {
    return this.optionalEquipmentForm?.get('categoryId') as FormControl;
  }
  get getNameId(): FormControl {
    return this.optionalEquipmentForm?.get('nameId') as FormControl;
  }
  onChangeCategory(categoryId: number | undefined): void {
    if (!categoryId) {
      return;
    }
    const names = this.optionalEquipmentCategories?.find(
      (e) => categoryId === e.categoryId
    )?.names;
    if (names) {
      this.categoryNames = names;
    }
  }

  submit(): void {
    this.optionalEquipmentForm.value.stationId = this.selectedStationId;
    const newOptionalEquipment: OptionalEquipment = {
      ...this.optionalEquipmentForm.value,
    };
    this.optionalEquipmentForm.reset();
    this.submitOptionalEquipment.emit(newOptionalEquipment);
  }

  back(): void {
    this.optionalEquipmentForm.reset();
    this.submitOptionalEquipment.emit();
  }

  isCategoryPreselected(): boolean {
    return this.mode === OptionalEquipmentFormMode.CreateCategoriesPreselected;
  }
}
