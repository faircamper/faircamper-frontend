import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EquipmentLinkComponent } from './equipment-link.component';

describe('EquipmentLinkComponent', () => {
  let component: EquipmentLinkComponent;
  let fixture: ComponentFixture<EquipmentLinkComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EquipmentLinkComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EquipmentLinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
