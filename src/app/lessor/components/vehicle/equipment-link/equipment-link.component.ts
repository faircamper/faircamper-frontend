import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { EMPTY, map, Observable, Subscription } from 'rxjs';
import {
  Equipment,
  OptionalEquipmentCategory,
  OptionalEquipment,
} from 'src/app/lessor/models/equipment/equipment';
import { OptionalEquipmentService } from 'src/app/lessor/services/http/equipment/optionalEquipment.service';
import { modes } from '../optional-equipment-form/optional-equipment-form.component';

@Component({
  selector: 'app-equipment-link',
  templateUrl: './equipment-link.component.html',
  styleUrls: ['./equipment-link.component.css'],
})
export class EquipmentLinkComponent implements OnInit, OnDestroy, OnChanges {
  @Input() equipment: Equipment | null = null;
  @Input() categories: Observable<OptionalEquipmentCategory[]> = EMPTY;
  @Input() stationId: string | null | undefined = null;
  @Input() optionalEquipment: OptionalEquipment[] = [];
  @Output() linkOptionalEquipmentEvent = new EventEmitter<EquipmentLink>();

  optionalEquipmentSub: Subscription = EMPTY.subscribe();
  linkedOptionalEquipment: OptionalEquipment | null = null;
  showForm = false;
  formModes = modes;
  constructor(private optionalEquipmentService: OptionalEquipmentService) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (
      this.stationId &&
      this.equipment?.optionalEquipmentNameId &&
      this.hasStationIdChanged(changes)
    ) {
      this.optionalEquipmentSub = this.loadOptionalEquipment(
        this.stationId,
        this.equipment?.optionalEquipmentNameId
      ).subscribe((optionalEquipment) => {
        this.optionalEquipment = optionalEquipment;
      });
    }
  }
  private hasStationIdChanged(changes: SimpleChanges): boolean {
    console.log(changes);
    const stationIdChanges = changes['stationId'];
    return stationIdChanges.currentValue !== stationIdChanges.previousValue;
  }

  loadOptionalEquipment(
    stationId: string,
    nameId: number
  ): Observable<OptionalEquipment[]> {
    return this.optionalEquipmentService
      .list<OptionalEquipment>([`stationId=${stationId}`])
      .pipe(map((e) => e.filter((e) => (e ? e.nameId === nameId : false))));
  }

  ngOnInit(): void {
    if (this.equipment) {
      this.linkedOptionalEquipment = this.toOptionalEquipment(this.equipment);
    }
  }

  ngOnDestroy(): void {
    this.optionalEquipmentSub.unsubscribe();
  }

  private toOptionalEquipment(item: Equipment): OptionalEquipment | null {
    if (
      this.stationId &&
      this.stationId !== null &&
      item.optionalEquipmentNameId !== null &&
      item.optionalEquipmentName !== null &&
      item.optionalEquipmentCategoryId !== null &&
      item.optionalEquipmentCategory !== null
    ) {
      return {
        name: item.optionalEquipmentName,
        nameId: item.optionalEquipmentNameId,
        categoryId: item.optionalEquipmentCategoryId,
        categoryName: item.optionalEquipmentCategory,
        description: '',
        price: 0,
        priceTypeId: 0,
        maxPrice: 0,
        stationId: this.stationId,
      };
    }
    return null;
  }

  onOptionalEquipmentCreate(item: OptionalEquipment): void {
    this.showForm = !this.showForm;
    if (item) {
      this.optionalEquipmentService
        .create<OptionalEquipment>(item)
        .subscribe((response) => {
          if (this.stationId) {
            this.loadOptionalEquipment(
              this.stationId,
              response.nameId
            ).subscribe((optionalEquipment) => {
              this.optionalEquipment = optionalEquipment;
            });
          }
        });
    }
  }
  goBack(): void {
    this.linkOptionalEquipmentEvent.emit();
  }

  onSelectedOptionalEquipmentId(oe: OptionalEquipment): void {
    if (oe && this.equipment) {
      const link: EquipmentLink = {
        equipment: this.equipment,
        optionalEquipment: oe,
      };
      this.linkOptionalEquipmentEvent.emit(link);
    }
  }
}

export interface EquipmentLink {
  optionalEquipment: OptionalEquipment;
  equipment: Equipment;
}
