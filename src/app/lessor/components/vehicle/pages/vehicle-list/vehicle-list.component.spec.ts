// import { TestBed } from '@angular/core/testing';
// import { RouterTestingModule } from '@angular/router/testing';
// import { VehicleListComponent } from './vehicle-list.component';
// import { HttpClientTestingModule } from '@angular/common/http/testing';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// const PAGESIZE = 15;

// describe('VehicleListComponent', () => {
//   beforeEach(async () => {
//     await TestBed.configureTestingModule({
//       imports: [
//         RouterTestingModule,
//         HttpClientTestingModule,
//         BrowserAnimationsModule,
//       ],
//       declarations: [VehicleListComponent],
//     }).compileComponents();
//   });

//   describe('getParameters()', () => {
//     it(`should initially deliver page 0 and pagesize 15 and have 2 parameters`, () => {
//       const fixture = TestBed.createComponent(VehicleListComponent);
//       const component = fixture.componentInstance;
//       fixture.detectChanges();

//       let parameters = component.getParameters();

//       expect(parameters[0]).toEqual('p=0');
//       expect(parameters[1]).toEqual('ps=' + PAGESIZE);

//       expect(parameters.length).toBe(2);
//     });

//     it(`should not change parameters when only sort.direction is set`, () => {
//       const fixture = TestBed.createComponent(VehicleListComponent);
//       const component = fixture.componentInstance;
//       fixture.detectChanges();

//       component.sort.direction = 'asc';
//       component.sort.active = '';

//       let parameters = component.getParameters();

//       expect(parameters[0]).toEqual('p=0');
//       expect(parameters[1]).toEqual('ps=' + PAGESIZE);

//       expect(parameters.length).toBe(2);
//     });

//     it(`should not change parameters when only sort.active is set`, () => {
//       const fixture = TestBed.createComponent(VehicleListComponent);
//       const component = fixture.componentInstance;
//       fixture.detectChanges();

//       component.sort.direction = '';
//       component.sort.active = 'title';

//       let parameters = component.getParameters();

//       expect(parameters[0]).toEqual('p=0');
//       expect(parameters[1]).toEqual('ps=' + PAGESIZE);

//       expect(parameters.length).toBe(2);
//     });

//     it(`should set the sorting parameters right`, () => {
//       const fixture = TestBed.createComponent(VehicleListComponent);
//       const component = fixture.componentInstance;
//       fixture.detectChanges();

//       component.sort.direction = 'asc';
//       component.sort.active = 'title';

//       let parameters = component.getParameters();

//       expect(parameters[0]).toEqual('p=0');
//       expect(parameters[1]).toEqual('ps=' + PAGESIZE);
//       expect(parameters[2]).toEqual('so=asc');
//       expect(parameters[3]).toEqual('by=title');

//       expect(parameters.length).toBe(4);
//     });

//     it(`should set the filtering parameters right`, () => {
//       const fixture = TestBed.createComponent(VehicleListComponent);
//       const component = fixture.componentInstance;
//       fixture.detectChanges();

//       // Filter 1: city
//       component.filters.set('city', 'Kassel');

//       let parameters = component.getParameters();

//       expect(parameters[0]).toEqual('p=0');
//       expect(parameters[1]).toEqual('ps=' + PAGESIZE);
//       expect(parameters[2]).toEqual('city=Kassel');

//       expect(parameters.length).toBe(3);

//       // Filter 1: city, Filter 2: vehicleTypeId
//       component.filters.set('vehicleTypeId', '1');

//       parameters = component.getParameters();

//       expect(parameters[0]).toEqual('p=0');
//       expect(parameters[1]).toEqual('ps=' + PAGESIZE);
//       expect(parameters[2]).toEqual('city=Kassel');
//       expect(parameters[3]).toEqual('vehicleTypeId=1');

//       expect(parameters.length).toBe(4);
//     });
//   });
// });
