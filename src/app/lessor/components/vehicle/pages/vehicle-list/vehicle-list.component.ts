import { Subscription, EMPTY, Observable } from 'rxjs';
import { AfterViewInit, Component, OnDestroy, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { VehicleListItem } from 'src/app/lessor/models/vehicles/vehicleListItem';
import { StaticDataService } from 'src/app/lessor/services/http/static-data-service/static-data.service';
import { IdValue } from 'src/app/lessor/services/http/static-data-service/staticData';
import { VehicleService } from 'src/app/lessor/services/http/vehicle/vehicle.service';
import { VehicleCitiesService } from 'src/app/lessor/services/http/vehicle/vehicleCities.service';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'fc-vehicle-list',
  templateUrl: './vehicle-list.component.html',
  styleUrls: ['./vehicle-list.component.css'],
})
export class VehicleListComponent implements AfterViewInit, OnDestroy {
  vehicleListSubscription: Subscription = EMPTY.subscribe();
  vehicleTypeSubscription: Subscription = EMPTY.subscribe();
  numberOfReloads = 0;
  pageSize = 15;
  filters = new Map<string, string>();
  vehicles: VehicleListItem[] = [];
  vehicleType$: Observable<IdValue[]> =
    this.vehicleTypeService.getVehicleTypes();
  cities: string[] = [];
  searchInput = '';
  searchForm = new FormGroup({
    search: new FormControl(''),
  });
  filterForm = new FormGroup({
    wobiStatusId: new FormControl(''),
    onlineStatusId: new FormControl(''),
    bestPrice: new FormControl(''),
    directBooking: new FormControl(''),
    liveCalandar: new FormControl(''),
    city: new FormControl(''),
    vehicleClass: new FormControl(''),
    vehicleTypeId: new FormControl(''),
    masterVehicle: new FormControl(''),
    archivedVehicles: new FormControl(''),
  });

  sort = 'externalId';
  direction: 'asc' | 'desc' = 'desc';
  columns = [
    { name: 'Fahrzeug/Master', sortable: true, sort: 'externalId' },
    { name: 'WoBi-ID', sortable: true, sort: 'vehicleId' },
    { name: 'Typ', sortable: true, sort: 'vehicleTypeId' },
    { name: 'Status', sortable: true, sort: 'onlineStatusId' },
    { name: 'Standort', sortable: true, sort: 'city' },
  ];
  endOfResults = false;
  constructor(
    private router: Router,
    private vehicleService: VehicleService,
    private vehicleCitiesService: VehicleCitiesService,
    private vehicleTypeService: StaticDataService,
    private formBuilder: FormBuilder
  ) {}

  ngAfterViewInit(): void {
    this.loadVehicleList();
    this.loadStaticData();
  }

  setSort(str: string, sortable: boolean) {
    if (this.sort === str && sortable) {
      this.direction = this.direction === 'asc' ? 'desc' : 'asc';
    } else {
      this.direction = 'asc';
      this.sort = str;
    }
    this.loadVehicleList();
  }

  loadVehicleList(): void {
    this.numberOfReloads = 0;
    if (this.vehicleListSubscription) {
      this.vehicleListSubscription.unsubscribe();
    }
    this.vehicleListSubscription = this.vehicleService
      .list<VehicleListItem>(this.getParameters())
      .subscribe((data) => {
        if (data.length < this.pageSize) {
          this.endOfResults = true;
        } else {
          this.endOfResults = false;
        }
        this.vehicles = data;
      });
  }
  loadMoreVehicles(): void {
    this.numberOfReloads++;
    if (this.vehicleListSubscription) {
      this.vehicleListSubscription.unsubscribe();
    }
    this.vehicleListSubscription = this.vehicleService
      .list<VehicleListItem>(this.getParameters())
      .subscribe((data) => {
        if (data.length < this.pageSize) {
          this.endOfResults = true;
        }
        data.forEach((element) => {
          this.vehicles.push(element);
        });
      });
  }

  loadStaticData(): void {
    this.vehicleListSubscription = this.vehicleCitiesService
      .list<string>()
      .subscribe((data) => (this.cities = data));
  }

  getParameters(): string[] {
    const parameters = ['p=' + this.numberOfReloads, 'ps=' + this.pageSize];
    parameters.push('so=' + this.direction);
    parameters.push('by=' + this.sort);
    const controls = this.filterForm.controls;
    Object.keys(controls).forEach((key) => {
      if (controls[key].value) {
        parameters.push(key + '=' + controls[key].value);
      }
    });
    return parameters;
  }

  ngOnDestroy(): void {
    this.vehicleListSubscription.unsubscribe();
  }

  /**
   * copyVehicle
   */
  public onCopyVehicle(vehicle: VehicleListItem): void {
    this.loadVehicleList();
  }

  public onDelete(vehicle: VehicleListItem): void {
    vehicle.isDeleted = true;
    this.loadVehicleList();
  }

  /**
   * openDeleteDialog
   */
  public openDeleteDialog(vehicle: VehicleListItem): void {
    // const dialogRef = this.dialog.open(VehicleRemoveDialogComponent, {
    //   width: '250px',
    //   data: vehicle,
    // });
    // dialogRef.afterClosed().subscribe((result) => {
    //   if (result === undefined) {
    //     return;
    //   }
    //   this.onDelete(result);
    // });
  }
}
