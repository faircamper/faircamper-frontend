import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Vehicle } from 'src/app/lessor/models/vehicles/vehicle';
import { VehicleService } from 'src/app/lessor/services/http/vehicle/vehicle.service';
import { MessagesService } from 'src/app/lessor/services/message/messages.service';

@Component({
  selector: 'lessor-vehicle-edit',
  templateUrl: './vehicle-edit.component.html',
  styleUrls: ['./vehicle-edit.component.css'],
})
export class VehicleEditComponent implements OnInit, OnDestroy {
  [x: string]: any;
  @Input() vehicle: Vehicle | null = null;

  constructor(
    private route: ActivatedRoute,
    private vehicleService: VehicleService,
    private messagesService: MessagesService
  ) {}

  ngOnInit(): void {
    const vehicleId = this.route.snapshot.paramMap.get('id');
    if (vehicleId) {
      this.vehicleService.get<Vehicle>(vehicleId).subscribe((data) => {
        this.vehicle = data;
      });
    }
  }

  ngOnDestroy(): void {}

  onSubmit(vehicle: Vehicle): void {
    vehicle.version = this.vehicle?.version;
    if (vehicle.id) {
      this.vehicleService.update<Vehicle>(vehicle).subscribe((data) => {
        this.vehicle = data;
        // this.router.navigateByUrl('/vehicles');
        this.messagesService.send({ text: 'Vehicle Saved' });
      });
    }
  }
}
