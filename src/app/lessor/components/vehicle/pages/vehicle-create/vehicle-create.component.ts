import { Component, Input, OnDestroy } from '@angular/core';
import { EMPTY, Subscription } from 'rxjs';
import { Vehicle } from 'src/app/lessor/models/vehicles/vehicle';
import { VehicleService } from 'src/app/lessor/services/http/vehicle/vehicle.service';
import { MessagesService } from 'src/app/lessor/services/message/messages.service';

@Component({
  selector: 'lessor-vehicle-create',
  templateUrl: './vehicle-create.component.html',
  styleUrls: ['./vehicle-create.component.scss'],
})
export class VehicleCreateComponent implements OnDestroy {
  @Input() vehicle: Vehicle | null = null;
  vehicleSaved = false;
  vehicleId = 0;
  version: undefined | number = 0;
  sub: Subscription = EMPTY.subscribe();

  constructor(
    private vehicleService: VehicleService,
    private messagesService: MessagesService
  ) {}

  ngOnDestroy(): void {}

  onVehicleCreate(vehicle: Vehicle): void {
    console.log('Save Vehicle');
    const updatedVehicle: Vehicle = { ...vehicle };
    if (!this.vehicleSaved) {
      this.sub.unsubscribe();
      this.sub = this.vehicleService
        .create(updatedVehicle)
        .subscribe((data) => {
          this.messagesService.send({ text: 'Vehicle Saved' });
          this.vehicleSaved = true;
          this.vehicleId = data.id;
          this.version = data.version;
          this.sub.unsubscribe();
        });
    } else {
      this.sub.unsubscribe();
      updatedVehicle.version = this.version;
      updatedVehicle.id = this.vehicleId;
      this.sub = this.vehicleService
        .update(updatedVehicle)
        .subscribe((data) => {
          this.messagesService.send({ text: 'Vehicle Saved' });
          this.vehicleSaved = true;
          this.vehicleId = data.id;
          this.version = data.version;
          this.sub.unsubscribe();
        });
    }
  }
}
