import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Component, Input, OnDestroy } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { EMPTY } from 'rxjs';
import { Station } from 'src/app/lessor/models/station/station';
import { StaticDataService } from 'src/app/lessor/services/http/static-data-service/static-data.service';
import { IdValue } from 'src/app/lessor/services/http/static-data-service/staticData';

@Component({
  selector: 'lessor-vehicle-category-form',
  templateUrl: './vehicle-category-form.component.html',
  styleUrls: ['./vehicle-catgeory-form.component.css'],
})
export class VehicleCategoryFormComponent {
  @Input() vehicleForm: FormGroup | null = null;
  vehicleTypes: IdValue[] = [];
  constructor(private staticDataService: StaticDataService) {}

  ngOnInit(): void {
    this.staticDataService.getVehicleTypes().subscribe((data) => {
      this.vehicleTypes = data;
    });
  }
  get vehicleTypeId(): FormControl {
    return this.vehicleForm?.get('vehicleTypeId') as FormControl;
  }

  vehicleType(value: any) {
    return typeof value;
  }
}
