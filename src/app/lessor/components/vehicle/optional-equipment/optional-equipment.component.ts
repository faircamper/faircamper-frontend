import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { concatMap, EMPTY, Observable, Subscription, switchMap } from 'rxjs';
import { OptionalEquipmentService } from 'src/app/lessor/services/http/equipment/optionalEquipment.service';
import {
  OptionalEquipmentCategory,
  OptionalEquipment,
} from '../../../models/equipment/equipment';
import {
  isEditMode,
  modes,
} from '../optional-equipment-form/optional-equipment-form.component';

@Component({
  selector: 'app-optional-equipment',
  templateUrl: './optional-equipment.component.html',
  styleUrls: ['./optional-equipment.component.css'],
})
export class OptionalEquipmentComponent implements OnInit, OnChanges {
  @Input() selectedOptionalEquipmentIds: string[] | undefined = [];
  @Input() selectedStationId: string | undefined = '';

  @Output() selectedOptionalEquipmentIDsEvent = new EventEmitter<string[]>();

  @Input() optionalEquipment$: Observable<OptionalEquipment[]> = EMPTY;
  optionalEquipmentSub: Subscription = EMPTY.subscribe();
  categories$: Observable<OptionalEquipmentCategory[]> = EMPTY;

  selectedOptionalEquipment: OptionalEquipment | null = null;
  showForm = false;

  formModes = modes;
  formMode = modes.CreateWithCategories;

  constructor(private optionalEquipmentService: OptionalEquipmentService) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (this.selectedStationId) {
      this.optionalEquipment$ = this.loadOptionalEquipmentList(
        this.selectedStationId
      );
    }
  }

  ngOnInit(): void {
    this.categories$ = this.loadCategories();
  }

  onSubmittedItem(item: OptionalEquipment): void {
    this.showForm = !this.showForm;
    if (!item) {
      return;
    }
    if (isEditMode(this.formMode)) {
      this.optionalEquipmentService
        .update<OptionalEquipment>(item)
        .subscribe(() => {});
    } else {
      this.optionalEquipmentService
        .create<OptionalEquipment>(item)
        .subscribe(() => {});
    }
    this.selectedOptionalEquipment = null;
    this.formMode;
  }

  onEdit(item: OptionalEquipment): void {
    this.showForm = !this.showForm;
    this.selectedOptionalEquipment = null;
    if (!item) {
      return;
    }
  }

  private loadOptionalEquipmentList(
    id?: string
  ): Observable<OptionalEquipment[]> {
    if (!id) {
      return EMPTY;
    }
    return this.optionalEquipmentService.list<OptionalEquipment>([
      `stationId=${id}`,
    ]);
  }

  private loadCategories(): Observable<OptionalEquipmentCategory[]> {
    return this.optionalEquipmentService.getCategories();
  }

  onOptionalEquipmentIdsChange(ids: string[]): void {
    this.selectedOptionalEquipmentIDsEvent.emit(ids);
  }

  createOptionalEquipment(): void {
    this.showForm = true;
    this.formMode = this.formModes.CreateWithCategories;
  }

  editOptionalEquipment(item: OptionalEquipment): void {
    this.selectedOptionalEquipment = item;
    this.showForm = !this.showForm;
    this.formMode = this.formModes.EditWithCategories;
  }
}
