import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OptionalEquipmentComponent } from './optional-equipment.component';

describe('OptionalEquipmentComponent', () => {
  let component: OptionalEquipmentComponent;
  let fixture: ComponentFixture<OptionalEquipmentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OptionalEquipmentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OptionalEquipmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
