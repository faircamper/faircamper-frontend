import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { ImageCroppedEvent, ImageCropperComponent } from 'ngx-image-cropper';
import { ImageService } from 'src/app/lessor/services/http/image-service/image.service';
import { MessagesService } from 'src/app/lessor/services/message/messages.service';

@Component({
  selector: 'lessor-image-upload',
  templateUrl: './image-upload.component.html',
  styleUrls: ['./image-upload.component.scss'],
})
export class ImageUploadComponent implements OnInit {
  @ViewChild(ImageCropperComponent) imageCropper: ImageCropperComponent | null =
    null;
  color = 'primary';
  @Input() imageChangedEvent: any = '';
  @Output() imageSaved = new EventEmitter<string>();
  croppedImage: any = '';
  rotation = 0;
  transform = {
    flipH: false,
    flipV: false,
    rotate: 0,
    scale: 1,
  };
  showLoadError = false;

  constructor(
    private imageService: ImageService,
    private messagesService: MessagesService
  ) {}

  ngOnInit(): void {}

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
  }
  imageCropped(event: ImageCroppedEvent): void {
    if (!event) {
      return;
    }
    if (!event.base64) {
      return;
    }
    this.croppedImage = event.base64;
  }
  imageLoaded(image: any): void {
    this.showLoadError = false;
  }
  cropperReady(): void {
    // cropper ready
  }
  loadImageFailed(): void {
    this.showLoadError = true;
  }
  flipHorizontal(): void {
    this.transform = {
      ...this.transform,
      flipH: !this.transform.flipH,
    };
  }
  flipVertical(): void {
    this.transform = {
      ...this.transform,
      flipV: !this.transform.flipV,
    };
  }
  reset(): void {
    this.transform = {
      flipH: false,
      flipV: false,
      rotate: 0,
      scale: 1,
    };
    this.rotation = 0;
  }
  save(): void {
    const formData = new FormData();
    formData.append('file', this.croppedImage);
    this.imageService.add(formData).subscribe(
      (data) => {
        console.log(data);
        this.imageChangedEvent = null;
        this.imageSaved.emit(data.data);
      },
      (error) => {
        this.messagesService.send({
          text: 'Uploading Image failed',
          errorCode: error.status,
          httpError: error,
        });
      }
    );
  }
}
