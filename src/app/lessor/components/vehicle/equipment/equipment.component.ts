import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { EMPTY, map, Observable, Subscription } from 'rxjs';
import {
  Equipment,
  EquipmentId,
  OptionalEquipmentCategory,
  OptionalEquipment,
} from 'src/app/lessor/models/equipment/equipment';
import { EquipmentService } from 'src/app/lessor/services/http/equipment/equipment.service';
import { EquipmentFormState } from '../equipment-item-form/equipment-item-form.component';
import { EquipmentLink } from '../equipment-link/equipment-link.component';

@Component({
  selector: 'app-equipment',
  templateUrl: './equipment.component.html',
  styleUrls: ['./equipment.component.css'],
})
export class EquipmentComponent implements OnInit, OnDestroy, OnChanges {
  @Input() selectedEquipmentIds: EquipmentId[] | undefined = [];
  @Input() categories: Observable<OptionalEquipmentCategory[]> = EMPTY;
  @Input() stationId: string | null | undefined = null;
  @Input() optionalEquipmentList: OptionalEquipment[] | null = [];
  @Output() equipmentIdsEvent = new EventEmitter<EquipmentId[]>();

  showOptionalEquipmentLink: boolean = false;
  equipmentForLinking: Equipment | null = null;
  equipmentList: CategoryToEquipmentList[] = [];

  equipmentServiceSub: Subscription = EMPTY.subscribe();
  constructor(private equipmentService: EquipmentService) {}
  ngOnChanges(changes: SimpleChanges): void {
    if (this.optionalEquipmentList) {
      this.equipmentServiceSub = this.loadEquipmentList();
    }
  }

  ngOnInit(): void {}

  ngOnDestroy(): void {
    this.equipmentServiceSub.unsubscribe();
  }

  onChangeIsChargeableActive(item: EquipmentFormState): void {
    if (item.isChargeableActive) {
      this.showOptionalEquipmentLink = !this.showOptionalEquipmentLink;
      this.equipmentForLinking = item;
    } else {
      const formstate = this.findFormStateByEquipment(item);
      if (formstate && formstate.optionalEquipment) {
        formstate.optionalEquipmentId = null;
        formstate.optionalEquipment = undefined;
        this.equipmentForLinking = null;
      }
      const result = this.getSelectedEquipmentIds();
      this.equipmentIdsEvent.emit(result);
    }
  }

  onEquipmentSelect(item: EquipmentFormState): void {
    const formstate = this.findFormStateByEquipment(item);
    if (formstate) {
      formstate.isSelected = !formstate.isSelected;
    } else {
      throw new Error('no formstate found');
    }
    this.equipmentIdsEvent.emit(this.getSelectedEquipmentIds());
  }

  onLinkOptionalEquipmentEvent(link: EquipmentLink): void {
    if (link) {
      const formstate = this.findFormStateByEquipment(link.equipment);
      if (formstate && link.optionalEquipment.id) {
        formstate.optionalEquipmentId = link.optionalEquipment.id;
        formstate.isChargeableActive = true;
        formstate.optionalEquipment = link.optionalEquipment;
        formstate.isSelected = true;
      }
    }
    this.showOptionalEquipmentLink = !this.showOptionalEquipmentLink;
    this.equipmentIdsEvent.emit(this.getSelectedEquipmentIds());
  }

  private getSelectedEquipmentIds(): EquipmentId[] {
    return this.equipmentList.reduce((acc: EquipmentId[], category) => {
      return [...acc, ...category.equipmentList.filter((e) => e.isSelected)];
    }, []);
  }

  private findFormStateByEquipment(
    item: Equipment
  ): EquipmentFormState | undefined {
    const equipmentList = this.equipmentList.find(
      (e) => e.categoryName === item.categoryName
    );
    return equipmentList?.equipmentList.find((e) => e.id === item.id);
  }

  private loadEquipmentList(): Subscription {
    return this.equipmentService
      .list<Equipment>()
      .pipe(
        map((e) =>
          mapToFormState(
            e,
            this.selectedEquipmentIds,
            this.optionalEquipmentList
          )
        ),
        map(groupByCategoryName)
      )
      .subscribe((e) => {
        this.equipmentList = e;
      });
  }
}

interface CategoryToEquipmentList {
  categoryName: string;
  equipmentList: EquipmentFormState[];
}

const groupByCategoryName = (e: EquipmentFormState[]) =>
  e.reduce((acc: CategoryToEquipmentList[], equipment) => {
    const catName = equipment.categoryName;
    const cat = acc.find((e) => e.categoryName === catName);
    if (cat) {
      cat.equipmentList = [...cat.equipmentList, equipment];
    } else {
      acc = [
        ...acc,
        {
          categoryName: catName,
          equipmentList: [equipment],
        },
      ];
    }
    return acc;
  }, []);

const mapToFormState = (
  equipment: Equipment[],
  equipmentIds: EquipmentId[] | undefined,
  optionalEquipmentList: OptionalEquipment[] | null
): EquipmentFormState[] =>
  equipment.map((e) => {
    if (!equipmentIds) {
      return {
        ...e,
        isSelected: false,
        isChargeableActive: false,
        optionalEquipmentId: null,
      };
    } else {
      const eqId: EquipmentId | undefined = equipmentIds.find(
        (eqId) => eqId.id === e.id
      );
      const optionalEquipmentId = eqId?.optionalEquipmentId
        ? eqId?.optionalEquipmentId
        : null;
      const isChargeableactive: boolean =
        optionalEquipmentId !== null && optionalEquipmentId.length > 0;
      const optionalEquipment: OptionalEquipment | undefined =
        optionalEquipmentList?.find((e) => e.id === optionalEquipmentId);
      return {
        ...e,
        isSelected: !!eqId,
        isChargeableActive: isChargeableactive,
        optionalEquipmentId: optionalEquipmentId,
        optionalEquipment: optionalEquipment,
      };
    }
  });
