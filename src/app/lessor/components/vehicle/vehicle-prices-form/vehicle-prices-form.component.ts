import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { EMPTY } from 'rxjs';
import { SeasonCollection, Season } from 'src/app/lessor/models/seasons/season';
import { Price } from 'src/app/lessor/models/vehicles/price';
import { SeasonCollectionService } from 'src/app/lessor/services/http/season-calendar/season-calendar.service';

@Component({
  selector: 'lessor-vehicle-prices-form',
  templateUrl: './vehicle-prices-form.component.html',
  styleUrls: ['./vehicle-prices-form.component.scss'],
})
export class VehiclePricesFormComponent implements OnInit, OnDestroy {
  selectedSeasonCollection: SeasonCollection | null = null;
  @Input() selectedSeasonCollectionId: string = '';
  @Input() pricesForm: FormGroup | null = null;
  @Input() isEdit = false;
  seasonCollections: SeasonCollection[] | undefined = undefined;
  seasonCollectionGetSubscription = EMPTY.subscribe();

  constructor(
    private seasonCollectionService: SeasonCollectionService,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.seasonCollectionService.list<SeasonCollection>().subscribe((data) => {
      this.seasonCollections = data;
    });
    if (this.isEdit && this.selectedSeasonCollectionId) {
      this.seasonCollectionService
        .get<SeasonCollection>(this.selectedSeasonCollectionId)
        .subscribe((data) => {
          this.selectedSeasonCollection = data;
        });
    }
  }

  selectedASeasonCollection(event: any): void {
    this.seasonCollectionGetSubscription.unsubscribe();
    this.seasonCollectionGetSubscription = this.seasonCollectionService
      .get<SeasonCollection>(event.target.value)
      .subscribe((data) => {
        this.selectedSeasonCollection = data;
        const seasons = this.selectedSeasonCollection?.seasons.map(
          (season) => season
        );
        this.pricesForm?.setControl(
          'prices',
          this.buildPricesFormArray(this.toPrices(seasons))
        );
      });
  }

  ngOnDestroy(): void {
    this.seasonCollectionGetSubscription.unsubscribe();
  }

  private buildPricesFormArray(prices: Price[]): FormArray {
    return this.fb.array(
      prices.map((price) =>
        this.fb.group({
          priceLevelId: this.fb.control(price.priceLevelId),
          priceLevelName: this.fb.control(price.priceLevelName),
          amount: this.fb.control(price.amount, [Validators.required]),
          currency: this.fb.control(price.currency),
          minBookingDuration: this.fb.control(price.minBookingDuration),
        })
      )
    );
  }
  // when in create mode than vehicle.prices are empty
  // this function creates a price array from the unique pricelevels.
  // the user has to define a price for every price level
  private toPrices(seasons: Season[]): Price[] {
    // from every pricelevel in seasons map to n prices (number of pricelevles)
    const uniquePriceLevels = new Map<number, Price>();
    for (const season of seasons) {
      uniquePriceLevels.set(season.priceLevel.id, {
        amount: null,
        currency: 'EUR',
        priceLevelId: season.priceLevel.id,
        priceLevelName: season.priceLevel.name,
        minBookingDuration: null,
      });
    }
    return Array.from(uniquePriceLevels.values());
  }

  get prices(): FormArray {
    return this.pricesForm?.get('prices') as FormArray;
  }

  get seasonCollectionId(): FormControl {
    return this.pricesForm?.get('seasonCollectionId') as FormControl;
  }
}
