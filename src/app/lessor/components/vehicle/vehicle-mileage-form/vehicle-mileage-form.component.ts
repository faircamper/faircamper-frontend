import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MileagePack } from 'src/app/lessor/models/mileage';

@Component({
  selector: 'lessor-mileage-form',
  templateUrl: './vehicle-mileage-form.component.html',
  styleUrls: ['./vehicle-mileage-form.component.css'],
})
export class VehicleMileageFormComponent implements OnInit {
  @Input() mileageForm: FormGroup | null = null;
  @Input() isEdit = false;
  predefinedMileagePacks: MileagePack[] = [
    {
      id: 1,
      km: 500,
      discount: 5,
      isSelected: true,
    },
    {
      id: 2,
      km: 1000,
      discount: 5 * 2,
      isSelected: true,
    },
    {
      id: 3,
      km: 1500,
      discount: 5 * 3,
      isSelected: true,
    },
  ];

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    if (!this.isEdit && this.mileageForm) {
      this.mileageForm.setControl(
        'predefinedPackages',
        this.buildPackArray(this.predefinedMileagePacks)
      );
    }
  }

  showKmInput(): boolean {
    return !this.mileageForm?.get('isFree')?.value;
  }

  addCustomPackControl(): void {
    this.customPackages.push(
      this.fb.group({
        km: this.fb.control(''),
        discount: this.fb.control(''),
        isSelected: this.fb.control(true),
      })
    );
  }

  removeCustomPackControl(index: number): void {
    this.customPackages.removeAt(index);
  }

  priceForPack(index: number, packs: FormArray): number {
    const packGroup = packs.at(index) as FormGroup;
    const costPerKm = this.costPerKm?.value;
    const pack = packGroup.value as MileagePack;
    const price = pack.km * costPerKm * (1 - pack.discount / 100);
    return this.round2Decimals(price);
  }

  private round2Decimals(n: number): number {
    return Math.round((n + Number.EPSILON) * 100) / 100;
  }

  private buildPackArray(values: MileagePack[]): FormArray {
    return this.fb.array(
      values.map((p) =>
        this.fb.group({
          id: this.fb.control(p.id),
          km: this.fb.control(p.km, Validators.required),
          discount: this.fb.control(p.discount, [
            Validators.required,
            Validators.max(100),
            Validators.min(0),
          ]),
          isSelected: this.fb.control(p.isSelected),
        })
      )
    );
  }

  get costPerKm(): FormGroup {
    return this.mileageForm?.get('costPerKm') as FormGroup;
  }
  get customPackages(): FormArray {
    return this.mileageForm?.get('customPackages') as FormArray;
  }
  get predefinedPackages(): FormArray {
    return this.mileageForm?.get('predefinedPackages') as FormArray;
  }
}
