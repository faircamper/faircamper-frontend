import {
  Component,
  EventEmitter,
  Input, Output
} from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Station } from 'src/app/lessor/models/station/station';

@Component({
  selector: 'lessor-vehicle-name-description-form',
  templateUrl: './vehicle-name-description-form.component.html',
  styleUrls: ['./vehicle-name-description-form.component.scss'],
})
export class VehicleNameDescriptionFormComponent {
  @Input() vehicleForm: FormGroup | null = null;
  @Input() stations: Station[] = [];
  @Output() stationChanged = new EventEmitter<string>();

  constructor() {}

  get externalId(): FormControl {
    return this.vehicleForm?.get('externalId') as FormControl;
  }
  get title(): FormControl {
    return this.vehicleForm?.get('title') as FormControl;
  }
  get description(): FormControl {
    return this.vehicleForm?.get('description') as FormControl;
  }
  get stationId(): FormControl {
    return this.vehicleForm?.get('stationId') as FormControl;
  }

  onStationChanged(): void {
    console.log(this.stationId.value);
    this.stationChanged.emit(this.stationId.value);
  }
}
