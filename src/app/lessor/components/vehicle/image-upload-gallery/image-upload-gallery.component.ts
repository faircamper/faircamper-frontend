import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Image } from 'src/app/lessor/models/image';

@Component({
  selector: 'lessor-image-upload-gallery',
  templateUrl: './image-upload-gallery.component.html',
  styleUrls: ['./image-upload-gallery.component.css'],
})
export class ImageUploadGalleryComponent implements OnInit {
  color = 'primary';
  reloadImageUploader = false;
  imageChangedEvent: any = '';
  fileChangedInputValue: any = '';
  croppedImage: any = '';
  showImage = false;
  imgPreviewUrl = '';
  @Output() imageSaved = new EventEmitter();
  @Input() images: Image[] | undefined = [];
  constructor() {}

  ngOnInit(): void {
    console.log(this.images);
  }

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
    this.showImage = false;
  }

  showIMG(img: Image): void {
    this.showImage = true;
    this.imgPreviewUrl = img.url;
    this.imageChangedEvent = null;
  }
  closeImagePreview(): void {
    this.showImage = false;
  }
  imageAdded(str: any): void {
    this.imageSaved.emit(str);
    this.imageChangedEvent = {};
    this.imageChangedEvent = null;
    this.fileChangedInputValue = null;
  }
  getImageURL(img: Image): string {
    return img.url;
  }
}
