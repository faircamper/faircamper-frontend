import { Component, Input, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { StaticDataService } from 'src/app/lessor/services/http/static-data-service/static-data.service';
import { IdValue } from 'src/app/lessor/services/http/static-data-service/staticData';

@Component({
  selector: 'lessor-vehicle-beds-and-seats-form',
  templateUrl: './vehicle-beds-and-seats-form.component.html',
  styleUrls: ['./vehicle-beds-and-seats-form.component.css'],
})
export class VehicleBedsAndSeatsFormComponent implements OnInit {
  @Input()
  seatsAndBedsForm: FormGroup | null = null;
  bedTypes: IdValue[] = [];
  editBedMode = false;
  editBedIndex = -1;
  bedForm = this.formBuilder.group({
    designation: ['', [Validators.required]],
    length: ['', [Validators.min(120)]],
    width: ['', [Validators.min(60)]],
  });

  constructor(
    private formBuilder: FormBuilder,
    private staticDataService: StaticDataService
  ) {}

  ngOnInit(): void {
    this.staticDataService.getBedTypes().subscribe((data) => {
      this.bedTypes = data;
    });
  }
  get numSeatsOverall(): FormControl {
    return this.seatsAndBedsForm?.get('numSeatsOverall') as FormControl;
  }
  get numSeats3Point(): FormControl {
    return this.seatsAndBedsForm?.get('numSeats3Point') as FormControl;
  }
  get numSeatsLap(): FormControl {
    return this.seatsAndBedsForm?.get('numSeatsLap') as FormControl;
  }
  get numSeatsIsofix(): FormControl {
    return this.seatsAndBedsForm?.get('numSeatsIsofix') as FormControl;
  }
  get numBedsOverall(): FormControl {
    return this.seatsAndBedsForm?.get('numBedsOverall') as FormControl;
  }
  get numBedsComfort(): FormControl {
    return this.seatsAndBedsForm?.get('numBedsComfort') as FormControl;
  }
  get numBedsComfortKids(): FormControl {
    return this.seatsAndBedsForm?.get('numBedsComfortKids') as FormControl;
  }
  get beds(): FormControl {
    return this.seatsAndBedsForm?.get('beds') as FormControl;
  }

  get designation(): FormControl {
    return this.bedForm?.get('designation') as FormControl;
  }
  get width(): FormControl {
    return this.bedForm?.get('width') as FormControl;
  }
  get length(): FormControl {
    return this.bedForm?.get('length') as FormControl;
  }

  removeBed(i: number): void {
    this.beds.value.splice(i, 1);
  }

  editBed(i: number): void {
    const bed = this.beds.value[i];
    this.editBedIndex = i;
    this.editBedMode = true;
    this.length.patchValue(bed.length / 10);
    this.width.patchValue(bed.width / 10);
    this.designation.patchValue(bed.designation);
    console.log(this.editBed);
  }

  updateBed(): void {
    this.beds.value[this.editBedIndex] = {
      designation: this.designation.value,
      length: this.length.value * 10,
      width: this.width.value * 10,
    };
    this.editBedMode = false;
    this.editBedIndex = -1;
    this.length.reset();
    this.width.reset();
    this.designation.reset();
  }

  submitBed(): void {
    console.log('Called');
    this.length.patchValue(this.length.value * 10);
    this.width.patchValue(this.width.value * 10);
    this.beds.value.push({
      designation: this.designation.value,
      length: this.length.value,
      width: this.width.value,
    });
    this.length.reset();
    this.width.reset();
    this.designation.reset();
  }

  getBedTypeName(id: number): string {
    const name = this.bedTypes.find((e) => e.id === id)?.name;
    if (name) {
      return name;
    }
    return '' + id;
  }
}
