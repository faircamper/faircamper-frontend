import {
  Component,
  EventEmitter,
  Injectable,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges
} from '@angular/core';
import {
  AbstractControl,
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
  Validators
} from '@angular/forms';
import { EMPTY, Observable, Subscription } from 'rxjs';
import {
  EquipmentId,
  OptionalEquipmentCategory,
  OptionalEquipment,
} from 'src/app/lessor/models/equipment/equipment';
import { Image } from 'src/app/lessor/models/image';
import { SeasonCollection } from 'src/app/lessor/models/seasons/season';
import { Station } from 'src/app/lessor/models/station/station';
import {
  Package,
  PredefinedPackage
} from 'src/app/lessor/models/vehicles/mileage';
import { Price } from 'src/app/lessor/models/vehicles/price';
import { Vehicle } from 'src/app/lessor/models/vehicles/vehicle';
import { OptionalEquipmentService } from 'src/app/lessor/services/http/equipment/optionalEquipment.service';
import { StationService } from 'src/app/lessor/services/http/station/station.service';

@Component({
  selector: 'lessor-vehicle-form',
  templateUrl: './vehicle-form.component.html',
  styleUrls: ['./vehicle-form.component.css'],
})
export class VehicleFormComponent implements OnInit, OnDestroy, OnChanges {
  @Input() vehicle: Vehicle | null = null;

  @Input() isNewVehicle = true;

  @Output() submitVehicleEvent: EventEmitter<Vehicle> =
    new EventEmitter<Vehicle>();

  seasonCalendarSelection: SeasonCollection[] = [];

  stations: Station[] = [];
  selectedStation: Station | undefined = undefined;

  images: Image[] = [];

  selectedEquipmentIds: EquipmentId[] = [];
  selectOptionalEquipmentIds: string[] = [];
  categories$: Observable<OptionalEquipmentCategory[]> = EMPTY;
  optionalEquipmentCatSub: Subscription = EMPTY.subscribe();
  optionalEquipment$: Observable<OptionalEquipment[]> = EMPTY;

  priceLevelListSubscription = EMPTY.subscribe();
  stationServiceListSubscription = EMPTY.subscribe();
  stationList$: Observable<Station[]> = EMPTY;
  currentlyExpanded = 1;
  vehicleForm = this.formBuilder.group({
    // Meta Data
    nameDescriptionForm: this.formBuilder.group({
      externalId: ['', [Validators.required, Validators.maxLength(256)]],
      title: [
        '',
        [
          Validators.required,
          Validators.minLength(10),
          Validators.maxLength(150),
        ],
      ],
      description: ['', [Validators.maxLength(4096)]],
      stationId: [null, Validators.required],
    }),
    vehicleTypeId: this.formBuilder.control(0, Validators.required),
    basisDataForm: this.formBuilder.group({
      manufacturerId: [''],
      power: [0, [Validators.min(1), Validators.max(999), Validators.required]],
      powerMode: ['PS'],
      payload: [
        0,
        [Validators.min(20), Validators.max(2000), Validators.required],
      ],
      fuelId: [null, [Validators.required]],
      consumptionId: [null, Validators.required],
      weightId: [null, Validators.required],
      gearId: [null, Validators.required],
      height: [
        0,
        [Validators.min(150), Validators.max(400), Validators.required],
      ],
      length: [
        0,
        [Validators.min(300), Validators.max(1400), Validators.required],
      ],
      width: [
        0,
        [Validators.min(180), Validators.max(320), Validators.required],
      ],
      constructionYear: [
        0,
        [
          Validators.min(1950),
          Validators.max(new Date().getFullYear()),
          Validators.required,
        ],
      ],
      model: ['', Validators.required],
    }),

    seatsAndBedsForm: this.formBuilder.group(
      {
        // seats
        numSeatsOverall: [0, notZero()],
        numSeats3Point: [0],
        numSeatsLap: [0],
        numSeatsIsofix: [0],
        // beds
        numBedsOverall: [0],
        numBedsComfort: [0],
        numBedsComfortKids: [0],
        beds: [[]],
      },
      {
        validators: [
          this.bedsAndSeatsValidator.seatsSubSumsMatching(),
          this.bedsAndSeatsValidator.bedsSubSumsMatching(),
        ],
        updateOn: 'blur',
      }
    ),
    youtube: [''],
    // Equipment
    equipment: this.formBuilder.group({
      integratedEquipmentIds: [[], Validators.min(0)],
      optionalEquipmentIds: [[], Validators.min(0)],
    }),
    // images
    images: [[]],

    // prices
    pricesForm: this.formBuilder.group({
      seasonCollectionId: [null, Validators.required],
      prices: this.formBuilder.array([]),
      hasInstantBooking: this.formBuilder.control({
        value: true,
        disabled: true,
      }),
      hasBestPrice: this.formBuilder.control(false),
    }),
    mileage: this.formBuilder.group({
      costPerKm: this.formBuilder.control(0, Validators.required),
      isFree: this.formBuilder.control(false),
      freeAfterRentalDays: this.formBuilder.control(0, Validators.required),
      customPackages: this.formBuilder.array([], [Validators.maxLength(20)]),
      predefinedPackages: this.formBuilder.array([], [Validators.maxLength(3)]),
    }),

    rentalTermsForm: this.formBuilder.group({
      // Driver requirements
      minDriverAge: [18],
      drivingLicensePossession: [''],

      // Additional Drivers
      additionalDriversAllowed: [''],
      chargeForExtraDriver: [false],
      feePerDriver: [null],

      // Trips abroad
      region: [''],
      includeCountries: [[]],
      excludeCountries: [[]],

      // Insurance Costs
      deductiblePartialCover: [0],
      deductibleComprehensiveInsurance: [0],
      europeWideProtectionLetter: false,

      // Deposit
      depositAmount: [0],
      payOnSiteVia: [[]],
      payInAdvanceViaBankTransfer: false,
      daysInAdvance: [null],
      depositReduction: false,

      // Small animals
      smallAnimalsAllowed: [false],
      smallAnimalsPriceModell: [''],
      smallAnimalPrice: [0],
      numberOfSmallAnimals: [0],

      // Other
      smokingAllowed: false,
    }),
  });

  constructor(
    private stationService: StationService,
    private formBuilder: FormBuilder,
    private bedsAndSeatsValidator: BedsAndSeatsValidator,
    private optionalEquipmentService: OptionalEquipmentService
  ) {}

  ngOnChanges(changes: SimpleChanges): void {}

  ngOnDestroy(): void {
    this.priceLevelListSubscription.unsubscribe();
    this.stationServiceListSubscription.unsubscribe();
  }

  ngOnInit(): void {
    if (this.vehicle) {
      this.nameDescriptionForm.patchValue(this.vehicle);
      this.vehicleTypeId.patchValue(this.vehicle.vehicleTypeId);
      this.basisDataForm.patchValue(this.vehicle);
      this.seatsAndBedsForm.patchValue(this.vehicle);
      this.images = this.vehicle.images;
      this.mileageForm.patchValue(this.vehicle.mileage);
      const costPerKmFc = this.mileageForm.get('costPerKm');
      costPerKmFc?.setValue(costPerKmFc?.value / 100);

      this.pricesForm.patchValue(this.vehicle);
      this.pricesForm.setControl(
        'prices',
        this.buildPrices(this.vehicle.prices)
      );
      this.rentalTermsForm.patchValue(this.vehicle.rentalTerms);

      this.mileageForm.setControl(
        'predefinedPackages',
        this.buildPredefinedPackArray(this.vehicle.mileage.predefinedPackages)
      );
      this.mileageForm.setControl(
        'customPackages',
        this.buildCustomPackArray(this.vehicle.mileage.customPackages)
      );
      this.loadStation(this.vehicle.stationId);
    }
    this.stationList$ = this.stationService.list<Station>();
    this.categories$ = this.optionalEquipmentService.getCategories();
  }

  private loadStation(id: string): void {
    this.stationService.get<Station>(id).subscribe((station) => {
      this.selectedStation = station;
      this.optionalEquipment$ =
        this.optionalEquipmentService.list<OptionalEquipment>([
          `stationId=${station.id}`,
        ]);
    });
  }
  onEquipmentIdsChange(ids: EquipmentId[]): void {
    this.selectedEquipmentIds = ids;
  }

  private merge<T>(a: T[], b: T[]): T[] {
    return [...a, ...b];
  }

  private unqiue<T>(list: T[]): T[] {
    return [...new Set<T>(list)];
  }

  private getSelectedOptionalIds(): string[] {
    return this.unqiue(
      this.merge(
        this.selectOptionalEquipmentIds,
        this.selectedEquipmentIds
          .map((e) => (e.optionalEquipmentId ? e.optionalEquipmentId : ''))
          .filter((e) => e.length > 0)
      )
    );
  }

  get getSelOIds(): string[] {
    return this.selectedEquipmentIds
      .map((e) => (e.optionalEquipmentId ? e.optionalEquipmentId : ''))
      .filter((e) => e.length > 0);
  }
  onSelectedOptionalEquipment(ids: string[]): void {
    this.selectOptionalEquipmentIds = [...ids];
  }

  onStationSelect(id: string): void {
    this.loadStation(id);
  }

  isExpanded(n: number): boolean {
    return n === this.currentlyExpanded;
  }

  private expandSection(n: number): void {
    this.currentlyExpanded = n;
  }

  private buildPredefinedPackArray(values: PredefinedPackage[]): FormArray {
    return this.formBuilder.array(values.map((t) => this.formBuilder.group(t)));
  }

  private buildCustomPackArray(values: Package[]): FormArray {
    if (!values) {
      return this.formBuilder.array([]);
    }
    return this.formBuilder.array(
      values.map((t) =>
        this.formBuilder.group({
          km: [
            t.km,
            [Validators.required, Validators.max(10000), Validators.min(0)],
          ],
          discount: [
            t.discount,
            [Validators.required, Validators.max(100), Validators.min(0)],
          ],
          isSelected: true,
        })
      )
    );
  }

  private buildCountries(values: string[]): FormArray {
    return this.formBuilder.array(values);
  }

  private buildPrices(prices: Price[]): FormArray {
    return this.formBuilder.array(prices.map((p) => this.formBuilder.group(p)));
  }

  setRentaltermsID(id: string): void {
    this.vehicleForm.get('rentaltermsId')?.setValue(id);
  }

  get equipment(): FormGroup {
    return this.vehicleForm?.get('equipment') as FormGroup;
  }

  get integratedEquipmentIds(): FormGroup {
    return this.equipment.get('integratedEquipmentIds') as FormGroup;
  }

  get optionalEquipmentIds(): FormGroup {
    return this.equipment.get('optionalEquipmentIds') as FormGroup;
  }

  isEquipmentYellow(): boolean {
    const equipmentFg = this.equipment;

    return (
      (this.integratedEquipmentIds.value?.length === 0 ||
        this.optionalEquipmentIds.value?.length === 0) &&
      (!this.integratedEquipmentIds.pristine ||
        !this.optionalEquipmentIds.pristine)
    );
  }

  get nameDescriptionForm(): FormGroup {
    return this.vehicleForm.get('nameDescriptionForm') as FormGroup;
  }
  get nameDescriptionError(): boolean {
    const controls = this.nameDescriptionForm.controls;
    let value = false;
    for (const key in controls) {
      if (Object.prototype.hasOwnProperty.call(controls, key)) {
        const element = controls[key];
        if (!element.valid && !element.pristine) {
          value = true;
        }
      }
    }
    return value;
  }

  get nameDescriptionDataValueStatus(): boolean {
    return this.hasFormGroupEmptyValues(this.nameDescriptionForm);
  }

  get vehicleTypeId(): FormControl {
    return this.vehicleForm?.get('vehicleTypeId') as FormControl;
  }

  get vehicleTypeValueStatus(): boolean {
    const vehicleType = this.vehicleForm?.get('vehicleTypeId');
    return vehicleType?.value !== '';
  }

  get basisDataForm(): FormGroup {
    return this.vehicleForm.get('basisDataForm') as FormGroup;
  }

  get basisDataValueStatus(): boolean {
    return this.hasFormGroupEmptyValues(this.basisDataForm);
  }

  get seatsAndBedsForm(): FormGroup {
    return this.vehicleForm.get('seatsAndBedsForm') as FormGroup;
  }

  get seatsAndBedsValueStatus(): boolean {
    return this.hasFormGroupEmptyValues(this.seatsAndBedsForm);
  }

  get pricesForm(): FormGroup {
    return this.vehicleForm.get('pricesForm') as FormGroup;
  }

  get mileageForm(): FormGroup {
    return this.vehicleForm.get('mileage') as FormGroup;
  }

  get customPackages(): FormGroup {
    return this.vehicleForm.get('mileage.customPackages') as FormGroup;
  }

  get predefinedPackages(): FormGroup {
    return this.vehicleForm.get('mileage.predefinedPackages') as FormGroup;
  }

  get pricesValueStatus(): any {
    return this.hasFormGroupEmptyValues(this.pricesForm);
  }

  get rentalTermsForm(): FormGroup {
    return this.vehicleForm.get('rentalTermsForm') as FormGroup;
  }

  get includeCountries(): FormArray {
    return this.vehicleForm.get(
      'rentalTermsForm.includeCountries'
    ) as FormArray;
  }

  get rentalTermsFormValueStatus(): boolean {
    return this.hasFormGroupEmptyValues(this.rentalTermsForm);
  }

  hasFormGroupEmptyValues(fg: FormGroup): boolean {
    const controls = fg.controls;
    let value = true;
    for (const key in controls) {
      if (Object.prototype.hasOwnProperty.call(controls, key)) {
        const element = controls[key];
        if (element.value === '') {
          if (key !== 'includeCountries' && key !== 'excludeCountries') {
            value = false;
          }
        }
      }
    }
    return value;
  }

  get seasonCollectionId(): any {
    return this.pricesForm?.get('seasonCollectionId');
  }

  get beds(): any {
    return this.vehicleForm?.get('beds');
  }
  get stationId(): any {
    return this.vehicleForm?.get('stationId');
  }

  get powerMode(): any {
    return this.vehicleForm?.get('powerMode');
  }

  imageAdded(image: Image): void {
    this.images.push(image);
  }

  submitDraft(n?: number): void {
    if (n) {
      this.expandSection(n + 1);
    }
  }

  submit(): void {
    const newVehicle: Vehicle = {
      ...this.nameDescriptionForm.value,
      vehicleTypeId: +this.vehicleTypeId.value,
      ...this.basisDataForm.value,
      ...this.seatsAndBedsForm.value,
      images: this.images,
      mileage: { ...this.mileageForm.value },
      rentalTerms: {
        ...this.rentalTermsForm.value,
        minDriverAge: +this.rentalTermsForm.value.minDriverAge,
        drivingLicensePossession:
          +this.rentalTermsForm.value.drivingLicensePossession,
        numberOfSmallAnimals: +this.rentalTermsForm.value.numberOfSmallAnimals,
        region: ['delete when be is fixed'], // delete this when be is fixed
      },
      equipmentIds: [...this.selectedEquipmentIds],
      optionalEquipmentIds: [...this.getSelectedOptionalIds()],
      ...this.pricesForm.value,
      hasInstantBooking: false, // is disabled in priceform, when enabled in price form, this line could be deleted
      manufacturer: '' + this.basisDataForm.value.manufacturer, // delete this when be is fixed
      modelId: 1, // delete this when be is fixed
    };
    newVehicle.mileage.costPerKm = Math.round(
      newVehicle.mileage.costPerKm * 100
    );
    if (this.vehicle?.id) {
      newVehicle.id = this.vehicle.id;
    }
    if (newVehicle.powerMode === 'KWS') {
      newVehicle.power = Math.round(newVehicle.power / 1.36);
    }
    this.submitVehicleEvent.emit(newVehicle);
  }
}

function notZero(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const forbidden = control.value > 0;
    return !forbidden ? { length: { value: control.value } } : null;
  };
}

@Injectable({ providedIn: 'root' })
export class BedsAndSeatsValidator {
  public seatsSubSumsMatching(): ValidatorFn | null {
    return (formGroup: AbstractControl) => {
      let numSeatsOverall = formGroup.get('numSeatsOverall');
      let numSeats3Point = formGroup.get('numSeats3Point');
      let numSeatsLap = formGroup.get('numSeatsLap');

      const seatsNotMatching =
        parseInt(numSeatsOverall?.value) ===
        parseInt(numSeats3Point?.value) + parseInt(numSeatsLap?.value);

      return !seatsNotMatching ? { seatsNotMatching: { value: true } } : null;
    };
  }
  public bedsSubSumsMatching(): ValidatorFn | null {
    return (formGroup: AbstractControl) => {
      const numBedsOverall = formGroup.get('numBedsOverall');
      const numBedsComfort = formGroup.get('numBedsComfort');
      const numBedsComfortKids = formGroup.get('numBedsComfortKids');
      const bedsNotMatching =
        parseInt(numBedsOverall?.value) ===
        parseInt(numBedsComfort?.value) + parseInt(numBedsComfortKids?.value);
      return !bedsNotMatching ? { bedsNotMatching: { value: true } } : null;
    };
  }
}
