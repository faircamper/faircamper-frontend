import { Component, Input, OnDestroy } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { StaticDataService } from 'src/app/lessor/services/http/static-data-service/static-data.service';
import { IdValue } from 'src/app/lessor/services/http/static-data-service/staticData';

@Component({
  selector: 'lessor-vehicle-basis-data-form',
  templateUrl: './vehicle-basis-data-form.component.html',
  styleUrls: ['./vehicle-basis-data-form.component.css'],
})
export class VehicleBasisDataFormComponent implements OnDestroy {
  @Input() vehicleForm: FormGroup | null = null;
  fuelValues: IdValue[] = [];
  consumptionValues: IdValue[] = [];
  weightValues: IdValue[] = [];
  gearValues: IdValue[] = [];
  manufacturers: IdValue[] = [];
  constructor(private staticDataService: StaticDataService) {}

  ngOnInit(): void {
    this.staticDataService.getVehicleFuelTypes().subscribe((data) => {
      this.fuelValues = data;
    });
    this.staticDataService.getConsumptionValues().subscribe((data) => {
      this.consumptionValues = data;
    });
    this.staticDataService.getWeightValues().subscribe((data) => {
      this.weightValues = data;
    });
    this.staticDataService.getGearTypes().subscribe((data) => {
      this.gearValues = data;
    });
    this.staticDataService.getVehicleModels().subscribe((data) => {
      this.manufacturers = data;
    });
  }
  ngOnDestroy(): void {}

  get manufacturerId(): FormControl {
    return this.vehicleForm?.get('manufacturerId') as FormControl;
  }

  get model(): FormControl {
    return this.vehicleForm?.get('model') as FormControl;
  }

  get constructionYear(): FormControl {
    return this.vehicleForm?.get('constructionYear') as FormControl;
  }

  get power(): FormControl {
    return this.vehicleForm?.get('power') as FormControl;
  }

  get powerMode(): FormControl {
    return this.vehicleForm?.get('powerMode') as FormControl;
  }

  get payload(): FormControl {
    return this.vehicleForm?.get('payload') as FormControl;
  }

  get fuelId(): FormControl {
    return this.vehicleForm?.get('fuelId') as FormControl;
  }

  get consumptionId(): FormControl {
    return this.vehicleForm?.get('consumptionId') as FormControl;
  }

  get weightId(): FormControl {
    return this.vehicleForm?.get('weightId') as FormControl;
  }

  get gearId(): FormControl {
    return this.vehicleForm?.get('gearId') as FormControl;
  }

  get width(): FormControl {
    return this.vehicleForm?.get('width') as FormControl;
  }

  get height(): FormControl {
    return this.vehicleForm?.get('height') as FormControl;
  }

  get length(): FormControl {
    return this.vehicleForm?.get('length') as FormControl;
  }

  powerMath(): number {
    const power = this.power.value;
    const powerMode = this.powerMode.value;

    if (powerMode === 'PS') {
      return Math.round(power * 1.36);
    } else {
      return Math.round(power / 1.36);
    }
  }
}
