import {
  Component,
  EventEmitter,
  Input, OnInit,
  Output
} from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { OptionalEquipment } from 'src/app/lessor/models/equipment/equipment';

@Component({
  selector: 'app-equipment-link-select',
  templateUrl: './equipment-link-select.component.html',
  styleUrls: ['./equipment-link-select.component.css'],
})
export class EquipmentLinkSelectComponent implements OnInit {
  @Input() optionalEquipment: OptionalEquipment[] = [];
  @Input() selectedOptionEquipmentId: string | null = null;

  @Output() selectionChange = new EventEmitter<OptionalEquipment>();

  optionalEquipmentSelectionForm = this.fb.group({
    selectedOptionEquipmentId: this.fb.control(''),
  });
  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    if (this.optionalEquipment && this.optionalEquipment.length > 0) {
      const id = this.optionalEquipment[0].id;
      if (id) {
        this.selectedOptionEquipmentId = id;
      }
    }
    const v = this.optionalEquipmentSelectionForm
      .get('selectedOptionEquipmentId')
      ?.patchValue(this.selectedOptionEquipmentId);
    console.log(this);
  }

  get selectedId(): string {
    return this.optionalEquipmentSelectionForm.get('selectedOptionEquipmentId')
      ?.value;
  }
  onChange(): void {
    const oe = this.optionalEquipment.find((e) => this.selectedId === e.id);
    console.log(this.optionalEquipment, oe, this.selectedId);
    this.selectionChange.emit(oe);
  }
}
