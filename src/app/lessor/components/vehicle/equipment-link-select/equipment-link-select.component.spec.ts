import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EquipmentLinkSelectComponent } from './equipment-link-select.component';

describe('EquipmentLinkSelectComponent', () => {
  let component: EquipmentLinkSelectComponent;
  let fixture: ComponentFixture<EquipmentLinkSelectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EquipmentLinkSelectComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EquipmentLinkSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
