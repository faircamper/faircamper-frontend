import { Component, OnInit,} from '@angular/core';
import { EMPTY, Subscription } from 'rxjs';
import { VehicleBookingCalendar } from '../../models/booking-calendar';
import { BookingCalendarService } from '../../services/http/booking-calendar/booking-calendar.service';

@Component({
  selector: 'app-calendars-page',
  templateUrl: './booking-calendar-page.component.html',
  styleUrls: ['./booking-calendar-page.component.css']
})

export class BookingCalendarPageComponent implements OnInit {
  
  public calendars: VehicleBookingCalendar[] = new Array;
  calendarSubscription: Subscription = EMPTY.subscribe();
  constructor(public calendarService: BookingCalendarService,) {}

  ngOnInit(): void {
    this.calendarSubscription = this.calendarService
      .list<VehicleBookingCalendar>()
      .subscribe((data) => {
        this.calendars = data;
      });
  }

  ngOnDestroy(): void {
    this.calendarSubscription.unsubscribe();
  }

  
}
