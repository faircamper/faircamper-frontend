import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BookingCalendarPageHeaderComponent } from './booking-calendar-page-header.component';

describe('CalendarsPageHeaderComponent', () => {
  let component: BookingCalendarPageHeaderComponent;
  let fixture: ComponentFixture<BookingCalendarPageHeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BookingCalendarPageHeaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BookingCalendarPageHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
