import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-calendars-page-header',
  templateUrl: './booking-calendar-page-header.component.html',
  styleUrls: ['./booking-calendar-page-header.component.css']
})
export class BookingCalendarPageHeaderComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
