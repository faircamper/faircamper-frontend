import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { EMPTY, map, Observable, Subject } from 'rxjs';
import { Company, LessorAccount } from 'src/app/lessor/models/account/lessor';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.css'],
})
export class CompanyComponent implements OnInit {
  @Input() account$: Observable<LessorAccount> = EMPTY;
  company: Company | null = null;
  @Output() companyOut = new EventEmitter<Company>();
  edit: boolean = false;
  saveEvent: Subject<void> = new Subject<void>();

  constructor() {}

  ngOnInit(): void {
    this.account$.subscribe((account) => (this.company = account.company));
  }

  editCompany(): void {
    this.edit = !this.edit;
  }

  saveCompany(): void {
    this.saveEvent.next();
    this.edit = !this.edit;
  }

  onCompanyUpdate(c: Company): void {
    this.companyOut.emit(c);
  }
}
