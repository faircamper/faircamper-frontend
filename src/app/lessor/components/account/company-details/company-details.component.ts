import { Component, Input, OnInit } from '@angular/core';
import { EMPTY, Observable } from 'rxjs';
import { LessorAccount } from 'src/app/lessor/models/account/lessor';
@Component({
  selector: 'app-company-details',
  templateUrl: './company-details.component.html',
  styleUrls: ['./company-details.component.css'],
})
export class CompanyDataComponent implements OnInit {
  @Input() account$: Observable<LessorAccount> = EMPTY;

  constructor() {}

  ngOnInit(): void {}
}
