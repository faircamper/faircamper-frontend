import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EMPTY, Observable, Subscription } from 'rxjs';
import { Company, LessorAccount } from 'src/app/lessor/models/account/lessor';

@Component({
  selector: 'app-company-form',
  templateUrl: './company-form.component.html',
  styleUrls: ['./company-form.component.css'],
})
export class CompanyDataFormComponent implements OnInit, OnChanges, OnDestroy {
  companyForm: FormGroup = this.fb.group({});
  @Input() company: Company | null = null;
  @Input() saveEvent: Observable<void> = EMPTY;
  saveEventSubscription: Subscription = EMPTY.subscribe();
  @Output() companyFormOut = new EventEmitter<Company>();

  constructor(public fb: FormBuilder) {
    this.companyForm = this.fb.group({
      name: this.fb.control('', [Validators.required]),
      businessFormId: [0, [Validators.required]],
      street: ['', [Validators.required]],
      streetNumber: ['', [Validators.required]],
      postalCode: ['', [Validators.required]],
      city: ['', [Validators.required]],
      country: ['', [Validators.required]],
      preTaxDeductable: [false, [Validators.required]],
      website: ['', [Validators.required]],
    });
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (this.company) {
      this.companyForm.patchValue(this.company);
    }
  }
  ngOnDestroy(): void {
    this.saveEventSubscription.unsubscribe();
  }

  ngOnInit(): void {
    this.saveEventSubscription = this.saveEvent.subscribe(() => {
      const newCompany: Company = {
        ...this.companyForm.value,
        businessFormId: parseInt(this.companyForm.value.businessFormId),
      };
      if (this.companyForm.touched) {
        this.companyFormOut.emit(newCompany);
      }
    });
  }

  businessForms = [
    { id: 0, name: 'Privat' },
    { id: 1, name: 'Einzelkaufman' },
    { id: 2, name: 'e.k' },
    { id: 3, name: 'GbR' },
    { id: 4, name: 'GmbH' },
    { id: 5, name: 'UG' },
  ];

  get getCompany(): FormGroup {
    return this.companyForm as FormGroup;
  }
}
