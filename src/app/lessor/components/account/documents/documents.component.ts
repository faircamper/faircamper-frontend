import { Component, Input, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { EMPTY, Observable } from 'rxjs';
import { Doc } from 'src/app/lessor/models/account/lessor';
import {
  accountAddDocAction,
  accountDeleteDocAction,
} from 'src/app/lessor/ngrx/actions/account.actions';
import { selectDocuments } from 'src/app/lessor/ngrx/selectors/account.selector';

@Component({
  selector: 'app-documents',
  templateUrl: './documents.component.html',
  styleUrls: ['./documents.component.css'],
})
export class DocumentsComponent implements OnInit {
  documents$: Observable<Doc[]> = EMPTY;

  constructor(private store: Store) {}
  ngOnInit(): void {
    this.documents$ = this.store.select(selectDocuments);
    // .subscribe((e) => console.log(e));
  }

  documentCreateEvent(doc: Doc) {
    this.store.dispatch(accountAddDocAction({ doc: doc }));
  }

  onDocumentDelete(doc: Doc) {
    this.store.dispatch(accountDeleteDocAction({ doc: doc }));
  }
}
