import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { EMPTY, map, Observable, Subscription } from 'rxjs';
import { EMPTY_SUBSCRIPTION } from 'rxjs/internal/Subscription';
import { LessorAccount } from '../../../../lessor/models/account/lessor';

@Component({
  selector: 'app-personal-data',
  templateUrl: './personal-data.component.html',
  styleUrls: ['./personal-data.component.css'],
})
export class PersonalDataComponent implements OnInit {
  @Input() account$: Observable<LessorAccount> = EMPTY;

  constructor() {}

  ngOnInit(): void {}

  editPersonalInfo() {}
}
