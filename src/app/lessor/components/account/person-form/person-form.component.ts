import { formatDate } from '@angular/common';
import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import {
  FormBuilder,
  FormControlStatus,
  FormGroup,
  Validators,
} from '@angular/forms';
import { EMPTY, map, Observable, Subscription, switchMap, tap } from 'rxjs';
import {
  accountRoles,
  IdValue,
  Person,
} from 'src/app/lessor/models/account/lessor';

@Component({
  selector: 'app-person-form',
  templateUrl: './person-form.component.html',
  styleUrls: ['./person-form.component.css'],
})
export class PersonFormComponent implements OnInit, OnChanges, OnDestroy {
  @Input() min: string = '';//format '2022-05-22'
  @Input() max: string = formatDate(new Date(), 'YYYY-MM-dd', 'en_Us');//todays date
  @Input() disabledDate: boolean = false;
  
  @Input() person: Person | null = null;
  @Input() saveEvent: Observable<void> = EMPTY;
  saveEventSubscription: Subscription = EMPTY.subscribe();

  @Output() personOut = new EventEmitter<Person>();
  @Output() status = new EventEmitter<boolean>();
  formStatusSubscription: Subscription = EMPTY.subscribe();

  personForm: FormGroup;
  accountRoles: IdValue[] = accountRoles;

  constructor(fb: FormBuilder) {
    this.personForm = fb.group({
      id: [''],
      accountRoleId: ['', [Validators.required]],
      accountRole: [''],
      title: [''],
      name: ['', [Validators.required]],
      surname: ['', [Validators.required]],
      birthday: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      mobilePhone: ['', [Validators.required]],
      phone: ['', [Validators.required]],
    });
  }

  ngOnInit(): void {
    this.saveEventSubscription = this.listenToSaveInput();
    this.formStatusSubscription = this.listenToFormStatus();
    console.log(this.personForm.value);
  }

  ngOnChanges() {
    if (this.person) {
      this.setFormValues(this.person);
      console.log(this.personForm.value);
    }
  }

  ngOnDestroy(): void {
    this.saveEventSubscription.unsubscribe();
    this.formStatusSubscription.unsubscribe();
  }

  get getPerson(): FormGroup {
    return this.personForm as FormGroup;
  }

  public isFormCtrlValid(name: string): boolean | undefined {
    return (
      this.getPerson.get(name)?.invalid && this.getPerson.get(name)?.touched
    );
  }
  private setFormValues(p: Person) {
    this.personForm.patchValue(p);
  }

  private submit(): void {
    if (this.personForm.invalid) {
      return;
    }
    const newPerson: Person = {
      ...this.personForm.value,
      accountRoleId: parseInt(this.personForm.value.accountRoleId),
    };
    this.personOut.emit(newPerson);
    this.personForm.reset();
  }

  private listenToSaveInput(): Subscription {
    return this.saveEvent.subscribe(() => {
      this.submit();
    });
  }

  private listenToFormStatus(): Subscription {
    return this.personForm.statusChanges
      .pipe(
        map((s: FormControlStatus) => {
          switch (s) {
            case 'INVALID':
              return false;
            case 'DISABLED':
              return false;
            case 'VALID':
              return true;
            case 'PENDING':
              return false;
            default:
              return false;
          }
        })
      )
      .subscribe((isValid) => this.status.emit(isValid));
  }
}
