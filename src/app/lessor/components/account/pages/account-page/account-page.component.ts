import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { SSOStateService } from 'projects/wobi/sso/src/lib/services/state.service';
import { Company } from 'src/app/lessor/models/account/lessor';
import { LessorAccountService } from 'src/app/lessor/services/http/account/lessor-account.service';
import { accountUpdateCompanyAction } from '../../../../ngrx/actions/account.actions';
import { selectAccount } from '../../../../ngrx/selectors/account.selector';

@Component({
  selector: 'app-account-page',
  templateUrl: './account-page.component.html',
  styleUrls: ['./account-page.component.css'],
})
export class AccountPageComponent implements OnInit {
  account$ = this.store.select(selectAccount);
  constructor(
    public accountService: LessorAccountService,
    protected sso: SSOStateService,
    private store: Store
  ) {}

  ngOnInit(): void {}

  onCompanyUpdate(c: Company): void {
    this.store.dispatch(accountUpdateCompanyAction({ company: c }));
  }
}
