import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { map, switchMap } from 'rxjs';
import { Doc, DocumentResponse } from 'src/app/lessor/models/account/lessor';
import { DocumentService } from 'src/app/lessor/services/http/account/document.service';

@Component({
  selector: 'app-document-upload-form',
  templateUrl: './document-upload-form.component.html',
  styleUrls: ['./document-upload-form.component.css'],
})
export class DocumentUploadFormComponent {
  @Output() docOut = new EventEmitter<Doc>();
  constructor(public documentService: DocumentService) {}

  onFileSelect(event: any): void {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      const formData: FormData = new FormData();
      formData.append('file', file, file.name);
      this.documentService
        .uploadDocument(formData)
        .pipe(
          map((resp) => {
            const newDoc: Doc = {
              id: resp.data?.id,
              documentType: 'defaultDocType',
              documentTypeId: 0,
              fileName: file.name,
            };
            return newDoc;
          })
        )
        .subscribe((doc) => {
          this.docOut.emit(doc);
        });
    }
  }
}
