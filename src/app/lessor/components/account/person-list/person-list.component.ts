import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { EMPTY, Observable, Subject } from 'rxjs';
import { accountRoles, Person } from 'src/app/lessor/models/account/lessor';

@Component({
  selector: 'app-person-list',
  templateUrl: './person-list.component.html',
  styleUrls: ['./person-list.component.css'],
})
export class PersonListComponent implements OnInit {
  @Input() persons$: Observable<Person[]> = EMPTY;
  @Output() personCreateEvent = new EventEmitter<Person>();
  @Output() personDeleteEvent = new EventEmitter<Person>();
  @Output() personEditEvent = new EventEmitter<Person>();

  saveEvent: Subject<void> = new Subject<void>();

  selectedPerson: Person | null = null;

  showForm = false;
  isFormValid = false;
  editing = false;

  constructor(private store: Store) {}

  ngOnInit(): void {}

  createPerson(): void {
    this.showForm = !this.showForm;
    this.selectedPerson = null;
  }

  onPersonCreate(p: Person): void {
    this.showForm = !this.showForm;
    if (this.editing) {
      this.personEditEvent.emit(p);
      this.editing = false;
    } else {
      this.personCreateEvent.emit(p);
    }
  }

  deletePerson(p: Person): void {
    this.personDeleteEvent.emit(p);
  }

  onFormStatus(isValid: boolean): void {
    this.isFormValid = isValid;
  }

  findAccountRoleById(id: number | undefined): string | undefined {
    if (id !== undefined || id !== null) {
      return accountRoles.find((e) => e.id === id)?.name;
    }
    return '';
  }

  editPerson(p: Person): void {
    this.showForm = !this.showForm;
    this.editing = true;
    this.selectedPerson = p;
  }

  savePersonsList(): void {
    this.saveEvent.next();
    this.showForm = false;
  }

  backToList(): void {
    this.showForm = false;
    this.selectedPerson = null;
  }
}
