import { Component, EventEmitter, Input, Output } from '@angular/core';
import { EMPTY, Observable } from 'rxjs';
import { Doc } from 'src/app/lessor/models/account/lessor';

@Component({
  selector: 'app-document-list',
  templateUrl: './document-list.component.html',
  styleUrls: ['./document-list.component.css'],
})
export class DocumentListComponent {
  @Input() docs$: Observable<Doc[]> = EMPTY;
  @Output() documentCreateEvent = new EventEmitter<Doc>();
  @Output() documentDeleteEvent = new EventEmitter<Doc>();

  constructor() {}

  onDocumentUpload(d: Doc): void {
    this.documentCreateEvent.emit(d);
  }

  onDocDelete(d: Doc): void {
    this.documentDeleteEvent.emit(d);
  }
}
