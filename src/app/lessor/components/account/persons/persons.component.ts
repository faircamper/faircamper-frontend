import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { EMPTY, Observable } from 'rxjs';
import { Person } from 'src/app/lessor/models/account/lessor';
import {
  accountAddPersonAction,
  accountDeletePersonAction,
  accountUpdatePersonAction,
} from 'src/app/lessor/ngrx/actions/account.actions';
import { selectPersons } from 'src/app/lessor/ngrx/selectors/account.selector';

@Component({
  selector: 'app-persons',
  templateUrl: './persons.component.html',
  styleUrls: ['./persons.component.css'],
})
export class PersonsComponent implements OnInit {
  persons$: Observable<Person[]> = EMPTY;
  constructor(private store: Store) {}

  ngOnInit(): void {
    this.persons$ = this.store.select(selectPersons);
  }

  onPersonsAdd(p: Person): void {
    this.store.dispatch(accountAddPersonAction({ person: p }));
  }

  onPersonDeleted(p: Person): void {
    this.store.dispatch(accountDeletePersonAction({ person: p }));
  }

  onPersonEdit(p: Person): void {
    this.store.dispatch(accountUpdatePersonAction({ person: p }));
  }
}
