import { Component, OnInit } from '@angular/core';
import { EMPTY, Observable } from 'rxjs';
import { StationListItem } from 'src/app/lessor/models/station/station';
import { StationService } from 'src/app/lessor/services/http/station/station.service';

@Component({
  selector: 'fc-station-list',
  templateUrl: './station-list.component.html',
  styleUrls: ['./station-list.component.scss'],
})
export class StationListComponent implements OnInit {
  stationList$: Observable<StationListItem[]> = EMPTY;

  columns = [
    {
      name: $localize`:@@fc-station-list:Name`,
      sortable: true,
      sort: 'externalId',
    },
    {
      name: $localize`:@@fc-station-list:Straße`,
      sortable: true,
      sort: 'externalId',
    },
    {
      name: $localize`:@@fc-station-list:PLZ`,
      sortable: true,
      sort: 'externalId',
    },
  ];

  constructor(private stationService: StationService) {}

  ngOnInit(): void {
    this.stationList$ = this.stationService.list<StationListItem>();
  }
}
