import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import {
  AbstractControl,
  FormArray,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { EMPTY, Subscription } from 'rxjs';
import { debounceTime, filter } from 'rxjs/operators';
import { FormStatus } from 'src/app/lessor/models/formstate';
import {
  defaultOpeningHours,
  handoverType,
  handoverValues,
  OpeningHours,
  weekdays,
} from 'src/app/lessor/models/station/station-opening-hours';

@Component({
  selector: 'fc-station-opening-times',
  templateUrl: './station-opening-hours.component.html',
  styleUrls: ['./station-opening-hours.component.css'],
})
export class StationsOpeningTimesComponent
  implements OnInit, OnDestroy, OnChanges
{
  @Input()
  hours: OpeningHours[] = [];

  @Input()
  editing = false;

  @Output()
  hoursEvent = new EventEmitter<OpeningHours[]>();

  @Output()
  hoursStatusEvent = new EventEmitter<FormStatus>();

  openingForm: FormGroup = new FormGroup({});
  openingFormSubscription: Subscription = EMPTY.subscribe();
  formStatusSubscription: Subscription = EMPTY.subscribe();
  handoverValues = handoverValues;
  constructor(private fb: FormBuilder) {}

  ngOnChanges(changes: SimpleChanges): void {
    console.log('onchanges: %o', changes, this.openingForm);
    if (this.hours.length > 0) {
      this.setFormValues(this.hours);
    }
  }

  ngOnInit(): void {
    this.initForm();
    this.openingFormSubscription = this.listenToForm();
    this.formStatusSubscription = this.listenToStatus();
  }

  ngOnDestroy(): void {
    this.openingFormSubscription.unsubscribe();
    this.formStatusSubscription.unsubscribe();
  }
  private initForm(): void {
    this.openingForm = this.fb.group({
      days: this.buildDaysArray(defaultOpeningHours),
    });
    // sending an event with opening hours in the case of an unchanged but valid form
    this.hoursEvent.emit(this.hours);
    // sending initial status/data
    this.hoursStatusEvent.emit(this.openingForm.status as FormStatus);
    const days = [...this.openingForm.value.days];
    this.hoursEvent.emit(days);
  }

  private buildDaysArray(oh: OpeningHours[]): FormArray {
    const days = oh.map((day, i) => {
      return this.fb.group({
        day: [i],
        handoverId: [day.handover?.id],
        checkin: this.fb.group({
          from: [day.checkin?.from, Validators.required],
          to: [day.checkin?.to, Validators.required],
        }),
        checkout: this.fb.group({
          from: [day.checkout?.from, Validators.required],
          to: [day.checkout?.to, Validators.required],
        }),
        isFee: [day.isFee],
      });
    });
    return this.fb.array(days);
  }

  private setFormValues(oh: OpeningHours[]): void {
    this.openingForm.patchValue({ days: oh });
  }

  get days(): FormArray {
    return this.openingForm.get('days') as FormArray;
  }

  isOpen(value: any): boolean {
    return value.handoverId === handoverType.OPEN;
  }

  isConsultation(formValue: any): boolean {
    return formValue?.handoverId === handoverType.CONSULTATION;
  }

  getHandoverId(i: number): number {
    const days = this.openingForm.get('days') as FormArray;
    const handoverId = days.at(i)?.value.handoverId;
    return handoverId;
  }

  disableFormElements(control: AbstractControl): void {
    switch (parseInt(control.value?.handoverId)) {
      case handoverType.OPEN:
        control.get('checkin')?.enable();
        control.get('checkout')?.enable();
        break;
      case handoverType.CLOSED:
        control.get('checkin')?.disable();
        control.get('checkout')?.disable();
        break;
      case handoverType.CONSULTATION:
        control.get('checkin')?.disable();
        control.get('checkout')?.disable();
        break;
      default:
        break;
    }
    control.updateValueAndValidity();
  }

  getStatusIcon(oh: OpeningHours): { icon: string; color: string } {
    switch (oh.handover.id) {
      case handoverType.OPEN:
        return {
          icon: 'query_builder',
          color: 'green',
        };
      case handoverType.CLOSED:
        return {
          icon: 'lock_clock',
          color: 'red',
        };
      case handoverType.CONSULTATION:
        return {
          icon: 'contact_phone',
          color: 'coral',
        };
      default:
        return {
          icon: 'disabled_by_default',
          color: 'red',
        };
    }
  }

  getWeekday(day: number): string | undefined {
    return weekdays.get(day);
  }

  listenToForm(): Subscription {
    if (!this.openingForm) {
      return EMPTY.subscribe();
    }
    return this.openingForm.valueChanges
      .pipe(
        filter((f: any) => this.openingForm.valid),
        debounceTime(50)
      )
      .subscribe((formVal: any) => {
        const days = [...formVal.days];
        this.hoursEvent.emit(days);
      });
  }

  private listenToStatus(): Subscription {
    return this.openingForm.statusChanges.subscribe((status: any) => {
      this.hoursStatusEvent.emit(status);
    });
  }
}
