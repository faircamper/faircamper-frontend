import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { FormStatus } from 'src/app/lessor/models/formstate';
import { Station } from 'src/app/lessor/models/station/station';

@Component({
  selector: 'fc-station-metadata-form',
  templateUrl: './station-metadata-form.component.html',
  styleUrls: ['./station-metadata-form.component.scss'],
})
export class StationMetadataFormComponent
  implements OnInit, OnDestroy, OnChanges
{
  @Input()
  station: Station | null = null;
  @Input()
  editing = false;
  @Output()
  stationFormEvent = new EventEmitter<Station>();
  @Output()
  stationFormStatus = new EventEmitter<FormStatus>();
  stationForm: FormGroup = new FormGroup({});
  stationFormSubscription: Subscription | null = null;
  stationFormStatusSubscription: Subscription | null = null;

  constructor(private fb: FormBuilder) {}

  ngOnChanges(changes: SimpleChanges): void {
    this.stationForm = this.initForm();
    this.stationFormSubscription = this.listenToChanges();
    this.stationFormStatusSubscription = this.listenToStatus();
    if (this.station !== null) {
      this.setFormValues(this.station);
    }
  }

  ngOnInit(): void {
    this.stationForm = this.initForm();
    this.stationFormSubscription = this.listenToChanges();
    this.stationFormStatusSubscription = this.listenToStatus();
  }

  ngOnDestroy(): void {
    this.stationFormSubscription?.unsubscribe();
    this.stationFormStatusSubscription?.unsubscribe();
  }

  private listenToChanges(): Subscription {
    return this.stationForm.valueChanges
      .pipe
      // debounceTime(this.config.formDebounceTime()),
      // filter(() => this.stationForm.valid)
      ()
      .subscribe((formVal: any) => {
        console.log(formVal, this.stationForm);

        const station: Station = {
          ...formVal,
          id: this.editing ? formVal.id : null,
          version: this.editing ? formVal.version : 0,
        };
        this.stationFormEvent.emit(station);
      });
  }

  private listenToStatus(): Subscription {
    return (this.stationFormStatusSubscription =
      this.stationForm.statusChanges.subscribe((status: any) => {
        this.stationFormStatus.emit(status);
      }));
  }

  public setFormValues(station: Station): void {
    this.stationForm.patchValue(station);
  }

  private initForm(): FormGroup {
    return this.fb.group({
      id: [''],
      version: [''],
      name: ['', [Validators.minLength(3), Validators.required]],
      description: ['', [Validators.required]],
      organization: ['', [Validators.required]],
      optional: [''],
      street: ['', [Validators.minLength(1), Validators.required]],
      postalCode: [
        '',
        [
          Validators.minLength(5),
          Validators.maxLength(5),
          Validators.pattern(/[0-9]/),
          Validators.required,
        ],
      ],
      city: ['', [Validators.minLength(3), Validators.required]],
      country: ['', [Validators.required]],
      // https://support.google.com/maps/answer/18539?hl=de&co=GENIE.Platform%3DDesktop
      geoLocation: this.fb.group({
        latitude: ['', [Validators.min(-90), Validators.max(90)]],
        longitude: ['', [Validators.min(-180), Validators.max(180)]],
      }),
      fee: ['', [Validators.required]],
    });
  }
}
