import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EMPTY, Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { Station } from 'src/app/lessor/models/station/station';
import { StationService } from 'src/app/lessor/services/http/station/station.service';
import { MessagesService } from 'src/app/lessor/services/message/messages.service';

@Component({
  selector: 'fc-station-edit',
  templateUrl: './station-edit.component.html',
  styleUrls: ['./station-edit.component.scss'],
})
export class StationEditComponent implements OnInit {
  station$: Observable<Station> = EMPTY;

  constructor(
    private activeRoute: ActivatedRoute,
    private stationService: StationService,
    private messageService: MessagesService
  ) {}

  ngOnInit(): void {
    this.station$ = this.createStationStream();
  }

  onStationUpdate(station: Station): void {
    this.stationService.update(station).subscribe((e) => {
      this.messageService.send({ text: 'Station updated' });
      this.station$ = this.createStationStream();
    });
  }

  createStationStream(): Observable<Station> {
    return this.activeRoute.paramMap.pipe(
      map((params) => params.get('id')),
      map((id: string | null) => (id === null ? '' : id)),
      switchMap((id: string) => this.stationService.get<Station>(id))
    );
  }
}
