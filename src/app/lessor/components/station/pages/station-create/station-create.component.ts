import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Station } from 'src/app/lessor/models/station/station';
import { StationService } from 'src/app/lessor/services/http/station/station.service';

@Component({
	selector: 'fc-station-create',
	templateUrl: './station-create.component.html',
	styleUrls: ['./station-create.component.scss'],
})
export class StationCreateComponent {
	constructor(private router: Router, private stationService: StationService) {}

	onStationCreate(station: Station): void {
		this.stationService
			.create(station)
			.toPromise()
			.then(() => {
				this.router.navigate(['lessor/stations']);
			});
	}
}
