import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { FormStatus } from 'src/app/lessor/models/formstate';
import { Station } from 'src/app/lessor/models/station/station';
import { OpeningHours, defaultOpeningHours } from 'src/app/lessor/models/station/station-opening-hours';
@Component({
	selector: 'fc-stationform',
	templateUrl: './station-form.component.html',
	styleUrls: ['./station-form.component.scss'],
})
export class StationFormComponent implements OnChanges {
	@Input()
	station: Station | null = null;

	@Input()
	editing = false;

	@Output()
	stationFormEvent = new EventEmitter<Station>();

	// state that is returned by the child components
	stationSnapshot: Station | null = null;

	stationFormStatus: Map<Panel, FormStatus> = new Map([
		[Panel.Metadata, FormStatus.INVALID],
		[Panel.OpeningHours, FormStatus.INVALID],
		[Panel.Equipment, FormStatus.VALID],
	]);

	constructor() {
		if (!this.editing) {
			this.stationSnapshot = {
        name: '',
        description: '',
        organization: '',
        city: '',
        fee: 0,
        street: '',
        optional: '',
        country: '',
        postalCode: '',
        openingHours: [],
        geoLocation: {
          latitude: 0,
          longitude: 0,
        },
        version: 0,
      };
		}
	}

	ngOnChanges(changes: SimpleChanges): void {
		console.log('initStationForm: %o', this.station);
		if (this.station === null) {
			return;
		}
		console.log(this.stationFormStatus);

		this.stationSnapshot = { ...this.station };
	}

	onStationCreate(station: Station): void {
		this.stationSnapshot = {
			...this.stationSnapshot,
			...station,
		};
	}

	onOpeningHoursStatus(status: FormStatus): void {
		this.stationFormStatus.set(Panel.OpeningHours, status);
	}

	onEquipmentListStatus(status: FormStatus): void {
		this.stationFormStatus.set(Panel.Equipment, status);
	}

	onStationMetadataStatus(status: FormStatus): void {
		this.stationFormStatus.set(Panel.Metadata, status);
	}

	onOpeningHoursChanged(oh: OpeningHours[]): void {
		console.log(oh);

		if (this.stationSnapshot === null) {
			return;
		}
		this.stationSnapshot.openingHours = oh;
	}

	submit(): void {
		console.log(this.stationSnapshot);

		if (this.stationSnapshot === null) {
			return;
		}
		this.stationFormEvent.emit(this.stationSnapshot);
	}

	isStationFormValid(): boolean {
		for (const status of this.stationFormStatus.values()) {
			switch (status) {
				case FormStatus.DISABLED:
				case FormStatus.INVALID:
				case FormStatus.PENDING:
					return false;
			}
		}
		return true;
	}

	defaultOpeningHours(): OpeningHours[] {
		return defaultOpeningHours;
	}

	getOpeningHours(): OpeningHours[] {
		if (this.station === null) {
			return [];
		}
		return this.station.openingHours;
	}
}

enum Panel {
	Metadata = 'StationMetadata',
	OpeningHours = 'OpeningHours',
	Equipment = 'Equipment',
}
