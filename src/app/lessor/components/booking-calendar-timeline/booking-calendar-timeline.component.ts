import { Component, Input, OnInit } from '@angular/core';
import { BookingCalendar } from '../../models/booking-calendar';
import * as dayjs from 'dayjs';

@Component({
  selector: 'app-calendars-timeline',
  templateUrl: './booking-calendar-timeline.component.html',
  styleUrls: ['./booking-calendar-timeline.component.css']
})

export class BookingCalendarTimelineComponent implements OnInit {
  daysInMonth: Array<Day> = [];
  @Input() vehicleId: string = '';
  @Input() calendars: Array<BookingCalendar> = [];
  currentMonth:string= '';
  private dateFormat = 'YYYY-MM-DD';
  
  constructor() { }

  ngOnInit(): void {
    this.daysInMonth = this.getTimeLine(dayjs(new Date()).locale('de').format(this.dateFormat));
    this.currentMonth = dayjs(new Date()).locale('de').format('YYYY-MM')
  }

  private getTimeLine(date:string=''){
    const daysArray:Array<Day> = []
    const currentDate = dayjs(new Date(date)).format(this.dateFormat)
    const currentDays = dayjs(new Date(date)).daysInMonth()

    for (let i = 0; i < currentDays; i++) {
      let currentDay = dayjs(currentDate).add(i, 'day').format(this.dateFormat)
      daysArray.push({number:i+1,date:currentDay,bookingType:''}) 
    }

    if(this.vehicleId != ''){
      let count = 0
      this.calendars.forEach(function (val:any){
        if(dayjs(currentDate).get('month') == dayjs(val.daterange.from).get('month') 
          && dayjs(currentDate).get('month') == dayjs(val.daterange.to).get('month')){
          let fromToDays = (dayjs(val.daterange.from).diff(val.daterange.to, 'day')*-1+1)
          for (let j = 0; j < fromToDays; j++) {
            daysArray[count].bookingType = val.bookingType
            count++
          }
        }
      });
    }
    return daysArray  
  }

  onChangeUpdateList(event: any) {
    let newDate = event.target.value + '-01';
    this.daysInMonth = this.getTimeLine(newDate);
  }

}

interface Day{
  number:number;
  date:string;
  bookingType:string;
}