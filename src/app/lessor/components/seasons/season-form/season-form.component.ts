import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { EMPTY, Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import {
  PriceLevel,
  Season,
  SeasonCollection,
} from 'src/app/lessor/models/seasons/season';

@Component({
  selector: 'fc-season-form',
  templateUrl: './season-form.component.html',
  styleUrls: ['./season-form.component.scss'],
})
export class SeasonFormComponent implements OnInit, OnDestroy, OnChanges {
  @Input() season: Season = {
    priceLevel: { name: 'default', id: -1, color: '#000000' },
    from: new Date(2021, 1, 1),
    to: new Date(2021, 1, 2),
  };
  @Input() priceLevels: PriceLevel[] | null = [];
  @Input() createMode = false;
  @Input() lastSeason = false;
  @Output() changedSeason = new EventEmitter<Season>();
  @Output() deleteSeason = new EventEmitter<Season>();

  @Output() seasonYearEvent = new EventEmitter<SeasonCollection>();
  seasonForm = this.fb.group({
    priceLevel: this.fb.control('', {
      validators: [Validators.required],
      updateOn: 'blur',
    }),
    from: this.fb.control('', {
      validators: [Validators.required],
      updateOn: 'blur',
    }),
    to: this.fb.control('', {
      validators: [Validators.required],
      updateOn: 'blur',
    }),
  });
  seasonChangeSubscription: Subscription = EMPTY.subscribe();
  priceLevelChangeSubscription: Subscription = EMPTY.subscribe();

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    if (!this.createMode) {
      this.setFormValues(this.season);
    }
    this.onChanges();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.createMode) {
      return;
    }
    if (changes?.['season']) {
      this.setFormValues(changes['season'].currentValue);
    }
  }

  setFormValues(season: Season): void {
    this.seasonForm.patchValue({
      priceLevel: season.priceLevel,
      from: new Date(season.from),
      to: new Date(season.to),
    });
  }

  onChanges(): void {
    const seasonFormPriceLevel = this.seasonForm.get('priceLevel');
    if (seasonFormPriceLevel) {
      this.priceLevelChangeSubscription = seasonFormPriceLevel?.valueChanges
        .pipe(
          filter(() => {
            return this.seasonForm.valid;
          })
        )
        .subscribe((val: PriceLevel) => {
          if (val && !this.createMode) {
            this.season.priceLevel = val;
            this.changedSeason.emit(this.season);
          }
        });
    }
  }

  ngOnDestroy(): void {
    this.seasonForm.reset();
    this.seasonChangeSubscription.unsubscribe();
    this.priceLevelChangeSubscription.unsubscribe();
  }

  comparePriceLevel(priceLevel1: PriceLevel, priceLevel2: PriceLevel): boolean {
    return priceLevel1 && priceLevel2 && priceLevel1.id === priceLevel2.id;
  }

  submit(): void {
    const newSeason: Season = {
      ...this.seasonForm.value,
    };
    console.log(newSeason);

    this.changedSeason.emit(newSeason);
    this.seasonForm.reset();
  }

  cancelSeasonForm(): void {
    this.changedSeason.emit();
    this.seasonForm.reset();
  }
}
