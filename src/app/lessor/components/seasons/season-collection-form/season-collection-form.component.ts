import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { SeasonCollection } from 'src/app/lessor/models/seasons/season';

@Component({
  selector: 'fc-season-collection-form',
  templateUrl: './season-collection-form.component.html',
  styleUrls: ['./season-collection-form.component.css'],
})
export class SeasonCollectionFormComponent implements OnInit {
  @Output() seasonCollectionCreateEvent = new EventEmitter<SeasonCollection>();
  seasonCollectionForm: FormGroup = new FormGroup({
    name: new FormControl(''),
  });
  constructor() {}

  ngOnInit(): void {}

  /**
   * submit
   */
  submit() {
    const sc: SeasonCollection = {
      ...this.seasonCollectionForm.value,
    };
    this.seasonCollectionCreateEvent.emit(sc);
    this.seasonCollectionForm.reset();
  }

  cancel() {
    this.seasonCollectionCreateEvent.emit();
    this.seasonCollectionForm.reset();
  }
}
