import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SeasonCollectionFormComponent } from './season-collection-form.component';

describe('SeasonCollectionFormComponent', () => {
  let component: SeasonCollectionFormComponent;
  let fixture: ComponentFixture<SeasonCollectionFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SeasonCollectionFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SeasonCollectionFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
