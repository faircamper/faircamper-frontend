import { Component, Input } from '@angular/core';
import { MessagesService } from 'src/app/lessor/services/message/messages.service';
import { PriceLevel, Season } from '../models/season';

@Component({
  selector: 'fc-season-create-dialog',
  templateUrl: './season-create-dialog.component.html',
  styleUrls: ['./season-create-dialog.component.scss'],
})
export class SeasonCreateDialogComponent {
  @Input() priceLevels: PriceLevel[] = [];

  season: Season = {
    priceLevel: {
      id: -1,
      color: '#dd7777',
      name: 'Select a Pricelevel',
    },
    from: new Date(new Date().getFullYear(), 5, 1),
    to: new Date(new Date().getFullYear(), 5, 2),
  };

  constructor(
    // public dialogRef: MatDialogRef<SeasonCollectionCreateDialogComponent>,
    private messagesService: MessagesService // @Inject(MAT_DIALOG_DATA) data: any
  ) {
    // this.priceLevels = data.priceLevels;
  }

  onDialogSubmit(): void {
    // if (this.season.from.getTime() === this.season.to.getTime()) {
    //   // TODO: i18n this
    //   this.messagesService.send({
    //     text: 'From and Start date aren not allowed to be identically',
    //   });
    //   return;
    // }
    // if (this.season.priceLevel.id === -1) {
    //   // TODO: i18n this
    //   this.messagesService.send({ text: 'Please select a Price Level' });
    //   return;
    // }
    // this.dialogRef.close(this.season);
  }

  onCancel(): void {
    // this.dialogRef.close();
  }
}
