// /* tslint:disable:no-unused-variable */

// import { TestBed, async, inject } from '@angular/core/testing';
// import { SeasonCollection } from '../models/season';
// import { isOverlapping, SeasonDateManipulationService } from './season-date-manipulation.service';

// describe('Service: SeasonDateManipulation', () => {
// 	beforeEach(() => {
// 		TestBed.configureTestingModule({
// 			providers: [SeasonDateManipulationService],
// 		});
// 	});

// 	it('should ...', inject([SeasonDateManipulationService], (service: SeasonDateManipulationService) => {
// 		expect(service).toBeTruthy();
// 	}));
// });

// // to start the test:
// // ng test --include='**/season-year.service.spec.ts'
// // COPIED FROM DELETED FILE season-year.service.spec.ts

// describe('SeasonDateManipulationService', () => {
// 	let service: SeasonDateManipulationService;

// 	beforeEach(() => {
// 		TestBed.configureTestingModule({});
// 		service = TestBed.inject(SeasonDateManipulationService);
// 	});

// 	it('should be created', () => {
// 		expect(service).toBeTruthy();
// 	});

// 	it('should not override when no period is overlapping', () => {
// 		// given
// 		const given: SeasonCollection = {
// 			id: 1,
// 			name: 'name',
// 			seasons: [
// 				{
// 					id: 1,
// 					priceLevel: { id: 1, name: 'name', color: '' },
// 					from: new Date(1995, 11, 1),
// 					to: new Date(1995, 11, 17),
// 				},
// 				{
// 					id: 2,
// 					priceLevel: { id: 2, name: 'name', color: '' },
// 					from: new Date(1995, 11, 18),
// 					to: new Date(1995, 11, 20),
// 				},
// 			],
// 		};

// 		const givenChangedPeriod = {
// 			id: 11,
// 			priceLevel: { id: 11, name: 'name', color: '' },
// 			from: new Date(1995, 11, 1),
// 			to: new Date(1995, 11, 17),
// 		};

// 		// when
// 		const result = service.updatedPeriodsIfIncreased(given, givenChangedPeriod);

// 		// then
// 		expect(result.seasons).toHaveSize(2);
// 	});

// 	it('should return true when overlapping', () => {
// 		const p1 = {
// 			from: new Date(1995, 11, 15),
// 			to: new Date(1995, 11, 20),
// 			priceLevel: { id: 2, name: 'name', color: '' },
// 		};
// 		const p2 = {
// 			from: new Date(1995, 11, 18),
// 			to: new Date(1995, 11, 21),
// 			priceLevel: { id: 1, name: 'name', color: '' },
// 		};
// 		const result = isOverlapping(p1, p2);

// 		expect(result).toBeTrue();
// 	});

// 	it('should override seasons to the left', () => {
// 		// given
// 		const given: SeasonCollection = {
// 			id: 0,
// 			name: 'name',
// 			seasons: [
// 				{
// 					id: 1,
// 					priceLevel: { id: 2, name: 'name', color: '' },
// 					from: new Date(1995, 11, 1),
// 					to: new Date(1995, 11, 4),
// 				},
// 				{
// 					id: 2,
// 					priceLevel: { id: 3, name: 'name', color: '' },
// 					from: new Date(1995, 11, 5),
// 					to: new Date(1995, 11, 6),
// 				},
// 				{
// 					id: 3,
// 					priceLevel: { id: 1, name: 'changedPeriod', color: '' },
// 					from: new Date(1995, 11, 1),
// 					to: new Date(1995, 11, 17),
// 				},
// 			],
// 		};

// 		const givenChangedPeriod = {
// 			id: 3,
// 			priceLevel: { id: 1, name: 'changedPeriod', color: '' },
// 			from: new Date(1995, 11, 1),
// 			to: new Date(1995, 11, 17),
// 		};

// 		// when
// 		const result = service.updatedPeriodsIfIncreased(given, givenChangedPeriod);

// 		// then
// 		expect(result.seasons).toContain(givenChangedPeriod);
// 		expect(result.seasons[0].from).toEqual(new Date(1995, 11, 1));
// 		expect(result.seasons[0].to).toEqual(new Date(1995, 11, 17));
// 		expect(result.seasons.length).toEqual(1);
// 	});

// 	it('should override partial seasons to the left ', () => {
// 		// given
// 		const given: SeasonCollection = {
// 			id: 1,
// 			name: 'name',
// 			seasons: [
// 				{
// 					id: 2,
// 					priceLevel: { id: 4, name: 'name', color: '' },
// 					from: new Date(1995, 11, 1),
// 					to: new Date(1995, 11, 4),
// 				},
// 				{
// 					id: 3,
// 					priceLevel: { id: 5, name: 'changedPeriod', color: '' },
// 					from: new Date(1995, 11, 3),
// 					to: new Date(1995, 11, 17),
// 				},
// 			],
// 		};

// 		const givenChangedPeriod = {
// 			id: 3,
// 			priceLevel: { id: 5, name: 'changedPeriod', color: '' },
// 			from: new Date(1995, 11, 3),
// 			to: new Date(1995, 11, 17),
// 		};

// 		// when
// 		const result = service.updatedPeriodsIfIncreased(given, givenChangedPeriod);

// 		// then
// 		const expected: SeasonCollection = {
// 			id: 1,
// 			name: 'name',
// 			seasons: [
// 				{
// 					id: 2,
// 					priceLevel: { id: 4, name: 'name', color: '' },
// 					from: new Date(1995, 11, 1),
// 					to: new Date(1995, 11, 3),
// 				},
// 				{
// 					id: 3,
// 					priceLevel: { id: 5, name: 'changedPeriod', color: '' },
// 					from: new Date(1995, 11, 3),
// 					to: new Date(1995, 11, 17),
// 				},
// 			],
// 		};
// 		console.log(result, expected);
// 		expect(result.seasons.length).toEqual(2);
// 		expect(result).toEqual(expected);
// 	});

// 	it('should override partial seasons to the right ', () => {
// 		// given
// 		const given: SeasonCollection = {
// 			id: 221,
// 			name: 'name',
// 			seasons: [
// 				{
// 					id: 3,
// 					priceLevel: { id: 3, name: 'name', color: '' },
// 					from: new Date(1995, 11, 18),
// 					to: new Date(1995, 11, 25),
// 				},
// 				{
// 					id: 2,
// 					priceLevel: { id: 2, name: 'changedPeriod', color: '' },
// 					from: new Date(1995, 11, 3),
// 					to: new Date(1995, 11, 19),
// 				},
// 			],
// 		};

// 		const givenChangedPeriod = {
// 			id: 2,
// 			priceLevel: { id: 212, name: 'changedPeriod', color: '' },
// 			from: new Date(1995, 11, 3),
// 			to: new Date(1995, 11, 19),
// 		};

// 		// when
// 		const result = service.updatedPeriodsIfIncreased(given, givenChangedPeriod);

// 		// then
// 		const expected: SeasonCollection = {
// 			id: 221,
// 			name: 'name',
// 			seasons: [
// 				{
// 					id: 2,
// 					priceLevel: { id: 2, name: 'changedPeriod', color: '' },
// 					from: new Date(1995, 11, 3),
// 					to: new Date(1995, 11, 19),
// 				},
// 				{
// 					id: 3,
// 					priceLevel: { id: 3, name: 'name', color: '' },
// 					from: new Date(1995, 11, 19),
// 					to: new Date(1995, 11, 25),
// 				},
// 			],
// 		};
// 		console.log('result %o\nexpected %o', result, expected)
// 		expect(result.seasons.length).toEqual(2);
// 		expect(result).toEqual(expected);
// 	});

// 	xit('should divide period if it is inside of a period', () => {
// 		// given
// 		const given: SeasonCollection = {
// 			id: 1,
// 			name: 'name',
// 			seasons: [
// 				{
// 					id: 1,
// 					priceLevel: { id: 1, name: 'name', color: '' },
// 					from: new Date(1995, 11, 1),
// 					to: new Date(1995, 11, 12),
// 				},
// 				{
// 					id: 2,
// 					priceLevel: { id: 2, name: 'name', color: '' },
// 					from: new Date(1995, 11, 4),
// 					to: new Date(1995, 11, 7),
// 				},
// 			],
// 		};

// 		const givenChangedPeriod = {
// 			id: 3,
// 			priceLevel: { id: 3, name: 'changedPeriod', color: '' },
// 			from: new Date(1995, 11, 4),
// 			to: new Date(1995, 11, 7),
// 		};

// 		// when
// 		const result = service.updatedPeriodsIfIncreased(given, givenChangedPeriod);

// 		// then
// 		const expected: SeasonCollection = {
// 			id: 1,
// 			name: 'name',
// 			seasons: [
// 				{
// 					id: 1,
// 					priceLevel: { id: 1, name: 'name', color: '' },
// 					from: new Date(1995, 11, 1),
// 					to: new Date(1995, 11, 4),
// 				},
// 				{
// 					id: 3,
// 					priceLevel: { id: 3, name: 'changedPeriod', color: '' },
// 					from: new Date(1995, 11, 4),
// 					to: new Date(1995, 11, 7),
// 				},
// 				{
// 					id: 2,
// 					priceLevel: { id: 2, name: 'name', color: '' },
// 					from: new Date(1995, 11, 7),
// 					to: new Date(1995, 11, 12),
// 				},
// 			],
// 		};
// 		console.log('Halasdaslo bla %o\n%o', expected, result);
// 		expect(result.seasons).toContain(givenChangedPeriod);
// 		// expect(result).toEqual(expected);
// 		// expect(result.seasons.length).toEqual(3);
// 	});
// });
