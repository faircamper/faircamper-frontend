import { Injectable } from '@angular/core';
import {
  Daterange,
  Season,
  SeasonCollection,
} from 'src/app/lessor/models/seasons/season';

@Injectable({
  providedIn: 'root',
})
export class SeasonDateManipulationService {
  constructor() {}

  public updatedPeriodsIfIncreased(
    seasonCollection: SeasonCollection,
    changed: Season
  ): SeasonCollection {
    const changedMerged = differChangedPeriod(
      seasonCollection.seasons,
      changed
    );
    const seasonsWithoutGap = mergeAndFillGaps(changedMerged, changed);
    seasonCollection.seasons = sortByStarttime(seasonsWithoutGap);
    deleteSingleDays(seasonCollection.seasons);
    return seasonCollection;
  }

  public updatePeriodsIfDownsized(
    seasonCollection: SeasonCollection,
    bounds: Daterange
  ): SeasonCollection {
    const seasons = sortByStarttime(mergeAndFillGaps(seasonCollection.seasons));
    // snaps to left or right to the outer bounds
    if (seasons.length > 1) {
      snapTo(seasons[0], bounds.from);
      snapTo(seasons[seasons.length - 1], bounds.to);
    } else if (seasons.length === 1) {
      snapTo(seasons[0], bounds.from);
      snapTo(seasons[0], bounds.to);
    }
    seasonCollection.seasons = seasons;
    return seasonCollection;
  }
}

function deleteSingleDays(seasons: Season[]) {
  for (let index = 0; index < seasons.length; index++) {
    const season = seasons[index];
    if (season.from.getTime() === season.to.getTime()) {
      seasons.splice(index, 1);
      index--;
    }
  }
}
function mergeAndFillGaps(seasons: Season[], changed?: Season): Season[] {
  if (seasons.length < 1) {
    return [];
  }
  const stack: Season[] = [];
  function peek(s: Season[]): Season | null {
    if (s.length < 1) {
      return null;
    }
    return s[s.length - 1];
  }
  sortByStarttime(seasons).forEach((season) => {
    // peek stack
    const top = peek(stack);
    if (top === null) {
      stack.push(season);
      return;
    }

    if (meets(top, season)) {
      if (equalsSeason(top, season)) {
        top.priceLevel = season.priceLevel;
        top.to = season.to;
        return;
      }
      stack.push(season);
      return;
    }
    if (changed !== undefined && equals(top, changed)) {
      snap(season, top);
    } else {
      snap(top, season);
    }
    if (equalsSeason(top, season)) {
      top.priceLevel = season.priceLevel;
      top.to = season.to;
      return;
    }
    stack.push(season);
    return;
  });
  return stack;
}

function equalsSeason(p1: Season, p2: Season): boolean {
  return p1.priceLevel.id === p2.priceLevel.id;
}

function meets(p1: Daterange, p2: Daterange): boolean {
  return p1.to.getTime() === p2.from.getTime();
}

// snap resizes p1 so that it will meet p2, regardless if p1 is before or after p2
// returns false if p1 meets p2 or overlaps
function snap(p1: Daterange, p2: Daterange): boolean {
  if (isBefore(p1, p2)) {
    p1.to = p2.from;
    return true;
  } else if (isBefore(p2, p1)) {
    p1.from = p2.to;
    return true;
  }
  return false;
}

export function snapTo(r: Daterange, snapDest: Date): boolean {
  if (r.from.getTime() >= snapDest.getTime()) {
    r.from = snapDest;
    return true;
  } else if (r.to.getTime() <= snapDest.getTime()) {
    r.to = snapDest;
    return true;
  } else {
    return false;
  }
}

function isBefore(p1: Daterange, p2: Daterange): boolean {
  return p1.to.getTime() < p2.from.getTime();
}

export function sortByStarttime(seasons: Season[]): Season[] {
  return seasons.sort((a, b) => {
    if (a.from.getTime() < b.from.getTime()) {
      return -1;
    }

    if (a.from.getTime() > b.from.getTime()) {
      return 1;
    }
    return 0;
  });
}

// equals verifies if season equals
// the identity of an season is based on the season and range
function equals(p1: Season, p2: Season): boolean {
  const isSameSeason = p1.priceLevel.id === p2.priceLevel.id;
  const from = p1.from.getTime() === p2.from.getTime();
  const to = p1.to.getTime() === p2.to.getTime();
  return isSameSeason && from && to;
}

// changedPeriod is the interval changed by user
// changedPeriod always wins
export function differChangedPeriod(
  seasons: Season[],
  changedPeriod: Season
): Season[] {
  const result: Season[] = [];

  seasons.forEach((season) => {
    if (equals(season, changedPeriod)) {
      result.push(changedPeriod);
      return;
    }
    if (!isOverlapping(season, changedPeriod)) {
      result.push(season);
      return;
    }
    if (isWrapping(season, changedPeriod)) {
      result.push(
        newPeriod(season, season.from, changedPeriod.from),
        newPeriod(season, changedPeriod.to, season.to, true)
      );
      return;
    }
    if (isWrapping(changedPeriod, season)) {
      return;
    }
    if (isOverlap(changedPeriod, season)) {
      result.push(newPeriod(season, changedPeriod.to, season.to));
      return;
    }
    if (isOverlap(season, changedPeriod)) {
      result.push(newPeriod(season, season.from, changedPeriod.from));
      return;
    }
  });
  return result;
}

export function isOverlapping(r1: Season, r2: Season): boolean {
  return r1.to > r2.from && r1.from < r2.to;
}

// checks if r2 is included in r1
function isWrapping(r1: Season, r2: Season): boolean {
  return r1.from <= r2.from && r1.to >= r2.to;
}

function isOverlap(r1: Season, r2: Season): boolean {
  return r1.from < r2.from && r2.from <= r1.to;
}

function newPeriod(
  old: Season,
  from: Date,
  to: Date,
  deleteId?: boolean
): Season {
  const newSeason = {
    ...old,
    from,
    to,
  };
  if (newSeason.id && deleteId) {
    delete newSeason.id;
  }
  return newSeason;
}
export function find(
  seasonCollection: SeasonCollection,
  change: Season
): Season | undefined {
  function equalsID(value: Season): value is Season {
    return equals(value, change);
  }
  return seasonCollection.seasons.find(equalsID);
}
