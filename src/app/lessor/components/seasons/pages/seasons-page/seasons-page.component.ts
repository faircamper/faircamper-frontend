import { formatDate } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { EMPTY, Subscription } from 'rxjs';
import {
  Daterange,
  PriceLevel,
  Season,
  SeasonCollection,
} from 'src/app/lessor/models/seasons/season';
import { PriceLevelService } from 'src/app/lessor/services/http/price-level/price-level.service';
import { SeasonCollectionService } from 'src/app/lessor/services/http/season-calendar/season-calendar.service';
import { SeasonDateManipulationService } from '../../services/season-date-manipulation.service';

@Component({
  selector: 'fc-seasons-page',
  templateUrl: './seasons-page.component.html',
  styleUrls: ['./seasons-page.component.scss'],
})
export class SeasonsPageComponent implements OnInit, OnDestroy {
  seasonCollections: SeasonCollection[] = [];

  seasonCollectionsSubscribtion: Subscription = EMPTY.subscribe();
  selectedSeasonCollection: SeasonCollection | null = null;
  showSeasonCollectionForm = false;
  showSeasonForm = false;
  priceLevels: PriceLevel[] | null = null;
  year = new Date().getFullYear();
  visibleTimeline: Daterange = {
    from: new Date(Date.UTC(this.year, 0, 1)),
    to: new Date(Date.UTC(this.year + 1, 11, 31, 0, 0)),
  };
  timelineRange: Daterange = {
    from: new Date(Date.UTC(2021, 0, 1)),
    to: new Date(Date.UTC(2022, 11, 31)),
  };
  constructor(
    private seasonCollectionService: SeasonCollectionService,
    private seasonDateManipulationService: SeasonDateManipulationService,
    private priceLevelService: PriceLevelService
  ) {}

  ngOnInit(): void {
    this.loadPriceLevels();
    this.loadSeasonCollections();
  }

  seasonChanged(season: Season, index: number): void {
    if (season.from && season.to) {
      if (
        this.selectedSeasonCollection &&
        this.selectedSeasonCollection !== null
      ) {
        this.selectedSeasonCollection.seasons[index] = { ...season };
        this.selectedSeasonCollection = {
          ...this.seasonDateManipulationService.updatePeriodsIfDownsized(
            {
              ...this.seasonDateManipulationService.updatedPeriodsIfIncreased(
                this.selectedSeasonCollection,
                season
              ),
            },
            this.timelineRange
          ),
        };
        this.selectedSeasonCollection.seasons = [
          ...this.selectedSeasonCollection.seasons,
        ];
      }
    }
  }

  loadSeasonCollections(): void {
    this.seasonCollectionsSubscribtion = this.seasonCollectionService
      .list<SeasonCollection>()
      .subscribe((data: SeasonCollection[]) => {
        console.log(data);
        this.seasonCollections = data;
      });
  }

  onSelect(seasonCollection: SeasonCollection): void {
    this.selectedSeasonCollection = seasonCollection;
    this.seasonCollectionsSubscribtion = this.seasonCollectionService
      .get<SeasonCollection>(seasonCollection.id)
      .subscribe(
        (data: SeasonCollection) =>
          (this.selectedSeasonCollection = { ...data })
      );
  }

  loadPriceLevels(): void {
    this.priceLevelService
      .list<PriceLevel>()
      .subscribe((data: PriceLevel[]) => {
        this.priceLevels = data;
      });
  }

  ngOnDestroy(): void {
    this.seasonCollectionsSubscribtion.unsubscribe();
  }

  onSeasonCollectionCreate(sc: SeasonCollection): void {
    if (sc === undefined) {
      this.showSeasonCollectionForm = false;
      return;
    }
    sc.seasons = [
      {
        priceLevel: { id: 1, name: 'Summer', color: '000000' },
        from: new Date(2021, 0, 1),
        to: new Date(2022, 11, 31),
      },
    ];
    this.selectedSeasonCollection = sc;
    this.loadPriceLevels();
    this.showSeasonCollectionForm = false;
  }

  onSeasonCreate(season: Season): void {
    if (season) {
      this.selectedSeasonCollection?.seasons.push(season);
      if (this.selectedSeasonCollection?.seasons) {
        const index = this.selectedSeasonCollection?.seasons.length - 1;
        this.seasonChanged(
          this.selectedSeasonCollection?.seasons[index],
          index
        );
      }
    }
    this.showSeasonForm = false;
  }

  openSeasonyearCreateDialog(): void {
    this.showSeasonCollectionForm = !this.showSeasonCollectionForm;
  }

  openSeasonCreateDialog(): void {
    this.showSeasonForm = !this.showSeasonForm;
  }

  submit(): void {
    const dateFormat = 'yyyy-MM-dd';
    this.selectedSeasonCollection?.seasons.forEach((season) => {
      season.from.toJSON = function (key): string {
        return formatDate(season.from, dateFormat, 'en_US');
      };
      season.to.toJSON = function (key): string {
        return formatDate(season.to, dateFormat, 'en_US');
      };
    });
    if (this.selectedSeasonCollection?.id) {
      const updateSub = this.seasonCollectionService
        .update<SeasonCollection>(this.selectedSeasonCollection)
        .subscribe((data) => {
          this.selectedSeasonCollection = null;
          this.loadSeasonCollections();
          updateSub.unsubscribe();
        });
    } else if (this.selectedSeasonCollection) {
      const createSub = this.seasonCollectionService
        .create<SeasonCollection>(this.selectedSeasonCollection)
        .subscribe((data) => {
          this.selectedSeasonCollection = null;
          this.loadSeasonCollections();
          createSub.unsubscribe();
        });
    }
  }

  private daterange(numOfYears: number): Daterange {
    return {
      from: new Date(Date.UTC(this.year, 0, 1)),
      to: new Date(Date.UTC(this.year + numOfYears, 11, 31, 0, 0)),
    };
  }

  deleteSeason(index: number): void {
    this.selectedSeasonCollection?.seasons.splice(index, 1);
    if (this.selectedSeasonCollection?.seasons[0]) {
      this.seasonChanged(this.selectedSeasonCollection.seasons[0], 0);
    }
  }
}
