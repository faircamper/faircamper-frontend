import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
// import * as moment from 'moment';
import * as dayjs from 'dayjs';
import * as duration from 'dayjs/plugin/duration'
import 'dayjs/plugin/duration'
import { Daterange, Season } from 'src/app/lessor/models/seasons/season';
import { Day, SeasonShape } from 'src/app/lessor/models/seasons/season-shape';

@Component({
	selector: 'fc-seasons-bar',
	templateUrl: './calendar-timeline.component.html',
	styleUrls: ['./calendar-timeline.component.scss'],
})
export class CalendarTimelineComponent implements OnInit, OnChanges {
	isMobile = false;

	@Input()
	seasons: Season[] | null | undefined;

	@Input()
	visibleRange: Daterange;

	@Input()
	viewBoxWidth = this.isMobile ? 2000 : 1000;

	@Input()
	viewboxHeight = 200;

	@Input()
	hasMonthGrid = true;

	@Output()
	selectPeriodEvent = new EventEmitter<Season>();

	readonly rangeInDays: number;

	shapes: SeasonShape[];
	grid: Day[];

	selectedShape: SeasonShape | null = null;

	constructor() {
		this.grid = [];
		this.seasons = [];
		this.shapes = [];
		const year = new Date().getFullYear();
		this.visibleRange = { from: new Date(year, 0, 1), to: new Date(year + 1, 0, 1) };
		this.rangeInDays = this.daterangeInDays();
	}

	ngOnChanges(changes: SimpleChanges): void {
		this.shapes = [];
		this.createShapes();
		this.grid = [];
		this.drawMonthGrid();
	}

	ngOnInit(): void {
		if (this.hasMonthGrid) {
			this.drawMonthGrid();
		}
	}

	drawMonthGrid(): void {
		dayjs.extend(duration);
		const months =
      Math.floor(
        dayjs
          .duration(
            this.visibleRange.to.getTime() - this.visibleRange.from.getTime()
          )
          .asMonths()
      ) + 1;
		const widthPx: number = this.viewBoxWidth / months;
		for (let i = 0; i < months + 1; i++) {
			this.grid.push(new Day(widthPx * i, i + 1, new Date()));
		}
	}

	createShapes(): void {
		if (this.seasons === null || this.seasons === undefined) {
			return;
		}
		this.shapes = []; // erase all svg, before painting new ones
		this.seasons.forEach((period) => {
			this.shapes.push(
				new SeasonShape(
					period.priceLevel.name,
					period.from,
					period.to,
					this.viewboxHeight,
					this.getConversionFunc(),
					period.priceLevel.color
				)
			);
		});
	}

	onPeriodClick(e: MouseEvent, shape: SeasonShape): void {
		if (this.selectedShape === null) {
			this.selectedShape = shape;
		}
		if (this.selectedShape.equals(shape) && shape.selected) {
			this.selectedShape.unSelect();
			return;
		}
		this.selectedShape.unSelect();
		shape.select();
		this.selectedShape = shape;
		const period: Season = {
			// TODO: No fixed ID
			priceLevel: {
				id: 1,
				name: shape.name,
				color: shape.color,
			},
			from: shape.from,
			to: shape.to,
		};
		// this.selectPeriodEvent.emit(period);
	}

	onViewboxDblClick(e: MouseEvent): void {
		// this.openCreateDialog();
	}

	viewBoxSize(): string {
		return `0 0 ${this.viewBoxWidth} ${this.viewboxHeight}`;
	}

	private daterangeInDays(): number {
		dayjs.extend(duration);
		
    return dayjs
      .duration(dayjs(this.visibleRange.to).diff(dayjs(this.visibleRange.from)))
      .asDays();
	}

	private dayInTimeline(date: Date): number {
		return dayjs
      .duration(dayjs(date).diff(dayjs(this.visibleRange.from)))
      .asDays();
	}
	/**
	 * getConversionFunc
	 * gets a function to convert a day to the coordination system of the svg world
	 */
	public getConversionFunc(): (date: Date) => number {
		return (date: Date) => {
			return (this.dayInTimeline(date) / this.daterangeInDays()) * this.viewBoxWidth;
		};
	}
}
