import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { SeasonCollection } from 'src/app/lessor/models/seasons/season';

@Component({
  selector: 'fc-season-collection-selector',
  templateUrl: './season-collection-selector.component.html',
  styleUrls: ['./season-collection-selector.component.scss'],
})
export class YearSelectorComponent {
  @Input() seasonCollections: SeasonCollection[] = [];
  @Output() onSelection = new EventEmitter<SeasonCollection>();
  fc: FormGroup = new FormGroup({
    seasonColl: new FormControl(''),
  });
  constructor() {}
  onSelect(event: any): void {
    const scId = +event.target?.value;
    const sc = this.seasonCollections.find((sc) => sc.id === scId);
    this.onSelection.emit(sc);
  }
}
