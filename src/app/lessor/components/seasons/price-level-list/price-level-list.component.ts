import {
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { EMPTY, Subscription } from 'rxjs';
import { PriceLevel } from 'src/app/lessor/models/seasons/season';
import { PriceLevelService } from 'src/app/lessor/services/http/price-level/price-level.service';
@Component({
  selector: 'fc-price-level-list',
  templateUrl: './price-level-list.component.html',
  styleUrls: ['./price-level-list.component.scss'],
})
export class PriceLevelListComponent implements OnInit, OnDestroy {
  pricelevel: PriceLevel[] = [];
  selectedPriceLevel: PriceLevel | null = null;
  isEditing = false;
  showForm = false;

  cancelSubscription: Subscription = EMPTY.subscribe();
  seasonsSubscription: Subscription = EMPTY.subscribe();
  updatePricelevelSub: Subscription = EMPTY.subscribe();

  columns = [
    {
      name: $localize`:@@fc-pricelevel-list:Name`,
      sortable: false,
      sort: 'externalId',
    },
    {
      name: $localize`:@@fc-pricelevel-list:Farbe`,
      sortable: false,
      sort: 'Farbe',
    },
  ];

  constructor(private priceLevelService: PriceLevelService) {}

  ngOnInit(): void {
    this.loadPricelevel();
  }

  ngOnDestroy(): void {
    this.seasonsSubscription.unsubscribe();
    this.updatePricelevelSub.unsubscribe();
  }

  private loadPricelevel(): void {
    this.seasonsSubscription = this.priceLevelService
      .list<PriceLevel>()
      .subscribe((data) => {
        this.pricelevel = data;
      });
  }

  editPricelevel(pl: PriceLevel): void {
    this.selectedPriceLevel = pl;
    this.showForm = true;
    this.isEditing = true;
  }

  createNewPricelevel(): void {
    this.isEditing = false;
    this.showForm = true;
  }

  onPriceLevelUpdated(pl: PriceLevel): void {
    this.updatePricelevelSub = this.priceLevelService
      .update(pl)
      .subscribe((resp) => {
        this.pricelevel = [
          ...this.pricelevel.filter((e) => e.id !== pl.id),
          resp,
        ];
      });
    this.selectedPriceLevel = null;
    this.showForm = false;
  }

  onPriceLevelCreated(pl: PriceLevel): void {
    this.updatePricelevelSub = this.priceLevelService
      .create(pl)
      .subscribe((resp) => {
        this.pricelevel = [...this.pricelevel, resp];
      });
    this.selectedPriceLevel = null;
    this.showForm = false;
  }

  onCancelEdit(pl: PriceLevel): void {
    this.selectedPriceLevel = null;
  }
}
