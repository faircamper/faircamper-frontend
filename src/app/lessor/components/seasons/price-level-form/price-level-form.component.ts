import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PriceLevel } from 'src/app/lessor/models/seasons/season';
@Component({
  selector: 'fc-price-level-form',
  templateUrl: './price-level-form.component.html',
  styleUrls: ['./price-level-form.component.scss'],
})
export class PriceLevelFormComponent implements OnInit {
  @Input() pricelevel: PriceLevel | null = null;
  @Input() isEditing = false;
  @Output() pricelevelUpdateEvent = new EventEmitter<PriceLevel>();
  @Output() pricelevelCreateEvent = new EventEmitter<PriceLevel>();
  @Output() cancelEvent = new EventEmitter();
  pricelevelForm: FormGroup;

  constructor(private fb: FormBuilder) {
    this.pricelevelForm = this.initForm();
  }

  ngOnInit(): void {
    if (this.pricelevel || this.pricelevel !== null) {
      this.pricelevelForm.patchValue(this.pricelevel);
    }
  }

  initForm(): FormGroup {
    return this.fb.group({
      name: ['', [Validators.required]],
      color: ['', [Validators.required]],
    });
  }

  submitForm(): void {
    const formValue = this.pricelevelForm.value;
    if (this.isEditing) {
      if (this.pricelevel) {
        const changedSeason: PriceLevel = {
          ...formValue,
          id: this.pricelevel.id,
          version: this.pricelevel.version,
        };
        this.pricelevelUpdateEvent.emit(changedSeason);
      }
    } else {
      const createdPricelevel: PriceLevel = {
        ...formValue,
      };
      this.pricelevelCreateEvent.emit(createdPricelevel);
    }
    this.pricelevelForm.reset();
  }

  cancel(): void {
    this.cancelEvent.emit();
    this.pricelevelForm.reset();
  }
}
