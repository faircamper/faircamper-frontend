import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

import calendarsJson from './mock-data/booking-calendars_GET.json';

import { RequestInfoUtilities } from 'angular-in-memory-web-api';


enum Collections {
  VehicleBookingCalendar,
}
@Injectable({
  providedIn: 'root',
})
export class FakeCalendarApiService {
  enabledUrls: number[] = [Collections.VehicleBookingCalendar];

  createDb() {
    let db = {} as any;
    if (environment.karma) {
      db = this.createTestDB();
    } else {
      db = this.createDevDB();
    }
    return { ...db };
  }

  createTestDB() {
    let db = {} as any;
    if (this.enabledUrls.find((e) => e === Collections.VehicleBookingCalendar) !== undefined) {
      db.calendars = calendarsJson;
    }
    return db;
  }
  createDevDB() {
    let db = {} as any;
    db.calendars = calendarsJson;
    return db;
  }
  parseRequestUrl(url: string, utils: RequestInfoUtilities): string {
    const newUrl = url.replace(/\/lessor\/calendars/, '/calendars');
    return newUrl;
  }
}
