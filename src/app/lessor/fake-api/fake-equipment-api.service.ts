import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import optionalEquipmentJson from './mock-data/optionalEquipmentV2_GET.json';
import { RequestInfoUtilities } from 'angular-in-memory-web-api';

enum Collections {
  OptionalEquipmentComponent,
}
@Injectable({
  providedIn: 'root',
})
export class FakeEquipmentApiService {
  enabledUrls: number[] = [Collections.OptionalEquipmentComponent];

  createDb(): any {
    let db = {} as any;
    if (environment.karma) {
      db = this.createTestDB();
    } else {
      db = this.createDevDB();
    }
    return { ...db };
  }

  createTestDB() {
    let db = {} as any;
    db.optionalEquipment = optionalEquipmentJson;
    return db;
  }
  createDevDB() {
    let db = {} as any;
    if (
      this.enabledUrls.find((e) => e === Collections.OptionalEquipmentComponent) !== undefined
    ) {
      db.optionalEquipment = optionalEquipmentJson;
    }
    return db;
  }
  parseRequestUrl(url: string, utils: RequestInfoUtilities): string {
    const newUrl = url.replace(
        /\/vehicles\/create/,
        'vehicles/create'
    );
    return newUrl;
  }
}
