import { TestBed } from '@angular/core/testing';

import { FakeLessorApiService } from './fake-lessor-api.service';

describe('FakeApiService', () => {
  let service: FakeLessorApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FakeLessorApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
