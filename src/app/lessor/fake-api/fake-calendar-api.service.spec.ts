import { TestBed } from '@angular/core/testing';
import { FakeCalendarApiService } from './fake-calendar-api.service';


describe('FakeApiService', () => {
  let service: FakeCalendarApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FakeCalendarApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
