import { TestBed } from '@angular/core/testing';
import { FakeEquipmentApiService } from './fake-equipment-api.service';



describe('FakeApiService', () => {
  let service: FakeEquipmentApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FakeEquipmentApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
