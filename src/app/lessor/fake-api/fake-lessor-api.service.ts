import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import vehiclesJson from './mock-data/vehicles_GET.json';
import equipmentJson from './mock-data/equipment_GET.json';
import staticJson from './mock-data/static.json';
import equipmentCategoriesJson from './mock-data/equipmentCategories_GET.json';
import stationsJson from './mock-data/stations_GET.json';
import stationsEquipmentJson from './mock-data/stations-equipment_GET.json';
import { RequestInfoUtilities } from 'angular-in-memory-web-api';

enum Collections {
  Vehicles,
  Equipment,
  Static,
  EquipmentCategories,
  Stations,
  StationsEquipment,
}
@Injectable({
  providedIn: 'root',
})
export class FakeLessorApiService {
  enabledUrls: number[] = [];
  createDb(): any {
    let db = {} as any;
    if (environment.karma) {
      db = this.createTestDB();
    } else {
      db = this.createDevDB();
    }
    return { ...db };
  }

  createTestDB() {
    let db = {} as any;
    db.vehicles = vehiclesJson;
    db.equipment = equipmentJson;
    db.static = staticJson;
    db.equipmentCategories = equipmentCategoriesJson;
    db.stations = stationsJson;
    db.stationsEquipment = stationsEquipmentJson;
    return db;
  }
  createDevDB() {
    let db = {} as any;
    if (
      this.enabledUrls.find((e) => e === Collections.Vehicles) !== undefined
    ) {
      db.vehicles = vehiclesJson;
    }
    if (
      this.enabledUrls.find((e) => e === Collections.Equipment) !== undefined
    ) {
      db.equipment = equipmentJson;
    }
    if (this.enabledUrls.find((e) => e === Collections.Static) !== undefined) {
      db.static = staticJson;
    }
    if (
      this.enabledUrls.find((e) => e === Collections.EquipmentCategories) !==
      undefined
    ) {
      db.equipmentCategories = equipmentCategoriesJson;
    }
    if (
      this.enabledUrls.find((e) => e === Collections.Stations) !== undefined
    ) {
      db.equipmentCategories = stationsJson;
    }
    if (
      this.enabledUrls.find((e) => e === Collections.StationsEquipment) !==
      undefined
    ) {
      db.equipmentCategories = stationsEquipmentJson;
    }
    return db;
  }
  parseRequestUrl(url: string, utils: RequestInfoUtilities): string {
    const newUrl = url.replace(
      /\/equipment\/categories/,
      '/equipmentCategories'
    );
    return newUrl;
  }
}
