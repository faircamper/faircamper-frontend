import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccountPageComponent } from './components/account/pages/account-page/account-page.component';
import { DashboardComponent } from './components/dashboard/pages/dashboard/dashboard.component';
import { VehicleCreateComponent } from './components/vehicle/pages/vehicle-create/vehicle-create.component';
import { LessorComponent } from './lessor.component';
import { RouteGuardService } from '@wobi/sso';
import { ERoles, ISSORouteGuardData } from '@wobi/sso-state';
import { BookingCalendarPageComponent } from './components/booking-calendar-page/booking-calendar-page.component';
import { SeasonsPageComponent } from './components/seasons/pages/seasons-page/seasons-page.component';
import { StationCreateComponent } from './components/station/pages/station-create/station-create.component';
import { StationEditComponent } from './components/station/pages/station-edit/station-edit.component';
import { StationListComponent } from './components/station/station-list/station-list.component';
import { StationsComponent } from './components/station/stations/stations.component';
import { VehicleEditComponent } from './components/vehicle/pages/vehicle-edit/vehicle-edit.component';
import { VehicleListComponent } from './components/vehicle/pages/vehicle-list/vehicle-list.component';

const routes: Routes = [
  {
    path: '',
    component: LessorComponent,
    // canActivate: [RouteGuardService],
    // data: <ISSORouteGuardData>{
    //     guard: {
    //         in: [ERoles.Lessor, ERoles.LessorMod],
    //     },
    //     visitorRedirect: {
    //     enabled: true,
    //     }
    // },
    children: [
      {
        path: 'account',
        component: AccountPageComponent,
        // canActivate: [RouteGuardService],
        // data: <ISSORouteGuardData>{
        //     guard: {
        //         in: [ERoles.Lessor, ERoles.LessorMod],
        //     },
        //     visitorRedirect: {
        //       enabled: true,
        //     }
        // },
      },
      {
        path: 'dashboard',
        component: DashboardComponent,
        canActivate: [RouteGuardService],
        data: <ISSORouteGuardData>{
          guard: {
            in: [ERoles.Lessor, ERoles.LessorMod],
          },
          visitorRedirect: {
            enabled: true,
          },
        },
      },
      {
        path: 'vehicles',
        component: VehicleListComponent,
        // canActivate: [RouteGuardService],
        // data: <ISSORouteGuardData>{
        //   guard: {
        //     in: [ERoles.Lessor, ERoles.LessorMod],
        //   },
        //   visitorRedirect: {
        //     enabled: true,
        //   },
        // },
      },
      {
        path: 'vehicles/create',
        component: VehicleCreateComponent,
        // canActivate: [RouteGuardService],
        // data: <ISSORouteGuardData>{
        //     guard: {
        //         in: [ERoles.Lessor, ERoles.LessorMod],
        //     },
        //     visitorRedirect: {
        //       enabled: true,
        //     }
        // },
      },
      {
        path: 'vehicles/edit/:id',
        component: VehicleEditComponent,
        // canActivate: [RouteGuardService],
        // data: <ISSORouteGuardData>{
        //     guard: {
        //         in: [ERoles.Lessor, ERoles.LessorMod],
        //     },
        //     visitorRedirect: {
        //       enabled: true,
        //     }
        // },
      },
      {
        path: 'calendars',
        component: BookingCalendarPageComponent,
        // canActivate: [RouteGuardService],
        // data: <ISSORouteGuardData>{
        //     guard: {
        //         in: [ERoles.Lessor, ERoles.LessorMod],
        //     },
        //     visitorRedirect: {
        //       enabled: true,
        //     }
        // },
      },
      {
        path: 'seasons',
        component: SeasonsPageComponent,
        // canActivate: [RouteGuardService],
        // data: <ISSORouteGuardData>{
        //     guard: {
        //         in: [ERoles.Lessor, ERoles.LessorMod],
        //     },
        //     visitorRedirect: {
        //       enabled: true,
        //     }
        // },
      },
      {
        path: 'stations',
        component: StationsComponent,
        children: [
          {
            path: 'list',
            component: StationListComponent,
          },
          {
            path: 'edit/:id',
            component: StationEditComponent,
          },
          {
            path: 'create',
            component: StationCreateComponent,
          },
          {
            path: '',
            component: StationListComponent,
          },
        ],
      },
      {
        path: 'seasons',
        component: SeasonsPageComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LessorRoutingModule {}
