
export interface MileagePack {
    id: number;
    km: number;
    discount: number;
    isSelected: boolean;
}
