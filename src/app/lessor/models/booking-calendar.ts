export interface VehicleBookingCalendar {
    vehicleId: string;
    wobiId:string;
    seats: number;
    beds: number;
    vehicleTypeId: number;
    vehicleType: string;
    vehicleStatus: string;
    city: string;
    calendar: BookingCalendar[];
}

export interface BookingCalendar {
    id: string;
    dateranges: BookingDateranges[];
}

export interface BookingDateranges {
    from: string;
    to: string;
}
