export interface Price {
  priceLevelId: number;
  priceLevelName: string;
  amount: number | null;
  currency: string;
  minBookingDuration: number | null;
}
