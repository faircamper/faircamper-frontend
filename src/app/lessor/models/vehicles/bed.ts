export interface Bed {
	designation: string;
	length: number;
	width: number;
}
