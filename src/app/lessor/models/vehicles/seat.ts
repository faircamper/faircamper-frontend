export interface Seat {
	seatTypeId: SeatType;
	hasIsofix: boolean;
}

export enum SeatType {
	SafetyBelt = 'SafetyBelt',
	Lapbelt = 'Lapbelt',
	WithoutBelt = 'WithoutBelt',
}
