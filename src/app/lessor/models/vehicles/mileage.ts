export interface Package {
    km: number;
    discount: number;
}

export interface PredefinedPackage extends Package {
    id: number;
    isSelected: boolean;
}

export interface Mileage {
    costPerKm: number;
    isFree: boolean;
    freeAfterRentalDays: number;
    customPackages: Package[];
    predefinedPackages: PredefinedPackage[];
}
