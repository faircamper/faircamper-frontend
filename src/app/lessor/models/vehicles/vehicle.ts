import { EquipmentId } from '../equipment/equipment';
import { Image } from '../image';
import { SeasonCollection } from '../seasons/season';
import { Bed } from './bed';
import { Mileage } from './mileage';
import { Price } from './price';
import { RentalTerms } from './rentalTerms';
import { Seat } from './seat';

export interface Vehicle {
  id: number;
  vehicleId: string;
  stationId: string;
  // name and description
  externalId?: string;
  title: string;
  description: string;
  // vehicleTypeId
  vehicleTypeId: string;

  // basis data
  manufacturer: string;
  power: number;
  powerMode?: string;
  payload: number;
  fuelId: number;
  consumptionId: string;
  gearId: number;
  height: number;
  length: number;
  width: number;
  constructionYear: number;
  model: string;

  // seats and beds
  seats: Seat[];
  beds: Bed[];
  images: Image[];

  // equipment
  optionalEquipmentIds: string[];
  equipmentIds: EquipmentId[];
  prices: Price[];
  mileage: Mileage;
  seasonCollection: SeasonCollection;
  isApproved: boolean; // unlocked by faircamper
  isActive: boolean; // lessor can deactivate, so that the vehicle is not visble to end user
  isDeleted: boolean; // not visible for lessor

  youtube: string;

  rentalTerms: RentalTerms;

  weightId: number;
  statusId: number;
  version?: number;
}
