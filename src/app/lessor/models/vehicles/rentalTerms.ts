
export interface RentalTerms {
    minDriverAge: number;
    drivingLicensePossession: number;
    additionalDriversAllowed: string;
    chargeForExtraDriver: boolean;
    feePerDriver: null;
    region: string[];
    includeCountries: string[];
    excludeCountries: string[];
    deductiblePartialCover: number;
    deductibleComprehensiveInsurance: number;
    europeWideProtectionLetter: boolean;
    depositAmount: number;
    payOnSiteVia: string[];
    payInAdvanceViaBankTransfer: boolean;
    daysInAdvance: null;
    depositReduction: boolean;
    smallAnimalsAllowed: boolean;
    smallAnimalsPriceModell: string;
    smallAnimalPrice: number;
    numberOfSmallAnimals: number;
    smokingAllowed: boolean;
}
