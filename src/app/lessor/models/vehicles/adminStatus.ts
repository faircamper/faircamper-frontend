export enum AdminStatus {
	READY_TO_BE_APPROVED, // first time approving
	APPPROVED_BY_ADMIN, // approved
	NOT_APPROVED, // !approved
	FOLLOW_UP_APPROVE, // wiedervorlage
	FORBIDDEN, // removed by admin (eg penisbild)
}
