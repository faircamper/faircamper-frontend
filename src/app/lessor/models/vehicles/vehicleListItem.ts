export interface VehicleListItem {
    vehicleId: string;
    externalId: string;
    vehicleTypeId: number;
    vehicleTypeName: string;
    wobiStatusId: number;
    onlineStatusId: number;
    city: string;
    HasBestPrice: boolean;
    HasLiveCalendar: boolean;
    HasInstantBooking: boolean;
	isDeleted: boolean;
}
