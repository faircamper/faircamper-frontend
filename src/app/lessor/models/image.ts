export interface Image {
	url: string;
	id?: string;
	alt: string;
}
