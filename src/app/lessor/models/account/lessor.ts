export interface LessorAccount {
  id?: string;
  keycloakId: string;
  email: string;
  isEmailVerified: boolean;
  username: string;
  name: string;
  surname: string;
  accountType: string;
  persons: Person[];
  company: Company;
  documents: Doc[];
}
export interface Company {
  name: string;
  businessFormId: number;
  street: string;
  streetNumber: string;
  postalCode: string;
  city: string;
  country: string;
  preTaxDeductable: boolean;
  website: string;
}
export interface Person {
  id?: string;
  accountRoleId: number;
  accountRole: string;
  title: string;
  name: string;
  surname: string;
  birthday: Date;
  email: string;
  username: string;
  mobilePhone: string;
  phone: string;
}

export interface AccountInit {
  id?: string;
  keycloakId: string;
  email: string;
  isEmailVerified: boolean;
  username: string;
  name: string;
  surname: string;
  accountType: string;
}

export interface Doc {
  id?: string;
  documentTypeId: number;
  documentType: string;
  // mimeType: string;
  fileName: string;
}

export interface DocumentResponse {
  id: string;
}

export interface IdValue {
  id: number;
  name: string;
}
export const accountRoles: IdValue[] = [
  { id: 0, name: 'Inhaber' },
  { id: 1, name: 'Geschäftsführer' },
  { id: 2, name: 'Ansprechspartner' },
];
export const businessForms: IdValue[] = [
  { id: 0, name: 'Privat' },
  { id: 1, name: 'Einzelkaufman' },
  { id: 2, name: 'e.k' },
  { id: 3, name: 'GbR' },
  { id: 4, name: 'GmbH' },
  { id: 5, name: 'UG' },
];