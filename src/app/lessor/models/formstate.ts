// https://angular.io/api/forms/AbstractControl#status

export enum FormStatus {
	VALID = 'VALID',
	INVALID = 'INVALID',
	PENDING = 'PENDING',
	DISABLED = 'DISABLED',
}
