import { IdValue } from "../account/lessor";

export interface Equipment {
  id: number;
  name: string;
  /** */
  isChargeable: boolean;
  categoryId: number;
  categoryName: string;
  optionalEquipmentNameId: number | null;
  optionalEquipmentName: string | null;
  optionalEquipmentCategoryId: number | null;
  optionalEquipmentCategory: string | null;
}
export interface EquipmentId {
  id: number;
  optionalEquipmentId: string | null;
}

export interface OptionalEquipment {
  id?: string;
  name: string;
  nameId: number;
  description: string;
  categoryId: number;
  categoryName: string;
  price: number;
  priceTypeId: number;
  priceTypeName?: string;
  maxPrice: number;
  stationId: string;
  version?: number;
}

export interface OptionalEquipmentCategory {
  categoryId: number;
  categoryName: string;
  names: IdValue[];
}
