import { OpeningHours } from './station-opening-hours';

export interface Station {
  id?: string;
  name: string;
  description: string;
  organization: string;
  optional: string;
  street: string;
  postalCode: string;
  city: string;
  country: string;
  geoLocation: {
    latitude: number;
    longitude: number;
  };
  openingHours: OpeningHours[];
  fee: number;
  version?: number;
}

export interface StationListItem {
  id: string;
  name: string;
  description: string;
  organization: string;
  street: string;
  postalCode: string;
  latitude: number;
  longitude: number;
  version: number;
}
