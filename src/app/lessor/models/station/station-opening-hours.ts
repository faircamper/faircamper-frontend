import { IdValue } from '../account/lessor';

export interface OpeningHours {
  handover: IdValue;
  checkin?: {
    from: string;
    to: string;
  };
  checkout?: {
    from: string;
    to: string;
  };
  isFee: boolean;
}

export enum Day {
  SUNDAY = 0,
  MONDAY,
  TUESDAY,
  WEDNESDAY,
  THURSDAY,
  FRIDAY,
  SATURDAY,
}

export enum handoverType {
  OPEN = 0,
  CLOSED = 1,
  CONSULTATION = 2,
}
// TODO: include into static data
export const handoverValues: IdValue[] = [
  { id: handoverType.OPEN, name: $localize`:@@handover-open:offen` },
  { id: handoverType.CLOSED, name: $localize`:@@handover-closed:geschlossen` },
  {
    id: handoverType.CONSULTATION,
    name: $localize`:@@handover-consultation:nach Absprache`,
  },
];

export const weekdays = new Map<Day, string>([
  [Day.SUNDAY, $localize`:weekday|:Sonntag`],
  [Day.MONDAY, $localize`:weekday|:Montag`],
  [Day.TUESDAY, $localize`:weekday|:Dienstag`],
  [Day.WEDNESDAY, $localize`:weekday|:Mittwoch`],
  [Day.THURSDAY, $localize`:weekday|:Donnerstag`],
  [Day.FRIDAY, $localize`:weekday|:Freitag`],
  [Day.SATURDAY, $localize`:weekday|:Samstag`],
]);

export const defaultOpeningHours: OpeningHours[] = [
  {
    checkin: { from: '12:00', to: '12:00' },
    checkout: { from: '12:00', to: '12:00' },
    handover: handoverValues[0],
    isFee: false,
  },
  {
    checkin: { from: '12:00', to: '12:00' },
    checkout: { from: '12:00', to: '12:00' },
    handover: handoverValues[0],

    isFee: false,
  },
  {
    checkin: { from: '12:00', to: '12:00' },
    checkout: { from: '12:00', to: '12:00' },
    handover: handoverValues[0],

    isFee: false,
  },
  {
    checkin: { from: '12:00', to: '12:00' },
    checkout: { from: '12:00', to: '12:00' },
    handover: handoverValues[0],

    isFee: false,
  },
  {
    checkin: { from: '12:00', to: '12:00' },
    checkout: { from: '12:00', to: '12:00' },
    handover: handoverValues[0],

    isFee: false,
  },
  {
    checkin: { from: '12:00', to: '12:00' },
    checkout: { from: '12:00', to: '12:00' },
    handover: handoverValues[0],

    isFee: false,
  },
  {
    checkin: { from: '12:00', to: '12:00' },
    checkout: { from: '12:00', to: '12:00' },
    handover: handoverValues[0],

    isFee: false,
  },
];
