export interface ServerResponse<T> {
  errors: string[];
  events: string[];
  data?: T;
}
