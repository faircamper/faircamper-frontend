import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Action, Store } from '@ngrx/store';
import { map, Observable, switchMap, withLatestFrom } from 'rxjs';
import { LessorAccount } from '../../models/account/lessor';
import { LessorAccountService } from '../../services/http/account/lessor-account.service';
import {
  accountAddDocAction,
  accountAddPersonAction,
  accountDeleteDocAction,
  accountDeletePersonAction,
  accountLoadAction,
  accountLoadSuccessAction,
  accountUpdateCompanyAction,
  accountUpdatePersonAction,
} from '../actions/account.actions';
import { selectAccount } from '../selectors/account.selector';

@Injectable()
export class AccountEffects {
  constructor(
    private actions$: Actions,
    private accSvc: LessorAccountService,
    private store: Store
  ) {}

  loadAccount$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(accountLoadAction),
      switchMap(() =>
        this.accSvc
          .get<LessorAccount>()
          .pipe(
            map((account) => accountLoadSuccessAction({ account: account }))
          )
      )
    )
  );

  updateAccount: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(
        accountUpdateCompanyAction,
        accountAddDocAction,
        accountDeleteDocAction,
        accountAddPersonAction,
        accountUpdatePersonAction,
        accountDeletePersonAction
      ),
      withLatestFrom(this.store.select(selectAccount)),
      switchMap(([action, account]) =>
        this.accSvc
          .update<LessorAccount>(account)
          .pipe(
            map((account) => accountLoadSuccessAction({ account: account }))
          )
      )
    )
  );
}
