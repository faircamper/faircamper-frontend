import { createFeatureSelector, createSelector } from '@ngrx/store';
import { LessorAccount } from '../../models/account/lessor';

import { Person, Doc } from '../../models/account/lessor';
import { selectState } from '../module.state';

import { ILessorState } from '../reducers/module.reducer';

export const selectAccount = createSelector(
  selectState,
  (state: ILessorState): LessorAccount => state.account
);

export const selectPersons = createSelector(
  selectAccount,
  (acc: LessorAccount): Array<Person> => acc.persons
);

export const selectDocuments = createSelector(
  selectAccount,
  (acc: LessorAccount): Array<Doc> => acc.documents
);
