// import { IUser } from '@wobi/account';
import { ActionReducerMap } from '@ngrx/store';

import { accountReducer } from './account.reducer';

import { LessorAccount} from '../../models/account/lessor';

export interface ILessorState {
    account: LessorAccount,
}

export const moduleReducers: ActionReducerMap<ILessorState> = {
    account: accountReducer,
};
  

