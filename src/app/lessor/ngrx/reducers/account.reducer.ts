import { Action, createReducer, on } from '@ngrx/store';
import { LessorAccount } from '../../models/account/lessor';
import {
  accountAddDocAction,
  accountAddPersonAction,
  accountDeleteDocAction,
  accountDeletePersonAction,
  accountLoadAction,
  accountLoadSuccessAction,
  AccountResetAction,
  accountUpdateCompanyAction,
  accountUpdatePersonAction,
} from '../actions/account.actions';

// import { ILessorState } from './module.reducer';

export const initialAccountState: LessorAccount = {
  id: '',
  keycloakId: '',
  email: '',
  isEmailVerified: false,
  username: '',
  name: '',
  surname: '',
  accountType: '',
  persons: [],
  company: {
    name: '',
    businessFormId: 0,
    street: '',
    streetNumber: '',
    postalCode: '',
    city: '',
    country: '',
    preTaxDeductable: false,
    website: '',
  },
  documents: [],
};

const reducer = createReducer(
  initialAccountState,
  on(accountLoadAction, (state: LessorAccount) => ({
    ...state,
  })),
  on(accountLoadSuccessAction, (state: LessorAccount, { account }) => ({
    ...account,
    company: { ...account.company },
    documents: [...(account.documents ? account.documents : [])],
    persons: [...(account.persons ? account.persons : [])],
  })),
  on(accountUpdateCompanyAction, (state: LessorAccount, { company }) => ({
    ...state,
    company: { ...company },
  })),
  on(accountAddDocAction, (state: LessorAccount, { doc }) => ({
    ...state,
    documents: [...(state?.documents ? state.documents : []), doc],
  })),
  on(accountDeleteDocAction, (state: LessorAccount, { doc }) => ({
    ...state,
    documents: state.documents.filter((e) => e.id !== doc.id),
  })),
  on(accountAddPersonAction, (state: LessorAccount, { person }) => ({
    ...state,
    persons: [...state.persons, person],
  })),
  on(accountDeletePersonAction, (state: LessorAccount, { person }) => ({
    ...state,
    persons: [...state.persons.filter((p) => p.email !== person.email)],
  })),
  on(accountUpdatePersonAction, (state: LessorAccount, { person }) => ({
    ...state,
    persons: [...state.persons.filter((p) => p.email !== person.email), person],
  })),
  on(AccountResetAction, (state: LessorAccount) => ({
    ...initialAccountState,
  }))
);

export function accountReducer(
  state: LessorAccount | undefined,
  action: Action
) {
  return reducer(state, action);
}
