import { createFeatureSelector } from '@ngrx/store';
import { initialAccountState } from './reducers/account.reducer';
import { ILessorState } from './reducers/module.reducer';

export const initialModuleState: ILessorState = {
  account: initialAccountState,
};

export const selectState = createFeatureSelector<ILessorState>('@wobi/lessor');
