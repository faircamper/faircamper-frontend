import { createAction, props } from '@ngrx/store';

import { type } from '@wobi/core';
import {
  Company,
  Doc,
  LessorAccount,
  Person,
} from '../../models/account/lessor';

// Category to uniquely identify the actions
const CATEGORY = 'wobi.lessor.account';

/**
 * For each action type in an action group, make a simple
 * enum object for all of this group's action types.
 *
 * The 'type' utility function coerces strings into string
 * literal types and runs a simple check to guarantee all
 * action types in the application are unique.
 */
export interface IAccountActions {
  LOAD: string;
  LOAD_SUCCESS: string;

  UPDATE: string;
  UPDATE_SUCCESS: string;

  RESET: string;

  ADD_DOC: string;
  UPDATE_COMPANY: string;

  ADD_PERSON: string;
  UPDATE_PERSON: string;
  DELETE_PERSON: string;

  DELETE_DOC: string;
}

export const AccountActionTypes: IAccountActions = {
  LOAD: type(`${CATEGORY}.load`),
  LOAD_SUCCESS: type(`${CATEGORY}.loadSuccess`),
  UPDATE: type(`${CATEGORY}.update`),
  UPDATE_COMPANY: type(`${CATEGORY}.company`),
  UPDATE_SUCCESS: type(`${CATEGORY}.updateSuccess`),

  ADD_DOC: type(`${CATEGORY}.addDoc`),
  DELETE_DOC: type(`${CATEGORY}.deleteDoc`),

  ADD_PERSON: type(`${CATEGORY}.addPerson`),
  UPDATE_PERSON: type(`${CATEGORY}.updatePerson`),
  DELETE_PERSON: type(`${CATEGORY}.deletePerson`),

  RESET: type(`${CATEGORY}.reset`),
};

export const accountLoadAction = createAction(AccountActionTypes.LOAD);

export const accountLoadSuccessAction = createAction(
  AccountActionTypes.LOAD_SUCCESS,
  props<{ account: LessorAccount }>()
);

export const accountUpdateSuccessAction = createAction(
  AccountActionTypes.UPDATE_SUCCESS,
  props<{ account: LessorAccount }>()
);

export const accountUpdateCompanyAction = createAction(
  AccountActionTypes.UPDATE_COMPANY,
  props<{ company: Company }>()
);

export const accountAddDocAction = createAction(
  AccountActionTypes.ADD_DOC,
  props<{ doc: Doc }>()
);

export const accountDeleteDocAction = createAction(
  AccountActionTypes.DELETE_DOC,
  props<{ doc: Doc }>()
);

export const accountAddPersonAction = createAction(
  AccountActionTypes.ADD_PERSON,
  props<{ person: Person }>()
);

export const accountUpdatePersonAction = createAction(
  AccountActionTypes.UPDATE_PERSON,
  props<{ person: Person }>()
);

export const accountDeletePersonAction = createAction(
  AccountActionTypes.DELETE_PERSON,
  props<{ person: Person }>()
);

export const AccountResetAction = createAction(AccountActionTypes.RESET);
