#!/bin/bash
set -e  
echo $1
if [[ -z "$1" ]]; then
    echo "please set version"
    exit 1
fi
npm version $1 \
&& git push \
&& git push --tags