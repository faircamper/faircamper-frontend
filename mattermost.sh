#!/bin/bash
set -x
curl -i -X POST \
-H 'Content-Type: application/json' \
-d '{"channel_id":"'"$2"'", "message":"'"$3"'"}' \
-H "Authorization: Bearer $1" https://chat.faircamperintern.de/api/v4/posts