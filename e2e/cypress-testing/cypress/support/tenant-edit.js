/** 
 * own cy commands, callable via cy.functionName()
 **/

 import userData from '../fixtures/tenant/account-data.json'

// -- cy.checkLessorAccountPageData() or y.checkLessorAccountPageData(string)
Cypress.Commands.add( 'checkLessorAccountPageData', (a = null) =>{
    for(let i in userData['headlines']){
        if(a !== null){
            if(a === 'old-data'){
                //checks if the page contains the right titles and the associated values
                cy.get('div').contains(userData['headlines'][i]).next('div').invoke('text').then((text) => {
                    cy.expect(userData['data'][i]).to.equal(text.trimLeft().trimRight())
                })
            }
            else if(a !== null && a === 'edit-data'){
                //checks if the page contains the right titles and the associated values
                cy.get('div').contains(userData['headlines'][i]).next('div').invoke('text').then((text) => {
                    cy.expect(userData['after-edit'][i]).to.equal(text.trimLeft().trimRight())
                })
            }
        }
        else{
            //checks if the page contains the right titles
            cy.get('div').contains(userData['headlines'][i]).invoke('text').then((text) => {
                cy.expect(userData['headlines'][i]).to.equal(text.trimLeft().trimRight())
            })
        } 
    }
})

// if oder switch ?
// switch(a){
//     case 'old-data':
//         //checks if the page contains the right titles and the associated values
//         cy.get('div').contains(userData['headlines'][i]).next('div').invoke('text').then((text) => {
//             cy.expect(userData['data'][i]).to.equal(text.trimLeft().trimRight())
//         })
//         break;
//     case 'edit-data':
//         //checks if the page contains the right titles and the associated values after editing
//         cy.get('div').contains(userData['headlines'][i]).next('div').invoke('text').then((text) => {
//             cy.expect(userData['after-edit'][i]).to.equal(text.trimLeft().trimRight())
//         })
//         break;
//     default:
//       cy.log('wrong parameter in checkLessorAccountPageData()')
// }