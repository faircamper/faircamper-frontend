import editData from '../../fixtures/tenant/edit-data.json'

describe("check-data-tenant-account", () => {

    it('Visit', () => {
        //visits the required url
        cy.visit('http://localhost:4200/tenant/account')
        //checks if the page contains the right headline
        cy.contains('Account-Details')
    })
    
    it('Contains all headlines', () => {
        //checks if the page contains the right titles
        cy.checkLessorAccountPageData()
    })

    it('Contains right data at the right place', () => {
        //checks if the page contains the right titles and the associated values
        cy.checkLessorAccountPageData('old-data')
    })
  
})

describe("edit-save-tenant-account", () => {

    it('Edit and Save', () => {
        //checks if exists the edit button
        cy.contains('Editieren').click()
        //types the edit user data
        cy.get('input').then(() => {
            cy.wait(200)
            let count = 0
            for(let i in editData){
                cy.get('input').eq(count).clear().type(editData[i])
                count++
            }
        })
        //checks if exists the save button
        cy.contains('Speichern').click()
    })

    //Now check the right data ... 


})