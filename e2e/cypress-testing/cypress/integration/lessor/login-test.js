import loginData from '../../fixtures/lessor/login-data.json'

describe("Lessor-Login-Test", () => {
    
    //checks availability of the url
    it('Visit start-page', () => {
        cy.visit('http://localhost:4200/lessor')
        cy.contains('Login').click()
    })
    
})

describe("Redirect to wobi.lan", () => {
    
    //does the login
    it('Do Login', () =>  {
        let count = 0 
        for(let i in loginData){
            cy.get('input').eq(count).clear().type(loginData[i])
            count++
        }
        cy.contains('Sign In').click()
    })  
})

describe("Lessor-Logout-Check", () => {
    
    it('Visit page after Login', () => { 
        cy.visit('http://localhost:4200/lessor')
        //kann noch nicht die lösung sein ##
        cy.contains('Login').click().then(()=>{
            cy.contains('Logout').then((e) => {
                e.click()
                cy.log('user could log out!')
            })
        })// ##
    })
            
})


